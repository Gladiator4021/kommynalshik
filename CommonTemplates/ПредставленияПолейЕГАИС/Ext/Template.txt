﻿<Items Description="ПредставленияПолейЕГАИС" Columns="ПространствоИмен,ЛокальноеИмя,Глубина,Представление,Обязательное">
	<!--Акт постановки на баланс-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="Identity" Глубина="3" Представление="Идентификатор акта постановки на баланс" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="Number" Глубина="3" Представление="Номер акта постановки на баланс" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="ActDate" Глубина="3" Представление="Дата акта постановки на баланс" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="Note" Глубина="3" Представление="Комментарий" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="Identity" Глубина="5" Представление="Идентификатор позиции внутри акта" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="Product" Глубина="5" Представление="Алкогольная продукция" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="Quantity" Глубина="5" Представление="Количество" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="InformAB" Глубина="5" Представление="Информация о справках" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="MarkCodeInfo" Глубина="5" Представление="Список акцизных марок" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="MarkCode" Глубина="6" Представление="Код акцизной марки" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="FormA" Глубина="7" Представление="Регистрационный номер справки А" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="LastFormB" Глубина="7" Представление="Регистрационный номер справки Б" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActChargeOn" ЛокальноеИмя="InformA" Глубина="7" Представление="Справка А" Обязательное="1"/>
	<!--Акт постановки на баланс. Данные справки А-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryABInfo" ЛокальноеИмя="Quantity" Глубина="8" Представление="Количество по справке А" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryABInfo" ЛокальноеИмя="BottlingDate" Глубина="8" Представление="Дата розлива" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryABInfo" ЛокальноеИмя="TTNNumber" Глубина="8" Представление="Номер ТТН" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryABInfo" ЛокальноеИмя="TTNDate" Глубина="8" Представление="Дата составления ТТН" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryABInfo" ЛокальноеИмя="EGAISFixNumber" Глубина="8" Представление="Номер подтверждения о фиксации в ЕГАИС" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryABInfo" ЛокальноеИмя="EGAISFixDate" Глубина="8" Представление="Дата подтверждения о фиксации в ЕГАИС" Обязательное="0"/>
	<!--Регистрация справок по акту постановки на баланс-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="Identity" Глубина="4" Представление="Идентификатор акта постановки на баланс" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="ActRegId" Глубина="4" Представление="Идентификатор акта постановки на баланс в системе ЕГАИС" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="Number" Глубина="4" Представление="Номер акта постановки на баланс" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="Identity" Глубина="5" Представление="Идентификатор позиции внутри акта" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="InformARegId" Глубина="5" Представление="Регистрационный номер справки А" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="InformB" Глубина="5" Представление="Справка Б" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="Identity" Глубина="7" Представление="Идентификатор позиции в справке Б" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="BRegId" Глубина="7" Представление="Регистрационный номер справки Б" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActInventoryInformBReg" ЛокальноеИмя="MarkInfo" Глубина="7" Представление="Информация о марках" Обязательное="0"/>
	<!--Акт списания-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="Identity" Глубина="3" Представление="Идентификатор акта списания" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="ActNumber" Глубина="4" Представление="Номер акта списания" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="ActDate" Глубина="4" Представление="Дата составления акта списания" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="TypeWriteOff" Глубина="4" Представление="Причина списания" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="Note" Глубина="4" Представление="Комментарий" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="Identity" Глубина="5" Представление="Идентификатор позиции внутрни акта" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="Quantity" Глубина="5" Представление="Количество" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActWriteOff" ЛокальноеИмя="InformB" Глубина="5" Представление="Справка Б" Обязательное="1"/>
	<!--Организации ЕГАИС-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="Identity" Глубина="6" Представление="Идентификатор организации у поставщика" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="ClientRegId" Глубина="6" Представление="Код организации в ФСРАР" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="FullName" Глубина="6" Представление="Полное наименование организации" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="ShortName" Глубина="6" Представление="Наименование организации" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="INN" Глубина="6" Представление="ИНН организации" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="KPP" Глубина="6" Представление="КПП организации" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="UNP" Глубина="6" Представление="УНП организации (для республики Беларусь)" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="RNN" Глубина="6" Представление="РНН (БИН/ИИН) организации (для республики Казахстан)" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="address" Глубина="6" Представление="Фактический адрес организации" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="Country" Глубина="7" Представление="Фактический адрес организации: Код страны" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="Index" Глубина="7" Представление="Фактический адрес организации: Почтовый индекс" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="RegionCode" Глубина="7" Представление="Фактический адрес организации: Код региона" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="area" Глубина="7" Представление="Фактический адрес организации: Район" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="city" Глубина="7" Представление="Фактический адрес организации: Город" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="place" Глубина="7" Представление="Фактический адрес организации: Населенный пункт" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="street" Глубина="7" Представление="Фактический адрес организации: Улица" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="house" Глубина="7" Представление="Фактический адрес организации: Дом" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="building" Глубина="7" Представление="Фактический адрес организации: Строение" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="liter" Глубина="7" Представление="Фактический адрес организации: Корпус" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="description" Глубина="7" Представление="Представление адреса" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ClientRef" ЛокальноеИмя="State" Глубина="6" Представление="Статус организации" Обязательное="0"/>
	<!--Алкогольная продукция-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="Identity" Глубина="6" Представление="Идентификатор алкогольной продукции у поставщика" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="Type" Глубина="6" Представление="Тип алкогольной продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="FullName" Глубина="6" Представление="Полное наименование алкогольной продукции" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="ShortName" Глубина="6" Представление="Наименование алкогольной продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="AlcCode" Глубина="6" Представление="Код алкогольной продукции" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="Capacity" Глубина="6" Представление="Емкость упаковки алкогольной продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="AlcVolume" Глубина="6" Представление="Содержание этилового спирта, %" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="Producer" Глубина="6" Представление="Производитель алкогольной продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="Importer" Глубина="6" Представление="Импортер алкогольной продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="ProductVCode" Глубина="6" Представление="Код вида алкогольной продукции" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="RegId" Глубина="6" Представление="Регистрационный номер справки А" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ProductRef" ЛокальноеИмя="BRegId" Глубина="7" Представление="Регистрационный номер справки Б" Обязательное="1"/>
	<!--Подтверждение акта расхождений-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ConfirmTicket" ЛокальноеИмя="Identity" Глубина="3" Представление="Идентификатор подтверждения акта расхождений поставщика" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ConfirmTicket" ЛокальноеИмя="IsConfirm" Глубина="4" Представление="Признак подтверждения" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ConfirmTicket" ЛокальноеИмя="TicketNumber" Глубина="4" Представление="Номер подтверждения" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ConfirmTicket" ЛокальноеИмя="TicketDate" Глубина="4" Представление="Дата составления подтверждения" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ConfirmTicket" ЛокальноеИмя="WBRegId" Глубина="4" Представление="Идентификатор накладной в системе ЕГАИС" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ConfirmTicket" ЛокальноеИмя="Note" Глубина="4" Представление="Комментарий" Обязательное="0"/>
	<!--Чек ККМ-->
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="inn" Глубина="0" Представление="ИНН организации" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="kpp" Глубина="0" Представление="КПП обособленного подразделения" Обязательное="0"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="address" Глубина="0" Представление="Адрес обособленного подразделения" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="name" Глубина="0" Представление="Наименование магазина" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="kassa" Глубина="0" Представление="Заводской номер ККМ" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="shift" Глубина="0" Представление="Номер смены ККМ" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="number" Глубина="0" Представление="Номер чека ККМ" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="datetime" Глубина="0" Представление="Дата и время пробития чека" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="Bottle" Глубина="1" Представление="Маркируемая продукция" Обязательное="0"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="price" Глубина="1" Представление="Цена" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="barcode" Глубина="1" Представление="Код акцизной марки" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="ean" Глубина="1" Представление="Штрихкод алкогольной продукции" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="volume" Глубина="1" Представление="Емкость алкогольной продукции" Обязательное="0"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="nopdf" Глубина="1" Представление="Пивная продукция" Обязательное="0"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="code" Глубина="1" Представление="Код вида алкогольной продукции" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="bname" Глубина="1" Представление="Наименование товара" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="alc" Глубина="1" Представление="Крепость" Обязательное="1"/>
	<Item ПространствоИмен="egaischeque.joint.2" ЛокальноеИмя="count" Глубина="1" Представление="Количество единиц продукции" Обязательное="1"/>
	<!--Запрос справок-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/QueryFormAB" ЛокальноеИмя="FormRegId" Глубина="3" Представление="Регистрационный номер справки" Обязательное="1"/>
	<!--Запрос классификатора-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/QueryParameters" ЛокальноеИмя="Name" Глубина="5" Представление="Имя параметра" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/QueryParameters" ЛокальноеИмя="Value" Глубина="5" Представление="Значение параметра" Обязательное="1"/>
	<!--Справка А-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="InformARegId" Глубина="3" Представление="Регистрационный номер справки А" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="TTNNumber" Глубина="3" Представление="Номер ТТН" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="TTNDate" Глубина="3" Представление="Дата ТТН" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="Shipper" Глубина="3" Представление="Грузоотправитель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="Consignee" Глубина="3" Представление="Грузополучатель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="ShippingDate" Глубина="3" Представление="Дата отгрузки" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="Product" Глубина="3" Представление="Алкогольная продукция" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="BottlingDate" Глубина="3" Представление="Дата розлива" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="Quantity" Глубина="3" Представление="Количество" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="EGAISNumber" Глубина="3" Представление="Номер подтверждения о фиксации в ЕГАИС" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="EGAISDate" Глубина="3" Представление="Дата подтверждения о фиксации в ЕГАИС" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormA" ЛокальноеИмя="MarkInfo" Глубина="3" Представление="Информация о марках" Обязательное="0"/>
	<!--Справка Б-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="InformBRegId" Глубина="3" Представление="Регистрационный номер справки Б" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="TTNNumber" Глубина="3" Представление="Номер ТТН" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="TTNDate" Глубина="3" Представление="Дата ТТН" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="Shipper" Глубина="3" Представление="Грузоотправитель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="Consignee" Глубина="3" Представление="Грузополучатель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="ShippingDate" Глубина="3" Представление="Дата отгрузки продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="Product" Глубина="3" Представление="Алкогольная продукция" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="Quantity" Глубина="3" Представление="Количество" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyFormB" ЛокальноеИмя="MarkInfo" Глубина="3" Представление="Информация о марках" Обязательное="0"/>
	<!--Ответ на запрос остатков-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyRests" ЛокальноеИмя="RestsDate" Глубина="3" Представление="Дата остатков" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyRests" ЛокальноеИмя="Product" Глубина="5" Представление="Алкогольная продукция" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyRests" ЛокальноеИмя="Quantity" Глубина="5" Представление="Остаток" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyRests" ЛокальноеИмя="InformARegId" Глубина="5" Представление="Регистрационный номер справки А" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ReplyRests" ЛокальноеИмя="InformBRegId" Глубина="5" Представление="Регистрационный номер справки Б" Обязательное="1"/>
	<!--Квитанция-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="TicketDate" Глубина="3" Представление="Дата квитанции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="Identity" Глубина="3" Представление="Идентификатор исходного документа" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="DocId" Глубина="3" Представление="Идентификатор документа в УТМ" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="TransportId" Глубина="3" Представление="Идентификатор запроса" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="RegID" Глубина="3" Представление="Идентификатор документа в системе ЕГАИС" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="DocHash" Глубина="3" Представление="Хэш документа" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="DocType" Глубина="3" Представление="Тип исходного документа" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="Result" Глубина="3" Представление="Результат фиксации" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="Conclusion" Глубина="4" Представление="Результат фиксации" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="ConclusionDate" Глубина="4" Представление="Дата фиксации" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="Comments" Глубина="4" Представление="Комментарий" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="OperationResult" Глубина="3" Представление="Результат проведения" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="OperationName" Глубина="4" Представление="Название операции" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="OperationResult" Глубина="4" Представление="Результат проведения" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="OperationComment" Глубина="4" Представление="Комментарий" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/Ticket" ЛокальноеИмя="OperationDate" Глубина="4" Представление="Дата операции" Обязательное="0"/>
	<!--Акт подтверждения ТТН-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="Identity" Глубина="3" Представление="Идентификатор акта грузополучателя" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="IsAccept" Глубина="4" Представление="Признак подтверждения" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="ACTNUMBER" Глубина="4" Представление="Номер акта" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="ActDate" Глубина="4" Представление="Дата составления акта" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="WBRegId" Глубина="4" Представление="Идентификатор накладной в системе ЕГАИС" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="Note" Глубина="4" Представление="Комментарий" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="Identity" Глубина="5" Представление="Идентификатор позиции внутри накладной" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="InformBRegId" Глубина="5" Представление="Регистрационный номер справки Б" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/ActTTNSingle" ЛокальноеИмя="RealQuantity" Глубина="5" Представление="Количество по факту" Обязательное="1"/>
	<!--Регистрация справок Б по ТТН-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="Identity" Глубина="4" Представление="Идентификатор накладной поставщика" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="WBRegId" Глубина="4" Представление="Идентификатор накладной в системе ЕГАИС" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="EGAISFixNumber" Глубина="4" Представление="Номер фиксации накладной в системе ЕГАИС" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="EGAISFixDate" Глубина="4" Представление="Дата фиксации накладной в системе ЕГАИС" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="WBNUMBER" Глубина="4" Представление="Номер накладной" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="WBDate" Глубина="4" Представление="Дата составления накладной" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="Shipper" Глубина="4" Представление="Грузоотправитель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="Consignee" Глубина="4" Представление="Грузополучатель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="Supplier" Глубина="4" Представление="Поставщик" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="Identity" Глубина="5" Представление="Идентификатор позиции внутри накладной" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNInformBReg" ЛокальноеИмя="InformBRegId" Глубина="5" Представление="Регистрационный номер справки Б" Обязательное="1"/>
	<!--ТТН-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Identity" Глубина="3" Представление="Идентификатор накладной поставщика" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Type" Глубина="4" Представление="Тип накладной (продажа или возврат)" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="UnitType" Глубина="4" Представление="Тип упаковки (фасованная или нефасованная)" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="NUMBER" Глубина="4" Представление="Номер накладной" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Date" Глубина="4" Представление="Дата составления накладной" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="ShippingDate" Глубина="4" Представление="Дата отгрузки продукции" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Transport" Глубина="4" Представление="Транспортный раздел" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_TYPE" Глубина="5" Представление="Тип перевозки" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_COMPANY" Глубина="5" Представление="Название компании перевозчика" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_CAR" Глубина="5" Представление="Данные об автомобиле, перевозившем продукцию" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_TRAILER" Глубина="5" Представление="Данные о прицепе автомобиля, перевозившем продукцию" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_CUSTOMER" Глубина="5" Представление="Название компании заказчика перевозки груза" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_DRIVER" Глубина="5" Представление="Сведения о водителе, перевозившем груз" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_LOADPOINT" Глубина="5" Представление="Место погрузки продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_UNLOADPOINT" Глубина="5" Представление="Место разгрузки продукции" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_REDIRECT" Глубина="5" Представление="Данные о перенаправлении груза в другой пункт" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="TRAN_FORWARDER" Глубина="5" Представление="Экспедитор" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Shipper" Глубина="4" Представление="Грузоотправитель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Consignee" Глубина="4" Представление="Грузополучатель" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Supplier" Глубина="4" Представление="Поставщик" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Base" Глубина="4" Представление="Основание" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Note" Глубина="4" Представление="Произвольный комментарий" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Product" Глубина="5" Представление="Алкогольная продукция" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Pack_ID" Глубина="5" Представление="Идентификатор упаковки" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Quantity" Глубина="5" Представление="Количество" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Price" Глубина="5" Представление="Цена" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Party" Глубина="5" Представление="Номер партии" Обязательное="0"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="Identity" Глубина="5" Представление="Идентификатор позиции внутри накладной" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="InformA" Глубина="5" Представление="Справка А" Обязательное="1"/>
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/TTNSingle" ЛокальноеИмя="InformB" Глубина="5" Представление="Справка Б" Обязательное="1"/>
	<!--Документы-->
	<Item ПространствоИмен="http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01" ЛокальноеИмя="FSRAR_ID" Глубина="2" Представление="Код организации в ФСРАР" Обязательное="1"/>
</Items>