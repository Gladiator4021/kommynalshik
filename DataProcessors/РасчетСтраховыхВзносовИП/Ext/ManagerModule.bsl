﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

// Формирует таблицу для вызова обработки формирования платежных поручений
//
Функция ТаблицаПлатежейДляФормированияПлатежныхПоручений(Объект, ОтборПлатежей) Экспорт
	
	ТаблицаПлатежей = Обработки.ФормированиеПлатежныхПорученийНаУплатуНалогов.НоваяТаблицаПлатежей();
	
	// 69.06.5, ФиксированныеСтраховыеВзносы
	Если Объект.СуммаВзносаПФР <> 0 И ОтборПлатежей.Найти("ПФР") <> Неопределено Тогда
		НоваяСтрока = ТаблицаПлатежей.Добавить();
		НоваяСтрока.СчетУчета = ПланыСчетов.Хозрасчетный.ПФР_ОПС_ИП;
		НоваяСтрока.Субконто1 = Перечисления.ВидыПлатежейВГосБюджет.Налог;
		НоваяСтрока.Сумма     = Объект.СуммаВзносаПФР;
	КонецЕсли;
	
	// 69.06.5, СтраховыеВзносыСДоходов
	Если Объект.СуммаВзносаПФРсДоходов <> 0 И ОтборПлатежей.Найти("ПФРсДоходов") <> Неопределено Тогда
		НоваяСтрока = ТаблицаПлатежей.Добавить();
		НоваяСтрока.СчетУчета = ПланыСчетов.Хозрасчетный.ПФР_ОПС_ИП;
		НоваяСтрока.Субконто1 = Перечисления.ВидыПлатежейВГосБюджет.ВзносыСвышеПредела;
		НоваяСтрока.Сумма     = Объект.СуммаВзносаПФРсДоходов;
	КонецЕсли;
	
	// 69.06.3
	Если Объект.СуммаВзносаФФОМС <> 0 И ОтборПлатежей.Найти("ФФОМС") <> Неопределено Тогда
		НоваяСтрока = ТаблицаПлатежей.Добавить();
		НоваяСтрока.СчетУчета = ПланыСчетов.Хозрасчетный.ФОМС_СтраховойГод;
		НоваяСтрока.Субконто1 = Перечисления.ВидыПлатежейВГосБюджет.Налог;
		НоваяСтрока.Сумма     = Объект.СуммаВзносаФФОМС;
	КонецЕсли;
	
	// 69.06.4
	Если Объект.СуммаВзносаФСС <> 0 И ОтборПлатежей.Найти("ФСС") <> Неопределено Тогда
		НоваяСтрока = ТаблицаПлатежей.Добавить();
		НоваяСтрока.СчетУчета = ПланыСчетов.Хозрасчетный.ФСС_СтраховойГод;
		НоваяСтрока.Субконто1 = Перечисления.ВидыПлатежейВГосБюджет.Налог;
		НоваяСтрока.Сумма     = Объект.СуммаВзносаФСС;
	КонецЕсли;
	
	Обработки.ФормированиеПлатежныхПорученийНаУплатуНалогов.ЗаполнитьВидыНалогов(
		ТаблицаПлатежей, Объект.Организация, Объект.Период);
	
	Возврат ТаблицаПлатежей;
	
КонецФункции

// Возвращает список документов на уплату взносов
//
// Параметры:
//   Организация - СправочникСсылка.Организации
//   Период - Дата - период уплаты
//   Правило - СправочникСсылка.ПравилаПредставленияОтчетовУплатыНалогов
//
// Возвращаемое значение:
//   ТаблицаЗначений, Неопределено
//
Функция ДокументыУплаты(Организация, Период, Правило = Неопределено, ВидНалога = Неопределено, ВидНалоговогоОбязательства = Неопределено) Экспорт
	
	Если ОбщегоНазначенияБПВызовСервераПовтИсп.ЭтоЮрЛицо(Организация) Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Периодичность = УчетСтраховыхВзносовИП.ПериодичностьУплатыФиксированныхСтраховыхВзносов(Организация, Период);
	
	Если ЗначениеЗаполнено(Правило) И Периодичность = Перечисления.Периодичность.Квартал Тогда
		НачалоПериода = НачалоКвартала(Период);
	Иначе
		НачалоПериода = НачалоГода(Период);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ВидНалога) Тогда
		ВидыНалогов = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ВидНалога);
	Иначе
		ВидыНалогов = Новый Массив;
		ВидыНалогов.Добавить(Перечисления.ВидыНалогов.ФиксированныеВзносы_ПФР_СтраховаяЧасть);
		ВидыНалогов.Добавить(Перечисления.ВидыНалогов.ФиксированныеВзносы_ФФОМС);
		Если УчетнаяПолитика.УплачиватьДобровольныеВзносыВФСС(Организация, Период) Тогда
			ВидыНалогов.Добавить(Перечисления.ВидыНалогов.ФиксированныеВзносы_ФСС);
		КонецЕсли;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ВидНалоговогоОбязательства) Тогда
		ВидыНалоговыхОбязательств = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ВидНалоговогоОбязательства);
	Иначе
		ВидыНалоговыхОбязательств = Новый Массив;
		ВидыНалоговыхОбязательств.Добавить(Перечисления.ВидыПлатежейВГосБюджет.Налог);
		ВидыНалоговыхОбязательств.Добавить(Перечисления.ВидыПлатежейВГосБюджет.ВзносыСвышеПредела);
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("НачалоПериода", НачалоПериода);
	Запрос.УстановитьПараметр("КонецПериода",  КонецДня(Период));
	Запрос.УстановитьПараметр("ВидыНалогов", ВидыНалогов);
	Запрос.УстановитьПараметр("ВидыНалоговыхОбязательств", ВидыНалоговыхОбязательств);
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ВидыНалоговИПлатежейВБюджет.Ссылка КАК Ссылка,
	|	ВидыНалоговИПлатежейВБюджет.ВидНалога КАК ВидНалога
	|ПОМЕСТИТЬ ОтборНалог
	|ИЗ
	|	Справочник.ВидыНалоговИПлатежейВБюджет КАК ВидыНалоговИПлатежейВБюджет
	|ГДЕ
	|	ВидыНалоговИПлатежейВБюджет.ВидНалога В(&ВидыНалогов)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЗадачиБухгалтераНалоговыеПлатежи.ПлатежноеПоручение КАК Ссылка,
	|	ЗадачиБухгалтераНалоговыеПлатежи.Правило КАК Правило
	|ПОМЕСТИТЬ ОтборПравило
	|ИЗ
	|	РегистрСведений.ЗадачиБухгалтераНалоговыеПлатежи КАК ЗадачиБухгалтераНалоговыеПлатежи
	|ГДЕ
	|	ЗадачиБухгалтераНалоговыеПлатежи.Организация = &Организация
	|	И ЗадачиБухгалтераНалоговыеПлатежи.ПериодСобытия МЕЖДУ &НачалоПериода И &КонецПериода
	|	И &УсловиеПоПравилу
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПлатежноеПоручение.Ссылка КАК Ссылка,
	|	ПлатежноеПоручение.Дата КАК Дата,
	|	ПлатежноеПоручение.Номер КАК Номер,
	|	ПлатежноеПоручение.ВидНалоговогоОбязательства КАК ВидНалоговогоОбязательства,
	|	ПлатежноеПоручение.СуммаДокумента КАК Сумма,
	|	ВЫБОР
	|		КОГДА СостоянияБанковскихДокументов.Состояние ЕСТЬ NULL 
	|			ТОГДА ЛОЖЬ
	|		КОГДА СостоянияБанковскихДокументов.Состояние = ЗНАЧЕНИЕ(Перечисление.СостоянияБанковскихДокументов.Оплачено)
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК Оплачено,
	|	ОтборНалог.ВидНалога КАК ВидНалога
	|ИЗ
	|	Документ.ПлатежноеПоручение КАК ПлатежноеПоручение
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ОтборПравило КАК ОтборПравило
	|		ПО ПлатежноеПоручение.Ссылка = ОтборПравило.Ссылка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ОтборНалог КАК ОтборНалог
	|		ПО ПлатежноеПоручение.Налог = ОтборНалог.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.СостоянияБанковскихДокументов КАК СостоянияБанковскихДокументов
	|		ПО (СостоянияБанковскихДокументов.Организация = &Организация)
	|			И ПлатежноеПоручение.Ссылка = СостоянияБанковскихДокументов.СсылкаНаОбъект
	|ГДЕ
	|	ПлатежноеПоручение.Организация = &Организация
	|	И НЕ ПлатежноеПоручение.ПометкаУдаления
	|	И ПлатежноеПоручение.ВидОперации = ЗНАЧЕНИЕ(Перечисление.ВидыОперацийСписаниеДенежныхСредств.ПеречислениеНалога)
	|	И ПлатежноеПоручение.ВидНалоговогоОбязательства В(&ВидыНалоговыхОбязательств)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	РасходныйКассовыйОрдер.Ссылка,
	|	РасходныйКассовыйОрдер.Дата,
	|	РасходныйКассовыйОрдер.Номер,
	|	РасходныйКассовыйОрдер.ВидНалоговогоОбязательства,
	|	РасходныйКассовыйОрдер.СуммаДокумента,
	|	РасходныйКассовыйОрдер.Проведен,
	|	ОтборНалог.ВидНалога
	|ИЗ
	|	Документ.РасходныйКассовыйОрдер КАК РасходныйКассовыйОрдер
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ОтборПравило КАК ОтборПравило
	|		ПО РасходныйКассовыйОрдер.Ссылка = ОтборПравило.Ссылка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ОтборНалог КАК ОтборНалог
	|		ПО РасходныйКассовыйОрдер.Налог = ОтборНалог.Ссылка
	|ГДЕ
	|	РасходныйКассовыйОрдер.Организация = &Организация
	|	И НЕ РасходныйКассовыйОрдер.ПометкаУдаления
	|	И РасходныйКассовыйОрдер.ВидОперации = ЗНАЧЕНИЕ(Перечисление.ВидыОперацийРКО.УплатаНалога)
	|	И РасходныйКассовыйОрдер.ВидНалоговогоОбязательства В(&ВидыНалоговыхОбязательств)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Дата,
	|	Ссылка";
	
	Если ЗначениеЗаполнено(Правило) Тогда
		
		Запрос.УстановитьПараметр("Правило", Правило);
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&УсловиеПоПравилу", "ЗадачиБухгалтераНалоговыеПлатежи.Правило = &Правило");
		
	Иначе
		
		КодыЗадач = Новый Массив;
		КодыЗадач.Добавить("СтраховыеВзносы_Предприниматель");
		КодыЗадач.Добавить("СтраховыеВзносыСДоходов_Предприниматель");
		
		Запрос.УстановитьПараметр("КодыЗадач", КодыЗадач);
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&УсловиеПоПравилу", "ЗадачиБухгалтераНалоговыеПлатежи.Правило.Владелец.Код В(&КодыЗадач)");
	КонецЕсли;
	
	Запрос.Текст = ТекстЗапроса;
	
	Платежи = Запрос.Выполнить().Выгрузить();
	
	Возврат Платежи;
	
КонецФункции

// Возвращает порядок уплаты страховых взносов
//
// Параметры:
//   Организация - СправочникСсылка.Организации
//   Период - Дата - период уплаты
//
// Возвращаемое значение:
//   Структура
//     * Правило - СправочникСсылка.ПравилаПредставленияОтчетовУплатыНалогов
//     * Срок - Дата
//
Функция ПорядокУплаты(Организация, Период) Экспорт
	
	Перем СвойстваЗадачи;
	
	ПорядокУплаты = Новый Структура;
	ПорядокУплаты.Вставить("Правило", Справочники.ПравилаПредставленияОтчетовУплатыНалогов.ПустаяСсылка());
	ПорядокУплаты.Вставить("Срок", Дата(1, 1, 1));
	
	Если ОбщегоНазначенияБПВызовСервераПовтИсп.ЭтоЮрЛицо(Организация) Тогда
		Возврат ПорядокУплаты;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Организация) И ЗначениеЗаполнено(Период) Тогда
		ВидНалога = Перечисления.ВидыНалогов.ФиксированныеВзносы_ПФР_СтраховаяЧасть;
		СвойстваЗадачи = РегистрыСведений.ЗадачиБухгалтера.ПорядокПредоставленияОтчетаУплатыНалогаЗаПериод(
			Организация, ВидНалога, КонецКвартала(Период));
	КонецЕсли;
	
	Если СвойстваЗадачи <> Неопределено Тогда
		ЗаполнитьЗначенияСвойств(ПорядокУплаты, СвойстваЗадачи.Уплата);
	КонецЕсли;
	
	Возврат ПорядокУплаты;
	
КонецФункции

#КонецОбласти

#КонецЕсли
