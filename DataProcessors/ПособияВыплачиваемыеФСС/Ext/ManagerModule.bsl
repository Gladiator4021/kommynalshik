﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	// Печать заявления о выплате пособия.
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "ЗаявлениеСотрудникаНаВыплатуПособия";
	КомандаПечати.Представление = НСтр("ru = 'Печать заявления о выплате пособия'");
	КомандаПечати.ДополнительныеПараметры = Новый Структура("Раздел", "Заявление");
	
КонецПроцедуры

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	Документы.ЗаявлениеСотрудникаНаВыплатуПособия.Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода);	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
