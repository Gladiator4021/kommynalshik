﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// по умолчанию при отсутсвии пользовательских настроек
	ЗапрашиватьКоличество 	= Истина;
	ЗапрашиватьЦену 		= Истина;
	
	СписокСвойств = "ЕстьЦена, Валюта, ДатаРасчетов, ДоговорКонтрагента, Заголовок, ИмяТаблицы, Контрагент, Организация,
		|Склад, ТипЦен, Реализация, ДеятельностьНаПатенте, СуммаВключаетНДС, СтавкаНДС, Услуги, ВидПодбора";
		
	НеобязательныеПараметры = Новый Структура;
	
	НеобязательныеПараметры.Вставить("ПоказыватьОстатки",              Истина);
	НеобязательныеПараметры.Вставить("ПоказыватьЦены",                 Ложь);
	НеобязательныеПараметры.Вставить("УстанавливатьЦеныДокументов",    Ложь);
	НеобязательныеПараметры.Вставить("ПоказыватьОтрицательныеОстатки", Истина);
	НеобязательныеПараметры.Вставить("ПоказыватьЗабалансовыеОстатки",  Истина);
	НеобязательныеПараметры.Вставить("ЕстьКоличество",                 Истина);
	НеобязательныеПараметры.Вставить("Подразделение",                  Неопределено);
	
	НеобязательныеПараметры.Вставить("Курс",       0);
	НеобязательныеПараметры.Вставить("Кратность",  0);	
	
	Для каждого НеобязательныйПараметр Из НеобязательныеПараметры Цикл
		
		ИмяПараметра = НеобязательныйПараметр.Ключ;
		
		Если Параметры.Свойство(ИмяПараметра) Тогда
			СписокСвойств = СписокСвойств + "," + ИмяПараметра;
		КонецЕсли; 	
		
	КонецЦикла; 
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, НеобязательныеПараметры);
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Параметры, СписокСвойств);
	
	// Избавимся от ссылок на недоступные элементы
	Если Не ПравоДоступа("Чтение", Метаданные.Справочники.ТипыЦенНоменклатуры) Тогда
		ТипЦен = Неопределено;
	КонецЕсли;
	
	Если Параметры.ЗаполнятьЦеныПоПродаже тогда
		СпособЗаполненияЦены = ПредопределенноеЗначение("Перечисление.СпособыЗаполненияЦен.ПоПродажнымЦенам");
	ИначеЕсли Параметры.ЗаполнятьЦеныПоПокупке тогда
		СпособЗаполненияЦены = ПредопределенноеЗначение("Перечисление.СпособыЗаполненияЦен.ПоЗакупочнымЦенам");
	КонецЕсли;
	
	УстанавливатьЦеныДокументов = ЕстьЦена И НЕ ЗначениеЗаполнено(ТипЦен) И ЗначениеЗаполнено(СпособЗаполненияЦены);
	
	
	// Отключим невыполнимые опции
	Если Не ПравоДоступа("Просмотр", Метаданные.РегистрыБухгалтерии.Хозрасчетный) Тогда
		ПоказыватьОстатки = Ложь;
	КонецЕсли;
	
	Если Не ПравоДоступа("Просмотр", Метаданные.РегистрыСведений.ЦеныНоменклатуры) 
		Или Не ЗначениеЗаполнено(ТипЦен) Тогда
		ПоказыватьЦены = Ложь;
	КонецЕсли;
	
	Если Не ПравоДоступа("Чтение", Метаданные.РегистрыСведений.ЦеныНоменклатурыДокументов) Тогда
		УстанавливатьЦеныДокументов = Ложь;
	КонецЕсли;
	
	
	Если НЕ ЗначениеЗаполнено(Валюта) Тогда
		Валюта = ОбщегоНазначенияБПВызовСервераПовтИсп.ПолучитьВалютуРегламентированногоУчета();
	КонецЕсли;
	
	Если Курс = 0 или Кратность = 0 Тогда
		КурсКратность = РаботаСКурсамиВалют.ПолучитьКурсВалюты(Валюта, ДатаРасчетов);
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, КурсКратность);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ТипЦен) Тогда
		ВалютаЦены = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТипЦен, "ВалютаЦены");
	ИначеЕсли УстанавливатьЦеныДокументов Тогда
		ВалютаЦены = Валюта;
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(ВалютаЦены) Тогда
		ВалютаЦены = ОбщегоНазначенияБПВызовСервераПовтИсп.ПолучитьВалютуРегламентированногоУчета();
	КонецЕсли;
	
	// Определим настройку учета по складам и по подразделениям.
	Если ПоказыватьОстатки Тогда
		ВедетсяУчетПоСкладам = БухгалтерскийУчет.ВедетсяУчетПоСкладам(ПланыСчетов.Хозрасчетный.ТоварыНаСкладах);
	Иначе
		ВедетсяУчетПоСкладам = Ложь;
	КонецЕсли;
	
	ВедетсяУчетПоПодразделелениям = ПолучитьФункциональнуюОпцию("ВестиУчетПоПодразделениям");
	
	СпискиВыбораКлиентСервер.Загрузить("ИсторияПоискаНоменклатуры", Элементы.СтрокаПоиска.СписокВыбора);
	Настройки = ОбщегоНазначенияВызовСервера.ХранилищеОбщихНастроекЗагрузить("ПодборНоменклатуры", "");
	Если Настройки <> Неопределено Тогда
		Если Настройки.Свойство("ИсторияПоискаНоменклатуры") Тогда
			Элементы.СтрокаПоиска.СписокВыбора.ЗагрузитьЗначения(Настройки.ИсторияПоискаНоменклатуры);
		КонецЕсли;
		Если Настройки.Свойство("ЗапрашиватьКоличество") Тогда
			ЗапрашиватьКоличество = Настройки.ЗапрашиватьКоличество;
		КонецЕсли;
		Если Настройки.Свойство("ЗапрашиватьЦену") Тогда
			ЗапрашиватьЦену = Настройки.ЗапрашиватьЦену;
		КонецЕсли;
		Если Настройки.Свойство("ПоказыватьТолькоОстатки") Тогда
			ПоказыватьТолькоОстатки = Настройки.ПоказыватьТолькоОстатки;
		КонецЕсли;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Параметры.Заголовок) Тогда
		Заголовок = Параметры.Заголовок;
		АвтоЗаголовок = Ложь;
	КонецЕсли;
	
	Элементы.ПоказыватьТолькоОстатки.Видимость = ПоказыватьОстатки;
	
	НастроитьСписок();
	
	ОбновитьИнформационнуюНадписьПараметрыПодбора(ЭтотОбъект);
	Элементы.ИнформационнаяНадписьПараметровПодбора.Видимость = НЕ ПустаяСтрока(ИнформационнаяНадписьПараметровПодбора);
	
	УправлениеФормой(ЭтотОбъект);

	УстановитьУсловноеОформление();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	Если Параметр = ЭтаФорма Тогда
		Если ИмяСобытия = "ВводНовогоЭлементаСправочникаНоменклатура" Тогда
			Элементы.СписокНоменклатуры.Обновить();
			ЕстьКонтрольУслуги = (ТипЗнч(Услуги) = Тип("Булево"));
			Если НЕ ЕстьКонтрольУслуги ИЛИ (ЭтоУслуга(Источник) = Услуги) Тогда
				
				Элементы.СписокНоменклатуры.ТекущаяСтрока = Источник;
				
				ПараметрыНоменклатуры = Новый Структура("Номенклатура, Цена", Источник,	0);
				
				ОткрытьФормуВводаЦенаКоличество(ПараметрыНоменклатуры);
				
			КонецЕсли; 
		КонецЕсли; 
	КонецЕсли; 
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если ЗавершениеРаботы И Объект.ПодобраннаяНоменклатура.Количество() > 0 Тогда
		Отказ = Истина;
	ИначеЕсли Не ПеренестиВДокумент И Объект.ПодобраннаяНоменклатура.Количество() > 0 Тогда
		
		Отказ = Истина;
		
		ТекстВопроса = НСтр("ru = 'Подобранные товары не перенесены в документ.
			|
			|Перенести?'");
			
		Оповещение = Новый ОписаниеОповещения("ВопросПеренестиВДокументЗавершение", ЭтотОбъект);
		ПоказатьВопрос(Оповещение, ТекстВопроса, РежимДиалогаВопрос.ДаНетОтмена);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	СтруктураВозврата = ПриЗакрытииНаСервере();
	
	Если ПеренестиВДокумент Тогда
		ОповеститьОВыборе(СтруктураВозврата);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ПриЗакрытииНаСервере()
	
	СтруктураВозврата = Новый Структура();
	
	ПараметрыЗакрытия = Новый Структура;
	ПараметрыЗакрытия.Вставить("ИсторияПоискаНоменклатуры", Элементы.СтрокаПоиска.СписокВыбора.ВыгрузитьЗначения());
	ПараметрыЗакрытия.Вставить("ЗапрашиватьКоличество",     ЗапрашиватьКоличество);
	ПараметрыЗакрытия.Вставить("ЗапрашиватьЦену",           ЗапрашиватьЦену);
	ПараметрыЗакрытия.Вставить("ПоказыватьТолькоОстатки",   ПоказыватьТолькоОстатки);
	
	ОбщегоНазначенияВызовСервера.ХранилищеОбщихНастроекСохранить("ПодборНоменклатуры", "", ПараметрыЗакрытия);
	
	Если ПеренестиВДокумент Тогда
		АдресПодобраннойНоменклатурыВХранилище = ПоместитьПодобраннуюНоменклатуруВХранилище();
		СтруктураВозврата.Вставить("АдресПодобраннойНоменклатурыВХранилище", АдресПодобраннойНоменклатурыВХранилище);
	КонецЕсли;
	
	Возврат СтруктураВозврата;
	
КонецФункции

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СтрокаПоискаПриИзменении(Элемент)
	
	ПрименитьПоиск();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказыватьТолькоОстаткиПриИзменении(Элемент)
	
	УстановитьЗначениеПараметраПоказыватьТолькоОстаткиСпискаНоменклатуры(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыИерархияНоменклатуры

&НаКлиенте
Процедура ИерархияНоменклатурыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	УстановитьОтборПоИерархииНоменклатуры(ВыбраннаяСтрока);
	
КонецПроцедуры

&НаКлиенте
Процедура ИерархияНоменклатурыПриАктивизацииСтроки(Элемент)
	
	УстановитьОтборПоИерархииНоменклатуры(Элементы.ИерархияНоменклатуры.ТекущаяСтрока);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписокНоменклатуры

&НаКлиенте
Процедура СписокНоменклатурыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если Элемент.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыНоменклатуры = Новый Структура;
	ПараметрыНоменклатуры.Вставить("Номенклатура", Элемент.ТекущиеДанные.Ссылка);
	
	Цена = 0;
	Если Элемент.ТекущиеДанные.Свойство("Цена") Тогда
		
		ТекущаяВалюта = ?(Элемент.ТекущиеДанные.Свойство("Валюта") И ЗначениеЗаполнено(Элемент.ТекущиеДанные.Валюта),
			Элемент.ТекущиеДанные.Валюта, ВалютаЦены);
			
		Цена = ?(ТекущаяВалюта = Валюта, Элемент.ТекущиеДанные.Цена,
			ПолучитьЦенуПослеПересчета(Элемент.ТекущиеДанные.Цена, ТекущаяВалюта, Валюта, ДатаРасчетов, Курс, Кратность));
	ИначеЕсли УстанавливатьЦеныДокументов Тогда
		ДанныеОбъекта = Новый Структура("Организация, Склад, ДоговорКонтрагента, СпособЗаполненияЦены, СтавкаНДС,
			| КурсДокумента, КратностьДокумента, Реализация, ДеятельностьНаПатенте, СуммаВключаетНДС");
			
		ЗаполнитьЗначенияСвойств(ДанныеОбъекта,ЭтаФорма);
		
		ДанныеОбъекта.Вставить("Дата",			  ДатаРасчетов);
		ДанныеОбъекта.Вставить("ВалютаДокумента", Валюта);
		
		Цена = ПолучитьЦенуДокумента(ДанныеОбъекта, ПараметрыНоменклатуры.Номенклатура);
		
	КонецЕсли;
	
	ПараметрыНоменклатуры.Вставить("Цена", Цена);
	
	Если Элемент.ТекущиеДанные.Свойство("ЕдиницаИзмерения") Тогда
		ПараметрыНоменклатуры.Вставить("ЕдиницаИзмерения", Элемент.ТекущиеДанные.ЕдиницаИзмерения);
	КонецЕсли;
	
	ОткрытьФормуВводаЦенаКоличество(ПараметрыНоменклатуры);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокНоменклатурыВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	СписокНоменклатурыВыбор(Элемент, Значение, , СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура СписокНоменклатурыПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Отказ = Истина;
	
	
	Если Копирование Тогда
		
		ПараметрыФормыНоменклатура = Новый Структура("ЗначениеКопирования", Элемент.ТекущаяСтрока);
		
	Иначе
		
		ДанныеЗаполнения = Новый Структура;
		
		Если ТипЗнч(Услуги) = Тип("Булево") Тогда
			ДанныеЗаполнения.Вставить("Услуга",Услуги);
		КонецЕсли; 
		
		Если Элементы.ИерархияНоменклатуры.ТекущаяСтрока <> Неопределено Тогда
			ДанныеЗаполнения.Вставить("Родитель", Элементы.ИерархияНоменклатуры.ТекущаяСтрока);
		КонецЕсли; 
		
		ПараметрыФормыНоменклатура = Новый Структура("ЗначенияЗаполнения, ДополнительныеПараметры", ДанныеЗаполнения, Новый Структура("Наименование", СокрЛП(СтрокаПоиска)));
		
	КонецЕсли; 
	
	ОткрытьФорму("Справочник.Номенклатура.ФормаОбъекта", ПараметрыФормыНоменклатура,ЭтаФорма,,,,,РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыПодобраннаяНоменклатура

&НаКлиенте
Процедура ПодобраннаяНоменклатураПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобраннаяНоменклатураКоличествоПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.ПодобраннаяНоменклатура.ТекущиеДанные;
	ТекущиеДанные.Сумма = ТекущиеДанные.Цена * ТекущиеДанные.Количество;
	
	УправлениеФормой(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобраннаяНоменклатураЦенаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.ПодобраннаяНоменклатура.ТекущиеДанные;
	ТекущиеДанные.Сумма = ТекущиеДанные.Цена * ТекущиеДанные.Количество;
	
	УправлениеФормой(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобраннаяНоменклатураСуммаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.ПодобраннаяНоменклатура.ТекущиеДанные;
	Если ТекущиеДанные.Количество = 0 Тогда
		ТекущиеДанные.Цена = 0;
	Иначе
		ТекущиеДанные.Цена = ТекущиеДанные.Сумма / ТекущиеДанные.Количество;
	КонецЕсли;
	
	УправлениеФормой(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПеренестиВДокумент(Команда)
	
	ПеренестиВДокумент = Истина;
	Закрыть(КодВозвратаДиалога.OK);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФормуНастройкиПодбора(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПрименитьНастройкиПодбораЗавершение", ЭтотОбъект);
	
	ПараметрыФормы = Новый Структура("ЕстьЦена, ЕстьКоличество", ЕстьЦена, ЕстьКоличество);
	
	ОткрытьФорму("Обработка.ПодборНоменклатуры.Форма.НастройкиПодбора", ПараметрыФормы,
		ЭтотОбъект,,,, ОписаниеОповещения, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();


	// ПодобраннаяНоменклатураЦена, ПодобраннаяНоменклатураСумма

	ЭлементУО = УсловноеОформление.Элементы.Добавить();

	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "ПодобраннаяНоменклатураЦена");
	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "ПодобраннаяНоменклатураСумма");

	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(ЭлементУО.Отбор,
		"ЕстьЦена", ВидСравненияКомпоновкиДанных.Равно, Ложь);

	ЭлементУО.Оформление.УстановитьЗначениеПараметра("Видимость", Ложь);


	// СписокНоменклатурыКоличествоОстаток

	ЭлементУО = УсловноеОформление.Элементы.Добавить();

	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "СписокНоменклатурыКоличествоОстаток");

	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(ЭлементУО.Отбор,
		"ПоказыватьОстатки", ВидСравненияКомпоновкиДанных.Равно, Ложь);

	ЭлементУО.Оформление.УстановитьЗначениеПараметра("Видимость", Ложь);


	// СписокНоменклатурыЦена, СписокНоменклатурыВалюта

	ЭлементУО = УсловноеОформление.Элементы.Добавить();

	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "СписокНоменклатурыЦена");
	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "СписокНоменклатурыВалюта");

	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(ЭлементУО.Отбор,
		"ПоказыватьЦены", ВидСравненияКомпоновкиДанных.Равно, Ложь);

	ЭлементУО.Оформление.УстановитьЗначениеПараметра("Видимость", Ложь);


	// ПодобраннаяНоменклатураКоличество, ПодобраннаяНоменклатураСумма

	ЭлементУО = УсловноеОформление.Элементы.Добавить();

	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "ПодобраннаяНоменклатураКоличество");
	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "ПодобраннаяНоменклатураСумма");

	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(ЭлементУО.Отбор,
		"ЕстьКоличество", ВидСравненияКомпоновкиДанных.Равно, Ложь);

	ЭлементУО.Оформление.УстановитьЗначениеПараметра("Видимость", Ложь);


	// СписокНоменклатурыКоличествоОстаток

	ЭлементУО = УсловноеОформление.Элементы.Добавить();

	КомпоновкаДанныхКлиентСервер.ДобавитьОформляемоеПоле(ЭлементУО.Поля, "СписокНоменклатурыКоличествоОстаток");

	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(ЭлементУО.Отбор,
		"ПоказыватьОтрицательныеОстатки", ВидСравненияКомпоновкиДанных.Равно, Ложь);

	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(ЭлементУО.Отбор,
		"СписокНоменклатуры.КоличествоОстаток", ВидСравненияКомпоновкиДанных.Меньше, 0);

	ЭлементУО.Оформление.УстановитьЗначениеПараметра("Отображать", Ложь);

КонецПроцедуры

&НаКлиенте
Процедура ПрименитьПоиск()
	
	Использование = ЗначениеЗаполнено(СтрокаПоиска);
	
	ГруппаОтбора = ОтборыСписковКлиентСервер.СоздатьГруппуЭлементовОтбора(
		СписокНоменклатуры.Отбор.Элементы, "ПоискПоПодстроке",
		ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИли);
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораГруппыСписка(
		ГруппаОтбора, "Наименование", СтрокаПоиска,
		Использование, ВидСравненияКомпоновкиДанных.Содержит);
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораГруппыСписка(
		ГруппаОтбора, "НаименованиеПолное", СтрокаПоиска,
		Использование, ВидСравненияКомпоновкиДанных.Содержит);
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораГруппыСписка(
		ГруппаОтбора, "Код", СтрокаПоиска,
		Использование, ВидСравненияКомпоновкиДанных.Содержит);
		
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораГруппыСписка(
		ГруппаОтбора, "Артикул", СтрокаПоиска,
		Использование, ВидСравненияКомпоновкиДанных.Содержит);
	
	Если ЗначениеЗаполнено(СтрокаПоиска) Тогда
		СпискиВыбораКлиентСервер.ОбновитьСписокВыбора(Элементы.СтрокаПоиска.СписокВыбора, СтрокаПоиска);
	КонецЕсли; 
	
	ТекущийЭлемент = Элементы.СписокНоменклатуры;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФормуВводаЦенаКоличество(ПараметрыНоменклатуры)

	ПараметрыНоменклатуры.Вставить("Валюта",       Валюта);
	ПараметрыНоменклатуры.Вставить("Количество", 1);
	ПараметрыНоменклатуры.Вставить("ЕстьЦена" ,      ЗапрашиватьЦену И ЕстьЦена);
	ПараметрыНоменклатуры.Вставить("ЕстьКоличество", ЗапрашиватьКоличество И ЕстьКоличество);
	
	Если ЗапрашиватьКоличество И ЕстьКоличество ИЛИ ЗапрашиватьЦену И ЕстьЦена Тогда
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ДобавитьВыбраннуюНоменклатуруЗавершение", ЭтотОбъект);
		
		ОткрытьФорму("Обработка.ПодборНоменклатуры.Форма.ФормаВводаЦенаКоличество", ПараметрыНоменклатуры,
			ЭтотОбъект,,,, ОписаниеОповещения, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		
	Иначе
		
		ДобавитьВыбраннуюНоменклатуруЗавершение(ПараметрыНоменклатуры, Неопределено);
		
	КонецЕсли;

КонецПроцедуры 

&НаКлиенте
Процедура ДобавитьВыбраннуюНоменклатуруЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Результат.Свойство("Цена") Тогда
		ПараметрыПоиска = Новый Структура("Номенклатура, Цена", Результат.Номенклатура, Результат.Цена);
	Иначе
		ПараметрыПоиска = Новый Структура("Номенклатура", Результат.Номенклатура);
	КонецЕсли;
	
	РезультатПоиска = Объект.ПодобраннаяНоменклатура.НайтиСтроки(ПараметрыПоиска);
	
	Если РезультатПоиска.Количество() = 0 Тогда
		ТекущаяСтрока = Объект.ПодобраннаяНоменклатура.Добавить();
		ЗаполнитьЗначенияСвойств(ТекущаяСтрока, Результат,, "Количество");
	Иначе
		ТекущаяСтрока = РезультатПоиска[0];
	КонецЕсли;
	
	ТекущаяСтрока.Количество = Результат.Количество + ТекущаяСтрока.Количество;
	
	Если Результат.Свойство("Цена") Тогда
		ТекущаяСтрока.Сумма = ТекущаяСтрока.Количество * ТекущаяСтрока.Цена;
	КонецЕсли;
	
	// Активизируем текущую строку табличной части
	Элементы.ПодобраннаяНоменклатура.ТекущаяСтрока = ТекущаяСтрока.ПолучитьИдентификатор();
	
	УправлениеФормой(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ВопросПеренестиВДокументЗавершение(ОтветНаВопрос, ДополнительныеПараметры) Экспорт
	
	Если ОтветНаВопрос = КодВозвратаДиалога.Да Тогда
		ПеренестиВДокумент = Истина;
		Закрыть();
	ИначеЕсли ОтветНаВопрос = КодВозвратаДиалога.Нет Тогда
		ПеренестиВДокумент = Истина;
		Объект.ПодобраннаяНоменклатура.Очистить();
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПрименитьНастройкиПодбораЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЗапрашиватьКоличество = Результат.ЗапрашиватьКоличество;
	ЗапрашиватьЦену       = Результат.ЗапрашиватьЦену;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ЭтоУслуга(Ссылка)

	Возврат Ссылка.Услуга;

КонецФункции 

&НаКлиентеНаСервереБезКонтекста
Процедура УправлениеФормой(Форма)
	
	Элементы = Форма.Элементы;
	Объект   = Форма.Объект;
	
	ОбновитьИтоги(Форма);
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьДатуОстатковПоУмолчанию()
	
	ТекущаяРабочаяДата = ОбщегоНазначения.ТекущаяДатаПользователя();
	
	Если ТекущаяРабочаяДата = НачалоДня(ТекущаяДатаСеанса()) Тогда
		Возврат ТекущаяДатаСеанса();
	Иначе
		Возврат ТекущаяРабочаяДата;
	КонецЕсли; 
	
КонецФункции 

&НаСервереБезКонтекста
Функция ПолучитьСписокСчетов(Знач СписокСчетов, Знач СписокСубконто, Знач ПоказыватьЗабалансовые)

	Запрос = Новый Запрос;
	
	Исключения = Новый Массив;
	Исключения.Добавить(ПланыСчетов.Хозрасчетный.ГТД);
	Исключения.Добавить(ПланыСчетов.Хозрасчетный.МатериальныеЦенностиВЭксплуатации);
	
	Запрос.УстановитьПараметр("СписокСубконто", СписокСубконто);
	Запрос.УстановитьПараметр("Исключения", Исключения);
	Если СписокСчетов.Количество() <> 0 Тогда
	
		Запрос.УстановитьПараметр("СписокСчетов", СписокСчетов);
	
	КонецЕсли;
	
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ХозрасчетныйВидыСубконто.Ссылка
	|ИЗ
	|	ПланСчетов.Хозрасчетный.ВидыСубконто КАК ХозрасчетныйВидыСубконто
	|ГДЕ
	|	ХозрасчетныйВидыСубконто.ВидСубконто В(&СписокСубконто)
	|	И НЕ ХозрасчетныйВидыСубконто.Ссылка В (&Исключения)
	|	И НЕ ХозрасчетныйВидыСубконто.Ссылка.ЗапретитьИспользоватьВПроводках" 
	+ ?(ПоказыватьЗабалансовые, "", " И НЕ ХозрасчетныйВидыСубконто.Ссылка.Забалансовый")
	+?(СписокСчетов.Количество() = 0, "", " И ХозрасчетныйВидыСубконто.Ссылка В ИЕРАРХИИ (&СписокСчетов)");
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
	
КонецФункции

// Возвращает массив документов партий комитента
&НаСервереБезКонтекста
Функция ПолучитьСписокДокументовКомитента(СписокОрганизаций, СписокСчетов, ДатаОстатки, Комитент)
	Запрос = Новый Запрос;
	
	Запрос.УстановитьПараметр("СписокОрганизаций", СписокОрганизаций);
	Запрос.УстановитьПараметр("ДатаОстатки",       ДатаОстатки);
	Запрос.УстановитьПараметр("СписокСчетов",      СписокСчетов);
	Запрос.УстановитьПараметр("ВидыСубконто",      ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Партии);
	Запрос.УстановитьПараметр("Комитент",          Комитент);
	
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ПоступлениеТоваровУслуг.Ссылка КАК Ссылка
		|ПОМЕСТИТЬ ДокументыКомитента
		|ИЗ
		|	Документ.ПоступлениеТоваровУслуг КАК ПоступлениеТоваровУслуг
		|ГДЕ
		|	ПоступлениеТоваровУслуг.Контрагент = &Комитент
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ХозрасчетныйОстатки.Субконто1 КАК Партии
		|ИЗ
		|	РегистрБухгалтерии.Хозрасчетный.Остатки(
		|			&ДатаОстатки,
		|			Счет В (&СписокСчетов),
		|			&ВидыСубконто,
		|			Организация В (&СписокОрганизаций)
		|				И Субконто1 В
		|					(ВЫБРАТЬ
		|						ДокументыКомитента.Ссылка
		|					ИЗ
		|						ДокументыКомитента)) КАК ХозрасчетныйОстатки
		|ГДЕ
		|	ХозрасчетныйОстатки.КоличествоОстаток <> 0";
	
	УстановитьПривилегированныйРежим(Истина);

	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Партии");
КонецФункции  

&НаСервере
Процедура НастроитьСписок()
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
		СписокНоменклатуры,
		"Услуга",
		Услуги,
		ТипЗнч(Услуги) = Тип("Булево"));
		
	ДатаОстаткиЦены = ?(НЕ ЗначениеЗаполнено(ДатаРасчетов), ПолучитьДатуОстатковПоУмолчанию(), ДатаРасчетов);
	
	СписокНоменклатуры.ТекстЗапроса = ТекстЗапросаДинамическогоСписка(ПоказыватьЦены, ПоказыватьОстатки);
	
	// Отображение цен
	
	Элементы.СписокНоменклатурыЦена.Видимость   = ПоказыватьЦены;
	Элементы.СписокНоменклатурыВалюта.Видимость = ПоказыватьЦены;
	
	Если ПоказыватьЦены Тогда
	
		Если ВРег(ВидПодбора) = "НТТ" Тогда
			ТипЦенЗапроса = Склад.ТипЦенРозничнойТорговли;
		Иначе
			ТипЦенЗапроса = ТипЦен;
		КонецЕсли;
		
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
			СписокНоменклатуры,
			"ТипЦен",
			ТипЦенЗапроса,
			ЗначениеЗаполнено(ТипЦенЗапроса));
			
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
			СписокНоменклатуры,
			"ВалютаЦены",
			ВалютаЦены,
			ЗначениеЗаполнено(ВалютаЦены));
			
		СписокНоменклатуры.Параметры.УстановитьЗначениеПараметра("ДатаОстаткиЦены", ДатаОстаткиЦены);
		
	КонецЕсли;
	
	// Отображение остатков на складе
	
	Элементы.СписокНоменклатурыКоличествоОстаток.Видимость = ПоказыватьОстатки;
	
	Если ПоказыватьОстатки Тогда
		
		СписокДоступныхОрганизаций = ОбщегоНазначенияБПВызовСервераПовтИсп.ВсеОрганизацииДанныеКоторыхДоступныПоRLS(Ложь);
	
		Если НЕ ЗначениеЗаполнено(Организация) Тогда
		
			СписокОрганизаций = СписокДоступныхОрганизаций;
		
		ИначеЕсли СписокДоступныхОрганизаций.Найти(Организация) <> Неопределено Тогда
		
			СписокОрганизаций = Новый Массив;
			СписокОрганизаций.Добавить(Организация);
		
		Иначе
			СписокОрганизаций = Новый Массив;
		КонецЕсли;
		
		
		ВидыСубконто = Новый Массив;
		ВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Номенклатура);
		
		СписокСчетов = Новый Массив;
		
		ИспользоватьОтборПоСкладу     = Истина;
		ИспользоватьОтборПоСчетуНеГТД = Истина;
		
		Если ВРег(ВидПодбора) = "КОМИТЕНТ" Тогда
		
			СписокСчетов.Добавить(ПланыСчетов.Хозрасчетный.ТоварыНаСкладе);
		
			Если ЗначениеЗаполнено(Контрагент) Тогда
				ВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Партии);
			
				СписокДокументовКомитента = ПолучитьСписокДокументовКомитента(СписокОрганизаций, СписокСчетов, ДатаОстаткиЦены, Контрагент);
				ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
					СписокНоменклатуры,
					"Субконто2",
					СписокДокументовКомитента, 
					Истина,
					ВидСравненияКомпоновкиДанных.ВСписке);
		
			КонецЕсли; 
		
		ИначеЕсли ВРег(ВидПодбора) = "КОМИССИОНЕР" Тогда
			
			ИспользоватьОтборПоСкладу = Ложь;
			
			СписокСчетов.Добавить(ПланыСчетов.Хозрасчетный.ТоварыОтгруженные);
			СписокСчетов.Добавить(ПланыСчетов.Хозрасчетный.ТоварыПереданныеНаКомиссию);
			
			ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
				СписокНоменклатуры,
				"Субконто2",
				Контрагент,
				ЗначениеЗаполнено(Контрагент));
			
			ВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты);
			
		ИначеЕсли ВРег(ВидПодбора) = "МАТЕРИАЛЫЗАКАЗЧИКА" Тогда
			
			СписокСчетов.Добавить(ПланыСчетов.Хозрасчетный.МатериалыПринятыеВПереработку);
			
			ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
				СписокНоменклатуры,
				"Субконто2",
				Контрагент,
				ЗначениеЗаполнено(Контрагент));
			
			ВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты);
				
		КонецЕсли;
		
		Если ИспользоватьОтборПоСкладу Тогда
			Если ЗначениеЗаполнено(Склад) И ВедетсяУчетПоСкладам Тогда
				
				ВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Склады);
				
				ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
					СписокНоменклатуры,
					"Субконто"+ВидыСубконто.Количество(),
					Склад,
					Истина);
					
			КонецЕсли;
		КонецЕсли;
		
		Если ВедетсяУчетПоПодразделелениям Тогда
			
			ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
				СписокНоменклатуры,
				"Подразделение",
				Подразделение,
				Подразделение <> Неопределено);
				
		КонецЕсли; 
		
		СписокСчетов = ПолучитьСписокСчетов(СписокСчетов, ВидыСубконто, ПоказыватьЗабалансовыеОстатки);
		
		СписокНоменклатуры.Параметры.УстановитьЗначениеПараметра("ВидыСубконто",      ВидыСубконто);
		СписокНоменклатуры.Параметры.УстановитьЗначениеПараметра("СчетаЗапасов",      СписокСчетов);
		СписокНоменклатуры.Параметры.УстановитьЗначениеПараметра("СписокОрганизаций", СписокОрганизаций);
		СписокНоменклатуры.Параметры.УстановитьЗначениеПараметра("ДатаОстаткиЦены",   ДатаОстаткиЦены);
		
		УстановитьЗначениеПараметраПоказыватьТолькоОстаткиСпискаНоменклатуры(ЭтаФорма);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ТекстЗапросаДинамическогоСписка(ПоказыватьЦены, ПоказыватьОстатки)
	
	ТекстЗапроса = // Текст может быть модифицирован ниже
	"ВЫБРАТЬ
	|	СправочникНоменклатура.Ссылка КАК Ссылка,
	|	СправочникНоменклатура.Услуга КАК Услуга,
	|	СправочникНоменклатура.Наименование КАК Наименование,
	|	СправочникНоменклатура.НаименованиеПолное КАК НаименованиеПолное,
	|	СправочникНоменклатура.Код КАК Код,
	|	СправочникНоменклатура.Артикул КАК Артикул,
	|	СправочникНоменклатура.ЕдиницаИзмерения КАК ЕдиницаИзмерения,
	|	ЕСТЬNULL(ОстаткиНаСкладе.КоличествоОстаток, 0) КАК КоличествоОстаток,
	|	ЕСТЬNULL(ЦеныНоменклатуры.Цена, 0) КАК Цена,
	|	ЕСТЬNULL(ЦеныНоменклатуры.Валюта, ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка)) КАК Валюта
	|ИЗ
	|	Справочник.Номенклатура КАК СправочникНоменклатура
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Остатки({(&ДатаОстаткиЦены)}, Счет В (&СчетаЗапасов), {(&ВидыСубконто)}, Организация В (&СписокОрганизаций) {(Подразделение) КАК Подразделение, (ВЫРАЗИТЬ(Субконто1 КАК Справочник.Номенклатура)).* КАК Ссылка, (Субконто2)}) КАК ОстаткиНаСкладе
	|		ПО СправочникНоменклатура.Ссылка = ОстаткиНаСкладе.Субконто1
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры.СрезПоследних({(&ДатаОстаткиЦены)}, {(Номенклатура).* КАК Ссылка, (ТипЦен) КАК ТипЦен, (Валюта) КАК ВалютаЦены}) КАК ЦеныНоменклатуры
	|		ПО СправочникНоменклатура.Ссылка = ЦеныНоменклатуры.Номенклатура
	|ГДЕ
	|	НЕ СправочникНоменклатура.ЭтоГруппа";
	
	// В версии 8.3.8 с отключенным режимом совместимости информация о видимости колонок не передается динамическому списку.
	// Поэтому запрос динамического списка следует формировать в прикладном коде, в зависимости от того,
	// какие данные должны быть отображены в списке.
	
	Если ПоказыватьЦены И ПоказыватьОстатки Тогда
		Возврат ТекстЗапроса;
	КонецЕсли;
	
	СхемаЗапроса = Новый СхемаЗапроса;
	
	СхемаЗапроса.УстановитьТекстЗапроса(ТекстЗапроса);
	
	ОператорВыбора = СхемаЗапроса.ПакетЗапросов[0].Операторы[0];
	
	Если Не ПоказыватьОстатки Тогда
		ОператорВыбора.Источники.Удалить("ОстаткиНаСкладе"); // одновременно будет удалено поле КоличествоОстаток
	КонецЕсли;
	
	Если Не ПоказыватьЦены Тогда
		ОператорВыбора.Источники.Удалить("ЦеныНоменклатуры"); // одновременно будут удалены поля Валюта и Цена
	КонецЕсли;
	
	Возврат СхемаЗапроса.ПолучитьТекстЗапроса();
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьСписокПолейИнформационнойНадписи(Форма)
	
	СписокПолей = Новый Массив;
	
	Если Форма.ПоказыватьОстатки Тогда
		СписокПолей.Добавить("Организация");
		Если Форма.ВедетсяУчетПоПодразделелениям Тогда
			СписокПолей.Добавить("Подразделение");
		КонецЕсли; 
	КонецЕсли;
	
	Если Форма.ПоказыватьОстатки ИЛИ Форма.ПоказыватьЦены Тогда
		СписокПолей.Добавить("ДатаРасчетов");
	КонецЕсли;
	
	Если Форма.ПоказыватьОстатки И Форма.ВедетсяУчетПоСкладам
		И (Форма.ВидПодбора = "НТТ" ИЛИ Форма.ВидПодбора = "") Тогда
		
		СписокПолей.Добавить("Склад");
		
	КонецЕсли;
	
	Если Форма.ПоказыватьЦены Тогда
		СписокПолей.Добавить("ТипЦен");
	КонецЕсли;
	
	Если Форма.ВидПодбора = "Комиссионер" ИЛИ Форма.ВидПодбора = "Комитент" Тогда
		СписокПолей.Добавить("Контрагент");
	КонецЕсли;
	
	Возврат Новый ФиксированныйМассив(СписокПолей);
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьИнформационнуюНадписьПараметрыПодбора(Форма)
	
	СписокПолей = ПолучитьСписокПолейИнформационнойНадписи(Форма);
	
	ТекстНадписи = "";
	Для Каждого Поле Из СписокПолей Цикл
		Если ЗначениеЗаполнено(Форма[Поле]) Тогда
			ТекстНадписи = ТекстНадписи + Форма[Поле] + "; ";
		КонецЕсли;
	КонецЦикла;
	
	ТекстНадписи = Лев(ТекстНадписи, СтрДлина(ТекстНадписи) - 2);
	
	Форма.ИнформационнаяНадписьПараметровПодбора = ТекстНадписи;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьОтборПоИерархииНоменклатуры(ГруппаНоменклатуры)
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(СписокНоменклатуры, "Ссылка",
		ГруппаНоменклатуры, ЗначениеЗаполнено(ГруппаНоменклатуры), ВидСравненияКомпоновкиДанных.ВИерархии);
	
КонецПроцедуры

&НаСервере
Функция ПоместитьПодобраннуюНоменклатуруВХранилище()
	
	ТаблицаНоменклатуры = Объект.ПодобраннаяНоменклатура.Выгрузить();
	ТаблицаНоменклатуры.Колонки.Добавить("КиЗ_ГИСМ", Новый ОписаниеТипов("СправочникСсылка.КонтрольныеЗнакиГИСМ"));
	
	АдресПодобраннойНоменклатурыВХранилище = ПоместитьВоВременноеХранилище(ТаблицаНоменклатуры, УникальныйИдентификатор);
	
	Возврат АдресПодобраннойНоменклатурыВХранилище;
	
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьЦенуПослеПересчета(Знач Цена, Знач ВалютаНач, Знач ВалютаКон, Знач ДатаПересчета, Знач Курс, Знач Кратность)
	
	КурсКратностьНач = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаНач, ДатаПересчета);
	Если Курс = 0 или Кратность = 0 Тогда
		КурсКратностьКон = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаКон, ДатаПересчета);
		
		Возврат РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(Цена, ВалютаНач, ВалютаКон,
			КурсКратностьНач.Курс, КурсКратностьКон.Курс,
			КурсКратностьНач.Кратность, КурсКратностьКон.Кратность);
	Иначе
		Возврат РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(Цена, ВалютаНач, ВалютаКон,
			КурсКратностьНач.Курс, Курс,
			КурсКратностьНач.Кратность, Кратность);
	КонецЕсли;
		
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьЦенуДокумента(ДанныеОбъекта, Номенклатура)
	
	СведенияОНоменклатуре = БухгалтерскийУчетПереопределяемый.ПолучитьСведенияОНоменклатуре(
	Номенклатура, ДанныеОбъекта, Ложь, Истина);
	
	Цена = СведенияОНоменклатуре.Цена;
	
	Возврат Цена;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьЗначениеПараметраПоказыватьТолькоОстаткиСпискаНоменклатуры(Форма)

	Если Форма.ПоказыватьОстатки Тогда
		
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
			Форма.СписокНоменклатуры, 
			"КоличествоОстаток", 
			0, 
			Форма.ПоказыватьТолькоОстатки, 
			?(Форма.ПоказыватьОтрицательныеОстатки, ВидСравненияКомпоновкиДанных.НеРавно, ВидСравненияКомпоновкиДанных.Больше)
			);
		
	КонецЕсли;

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьИтоги(Форма)
	
	Форма.ИтогиСумма = Форма.Объект.ПодобраннаяНоменклатура.Итог("Сумма");
	
КонецПроцедуры

#КонецОбласти