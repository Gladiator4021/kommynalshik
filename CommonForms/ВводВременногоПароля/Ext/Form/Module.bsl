﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Сертификат = Параметры.Сертификат;
	
	Если Не ЗначениеЗаполнено(Сертификат) Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	СпособДоставкиПаролей = "phone";
	
	Результат = СервисКриптографии.ПолучитьНастройкиПолученияВременныхПаролей(Сертификат.Идентификатор);
	Телефон          = Результат.Телефон;
	ЭлектроннаяПочта = Результат.ЭлектроннаяПочта;
	
	Элементы.ОткрытьНастройкиПолученияВременныхПаролей.Заголовок = Телефон;
	
	ПарольОтправлен = ПолучитьПарольНаСервере();
	
	Элементы.ПолучитьВременныйПарольДругимСпособом.Видимость = Ложь;	
	УправлениеФормой(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если ПарольОтправлен Тогда	
		ЗапуститьОбратныйОтсчет();
	Иначе
		ПоказатьПредупреждение(, НСтр("ru = 'Сервис отправки SMS-сообщений временно недоступен. Повторите попытку позже.'"));
		Отказ = Истина;
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОткрытьНастройкиПолученияВременныхПаролей(Команда)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Сертификат", Сертификат);
	
	Закрыть(ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтправитьПарольПовторно(Команда)

	Пароль = Неопределено;
	ЗапуститьОбратныйОтсчет();

	ПарольОтправлен = ПолучитьПарольНаСервере(Истина);
	
	УправлениеФормой(ЭтаФорма);
	
	ПодключитьОбработчикОжидания("Подключаемый_АктивироватьПолеДляВводаПароля", 0.1, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура Подтвердить(Команда)
	
	ОчиститьСообщения();
	ТекстОшибки = "";
	Пароль = СокрЛП(Пароль);
	Если ЗначениеЗаполнено(Пароль) И СтрДлина(Пароль) = 6 Тогда
		Попытка
			ПодтвердитьНаСервере();
			Закрыть(КодВозвратаДиалога.ОК);
		Исключение
			ИнформацияОбОшибке = ИнформацияОбОшибке();
			Если СтрНайти(ПодробноеПредставлениеОшибки(ИнформацияОбОшибке), "Invalid password") Тогда
				ТекстОшибки = НСтр("ru = 'Указан неверный пароль'");
			Иначе 
				ПоказатьПредупреждение(, НСтр("ru = 'Выполнение операции временно невозможно'"));
			КонецЕсли;
		КонецПопытки;		
	ИначеЕсли ЗначениеЗаполнено(Пароль) И СтрДлина(Пароль) <> 6 Тогда
		ТекстОшибки = НСтр("ru = 'Пароль должен состоять из 6 цифр'");
	Иначе
		ТекстОшибки = НСтр("ru = 'Пароль не указан'");
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПолучитьВременныйПарольДругимСпособом(Команда)
	
	Если СпособДоставкиПаролей = "phone" Тогда
		СпособДоставкиПаролей = "email";
	Иначе
		СпособДоставкиПаролей = "phone";
	КонецЕсли;
	
	ОтправитьПарольПовторно(Неопределено);
	
	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура Подключаемый_АктивироватьПолеДляВводаПароля()
	
	Пароль = Неопределено;
	Элементы.Пароль.ОбновитьТекстРедактирования();
	ТекущийЭлемент = Элементы.Пароль;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьПарольНаСервере(Повторно = Ложь)
	
	НомерПароля = СервисКриптографииСлужебный.ПолучитьВременныйПароль(
		Сертификат.Идентификатор, 
		Повторно, 
		СпособДоставкиПаролей);
	
	Возврат Булево(НомерПароля);
	
КонецФункции

&НаКлиенте
Процедура ЗапуститьОбратныйОтсчет()
	
	Таймер = 60;
	ПодключитьОбработчикОжидания("Подключаемый_ОбработчикОбратногоОтсчета", 1, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбработчикОбратногоОтсчета()
	
	Таймер = Таймер - 1;
	Если Таймер >= 0 Тогда
		НадписьОбратногоОтсчета = СтрШаблон(НСтр("ru = 'Запросить пароль повторно можно будет через %1 сек.'"), Таймер);
		ПодключитьОбработчикОжидания("Подключаемый_ОбработчикОбратногоОтсчета", 1, Истина);		
	Иначе
		НадписьОбратногоОтсчета = "";
		ПарольОтправлен = Ложь;		
		Элементы.ПолучитьВременныйПарольДругимСпособом.Видимость = ЗначениеЗаполнено(ЭлектроннаяПочта);
		
		УправлениеФормой(ЭтаФорма);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УправлениеФормой(Форма)
	
	Элементы = Форма.Элементы;
	
	Элементы.НадписьОбратногоОтсчета.Видимость = Форма.ПарольОтправлен;	
	Элементы.ОтправитьПарольПовторно.Видимость = Не Форма.ПарольОтправлен;
	
	Если Форма.СпособДоставкиПаролей = "phone" Тогда
		Элементы.Пояснение.Заголовок = НСтр("ru = 'Пароль отправлен в SMS-сообщении на номер'");
		ТекстКоманды = НСтр("ru = 'Не получили SMS? Попробуйте отправить пароль на %1'");
		Элементы.ПолучитьВременныйПарольДругимСпособом.Заголовок = СтрШаблон(ТекстКоманды, Форма.ЭлектроннаяПочта);
		Элементы.ОткрытьНастройкиПолученияВременныхПаролей.Заголовок = Форма.Телефон;
		Если Форма.НомерПароля > 1 Тогда
			Элементы.Пароль.ПодсказкаВвода = СтрШаблон(НСтр("ru = 'Введите пароль №%1 из SMS'"), Форма.НомерПароля);
		Иначе
			Элементы.Пароль.ПодсказкаВвода = НСтр("ru = 'Введите пароль из SMS'");
		КонецЕсли;
	ИначеЕсли Форма.СпособДоставкиПаролей = "email" Тогда
		Элементы.Пояснение.Заголовок = НСтр("ru = 'Пароль отправлен в письме на адрес'");
		ТекстКоманды = НСтр("ru = 'Не получили письмо? Попробуйте отправить пароль на %1'");
		Элементы.ПолучитьВременныйПарольДругимСпособом.Заголовок = СтрШаблон(ТекстКоманды, Форма.Телефон);
		Элементы.ОткрытьНастройкиПолученияВременныхПаролей.Заголовок = Форма.ЭлектроннаяПочта;
		Если Форма.НомерПароля > 1 Тогда
			Элементы.Пароль.ПодсказкаВвода = СтрШаблон(НСтр("ru = 'Введите пароль №%1 из письма'"), Форма.НомерПароля);
		Иначе
			Элементы.Пароль.ПодсказкаВвода = НСтр("ru = 'Введите пароль из письма'");
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

&НаСервере
Функция ПодтвердитьНаСервере()
	
	Возврат СервисКриптографииСлужебный.ПолучитьСеансовыйКлюч(Сертификат.Идентификатор, Пароль);
	
КонецФункции

#КонецОбласти