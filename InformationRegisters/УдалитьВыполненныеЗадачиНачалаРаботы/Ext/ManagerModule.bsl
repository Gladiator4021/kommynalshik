﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

// Обработчик обновления информационной базы
//
Процедура ОчиститьРегистр() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ИСТИНА КАК ЕстьЗаписи
	|ИЗ
	|	РегистрСведений.УдалитьВыполненныеЗадачиНачалаРаботы КАК УдалитьВыполненныеЗадачиНачалаРаботы";
	
	Если Не Запрос.Выполнить().Пустой() Тогда
		НаборЗаписей = РегистрыСведений.УдалитьВыполненныеЗадачиНачалаРаботы.СоздатьНаборЗаписей();
		ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборЗаписей);
	КонецЕсли;
	
КонецПроцедуры

#КонецЕсли