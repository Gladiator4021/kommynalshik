﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Макет169 = РегистрыСведений.СведенияОбАлкогольнойПродукции.ПолучитьМакет("Макет169");
	
	Область = Макет169.Области.ВидыПродукции;
	Если Область.ТипОбласти = ТипОбластиЯчеекТабличногоДокумента.Строки Тогда
		ВерхОбласти = Область.Верх;
		НизОбласти = Область.Низ;
		Для НомСтр = ВерхОбласти По НизОбласти Цикл
			КодПоказателя = СокрП(Макет169.Область(НомСтр, 1).Текст);
			Если КодПоказателя <> "###" Тогда
				Представление = КодПоказателя + " - " + СокрП(Макет169.Область(НомСтр, 2).Текст);
				Элементы.КодВида169.СписокВыбора.Добавить(КодПоказателя, Представление); 
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
			
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура КодВида169ПриИзменении(Элемент)
	                     
	Если НЕ ПустаяСтрока(Запись.КодВида169) Тогда
		ЭлементСписка = Элементы.КодВида169.СписокВыбора.НайтиПоЗначению(Запись.КодВида169);
		Если НЕ ЭлементСписка = Неопределено Тогда
			Запись.НаименованиеВида169 = СтрЗаменить(ЭлементСписка.Представление, Запись.КодВида169 + " - ", "");
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
