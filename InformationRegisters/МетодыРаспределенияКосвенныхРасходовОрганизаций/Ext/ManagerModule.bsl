﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Процедура ЗаполнитьОрганизациюПриОднофирменномУчете() Экспорт
	
	// Когда используется одна организация, то поле Организация регистра не отображается пользователю.
	// В версиях ранее 3.0.34 в этом случае оно и не заполнялось, могли быть добавлены записи с незаполненной организацией.
	// Такие записи неверные, не имеют смысла. 
	// Однако, пользователь не мог увидеть, что они ошибочные, так как визуально они не отличаются от корректных.
	// 
	// Обработчик ЗаполнитьОрганизациюПриОднофирменномУчете() исправляет эту ошибку в данных: 
	// заполняет организацию при однофирменном учете.
	
	Если Справочники.Организации.ИспользуетсяНесколькоОрганизаций() Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	Настройки.Организация КАК Организация
	|ИЗ
	|	РегистрСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций КАК Настройки
	|ГДЕ
	|	Настройки.Организация = ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)";
	Если Запрос.Выполнить().Пустой() Тогда
		// Все данные корректны
		Возврат;
	КонецЕсли;
	
	Организация = Справочники.Организации.ОрганизацияПоУмолчанию();
	Если Не ЗначениеЗаполнено(Организация) Тогда
		// Исправить данные в регистре все равно не сможем
		Возврат;
	КонецЕсли;
	
	// Устраним коллизии: когда есть запись с пустой организацией и запись с организацией по умолчанию (остальные поля совпадают).
	// Записи с пустой организацией в этом случае не имеют никакого смысла, бесполезны.
	// Такие пары записей неотличимы для пользователя и поэтому создают проблему, вредны.
	// Для устранения проблемы удалим записи с пустой организацией, приводящие к коллизии.
	// Во всех остальных записях с пустой организацией заполним организацию.
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Неправильные.Период КАК Период,
	|	Неправильные.Организация КАК Организация,
	|	Неправильные.СчетЗатрат КАК СчетЗатрат,
	|	Неправильные.СтатьяЗатрат КАК СтатьяЗатрат,
	|	Неправильные.Подразделение КАК Подразделение
	|ИЗ
	|	РегистрСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций КАК Правильные
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций КАК Неправильные
	|		ПО Правильные.Период = Неправильные.Период
	|			И Правильные.СчетЗатрат = Неправильные.СчетЗатрат
	|			И Правильные.СтатьяЗатрат = Неправильные.СтатьяЗатрат
	|			И Правильные.Подразделение = Неправильные.Подразделение
	|ГДЕ
	|	Правильные.Организация = &Организация
	|	И Неправильные.Организация = ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)";
	
	Коллизии = Запрос.Выполнить().Выгрузить();
	
	// Подготовимся искать в таблице коллизий
	ИменаКолонок = Новый Массив;
	Для Каждого Колонка Из Коллизии.Колонки Цикл
		ИменаКолонок.Добавить(Колонка.Имя);
	КонецЦикла;
	ИменаКолонок = СтрСоединить(ИменаКолонок, ",");
	
	Коллизии.Индексы.Добавить(ИменаКолонок);
	Отбор = Новый Структура(ИменаКолонок);
	
	// Изменить нужно все или почти все записи, поэтому используем набор записей без отбора
	Записи = РегистрыСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций.СоздатьНаборЗаписей();
	Записи.Прочитать();
	
	// Так как можем удалять записи, то обходим коллекцию с конца
	КоличествоЗаписей = Записи.Количество();
	Для НомерСКонца = 1 По КоличествоЗаписей Цикл
		
		Индекс = КоличествоЗаписей - НомерСКонца;
		Запись = Записи[Индекс];
		
		Если ЗначениеЗаполнено(Запись.Организация) Тогда
			Продолжить;
		КонецЕсли;
		
		ЗаполнитьЗначенияСвойств(Отбор, Запись);
		Если Коллизии.НайтиСтроки(Отбор).Количество() = 0 Тогда
			Запись.Организация = Организация;
		Иначе // Коллизия
			Записи.Удалить(Запись);
		КонецЕсли;
		
	КонецЦикла;
	
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Записи);
		
КонецПроцедуры

Функция ОбновитьМетодыРаспределенияКосвенныхРасходовОрганизаций() Экспорт
	
	ПустоеПодразделение = БухгалтерскийУчетПереопределяемый.ПустоеПодразделение();
	//Удаляем записи с заполненным измерением Подразделение
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Подразделение", ПустоеПодразделение);
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.Период,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.Организация,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СчетЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СтатьяЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.Подразделение,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.БазаРаспределения,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СписокСтатейЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СчетПрямыхЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.ПодразделениеЗатрат
		|ИЗ
		|	РегистрСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций КАК МетодыРаспределенияКосвенныхРасходовОрганизаций
		|ГДЕ
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.Подразделение <> &Подразделение";

	РезультатЗапроса = Запрос.Выполнить();
	Выборка = РезультатЗапроса.Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Запись = РегистрыСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(Запись, Выборка);
		Запись.Прочитать();
		Запись.Удалить();
		
	КонецЦикла;
	
	//Очищаем в записях заполненные ресурсы ПодразделениеЗатрат
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Подразделение", ПустоеПодразделение);
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.Период,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.Организация,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СчетЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СтатьяЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.Подразделение,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.БазаРаспределения,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СписокСтатейЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.СчетПрямыхЗатрат,
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.ПодразделениеЗатрат
		|ИЗ
		|	РегистрСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций КАК МетодыРаспределенияКосвенныхРасходовОрганизаций
		|ГДЕ
		|	МетодыРаспределенияКосвенныхРасходовОрганизаций.ПодразделениеЗатрат <> &Подразделение";

	РезультатЗапроса = Запрос.Выполнить();
	Выборка = РезультатЗапроса.Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Запись = РегистрыСведений.МетодыРаспределенияКосвенныхРасходовОрганизаций.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(Запись, Выборка);
		Запись.Прочитать();
		Запись.ПодразделениеЗатрат = ПустоеПодразделение;
		Запись.Записать();
		
	КонецЦикла;
	
КонецФункции

#КонецОбласти

#КонецЕсли
