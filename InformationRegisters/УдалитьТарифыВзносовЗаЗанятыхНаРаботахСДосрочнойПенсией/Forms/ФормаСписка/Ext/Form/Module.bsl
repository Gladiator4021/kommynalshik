﻿&НаКлиенте
Перем ПараметрыОбработчикаОжидания Экспорт;

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередУдалением(Элемент, Отказ)
	
	Отказ = Истина;
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура Подключаемый_ОжиданиеВыполненияДлительнойОперации()
	
	Если ЗарплатаКадрыКлиент.ВосстановлениеНачальныхЗначенийВыполнено(ЭтаФорма) Тогда
		Элементы.Список.Обновить();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
