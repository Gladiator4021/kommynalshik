﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Справочники.Организации.ДополнитьДанныеЗаполненияПриОднофирменномУчете(ДанныеЗаполнения);
	
	Если ДанныеЗаполнения = Неопределено Или ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		Для каждого Запись Из ЭтотОбъект Цикл
			РегистрыСведений.НастройкиСистемыНалогообложения.УстановкаНастроекПоУмолчанию(Запись, ?(ДанныеЗаполнения = Неопределено, Новый Структура, ДанныеЗаполнения));
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	НастройкиУчета.ПередЗаписью(ЭтотОбъект, Отказ, Замещение);
	
	Для каждого Строка Из ЭтотОбъект Цикл
		
		РеквизитыОрганизации = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(
			Строка.Организация, "ЮридическоеФизическоеЛицо,ОбособленноеПодразделение");
			
		Если РеквизитыОрганизации.ОбособленноеПодразделение Тогда
			Продолжить; // Записи по ОП создаются автоматически, их не проверяем
		ИначеЕсли ДополнительныеСвойства.ГоловныеОрганизации.Найти(Строка.Организация) = Неопределено Тогда
			ДополнительныеСвойства.ГоловныеОрганизации.Добавить(Строка.Организация);
		КонецЕсли;
		
		ЭтоФизЛицо = РеквизитыОрганизации.ЮридическоеФизическоеЛицо
			= Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
			
		Если НЕ ЭтоФизЛицо И Строка.СистемаНалогообложения = Перечисления.СистемыНалогообложения.ОсобыйПорядок Тогда
			Строка.СистемаНалогообложения = Перечисления.СистемыНалогообложения.Общая;
		КонецЕсли;

		Строка.ПлательщикНалогаНаПрибыль = Строка.СистемаНалогообложения = Перечисления.СистемыНалогообложения.Общая 
			И НЕ ЭтоФизЛицо;

		Строка.ПрименяетсяУСН = Строка.СистемаНалогообложения = Перечисления.СистемыНалогообложения.Упрощенная;

		Строка.ПрименяетсяУСНДоходы = Строка.ПрименяетсяУСН
			И Строка.ОбъектНалогообложенияУСН = Перечисления.ОбъектыНалогообложенияПоУСН.Доходы;

		Строка.ПрименяетсяУСНДоходыМинусРасходы = Строка.ПрименяетсяУСН
			И Строка.ОбъектНалогообложенияУСН = Перечисления.ОбъектыНалогообложенияПоУСН.ДоходыМинусРасходы;

		Строка.ПлательщикНДС = Строка.СистемаНалогообложения = Перечисления.СистемыНалогообложения.Общая
			И Строка.ПлательщикНДС;
			
		Строка.ПлательщикНДФЛ	= ЭтоФизЛицо
			И Строка.СистемаНалогообложения = Перечисления.СистемыНалогообложения.Общая;
		
		Строка.ПрименяетсяУСНПатент	= ЭтоФизЛицо И Строка.ПрименяетсяУСНПатент;
		
		Строка.ПрименяетсяОсобыйПорядокНалогообложения	=
			(Строка.СистемаНалогообложения = Перечисления.СистемыНалогообложения.ОсобыйПорядок);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	НастройкиУчета.ПриЗаписи(ЭтотОбъект, Отказ, Замещение);
	СпециальныйНалоговыйРежимПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура СпециальныйНалоговыйРежимПриИзменении(ЭтотОбъект) Экспорт
	
	ТаблицаЗаписей = ЭтотОбъект.Выгрузить();
	Если ТаблицаЗаписей.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Запись = ТаблицаЗаписей[0];
	
	ПроверкаПорядкаОтраженияАванса(Запись); // для УСН
	ПроверкаВидовДеятельности(Запись);      // для ИП
	
КонецПроцедуры

Процедура ПроверкаПорядкаОтраженияАванса(Запись)
	
	Если НЕ Запись.ПрименяетсяУСН Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ Запись.ПлательщикЕНВД И НЕ Запись.ПрименяетсяУСНПатент Тогда
		Возврат;
	КонецЕсли;
	
	МенеджерЗаписи = РегистрыСведений.НастройкиУчетаУСН.СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Организация = Запись.Организация;
	МенеджерЗаписи.Период      = НачалоГода(Запись.Период);
	МенеджерЗаписи.Прочитать();
	
	МенеджерЗаписи.Организация = Запись.Организация;
	МенеджерЗаписи.Период      = НачалоГода(Запись.Период);
	
	Если (НЕ Запись.ПлательщикЕНВД И МенеджерЗаписи.ПорядокОтраженияАванса = ПредопределенноеЗначение("Перечисление.ПорядокОтраженияАвансов.ДоходЕНВД"))
		ИЛИ (НЕ Запись.ПрименяетсяУСНПатент И МенеджерЗаписи.ПорядокОтраженияАванса = ПредопределенноеЗначение("Перечисление.ПорядокОтраженияАвансов.ДоходПатент")) Тогда
		
		СписокОтраженияАванса = Новый СписокЗначений;
		СписокПатентов        = Новый СписокЗначений;
		НастройкиУчетаУСНФормыВызовСервера.СписокВыбораОтраженияАвансов(МенеджерЗаписи, СписокОтраженияАванса, СписокПатентов);
		
		Если СписокОтраженияАванса.Количество() = 1 Тогда
			МенеджерЗаписи.ПорядокОтраженияАванса = СписокОтраженияАванса[0].Значение;
		Иначе
			МенеджерЗаписи.ПорядокОтраженияАванса = ПредопределенноеЗначение("Перечисление.ПорядокОтраженияАвансов.ДоходУСН");
			МенеджерЗаписи.Патент                 = Неопределено;
		КонецЕсли;
		
		Если НЕ ЗначениеЗаполнено(МенеджерЗаписи.ПорядокОтраженияАванса) Тогда
			МенеджерЗаписи.ПорядокОтраженияАванса = ПредопределенноеЗначение("Перечисление.ПорядокОтраженияАвансов.ДоходУСН");
			МенеджерЗаписи.Патент                 = Неопределено;
		КонецЕсли;
		
	КонецЕсли;
	МенеджерЗаписи.Записать(Истина);
	
КонецПроцедуры

Процедура ПроверкаВидовДеятельности(Запись)
	
	Если НЕ Запись.ПлательщикНДФЛ Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ Запись.ПлательщикЕНВД И НЕ Запись.ПрименяетсяУСНПатент Тогда
		Возврат;
	КонецЕсли;
	
	МенеджерЗаписи = РегистрыСведений.НастройкиУчетаНДФЛ.СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Организация = Запись.Организация;
	МенеджерЗаписи.Период      = НачалоГода(Запись.Период);
	МенеджерЗаписи.Прочитать();
	
	МенеджерЗаписи.Организация = Запись.Организация;
	МенеджерЗаписи.Период      = НачалоГода(Запись.Период);
	
	ХарактерыДеятельности = РегистрыСведений.НастройкиУчетаНДФЛ.ПолучитьХарактерыВидовДеятельности(МенеджерЗаписи.ОсновнойВидДеятельности, МенеджерЗаписи.ВидДеятельностиДоходовПоАвансам);
	
	Если НЕ Запись.ПлательщикЕНВД Тогда
		
		Если ХарактерыДеятельности.ОсновнойХарактерДеятельности = ПредопределенноеЗначение("Перечисление.ХарактерДеятельности.РозничнаяТорговляЕНВД")
			ИЛИ ХарактерыДеятельности.ОсновнойХарактерДеятельности = ПредопределенноеЗначение("Перечисление.ХарактерДеятельности.УслугиЕНВД") Тогда
			
			МенеджерЗаписи.ОсновнойВидДеятельности = Неопределено;
			
		КонецЕсли;
	
		Если ХарактерыДеятельности.ХарактерДеятельностиДоходовПоАвансамИП = ПредопределенноеЗначение("Перечисление.ХарактерДеятельности.РозничнаяТорговляЕНВД")
			ИЛИ ХарактерыДеятельности.ХарактерДеятельностиДоходовПоАвансамИП = ПредопределенноеЗначение("Перечисление.ХарактерДеятельности.УслугиЕНВД") Тогда
			
			МенеджерЗаписи.ВидДеятельностиДоходовПоАвансам = МенеджерЗаписи.ОсновнойВидДеятельности;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если НЕ Запись.ПрименяетсяУСНПатент Тогда
	
		Если ХарактерыДеятельности.ОсновнойХарактерДеятельности = ПредопределенноеЗначение("Перечисление.ХарактерДеятельности.ВсяДеятельностьНаПатенте") Тогда
			
			МенеджерЗаписи.ОсновнойВидДеятельности = Неопределено;
			
		КонецЕсли;
	
		Если ХарактерыДеятельности.ХарактерДеятельностиДоходовПоАвансамИП = ПредопределенноеЗначение("Перечисление.ХарактерДеятельности.ВсяДеятельностьНаПатенте") Тогда
			
			МенеджерЗаписи.ВидДеятельностиДоходовПоАвансам = МенеджерЗаписи.ОсновнойВидДеятельности;
			
		КонецЕсли;
		
	КонецЕсли;
	
	МенеджерЗаписи.Записать(Истина);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли