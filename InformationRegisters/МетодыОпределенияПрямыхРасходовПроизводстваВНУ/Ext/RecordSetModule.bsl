﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

Процедура ЗаполнитьПоУмолчанию() Экспорт
	
	Счет20 = ПланыСчетов.Хозрасчетный.ОсновноеПроизводство;
	Счет23 = Планысчетов.Хозрасчетный.ВспомогательныеПроизводства;

	ВидыРасходов = Перечисления.ВидыРасходовНУ;
	ПланСчетов   = ПланыСчетов.Хозрасчетный;
	
	ДобавитьЗапись(ВидыРасходов.МатериальныеРасходы);
	ДобавитьЗапись(ВидыРасходов.Амортизация);
	ДобавитьЗапись(ВидыРасходов.ОплатаТруда);

	СтраховыеВзносы = Перечисления.ВидыРасходовНУ.СтраховыеВзносы;

	ДобавитьЗапись(СтраховыеВзносы, Счет20, ПланСчетов.ФСС);
	ДобавитьЗапись(СтраховыеВзносы, Счет23, ПланСчетов.ФСС);

	ДобавитьЗапись(СтраховыеВзносы, Счет20, ПланСчетов.ПФР_страх);
	ДобавитьЗапись(СтраховыеВзносы, Счет23, ПланСчетов.ПФР_страх);
	ДобавитьЗапись(СтраховыеВзносы, Счет20, ПланСчетов.ПФР_нак);
	ДобавитьЗапись(СтраховыеВзносы, Счет23, ПланСчетов.ПФР_нак);
	ДобавитьЗапись(СтраховыеВзносы, Счет20, ПланСчетов.ПФР_доп);
	ДобавитьЗапись(СтраховыеВзносы, Счет23, ПланСчетов.ПФР_доп);
	
	Для Каждого СчетВзносов Из РегистрыСведений.МетодыОпределенияПрямыхРасходовПроизводстваВНУ.СчетаВзносовС2014Года() Цикл
		ДобавитьЗапись(СтраховыеВзносы, Счет20, СчетВзносов);
		ДобавитьЗапись(СтраховыеВзносы, Счет23, СчетВзносов);
	КонецЦикла;
		
	ДобавитьЗапись(СтраховыеВзносы, Счет20, ПланСчетов.ФФОМС);
	ДобавитьЗапись(СтраховыеВзносы, Счет23, ПланСчетов.ФФОМС);
	ДобавитьЗапись(СтраховыеВзносы, Счет20, ПланСчетов.ТФОМС);
	ДобавитьЗапись(СтраховыеВзносы, Счет23, ПланСчетов.ТФОМС);
	ДобавитьЗапись(СтраховыеВзносы, Счет20, ПланСчетов.ОценочныеОбязательстваПостраховымВзносам);
	ДобавитьЗапись(СтраховыеВзносы, Счет23, ПланСчетов.ОценочныеОбязательстваПостраховымВзносам);
	
	ПрочиеРасходы = Перечисления.ВидыРасходовНУ.ПрочиеРасходы;
	
	ДобавитьЗапись(ПрочиеРасходы, Счет20, ПланСчетов.ФСС_НСиПЗ);
	ДобавитьЗапись(ПрочиеРасходы, Счет23, ПланСчетов.ФСС_НСиПЗ);
	ДобавитьЗапись(ПрочиеРасходы, Счет20, ПланСчетов.ОценочныеОбязательстваПостраховымВзносам);
	ДобавитьЗапись(ПрочиеРасходы, Счет23, ПланСчетов.ОценочныеОбязательстваПостраховымВзносам);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ 

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ЗапрещенныеВидыРасходовНУ = РегистрыСведений.МетодыОпределенияПрямыхРасходовПроизводстваВНУ.ЗапрещенныеВидыРасходовНУ();
	
	ОшибкиЗапрещенныеВидыРасходовНУ = Новый Массив;
	
	Для Каждого Запись Из ЭтотОбъект Цикл
		Если ЗапрещенныеВидыРасходовНУ.Найти(Запись.ВидРасходовНУ) <> Неопределено Тогда
			ОшибкиЗапрещенныеВидыРасходовНУ.Добавить(Запись);
		КонецЕсли;
	КонецЦикла;
	
	Если ОшибкиЗапрещенныеВидыРасходовНУ.Количество() > 0 Тогда
	
		ОписаниеКлючаЗаписи = Новый Структура; // Для навигации в сообщении об ошибке
		Для Каждого Измерение Из Метаданные.РегистрыСведений.МетодыОпределенияПрямыхРасходовПроизводстваВНУ.Измерения Цикл
			ОписаниеКлючаЗаписи.Вставить(Измерение.Имя);
		КонецЦикла;
		
		Для Каждого Ошибка Из ОшибкиЗапрещенныеВидыРасходовНУ Цикл
			
			ТекстСообщения = НСтр("ru = 'Расходы с видом ""%1"" не могут быть прямыми'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Ошибка.ВидРасходовНУ);
			
			ЗаполнитьЗначенияСвойств(ОписаниеКлючаЗаписи, Ошибка);
			КлючЗаписи = РегистрыСведений.МетодыОпределенияПрямыхРасходовПроизводстваВНУ.СоздатьКлючЗаписи(ОписаниеКлючаЗаписи);
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, КлючЗаписи, , , Отказ);
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОсновнаяОрганизация = БухгалтерскийУчетПереопределяемый.ПолучитьЗначениеПоУмолчанию("ОсновнаяОрганизация");
	
	Для Каждого Запись Из ЭтотОбъект Цикл
		
		Если НЕ ЗначениеЗаполнено(Запись.Организация) Тогда
			Запись.Организация = ОсновнаяОрганизация;
		КонецЕсли;
		
		Если НачалоГода(Запись.ПериодДействия) <> Запись.ПериодДействия Тогда
			Запись.ПериодДействия = НачалоГода(Запись.ПериодДействия);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Справочники.Организации.ДополнитьДанныеЗаполненияПриОднофирменномУчете(ДанныеЗаполнения);
	
	// Заполнять можем только, если установлен отбор по организации и периоду 
	// и не установлены другие отборы
	Организация = Неопределено;
	Если Не УстановленОтбор("Организация", Организация) Тогда
		Возврат;
	КонецЕсли;
	
	ПериодДействия = Неопределено;
	Если Не УстановленОтбор("ПериодДействия", ПериодДействия) Тогда
		Возврат;
	КонецЕсли;
	
	Измерения = Метаданные.РегистрыСведений.МетодыОпределенияПрямыхРасходовПроизводстваВНУ.Измерения;
	Для Каждого Измерение Из Измерения Цикл
		
		Если Измерение = Измерения.Организация Или Измерение = Измерения.ПериодДействия Тогда
			Продолжить;
		КонецЕсли;
		
		Если УстановленОтбор(Измерение.Имя) Тогда
			Возврат;
		КонецЕсли;
		
	КонецЦикла;
	
	Если ПериодДействия <> НачалоГода(ПериодДействия) Тогда
		Возврат;
	КонецЕсли;
	
	// Определим, какими данными будем заполнять
	ВариантПоУмолчанию      = "ПоУмолчанию";
	ВариантПредыдущийПериод = "ПредыдущийПериод";
	
	ИсточникДанных = "ПоУмолчанию";
	ДанныеПредыдущегоГода   = Неопределено;
	
	Если ДанныеЗаполнения = ВариантПоУмолчанию Или ДанныеЗаполнения = ВариантПредыдущийПериод Тогда
		ИсточникДанных = ДанныеЗаполнения;
	Иначе
		ИсточникДанных = ВариантПоУмолчанию;
		// Нет ли данных за предыдущий период?
		ДанныеПредыдущегоГода = ДанныеРегистраСведений(Организация, НачалоГода(ПериодДействия - 1));
		Если ДанныеПредыдущегоГода <> Неопределено Тогда
			ИсточникДанных = ВариантПредыдущийПериод;
		КонецЕсли;
	КонецЕсли;
	
	Если ИсточникДанных = ВариантПоУмолчанию Тогда
		ЗаполнитьПоУмолчанию();
	Иначе
		// Заполним данными предыдущего года
		Если ДанныеПредыдущегоГода = Неопределено Тогда
			ДанныеПредыдущегоГода = ДанныеРегистраСведений(Организация, НачалоГода(ПериодДействия - 1));
		КонецЕсли;
		Если ДанныеПредыдущегоГода = Неопределено Тогда
			Возврат;
		КонецЕсли;
		Загрузить(ДанныеПредыдущегоГода);
	КонецЕсли;
	
	Для Каждого Строка Из ЭтотОбъект Цикл
		Строка.Организация    = Организация;
		Строка.ПериодДействия = ПериодДействия;
	КонецЦикла;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Инициализация и заполнение

Функция ДанныеРегистраСведений(Организация, Период)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("Период",      НачалоГода(Период));
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Настройки.ВидРасходовНУ,
	|	Настройки.Подразделение,
	|	Настройки.Счет,
	|	Настройки.КорСчет,
	|	Настройки.СтатьяЗатрат
	|ИЗ
	|	РегистрСведений.МетодыОпределенияПрямыхРасходовПроизводстваВНУ КАК Настройки
	|ГДЕ
	|	Настройки.ПериодДействия = &Период
	|	И Настройки.Организация = &Организация";
	РезультатЗапроса = Запрос.Выполнить();
	Если РезультатЗапроса.Пустой() Тогда
		Возврат Неопределено;
	Иначе
		Возврат РезультатЗапроса.Выгрузить();
	КонецЕсли;

КонецФункции

Процедура ДобавитьЗапись(ВидРасходовНУ, СчетДебета = Неопределено,СчетКредита = Неопределено)

	НоваяЗапись = Добавить();
	НоваяЗапись.ВидРасходовНУ  = ВидРасходовНУ;
	НоваяЗапись.Счет           = СчетДебета;
	НоваяЗапись.КорСчет        = СчетКредита;

КонецПроцедуры

Функция УстановленОтбор(Знач Имя, Значение = Неопределено)
	
	ЭлементОтбора = Отбор.Найти(Имя);
	Если ЭлементОтбора = Неопределено Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если Не ЭлементОтбора.Использование Тогда
		Возврат Ложь;
	КонецЕсли;
		
	Если ЭлементОтбора.ВидСравнения <> ВидСравнения.Равно Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ЭлементОтбора.Значение) Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Значение = ЭлементОтбора.Значение;
	
	Возврат Истина;
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// Прочее

#КонецЕсли
