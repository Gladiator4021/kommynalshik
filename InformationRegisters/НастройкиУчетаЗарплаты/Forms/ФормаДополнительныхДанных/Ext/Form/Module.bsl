﻿&НаКлиенте
Перем ПараметрыОбработчикаОжидания Экспорт;

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ
//

&НаСервере
Процедура ЗаписатьПараметры(Отказ)
	
	Если ТолькоПросмотр Тогда
		Отказ = Истина;
		Возврат;		
	КонецЕсли;
	
	НеУдалосьЗаписать = Ложь;
	
	Если Вид = "Подразделения" Тогда
		
		Для Каждого СтрокаТаблицы ИЗ ОбособленныеПодразделения Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;
			
			// особенности записи регистра
			// заполнение сведений для подразделений
			// осуществляется каскадно при записи обособленного подразделения
			МенеджерЗаписи = РегистрыСведений.ТерриториальныеУсловияПФР.СоздатьМенеджерЗаписи();
			МенеджерЗаписи.Период      = Период;
			МенеджерЗаписи.СтруктурнаяЕдиница = СтрокаТаблицы.Ссылка;
			МенеджерЗаписи.Прочитать();
			ЗаполнитьЗначенияСвойств(МенеджерЗаписи, СтрокаТаблицы);
			МенеджерЗаписи.Период             = Период;
			МенеджерЗаписи.СтруктурнаяЕдиница = СтрокаТаблицы.Ссылка;
			МенеджерЗаписи.Записать();
			
			ПодразделениеОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				ПодразделениеОбъект.Заблокировать();
				ПодразделениеОбъект.РайонныйКоэффициент   = СтрокаТаблицы.РайонныйКоэффициент;
				ПодразделениеОбъект.РайонныйКоэффициентРФ = СтрокаТаблицы.РайонныйКоэффициентРФ;
				ПодразделениеОбъект.Записать();
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ПодразделениеОбъект + Символы.ПС + НСтр("ru='Запись районного коэффициента не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;	
		
	ИначеЕсли Вид = "Суда" Тогда
			
		Для Каждого СтрокаТаблицы ИЗ Суда Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ПодразделениеОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				Если НЕ ПодразделениеОбъект.СоответствуетСудамЗарегистрированнымВРоссийскомМеждународномРеестреСудов Тогда
					ПодразделениеОбъект.Заблокировать();
					ПодразделениеОбъект.СоответствуетСудамЗарегистрированнымВРоссийскомМеждународномРеестреСудов = Истина;
					ПодразделениеОбъект.Записать();
				КонецЕсли;
				
				ПараметрыОтбора = Новый Структура("Ссылка", СтрокаТаблицы.Ссылка);
				МассивПодразделений = СудаПрежняя.НайтиСтроки(ПараметрыОтбора);				
				Для Каждого СтрокаМассива ИЗ МассивПодразделений Цикл					
					СудаПрежняя.Удалить(СтрокаМассива);
				КонецЦикла;				
				
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ПодразделениеОбъект + Символы.ПС + НСтр("ru='Запись данных подразделения не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;	
		
		Для Каждого СтрокаТаблицы ИЗ СудаПрежняя Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ПодразделениеОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				ПодразделениеОбъект.Заблокировать();
				ПодразделениеОбъект.СоответствуетСудамЗарегистрированнымВРоссийскомМеждународномРеестреСудов = Ложь;
				ПодразделениеОбъект.Записать();
								
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ПодразделениеОбъект + Символы.ПС + НСтр("ru='Запись данных подразделения не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;	
		
	ИначеЕсли Вид = "Должности" Тогда
			
		Для Каждого СтрокаТаблицы ИЗ Фармацевты Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ДолжностьОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				Если НЕ ДолжностьОбъект.ЯвляетсяФармацевтическойДолжностью Тогда
					ДолжностьОбъект.Заблокировать();
					ДолжностьОбъект.ЯвляетсяФармацевтическойДолжностью = Истина;
					ДолжностьОбъект.Записать();
				КонецЕсли;
				
				ПараметрыОтбора = Новый Структура("Ссылка", СтрокаТаблицы.Ссылка);
				МассивДолжностей = ФармацевтыПрежняя.НайтиСтроки(ПараметрыОтбора);				
				Для Каждого СтрокаМассива ИЗ МассивДолжностей Цикл					
					ФармацевтыПрежняя.Удалить(СтрокаМассива);
				КонецЦикла;				
				
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ДолжностьОбъект + Символы.ПС + НСтр("ru='Запись данных о должности не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;	
		
		Для Каждого СтрокаТаблицы ИЗ ФармацевтыПрежняя Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ДолжностьОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				ДолжностьОбъект.Заблокировать();
				ДолжностьОбъект.ЯвляетсяФармацевтическойДолжностью = Ложь;
				ДолжностьОбъект.Записать();
								
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ДолжностьОбъект + Символы.ПС + НСтр("ru='Запись данных о должности не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;
		
		Для Каждого СтрокаТаблицы ИЗ Шахтеры Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ДолжностьОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				Если НЕ ДолжностьОбъект.ЯвляетсяШахтерскойДолжностью Тогда
					ДолжностьОбъект.Заблокировать();
					ДолжностьОбъект.ЯвляетсяШахтерскойДолжностью = Истина;
					ДолжностьОбъект.Записать();
				КонецЕсли;
				
				ПараметрыОтбора = Новый Структура("Ссылка", СтрокаТаблицы.Ссылка);
				МассивДолжностей = ШахтерыПрежняя.НайтиСтроки(ПараметрыОтбора);				
				Для Каждого СтрокаМассива ИЗ МассивДолжностей Цикл					
					ШахтерыПрежняя.Удалить(СтрокаМассива);
				КонецЦикла;				
				
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ДолжностьОбъект + Символы.ПС + НСтр("ru='Запись данных о должности не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;	
		
		Для Каждого СтрокаТаблицы ИЗ ШахтерыПрежняя Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ДолжностьОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				ДолжностьОбъект.Заблокировать();
				ДолжностьОбъект.ЯвляетсяШахтерскойДолжностью = Ложь;
				ДолжностьОбъект.Записать();
								
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ДолжностьОбъект + Символы.ПС + НСтр("ru='Запись данных о должности не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;
		
		Для Каждого СтрокаТаблицы ИЗ ЛетныеЭкипажи Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ДолжностьОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				Если НЕ ДолжностьОбъект.ЯвляетсяДолжностьюЛетногоЭкипажа Тогда
					ДолжностьОбъект.Заблокировать();
					ДолжностьОбъект.ЯвляетсяДолжностьюЛетногоЭкипажа = Истина;
					ДолжностьОбъект.Записать();
				КонецЕсли;
				
				ПараметрыОтбора = Новый Структура("Ссылка", СтрокаТаблицы.Ссылка);
				МассивДолжностей = ЛетныеЭкипажиПрежняя.НайтиСтроки(ПараметрыОтбора);				
				Для Каждого СтрокаМассива ИЗ МассивДолжностей Цикл					
					ЛетныеЭкипажиПрежняя.Удалить(СтрокаМассива);
				КонецЦикла;				
				
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ДолжностьОбъект + Символы.ПС + НСтр("ru='Запись данных о должности не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;	
		
		Для Каждого СтрокаТаблицы ИЗ ЛетныеЭкипажиПрежняя Цикл
			
			Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Ссылка) Тогда 
				Продолжить;
			КонецЕсли;			
			
			ДолжностьОбъект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
			
			Попытка
				
				ДолжностьОбъект.Заблокировать();
				ДолжностьОбъект.ЯвляетсяДолжностьюЛетногоЭкипажа = Ложь;
				ДолжностьОбъект.Записать();
								
			Исключение
				ТекстСообщения = НСтр("ru = 'Не удалось заблокировать объект: '") + ДолжностьОбъект + Символы.ПС + НСтр("ru='Запись данных о должности не выполнена.'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				НеУдалосьЗаписать = Истина;
				Продолжить;		
			КонецПопытки;	
			
		КонецЦикла;	
		
	Иначе
		Возврат;		
	КонецЕсли;
	
	Модифицированность =  НеУдалосьЗаписать;
		
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ПолучитьПодразделения(Форма, Организация, ПрименятьСевернуюНадбавку, Период)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПодразделенияОрганизаций.Ссылка КАК Ссылка,
	|	ПодразделенияОрганизаций.РайонныйКоэффициент КАК РайонныйКоэффициент,
	|	ПодразделенияОрганизаций.РайонныйКоэффициентРФ КАК РайонныйКоэффициентРФ,
	|	ТерриториальныеУсловияПФРСрезПоследних.ТерриториальныеУсловияПФР КАК ТерриториальныеУсловияПФР,
	|	&ПрименятьСевернуюНадбавку КАК ПрименятьСевернуюНадбавкуВПодразделениях
	|ИЗ
	|	Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ТерриториальныеУсловияПФР.СрезПоследних(&Период, СтруктурнаяЕдиница.Владелец = &Организация) КАК ТерриториальныеУсловияПФРСрезПоследних
	|		ПО ПодразделенияОрганизаций.Ссылка = ТерриториальныеУсловияПФРСрезПоследних.СтруктурнаяЕдиница
	|ГДЕ
	|	ПодразделенияОрганизаций.ОбособленноеПодразделение
	|	И ПодразделенияОрганизаций.Владелец = &Организация";
	
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("Период", Период);
	Запрос.УстановитьПараметр("ПрименятьСевернуюНадбавку", ПрименятьСевернуюНадбавку);
	РезультатЗапроса =  Запрос.Выполнить().Выгрузить();
	
	ЗначениеВДанныеФормы(РезультатЗапроса, Форма.ОбособленныеПодразделения);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ПолучитьСуда(Форма, Организация)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПодразделенияОрганизаций.Ссылка
	|ИЗ
	|	Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
	|ГДЕ
	|	ПодразделенияОрганизаций.СоответствуетСудамЗарегистрированнымВРоссийскомМеждународномРеестреСудов
	|	И ПодразделенияОрганизаций.Владелец = &Организация";
	
	Запрос.УстановитьПараметр("Организация", Организация);
	РезультатЗапроса =  Запрос.Выполнить().Выгрузить();
	
	ЗначениеВДанныеФормы(РезультатЗапроса, Форма.Суда);
	ЗначениеВДанныеФормы(РезультатЗапроса, Форма.СудаПрежняя);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ПолучитьДолжности(Форма, ПараметрыДолжностей)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Должности.Ссылка,
	|	Должности.ЯвляетсяДолжностьюЛетногоЭкипажа,
	|	Должности.ЯвляетсяШахтерскойДолжностью,
	|	Должности.ЯвляетсяФармацевтическойДолжностью
	|ИЗ
	|	Справочник.Должности КАК Должности
	|ГДЕ
	|	(Должности.ЯвляетсяДолжностьюЛетногоЭкипажа = &ЯвляетсяДолжностьюЛетногоЭкипажа
	|			ИЛИ Должности.ЯвляетсяШахтерскойДолжностью = &ЯвляетсяШахтерскойДолжностью
	|			ИЛИ Должности.ЯвляетсяФармацевтическойДолжностью = &ЯвляетсяФармацевтическойДолжностью)";
	
	Запрос.УстановитьПараметр("ЯвляетсяФармацевтическойДолжностью",ПараметрыДолжностей.ИспользуетсяТрудФармацевтов);
	Запрос.УстановитьПараметр("ЯвляетсяДолжностьюЛетногоЭкипажа",ПараметрыДолжностей.ИспользуетсяТрудЧленовЛетныхЭкипажей);
	Запрос.УстановитьПараметр("ЯвляетсяШахтерскойДолжностью",ПараметрыДолжностей.ИспользуетсяТрудШахтеров);
	
	Результат = Запрос.Выполнить().Выгрузить();

	Если ПараметрыДолжностей.ИспользуетсяТрудФармацевтов Тогда
		
		ПараметрыОтбора = Новый Структура("ЯвляетсяФармацевтическойДолжностью", Истина);
		МассивДолжностей = Результат.НайтиСтроки(ПараметрыОтбора);
		Если МассивДолжностей.Количество() <> 0 Тогда
			ТаблицаДолжностей = Результат.Скопировать(МассивДолжностей,"Ссылка");
		Иначе
			ТаблицаДолжностей = Результат.СкопироватьКолонки("Ссылка");
		КонецЕсли;
		
		ЗначениеВДанныеФормы(ТаблицаДолжностей, Форма.Фармацевты);
		ЗначениеВДанныеФормы(ТаблицаДолжностей, Форма.ФармацевтыПрежняя);
		
	КонецЕсли;
	
	Если ПараметрыДолжностей.ИспользуетсяТрудЧленовЛетныхЭкипажей Тогда
		
		ПараметрыОтбора = Новый Структура("ЯвляетсяДолжностьюЛетногоЭкипажа", Истина);
		МассивДолжностей = Результат.НайтиСтроки(ПараметрыОтбора);
		Если МассивДолжностей.Количество() <> 0 Тогда
			ТаблицаДолжностей = Результат.Скопировать(МассивДолжностей,"Ссылка");
		Иначе
			ТаблицаДолжностей = Результат.СкопироватьКолонки("Ссылка");
		КонецЕсли;
		
		ЗначениеВДанныеФормы(ТаблицаДолжностей, Форма.ЛетныеЭкипажи);
		ЗначениеВДанныеФормы(ТаблицаДолжностей, Форма.ЛетныеЭкипажиПрежняя);
		
	КонецЕсли;
	
	Если ПараметрыДолжностей.ИспользуетсяТрудШахтеров Тогда
		
		ПараметрыОтбора = Новый Структура("ЯвляетсяШахтерскойДолжностью", Истина);
		МассивДолжностей = Результат.НайтиСтроки(ПараметрыОтбора);
		Если МассивДолжностей.Количество() <> 0 Тогда
			ТаблицаДолжностей = Результат.Скопировать(МассивДолжностей,"Ссылка");
		Иначе
			ТаблицаДолжностей = Результат.СкопироватьКолонки("Ссылка");
		КонецЕсли;
		
		ЗначениеВДанныеФормы(ТаблицаДолжностей, Форма.Шахтеры);
		ЗначениеВДанныеФормы(ТаблицаДолжностей, Форма.ШахтерыПрежняя);
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ДЕЙСТВИЯ КОМАНДНЫХ ПАНЕЛЕЙ ФОРМЫ
//

&НаКлиенте
Процедура Записать(Команда)
	
	Отказ = Ложь;
	ЗаписатьПараметры(Отказ);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	Отказ = Ложь;
	ЗаписатьПараметры(Отказ);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура ВосстановитьСтандартныеНастройки(Команда)
	
	ЗарплатаКадрыКлиент.ВосстановитьНачальныеЗначения(ЭтаФорма);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ДЕЙСТВИЯ ТАБЛИЧНЫХ ЧАСТЕЙ ФОРМЫ
//

&НаКлиенте
Процедура ОбособленныеПодразделенияПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбособленныеПодразделенияПередУдалением(Элемент, Отказ)
	
	Отказ = Истина;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
//

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если НЕ Параметры.Свойство("Вид") Тогда
		Отказ = Истина;
		Возврат;
	Иначе
		Вид = Параметры.Вид;
		
		Если Вид = "НДФЛ"
			ИЛИ Вид = "СтраховыеВзносы"
			ИЛИ Вид = "СтандартныеНастройки" Тогда
			ПоложениеКоманднойПанели = ПоложениеКоманднойПанелиФормы.Нет
		КонецЕсли;
	
		Элементы.ГруппаНДФЛ.Видимость                 = Вид = "НДФЛ";
		Элементы.ГруппаСтраховыеВзносы.Видимость      = Вид = "СтраховыеВзносы";
		Элементы.ГруппаПодразделения.Видимость        = Вид = "Подразделения";
		Элементы.ГруппаСуда.Видимость                 = Вид = "Суда";
		Элементы.ГруппаДолжности.Видимость            = Вид = "Должности";
		Элементы.ГруппаСтандартныеНастройки.Видимость = Вид = "СтандартныеНастройки";
		
		Если Вид = "НДФЛ" Тогда
			ЭтаФорма.Заголовок = НСтр("ru = 'Параметры расчета НДФЛ'");
		ИначеЕсли Вид = "СтраховыеВзносы" Тогда
			ЭтаФорма.Заголовок = НСтр("ru = 'Параметры расчета взносов'");
		ИначеЕсли Вид = "Подразделения" Тогда
			Организация                  = Параметры.Организация;
			Период                       = Параметры.Период;
			ПрименятьРайонныйКоэффициент = Параметры.ПрименятьРайонныйКоэффициент;
			ПрименятьСевернуюНадбавку    = Параметры.ПрименятьСевернуюНадбавку;
			ТекстПериод = НСтр("ru = 'Сведения о территориальных условиях указаны на дату: %1 г.'");
			ТекстПериод = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстПериод, Формат(Период, "ДФ=dd.MM.yyyy"));
			Элементы.ДекорацияПериод.Заголовок = ТекстПериод;
			Если ПрименятьРайонныйКоэффициент Тогда
				ЭтаФорма.Заголовок = НСтр("ru = 'Районные коэффициенты и территориальные условия обособленных подразделений'");
			Иначе
				ЭтаФорма.Заголовок = НСтр("ru = 'Территориальные условия обособленных подразделений'");
			КонецЕсли;
			Элементы.ОбособленныеПодразделенияГруппа.Видимость = ПрименятьРайонныйКоэффициент;
			ПолучитьПодразделения(ЭтаФорма, Организация, ПрименятьСевернуюНадбавку, Период);
		ИначеЕсли Вид = "Суда" Тогда			
			Организация =  Параметры.Организация;
			ЭтаФорма.Заголовок = НСтр("ru = 'Суда, зарегистрированные в Российском международном реестре судов'");
			ПолучитьСуда(ЭтаФорма, Организация);
		ИначеЕсли Вид = "Должности" Тогда			
			ЭтаФорма.Заголовок = НСтр("ru = 'Льготные категории должностей'");
			ПараметрыДолжностей = Параметры.ПараметрыДолжностей;
			Элементы.ГруппаФармацевты.Видимость    = ПараметрыДолжностей.ИспользуетсяТрудФармацевтов;
			Элементы.ГруппаЛетныеЭкипажи.Видимость = ПараметрыДолжностей.ИспользуетсяТрудЧленовЛетныхЭкипажей;
			Элементы.ГруппаШахтеры.Видимость       = ПараметрыДолжностей.ИспользуетсяТрудШахтеров;
			Если ПараметрыДолжностей.ИспользуетсяТрудФармацевтов
				+ ПараметрыДолжностей.ИспользуетсяТрудЧленовЛетныхЭкипажей
				+ ПараметрыДолжностей.ИспользуетсяТрудШахтеров = 1 Тогда
				Элементы.ГруппаПараметрыДолжности.ОтображениеСтраниц = ОтображениеСтраницФормы.Нет;
			КонецЕсли;
			ПолучитьДолжности(ЭтаФорма, ПараметрыДолжностей);
		ИначеЕсли Вид = "СтандартныеНастройки" Тогда
			ЭтаФорма.Заголовок = НСтр("ru = 'Восстановление стандартных настроек'");
		Иначе
			Отказ = Истина;
			Возврат;
		КонецЕсли
	КонецЕсли

КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если ЗавершениеРаботы И Модифицированность Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	Если Модифицированность Тогда
		
		ТекстВопроса = НСтр("ru = 'Данные были изменены. Сохранить изменения?'");
		Оповещение = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
		ПоказатьВопрос(Оповещение, ТекстВопроса, РежимДиалогаВопрос.ДаНетОтмена, 60, КодВозвратаДиалога.Нет); 
		Отказ = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		Отказ = Ложь;
		ЗаписатьПараметры(Отказ);
		Если Отказ = Истина Тогда
			Возврат;
		КонецЕсли;
		Закрыть();
	ИначеЕсли Результат = КодВозвратаДиалога.Нет Тогда
		Модифицированность = Ложь;
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура Подключаемый_ОжиданиеВыполненияДлительнойОперации()
	
	Если ЗарплатаКадрыКлиент.ВосстановлениеНачальныхЗначенийВыполнено(ЭтаФорма) Тогда
		ТекстСообщения = НСтр("ru = 'Стандартные настройки восстановлены!'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		ЭтаФорма.Закрыть();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
