﻿#Область ОписаниеПеременных

&НаКлиенте
Перем СтруктураСохраняемыхРеквизитов;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	Если Параметры.Ключ.Пустой() Тогда
		
		Запись.Период = НачалоГода(ОбщегоНазначения.ТекущаяДатаПользователя());
		Если Запись.Организация.Пустая() Тогда
			Запись.Организация = БухгалтерскийУчетПереопределяемый.ПолучитьЗначениеПоУмолчанию("ОсновнаяОрганизация");
		КонецЕсли;
		
		ПодготовитьФормуНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ТипЗнч(ВыбранноеЗначение) = Тип("ПеречислениеСсылка.ВидыИмущества") Тогда
		
		Запись.ВидИмущества = ВыбранноеЗначение;
		
		Модифицированность = Истина;
		
		УправлениеФормой(ЭтаФорма);		
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ИзмененОбъектОС" Тогда
		
		Если Параметр.Ссылка = Запись.ОсновноеСредство Тогда
			ЗаполнитьДанныеПоОсновномуСредству();
		КонецЕсли;
		
	КонецЕсли;
		
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПодготовитьФормуНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)

	Если Запись.НеоблагаемаяКадастроваяСтоимость > Запись.КадастроваяСтоимость Тогда
		ТекстСообщения = НСтр("ru = 'Необлагаемая кадастровая стоимость не может превышать кадастровую стоимость'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Запись.НеоблагаемаяКадастроваяСтоимость", "", Отказ);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Запись.ДатаПрекращенияПраваСобственности)
		И Запись.ДатаПрекращенияПраваСобственности < Запись.ДатаРегистрацииПраваСобственности Тогда
		
		ТекстСообщения = НСтр("ru = 'Дата прекращения права собственности не может быть меньше даты регистрации права собственности'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Запись.ДатаПрекращенияПраваСобственности", "", Отказ);
		
	КонецЕсли;
	
	Если Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.СДругимКодомПоОКАТО Тогда
		Если ПустаяСтрока(КодПоОКТМО_ДругойКод) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Код по ОКТМО'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "КодПоОКТМО_ДругойКод", "", Отказ);
		КонецЕсли;
		Если ПустаяСтрока(КодПоОКАТО_ДругойКод) И Год(Запись.Период) < 2014 Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Код по ОКАТО'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "КодПоОКАТО_ДругойКод", "", Отказ);
		КонецЕсли;
	КонецЕсли;

	Если Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.ВДругомНалоговомОргане Тогда
		Если Запись.НалоговыйОрган.Пустая() Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Налоговый орган'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "НалоговыйОрган", "Запись", Отказ);
		КонецЕсли;
		Если ПустаяСтрока(КодПоОКТМО_ДругойНалоговыйОрган) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Код по ОКТМО'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "КодПоОКТМО_ДругойНалоговыйОрган", "", Отказ);
		КонецЕсли;
		Если ПустаяСтрока(КодПоОКАТО_ДругойНалоговыйОрган) И Год(Запись.Период) < 2014 Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Код по ОКАТО'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "КодПоОКАТО_ДругойНалоговыйОрган", "", Отказ);
		КонецЕсли;
		Если Запись.НалоговаяСтавка = 0 Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Налоговая ставка в субъекте Российской Федерации'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "НалоговаяСтавка", "Запись", Отказ);
		КонецЕсли;
	КонецЕсли;

	Если Запись.НалоговаяБаза = Перечисления.НалоговаяБазаПоНалогуНаИмущество.КадастроваяСтоимость Тогда
		Если Запись.КадастроваяСтоимость = 0 Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Кадастровая стоимость'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "КадастроваяСтоимость", "Запись", Отказ);
		КонецЕсли;
		Если Запись.НалоговаяСтавка = 0 Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Налоговая ставка в субъекте Российской Федерации'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "НалоговаяСтавка", "Запись", Отказ);
		КонецЕсли;
		Если ПустаяСтрока(Запись.КадастровыйНомер) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Кадастровый номер здания (строения, сооружения)'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "КадастровыйНомер", "Запись", Отказ);
		КонецЕсли;
		Если НЕ ЗначениеЗаполнено(Запись.ДатаРегистрацииПраваСобственности) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Дата регистрации права собственности'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "ДатаРегистрацииПраваСобственности", "Запись", Отказ);
		КонецЕсли;
	КонецЕсли;

	Если Запись.ПорядокНалогообложения = Перечисления.ПорядокНалогообложенияИмущества.ОсвобождаетсяОтНалогообложения Тогда
		Если ПустаяСтрока(Запись.КодНалоговойЛьготыОсвобождениеОтНалогообложения) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Код налоговой льготы'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "КодНалоговойЛьготыОсвобождениеОтНалогообложения", "Запись", Отказ);
		КонецЕсли;
	КонецЕсли;

	Если Запись.ПорядокНалогообложения = Перечисления.ПорядокНалогообложенияИмущества.ОблагаетсяПоСниженнойСтавке Тогда
		Если Запись.СниженнаяНалоговаяСтавка = 0 Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Сниженная ставка'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "СниженнаяНалоговаяСтавка", "Запись", Отказ);
		КонецЕсли;
	КонецЕсли;

	Если Запись.ПорядокНалогообложения = Перечисления.ПорядокНалогообложенияИмущества.УменьшениеНалоговойБазы Тогда
		Если Запись.СуммаУменьшенияНалоговойБазы = 0 Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,, НСтр("ru = 'Сумма уменьшения налоговой базы'"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "СуммаУменьшенияНалоговойБазы", "Запись", Отказ);
		КонецЕсли;
	КонецЕсли;
	
	Если ДоляУказана = 1 И Запись.ДоляСтоимостиЧислитель = 0 Тогда
		ТекстСообщения = НСтр("ru = 'Не указан числитель дроби - доли стоимости'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "ДоляСтоимостиЧислитель", "Запись", Отказ);
	КонецЕсли;
	Если ДоляУказана = 1 И Запись.ДоляСтоимостиЗнаменатель = 0 Тогда
		ТекстСообщения = НСтр("ru = 'Не указан знаменатель дроби - доли стоимости'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "ДоляСтоимостиЗнаменатель", "Запись", Отказ);
	КонецЕсли;
	Если Запись.ДоляСтоимостиЧислитель <> 0
	   И Запись.ДоляСтоимостиЗнаменатель <> 0
	   И Запись.ДоляСтоимостиЧислитель > Запись.ДоляСтоимостиЗнаменатель Тогда
		ТекстСообщения = НСтр("ru = 'Доля стоимости: числитель дроби больше знаменателя'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "ДоляСтоимостиЧислитель", "Запись", Отказ);
	КонецЕсли;

	ТекущийКодПоОКТМО = ПолучитьКодПоОКТМО();

	Если НЕ Отказ И ((НЕ ТекущийОбъект.Выбран()) ИЛИ (ТекущийКодПоОКТМО <> КодПоОКТМО_Исходный)) Тогда

		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("Период",           Запись.Период);
		Запрос.УстановитьПараметр("Организация",      Запись.Организация);
		Запрос.УстановитьПараметр("ОсновноеСредство", Запись.ОсновноеСредство);
		Запрос.УстановитьПараметр("КодПоОКТМО",       ТекущийКодПоОКТМО);

		Запрос.Текст = "
			|ВЫБРАТЬ
			|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.ОсновноеСредство КАК ОсновноеСредство
			|ИЗ
			|	РегистрСведений.СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам КАК СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам
			|ГДЕ
			|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.Период = &Период
			|	И СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.Организация = &Организация
			|	И СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.ОсновноеСредство = &ОсновноеСредство
			|	И СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.КодПоОКТМО = &КодПоОКТМО
			|";
		Выборка = Запрос.Выполнить().Выбрать();
		Если Выборка.Количество() > 0 Тогда
			Выборка.Следующий();
			ТекстСообщения = НСтр("ru = 'На %1 по основному средству %2 уже есть запись с кодом ОКТМО %3 в организации %4'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения,
				Формат(Запись.Период, "ДФ=dd.MM.yyyy"),
				СокрЛП(Запись.ОсновноеСредство),
				СокрЛП(ТекущийКодПоОКТМО),
				СокрЛП(Запись.Организация));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Организация", "Запись", Отказ);
		КонецЕсли;
	КонецЕсли;
	
	Если Год(ТекущийОбъект.Период) < 2014 Тогда
	
		ТекущийКодПоОКАТО = ПолучитьКодПоОКАТО();

		Если НЕ Отказ И ((НЕ ТекущийОбъект.Выбран()) ИЛИ (ТекущийКодПоОКАТО <> КодПоОКАТО_Исходный)) Тогда

			Запрос = Новый Запрос;
			Запрос.УстановитьПараметр("Период",           Запись.Период);
			Запрос.УстановитьПараметр("Организация",      Запись.Организация);
			Запрос.УстановитьПараметр("ОсновноеСредство", Запись.ОсновноеСредство);
			Запрос.УстановитьПараметр("КодПоОКАТО",       ТекущийКодПоОКАТО);

			Запрос.Текст = "
				|ВЫБРАТЬ
				|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.ОсновноеСредство КАК ОсновноеСредство
				|ИЗ
				|	РегистрСведений.СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам КАК СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам
				|ГДЕ
				|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.Период = &Период
				|	И СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.Организация = &Организация
				|	И СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.ОсновноеСредство = &ОсновноеСредство
				|	И СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.КодПоОКАТО = &КодПоОКАТО
				|";
			Выборка = Запрос.Выполнить().Выбрать();
			Если Выборка.Количество() > 0 Тогда
				Выборка.Следующий();
				ТекстСообщения = НСтр("ru = 'На %1 по основному средству %2 уже есть запись с кодом ОКАТО %3 в организации %4'");
				ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения,
					Формат(Запись.Период, "ДФ=dd.MM.yyyy"),
					СокрЛП(Запись.ОсновноеСредство),
					СокрЛП(ТекущийКодПоОКАТО),
					СокрЛП(Запись.Организация));
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Организация", "Запись", Отказ);
			КонецЕсли;
		КонецЕсли;

	КонецЕсли;
	
	Если НЕ Отказ Тогда
		
		ТекущийОбъект.КодПоОКТМО = ПолучитьКодПоОКТМО();
		
		Если Год(ТекущийОбъект.Период) < 2014 Тогда
			ТекущийОбъект.КодПоОКАТО = ПолучитьКодПоОКАТО();
		КонецЕсли;
		
	КонецЕсли;
	
	Если Год(ТекущийОбъект.Период) >= 2014 Тогда
		КодПоОКАТО = "";
	КонецЕсли;
	
	// Данные информационной базы, влияющие на список КБК
	Если ТекущийОбъект.ВидИмущества = Перечисления.ВидыИмущества.ВходитВСоставЕСГС
	   И Не Перечисления.УсловияПримененияТребованийЗаконодательства.ЕстьИмуществоЕСГС() Тогда
		ПараметрыЗаписи.Вставить("ПоявилосьИмуществоЕСГС", Истина);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	Если ТекущийОбъект.ВидИмущества = Перечисления.ВидыИмущества.ВходитВСоставЕСГС
	   И ПараметрыЗаписи.Свойство("ПоявилосьИмуществоЕСГС") Тогда
		// в связи с записанными данными необходимо добавить КБК
		Справочники.ВидыНалоговИПлатежейВБюджет.СоздатьПоставляемыеЭлементы();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПериодПриИзменении(Элемент)
	
	Если Запись.Период < '20140101' Тогда
		Запись.НалоговаяБаза = ПредопределенноеЗначение("Перечисление.НалоговаяБазаПоНалогуНаИмущество.СреднегодоваяСтоимость");
	КонецЕсли;
	
	УстановитьРеквизитСовмещениеОСНОиЕНВД(ЭтаФорма);
	УправлениеФормой(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	УстановитьРеквизитСовмещениеОСНОиЕНВД(ЭтаФорма);
	УстановитьГоловнуюОрганизацию(ЭтаФорма);
	УправлениеФормой(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеСредствоПриИзменении(Элемент)

	ЗаполнитьДанныеПоОсновномуСредству();

КонецПроцедуры

&НаКлиенте
Процедура НеПодлежитНалогообложениюПриИзменении(Элемент)
	
	Если НеПодлежитНалогообложению Тогда
		
		СохранитьЗначениеРеквизитаФормы("ПорядокНалогообложения", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("КодНалоговойЛьготыОсвобождениеОтНалогообложения", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("СниженнаяНалоговаяСтавка", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		
		Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.НеПодлежитНалогообложению");
		
	Иначе
		
		ВосстановитьЗначениеРеквизитаФормы("ПорядокНалогообложения", Запись, СтруктураСохраняемыхРеквизитов);
		
		Если Не ЗначениеЗаполнено(Запись.ПорядокНалогообложения) 
			ИЛИ Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.НеПодлежитНалогообложению") Тогда
			Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОсобыеЛьготыНеУстановлены");
		КонецЕсли;
		
		Если Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОсвобождаетсяОтНалогообложения") Тогда
			ВосстановитьЗначениеРеквизитаФормы("КодНалоговойЛьготыОсвобождениеОтНалогообложения", Запись, СтруктураСохраняемыхРеквизитов);
		КонецЕсли;

		Если Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОблагаетсяПоСниженнойСтавке") Тогда
			ВосстановитьЗначениеРеквизитаФормы("СниженнаяНалоговаяСтавка", Запись, СтруктураСохраняемыхРеквизитов);
		КонецЕсли;
		
	КонецЕсли;
	
	УправлениеФормой(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовСтраницыОбъектНедвижимогоИмущества

&НаКлиенте
Процедура НалоговаяБазаПриИзменении(Элемент)
	
	Если Запись.НалоговаяБаза = ПредопределенноеЗначение("Перечисление.НалоговаяБазаПоНалогуНаИмущество.КадастроваяСтоимость") Тогда
		ВосстановитьЗначениеРеквизитаФормы("НалоговаяСтавка", Запись, СтруктураСохраняемыхРеквизитов);
	Иначе
		СохранитьЗначениеРеквизитаФормы("НалоговаяСтавка", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
	КонецЕсли;
	
	УправлениеФормой(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура НеоблагаемаяКадастроваяСтоимостьПриИзменении(Элемент)
	
	Если Запись.НеоблагаемаяКадастроваяСтоимость > Запись.КадастроваяСтоимость Тогда
		
		ТекстСообщения = НСтр("ru = 'Необлагаемая кадастровая стоимость не может превышать кадастровую стоимость'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Запись.НеоблагаемаяКадастроваяСтоимость");
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаРегистрацииПраваСобственностиПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Запись.ДатаПрекращенияПраваСобственности)
		И Запись.ДатаРегистрацииПраваСобственности > Запись.ДатаПрекращенияПраваСобственности Тогда
		
		ТекстСообщения = НСтр("ru = 'Дата регистрации права собственности не может быть больше даты прекращения права собственности'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Запись.ДатаРегистрацииПраваСобственности");
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаПрекращенияПраваСобственностиПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Запись.ДатаПрекращенияПраваСобственности)
		И Запись.ДатаПрекращенияПраваСобственности < Запись.ДатаРегистрацииПраваСобственности Тогда
		
		ТекстСообщения = НСтр("ru = 'Дата прекращения права собственности не может быть меньше даты регистрации права собственности'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Запись.ДатаПрекращенияПраваСобственности");
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДоляУказанаПриИзменении(Элемент)
	
	Если ДоляУказана = 1 Тогда
		ВосстановитьЗначениеРеквизитаФормы("ДоляСтоимостиЧислитель",   Запись, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("ДоляСтоимостиЗнаменатель", Запись, СтруктураСохраняемыхРеквизитов);
	Иначе
		СохранитьЗначениеРеквизитаФормы("ДоляСтоимостиЧислитель",   ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("ДоляСтоимостиЗнаменатель", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
	КонецЕсли;
	
	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ДоляСтоимостиЧислительПриИзменении(Элемент)

	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ДоляСтоимостиЗнаменательПриИзменении(Элемент)

	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовСтраницыНалоговыйОрган

&НаКлиенте
Процедура ПостановкаНаУчетВНалоговомОрганеПриИзменении(Элемент)

	Если Запись.ПостановкаНаУчетВНалоговомОргане = ПредопределенноеЗначение("Перечисление.ПостановкаНаУчетВНалоговомОргане.ВДругомНалоговомОргане") Тогда
		СохранитьЗначениеРеквизитаФормы("КодПоОКАТО_ДругойКод", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("КодПоОКТМО_ДругойКод", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("НалоговыйОрган", Запись, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("КодПоОКАТО_ДругойНалоговыйОрган", ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("КодПоОКТМО_ДругойНалоговыйОрган", ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("НалоговаяСтавка", Запись, СтруктураСохраняемыхРеквизитов);
	ИначеЕсли Запись.ПостановкаНаУчетВНалоговомОргане = ПредопределенноеЗначение("Перечисление.ПостановкаНаУчетВНалоговомОргане.СДругимКодомПоОКАТО") Тогда
		ВосстановитьЗначениеРеквизитаФормы("КодПоОКАТО_ДругойКод", ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("КодПоОКТМО_ДругойКод", ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("НалоговыйОрган", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("КодПоОКАТО_ДругойНалоговыйОрган", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("КодПоОКТМО_ДругойНалоговыйОрган", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("НалоговаяСтавка", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
	Иначе
		СохранитьЗначениеРеквизитаФормы("КодПоОКАТО_ДругойКод", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("КодПоОКТМО_ДругойКод", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("НалоговыйОрган", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("КодПоОКАТО_ДругойНалоговыйОрган", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("КодПоОКТМО_ДругойНалоговыйОрган", ЭтаФорма, ЭтаФорма, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("НалоговаяСтавка", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
	КонецЕсли;

	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура НалоговыйОрганПриИзменении(Элемент)

	НалоговыйОрганПриИзмененииНаСервере();

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовСтраницыНалогНаИмущество

&НаКлиенте
Процедура НалоговаяСтавкаПриИзменении(Элемент)

	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ПорядокНалогообложенияПриИзменении(Элемент)

	Если Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОсобыеЛьготыНеУстановлены") Тогда
		СохранитьЗначениеРеквизитаФормы("КодНалоговойЛьготыОсвобождениеОтНалогообложения", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		СохранитьЗначениеРеквизитаФормы("СниженнаяНалоговаяСтавка", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
	ИначеЕсли Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОсвобождаетсяОтНалогообложения") Тогда
		СохранитьЗначениеРеквизитаФормы("СниженнаяНалоговаяСтавка", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("КодНалоговойЛьготыОсвобождениеОтНалогообложения", Запись, СтруктураСохраняемыхРеквизитов);
	ИначеЕсли Запись.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОблагаетсяПоСниженнойСтавке") Тогда
		СохранитьЗначениеРеквизитаФормы("КодНалоговойЛьготыОсвобождениеОтНалогообложения", ЭтаФорма, Запись, СтруктураСохраняемыхРеквизитов);
		ВосстановитьЗначениеРеквизитаФормы("СниженнаяНалоговаяСтавка", Запись, СтруктураСохраняемыхРеквизитов);
	КонецЕсли;

	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура КодНалоговойЛьготыОсвобождениеОтНалогообложенияПриИзменении(Элемент)

	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура КодНалоговойЛьготыОсвобождениеОтНалогообложенияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)

	СтандартнаяОбработка = Ложь;
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ТипОбъекта",		"РегистрСведений");
	ПараметрыФормы.Вставить("НазваниеОбъекта",	"СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам");
	ПараметрыФормы.Вставить("НазваниеМакета",	"ЛьготыПоНалогуНаИмущество");
	ПараметрыФормы.Вставить("ТекущийПериод",	Запись.Период);

	ОповещениеОЗакрытии = Новый ОписаниеОповещения("КодНалоговойЛьготыОсвобождениеОтНалогообложенияНачалоВыбораЗавершение", ЭтотОбъект);
	
	ОткрытьФорму("ОбщаяФорма.ФормаВыбораКода", ПараметрыФормы,,,,,ОповещениеОЗакрытии);

КонецПроцедуры

&НаКлиенте
Процедура КодНалоговойЛьготыОсвобождениеОтНалогообложенияНачалоВыбораЗавершение(РезультатЗакрытия, ДополнительныеПараметры) Экспорт
	
	КодЛьготы = РезультатЗакрытия;
	
	Если КодЛьготы <> Неопределено Тогда
		Запись.КодНалоговойЛьготыОсвобождениеОтНалогообложения = КодЛьготы;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура СниженнаяНалоговаяСтавкаПриИзменении(Элемент)

	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПодготовитьФормуНаСервере();
	
	Если Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.СДругимКодомПоОКАТО Тогда
		КодПоОКТМО_ДругойКод = Запись.КодПоОКТМО;
		КодПоОКАТО_ДругойКод = Запись.КодПоОКАТО;
	ИначеЕсли Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.ВДругомНалоговомОргане Тогда
		КодПоОКТМО_ДругойНалоговыйОрган = Запись.КодПоОКТМО;
		КодПоОКАТО_ДругойНалоговыйОрган = Запись.КодПоОКАТО;
	КонецЕсли;
	КодПоОКТМО_Исходный = Запись.КодПоОКТМО;
	КодПоОКАТО_Исходный = Запись.КодПоОКАТО;
	
	ДоляУказана = ?(Запись.ДоляСтоимостиЧислитель > 0 И Запись.ДоляСтоимостиЗнаменатель > 0, 1, 0);

	НеПодлежитНалогообложению = Запись.ПорядокНалогообложения = Перечисления.ПорядокНалогообложенияИмущества.НеПодлежитНалогообложению;
	
	УстановитьРеквизитОбъектНедвижимогоИмущества();
	УстановитьГоловнуюОрганизацию(ЭтаФорма);
	УстановитьРеквизитСовмещениеОСНОиЕНВД(ЭтаФорма);
	
	// Установить видимость/доступность реквизитов и заголовков колонок.
	УправлениеФормой(ЭтаФорма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УправлениеФормой(Форма)

	Объект   = Форма.Запись;
	Элементы = Форма.Элементы;

	ПоказыватьКодПоОКАТО = Год(Объект.Период) < 2014;
	
	ПостановкаНаУчетСДругимКодомПоОКАТО = Объект.ПостановкаНаУчетВНалоговомОргане 
		= ПредопределенноеЗначение("Перечисление.ПостановкаНаУчетВНалоговомОргане.СДругимКодомПоОКАТО");

	Элементы.КодПоОКТМО_ДругойКод.Доступность               = ПостановкаНаУчетСДругимКодомПоОКАТО;
	Элементы.КодПоОКТМО_ДругойКод.АвтоОтметкаНезаполненного = ПостановкаНаУчетСДругимКодомПоОКАТО;
	Элементы.КодПоОКТМО_ДругойКод.ОтметкаНезаполненного     = ПостановкаНаУчетСДругимКодомПоОКАТО И ПустаяСтрока(Форма.КодПоОКТМО_ДругойКод);
	
	Элементы.КодПоОКАТО_ДругойКод.Видимость                 = ПоказыватьКодПоОКАТО;
	Элементы.КодПоОКАТО_ДругойКод.Доступность               = ПостановкаНаУчетСДругимКодомПоОКАТО;
	Элементы.КодПоОКАТО_ДругойКод.АвтоОтметкаНезаполненного = ПостановкаНаУчетСДругимКодомПоОКАТО;
	Элементы.КодПоОКАТО_ДругойКод.ОтметкаНезаполненного     = ПостановкаНаУчетСДругимКодомПоОКАТО И ПустаяСтрока(Форма.КодПоОКАТО_ДругойКод);

	ПостановкаНаУчетВДругомНалоговомОргане = Объект.ПостановкаНаУчетВНалоговомОргане 
		= ПредопределенноеЗначение("Перечисление.ПостановкаНаУчетВНалоговомОргане.ВДругомНалоговомОргане");
	ОпределениеНалоговойБазыПоКадастровойСтоимости = Объект.НалоговаяБаза = 
		ПредопределенноеЗначение("Перечисление.НалоговаяБазаПоНалогуНаИмущество.КадастроваяСтоимость");
	
	Элементы.НалоговыйОрган.Доступность               = ПостановкаНаУчетВДругомНалоговомОргане;
	Элементы.НалоговыйОрган.АвтоОтметкаНезаполненного = ПостановкаНаУчетВДругомНалоговомОргане;
	Элементы.НалоговыйОрган.ОтметкаНезаполненного     = 
		ПостановкаНаУчетВДругомНалоговомОргане И Объект.НалоговыйОрган.Пустая();

	Элементы.КодПоОКТМО_ДругойНалоговыйОрган.Доступность               = ПостановкаНаУчетВДругомНалоговомОргане;
	Элементы.КодПоОКТМО_ДругойНалоговыйОрган.АвтоОтметкаНезаполненного = ПостановкаНаУчетВДругомНалоговомОргане;
	Элементы.КодПоОКТМО_ДругойНалоговыйОрган.ОтметкаНезаполненного     = ПостановкаНаУчетВДругомНалоговомОргане И ПустаяСтрока(Форма.КодПоОКТМО_ДругойНалоговыйОрган);
	
	Элементы.КодПоОКАТО_ДругойНалоговыйОрган.Видимость                 = ПоказыватьКодПоОКАТО;
	Элементы.КодПоОКАТО_ДругойНалоговыйОрган.Доступность               = ПостановкаНаУчетВДругомНалоговомОргане;
	Элементы.КодПоОКАТО_ДругойНалоговыйОрган.АвтоОтметкаНезаполненного = ПостановкаНаУчетВДругомНалоговомОргане;
	Элементы.КодПоОКАТО_ДругойНалоговыйОрган.ОтметкаНезаполненного     = ПостановкаНаУчетВДругомНалоговомОргане И ПустаяСтрока(Форма.КодПоОКАТО_ДругойНалоговыйОрган);

	Элементы.ГруппаНалогНаИмущество.Видимость = ПостановкаНаУчетВДругомНалоговомОргане ИЛИ ОпределениеНалоговойБазыПоКадастровойСтоимости;
	
	Элементы.КадастроваяСтоимость.АвтоОтметкаНезаполненного = ОпределениеНалоговойБазыПоКадастровойСтоимости;
	Элементы.КадастроваяСтоимость.ОтметкаНезаполненного     = ОпределениеНалоговойБазыПоКадастровойСтоимости И Объект.КадастроваяСтоимость = 0;
	
	Элементы.КадастровыйНомер.АвтоОтметкаНезаполненного = ОпределениеНалоговойБазыПоКадастровойСтоимости;
	Элементы.КадастровыйНомер.ОтметкаНезаполненного     = ОпределениеНалоговойБазыПоКадастровойСтоимости И ПустаяСтрока(Объект.КадастровыйНомер);
	
	Элементы.ДатаРегистрацииПраваСобственности.АвтоОтметкаНезаполненного = ОпределениеНалоговойБазыПоКадастровойСтоимости;
	Элементы.ДатаРегистрацииПраваСобственности.ОтметкаНезаполненного     = ОпределениеНалоговойБазыПоКадастровойСтоимости И НЕ ЗначениеЗаполнено(Объект.ДатаРегистрацииПраваСобственности);
	
	ОсвобождениеОтНалогообложения = (Объект.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОсвобождаетсяОтНалогообложения"));

	Элементы.КодНалоговойЛьготыОсвобождениеОтНалогообложения.Доступность               = ОсвобождениеОтНалогообложения;
	Элементы.КодНалоговойЛьготыОсвобождениеОтНалогообложения.АвтоОтметкаНезаполненного = ОсвобождениеОтНалогообложения;
	Элементы.КодНалоговойЛьготыОсвобождениеОтНалогообложения.ОтметкаНезаполненного     = ОсвобождениеОтНалогообложения И ПустаяСтрока(Объект.КодНалоговойЛьготыОсвобождениеОтНалогообложения);

	ОблагаетсяПоСниженнойСтавке = (Объект.ПорядокНалогообложения = ПредопределенноеЗначение("Перечисление.ПорядокНалогообложенияИмущества.ОблагаетсяПоСниженнойСтавке"));

	Элементы.СниженнаяНалоговаяСтавка.Доступность                 = ОблагаетсяПоСниженнойСтавке;
	Элементы.СниженнаяНалоговаяСтавка.АвтоОтметкаНезаполненного   = ОблагаетсяПоСниженнойСтавке;
	Элементы.СниженнаяНалоговаяСтавка.ОтметкаНезаполненного       = ОблагаетсяПоСниженнойСтавке И Объект.СниженнаяНалоговаяСтавка = 0;
	Элементы.ДекорацияСниженнаяНалоговаяСтавкаПроцент.Доступность = ОблагаетсяПоСниженнойСтавке;

	Элементы.ДоляСтоимостиЧислитель.Доступность                   = Форма.ДоляУказана = 1;
	Элементы.ДоляСтоимостиЧислитель.АвтоОтметкаНезаполненного     = Форма.ДоляУказана = 1;
	Элементы.ДоляСтоимостиЧислитель.ОтметкаНезаполненного         = Форма.ДоляУказана = 1 И Объект.ДоляСтоимостиЧислитель = 0;

	Элементы.ДоляСтоимостиЗнаменатель.Доступность                 = Форма.ДоляУказана = 1;
	Элементы.ДоляСтоимостиЗнаменатель.АвтоОтметкаНезаполненного   = Форма.ДоляУказана = 1;
	Элементы.ДоляСтоимостиЗнаменатель.ОтметкаНезаполненного       = Форма.ДоляУказана = 1 И Объект.ДоляСтоимостиЗнаменатель = 0;
	
	Форма.Особенности = Объект.ВидИмущества;
	
	ТекстНадписиДоляСтоимости = НСтр("ru='Стоимость относится к %1'");
	ДополнениеНадписи = ?(Объект.ВидИмущества = ПредопределенноеЗначение("Перечисление.ВидыИмущества.НаходитсяНаТерриторииДругогоГосударства"),
		НСтр("ru='территории другого государства'"),
		НСтр("ru='указанному коду по ОКТМО'") + ?(Год(Форма.Запись.Период) < 2014, НСтр("ru=' (ОКАТО)'"), ""));
	ТекстНадписиДоляСтоимости = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстНадписиДоляСтоимости, ДополнениеНадписи);
	Элементы.ВПолнойСумме.Заголовок = ТекстНадписиДоляСтоимости;

	Элементы.НалоговаяБаза.ТолькоПросмотр = Объект.Период < '20140101';
	
	Элементы.ГруппаСтраницыПараметрыРегистрации.Видимость = Не Форма.НеПодлежитНалогообложению;
	
	Элементы.НалоговаяБаза.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.КадастровыйНомер.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.КадастровыйНомерПомещения.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.КадастроваяСтоимость.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.НеоблагаемаяКадастроваяСтоимость.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.ДатаРегистрацииПраваСобственности.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.ДатаПрекращенияПраваСобственности.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.ВПолнойСумме.Доступность = Форма.ОбъектНедвижимогоИмущества;
	Элементы.ГруппаДоляСтоимости.Доступность = Форма.ОбъектНедвижимогоИмущества;
	
	Элементы.ИспользуетсяТолькоВДеятельностиОблагаемойЕНВД.Видимость = Форма.СовмещениеОСНОиЕНВД;
	Элементы.ИспользуетсяТолькоВДеятельностиОблагаемойЕНВД.Доступность = Форма.ОбъектНедвижимогоИмущества
		И ОпределениеНалоговойБазыПоКадастровойСтоимости;

	Элементы.ДвижимоеИмуществоПодлежащееНалогообложению.Видимость = Не Форма.ОбъектНедвижимогоИмущества;
		
КонецПроцедуры

&НаСервере
Процедура УстановитьРеквизитОбъектНедвижимогоИмущества()
	
	ГруппаОС = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Запись.ОсновноеСредство, "ГруппаОС");
	
	ОбъектНедвижимогоИмущества = ГруппаОС = Перечисления.ГруппыОС.Здания
		ИЛИ ГруппаОС = Перечисления.ГруппыОС.Сооружения
		ИЛИ ГруппаОС = Перечисления.ГруппыОС.ПрочееИмуществоТребующееГосударственнойРегистрации;
		
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьГоловнуюОрганизацию(Форма)
	
	Форма.ГоловнаяОрганизация = ОбщегоНазначенияБПВызовСервераПовтИсп.ГоловнаяОрганизация(Форма.Запись.Организация);
		
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьРеквизитСовмещениеОСНОиЕНВД(Форма)
	
	Запись = Форма.Запись;
	
	Если Дата('20140701') <= Запись.Период И Запись.Период < Дата('20150101') Тогда
		Форма.СовмещениеОСНОиЕНВД = ЕстьСовмещениеОСНОиЕНВД(Запись.Организация, Запись.Период);
	Иначе
		Форма.СовмещениеОСНОиЕНВД = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ЕстьСовмещениеОСНОиЕНВД(Организация, Период)
	
	СовмещениеОСНОиЕНВД = 
		УчетнаяПолитика.СистемаНалогообложения(Организация, Период) = Перечисления.СистемыНалогообложения.Общая
		И УчетнаяПолитика.ПлательщикЕНВД(Организация, Период);
		
	Возврат СовмещениеОСНОиЕНВД;
	
КонецФункции

&НаКлиенте
Процедура СохранитьЗначениеРеквизитаФормы(ИмяРеквизита, Форма, Данные, СтруктураРеквизитов)

	Если НЕ Форма.Элементы[ИмяРеквизита].Доступность Тогда
		Возврат;
	КонецЕсли;

	СтруктураРеквизитов.Вставить(ИмяРеквизита, Данные[ИмяРеквизита]);

	Данные[ИмяРеквизита] = ОбщегоНазначенияБПКлиентСервер.ПустоеЗначениеТипа(ТипЗнч(Данные[ИмяРеквизита]));

КонецПроцедуры

&НаКлиенте
Процедура ВосстановитьЗначениеРеквизитаФормы(ИмяРеквизита, Данные, СтруктураРеквизитов)

	Если СтруктураРеквизитов.Свойство(ИмяРеквизита) Тогда
		Данные[ИмяРеквизита] = СтруктураРеквизитов[ИмяРеквизита];
	КонецЕсли;

КонецПроцедуры

&НаСервере
Функция ПолучитьКодПоОКТМО()

	Если Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.СДругимКодомПоОКАТО Тогда
		Возврат КодПоОКТМО_ДругойКод;
	ИначеЕсли Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.ВДругомНалоговомОргане Тогда
		Возврат КодПоОКТМО_ДругойНалоговыйОрган;
	Иначе
		Возврат "";
	КонецЕсли;

КонецФункции

&НаСервере
Функция ПолучитьКодПоОКАТО()

	Если Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.СДругимКодомПоОКАТО Тогда
		Возврат КодПоОКАТО_ДругойКод;
	ИначеЕсли Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.ВДругомНалоговомОргане Тогда
		Возврат КодПоОКАТО_ДругойНалоговыйОрган;
	Иначе
		Возврат "";
	КонецЕсли;

КонецФункции

&НаСервере
Процедура ЗаполнитьДанныеПоОсновномуСредству()

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Период",           Запись.Период);
	Запрос.УстановитьПараметр("Организация",      Запись.Организация);
	Запрос.УстановитьПараметр("ОсновноеСредство", Запись.ОсновноеСредство);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ПостановкаНаУчетВНалоговомОргане,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.НалоговыйОрган,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.КодПоОКАТО,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ПорядокНалогообложения,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.КодНалоговойЛьготыОсвобождениеОтНалогообложения,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.НалоговаяСтавка,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.СниженнаяНалоговаяСтавка,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ВидИмущества,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ДоляСтоимостиЧислитель,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ДоляСтоимостиЗнаменатель,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.НалоговаяБаза,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.КадастроваяСтоимость,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.НеоблагаемаяКадастроваяСтоимость,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.КадастровыйНомер,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.КадастровыйНомерПомещения,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ДатаРегистрацииПраваСобственности,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ДатаПрекращенияПраваСобственности,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ИспользуетсяТолькоВДеятельностиОблагаемойЕНВД,
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних.ДвижимоеИмуществоПодлежащееНалогообложению
	|ИЗ
	|	РегистрСведений.СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.СрезПоследних(
	|			&Период,
	|			Организация = &Организация
	|				И ОсновноеСредство = &ОсновноеСредство) КАК СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствамСрезПоследних";
	ВыборкаСтавок = Запрос.Выполнить().Выбрать();

	Если ВыборкаСтавок.Количество() > 0 Тогда
		ВыборкаСтавок.Следующий();
		ЗаполнитьЗначенияСвойств(Запись, ВыборкаСтавок);
		// Код по ОКАТО.
		Если Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.СДругимКодомПоОКАТО Тогда
			КодПоОКАТО_ДругойКод            = Запись.КодПоОКАТО;
			КодПоОКАТО_ДругойНалоговыйОрган = "";
		ИначеЕсли Запись.ПостановкаНаУчетВНалоговомОргане = Перечисления.ПостановкаНаУчетВНалоговомОргане.ВДругомНалоговомОргане Тогда
			КодПоОКАТО_ДругойКод            = "";
			КодПоОКАТО_ДругойНалоговыйОрган = Запись.КодПоОКАТО;
		Иначе
			КодПоОКАТО_ДругойКод            = "";
			КодПоОКАТО_ДругойНалоговыйОрган = "";
		КонецЕсли;
	КонецЕсли;

	УстановитьРеквизитОбъектНедвижимогоИмущества();
	
	Если Не ОбъектНедвижимогоИмущества Тогда
		Запись.НалоговаяБаза = Перечисления.НалоговаяБазаПоНалогуНаИмущество.СреднегодоваяСтоимость;
		Запись.КадастровыйНомер = "";
		Запись.КадастровыйНомерПомещения = "";
		Запись.КадастроваяСтоимость = 0;
		Запись.НеоблагаемаяКадастроваяСтоимость = 0;
		Запись.ДатаРегистрацииПраваСобственности = Дата(1,1,1);
		Запись.ДатаПрекращенияПраваСобственности = Дата(1,1,1);
		Запись.ИспользуетсяТолькоВДеятельностиОблагаемойЕНВД = Ложь;
		ДоляУказана = 0;
		Запись.ДоляСтоимостиЧислитель = 0;
		Запись.ДоляСтоимостиЗнаменатель = 0;
	КонецЕсли;	
	
	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаСервере
Процедура НалоговыйОрганПриИзмененииНаСервере()
	Если ЗначениеЗаполнено(Запись.НалоговыйОрган) Тогда 
		КодПоОКТМО_ДругойНалоговыйОрган = Запись.НалоговыйОрган.КодПоОКТМО;
		КодПоОКАТО_ДругойНалоговыйОрган = Запись.НалоговыйОрган.КодПоОКАТО;
	КонецЕсли;	
КонецПроцедуры

#КонецОбласти

#Область Инициализация

СтруктураСохраняемыхРеквизитов = Новый Структура; // Пустая структура

#КонецОбласти