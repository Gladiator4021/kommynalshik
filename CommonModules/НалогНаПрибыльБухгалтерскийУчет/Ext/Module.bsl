﻿// Выполнение требований ПБУ 18

#Область КоллекцииСумм

Функция ОписаниеКоллекцииСумм(ИспользуетсяНалоговыйУчет = Истина, ИспользуютсяРазницыПоНалогуНаПрибыль = Истина) Экспорт
	
	ОписаниеСумм = КоллекцииСумм.НовыйОписаниеКоллекцииСумм();
	
	КоллекцииСумм.ДобавитьНаборСумм(ОписаниеСумм, "БухгалтерскийУчет", "Сумма");
	
	Если ИспользуетсяНалоговыйУчет Тогда
		КоллекцииСумм.ДобавитьНаборСумм(ОписаниеСумм, "НалоговыйУчет", "СуммаНУ");
	КонецЕсли;
	
	Если ИспользуютсяРазницыПоНалогуНаПрибыль Тогда
		КоллекцииСумм.ДобавитьНаборСумм(ОписаниеСумм, ИмяНабораСуммРазницыПоНалогуНаПрибыль(), "СуммаПР, СуммаВР");
	КонецЕсли;
	
	Возврат ОписаниеСумм;
	
КонецФункции


Функция ИмяНабораСуммРазницыПоНалогуНаПрибыль() Экспорт
	
	Возврат "РазницыПоНалогуНаПрибыль";
	
КонецФункции

Функция ЕстьРазницыПоНалогуНаПрибыль(ОписаниеСумм) Экспорт
	
	// Всегда, когда есть набор с разницами, должны быть и наборы для сумм БУ и НУ
	
	Возврат КоллекцииСумм.ЕстьНаборСумм(ОписаниеСумм, ИмяНабораСуммРазницыПоНалогуНаПрибыль());
	
КонецФункции

Процедура РассчитатьРазницыПоНалогуНаПрибыль(Приемник, Источник, ОписаниеСумм, УчитыватьЗнакРазниц = Истина) Экспорт
	
	Если Не ЕстьРазницыПоНалогуНаПрибыль(ОписаниеСумм) Тогда
		Возврат;
	КонецЕсли;
		
	// Алгоритм расчета опирается на данные Источника.
	// - по возможности относим разницу к той, которая имеет подходящий знак
	//   -- если обе разницы одного знака, то делим пропорционально
	//   -- если знак разный, то относим к той, что отличается от нуля (при условии, что это допускается параметром функции)
	// - в остальных случаях считаем разницу постоянной
		
	Разница = Приемник.Сумма - Приемник.СуммаНУ;
	
	Если Источник.СуммаПР = 0 И Источник.СуммаВР = 0 Тогда
		Приемник.СуммаПР = Разница;
		Приемник.СуммаВР = 0;
	ИначеЕсли ЗнакЧисла(Источник.СуммаПР) = ЗнакЧисла(Источник.СуммаВР) Тогда
		// Делим пропорционально. Знаменатель заведомо ненулевой.
		Приемник.СуммаПР = Окр(Разница / (Источник.СуммаПР + Источник.СуммаВР) * Источник.СуммаПР, БухгалтерскийУчетКлиентСервер.РазрядностьДробнойЧастиСумм());
		Приемник.СуммаВР = Разница - Приемник.СуммаПР;
	ИначеЕсли Не УчитыватьЗнакРазниц И Источник.СуммаПР = 0 Тогда
		// Выше проверили, что СуммаВР <> 0
		Приемник.СуммаПР = 0;
		Приемник.СуммаВР = Разница;
	ИначеЕсли ЗнакЧисла(Источник.СуммаВР) = ЗнакЧисла(Разница) Тогда
		Приемник.СуммаПР = 0;
		Приемник.СуммаВР = Разница;
	Иначе
		Приемник.СуммаПР = Разница;
		Приемник.СуммаВР = 0;
	КонецЕсли;
	
КонецПроцедуры

Процедура ИсправитьПогрешностиОкругления(Доли, ИсходныеСуммы, ОписаниеСумм, Числитель, Знаменатель) Экспорт
	
	Если Не ЕстьРазницыПоНалогуНаПрибыль(ОписаниеСумм) Тогда
		Возврат;
	КонецЕсли;
	
	Если Числитель = Знаменатель Тогда
		Возврат;
	КонецЕсли;
	
	Если ИсходныеСуммы.СуммаПР = 0 И ИсходныеСуммы.СуммаВР = 0 Тогда
		Возврат;
	КонецЕсли;
	
	// В исходных суммах может соблюдаться равенство БУ = НУ + ПР + ВР.
	// При расчете долей равенство может нарушиться на копейки за счет погрешностей округления.
	//
	// В исходных суммах равенство может и не соблюдаться.
	// Чтобы не зависеть от того, соблюдалось равенство или нет в исходных суммах,
	// введем сумму Контроль = БУ - НУ - ПР - ВР.
	//
	// От этой суммы рассчитаем долю по тому же алгоритму, что и для других сумм.
	// А затем обеспечим, чтобы для коллекции с долями выполнялось равенство Контроль = БУ - НУ - ПР - ВР:
	// при необходимости исправим сумму ПР или ВР.
	
	КонтрольнаяСуммаИсходная = ИсходныеСуммы.Сумма - ИсходныеСуммы.СуммаНУ - ИсходныеСуммы.СуммаПР - ИсходныеСуммы.СуммаВР;
	КонтрольнаяСуммаДоли     = Доли.Сумма          - Доли.СуммаНУ          - Доли.СуммаПР          - Доли.СуммаВР;
	
	ДоляИсходнойКонтрольнойСуммы = КоллекцииСумм.ДоляСуммы(КонтрольнаяСуммаИсходная, Числитель, Знаменатель);
	Погрешность                  = КонтрольнаяСуммаДоли - ДоляИсходнойКонтрольнойСуммы; // Сумма, которую надо добавить к разницам
	
	Если Погрешность = 0 Тогда
		Возврат;
	КонецЕсли;
		
	СуммыПогрешности = КоллекцииСумм.НовыйКоллекцияСумм(ОписаниеСумм);
	СуммыПогрешности.Сумма = Погрешность;
	РассчитатьРазницыПоНалогуНаПрибыль(СуммыПогрешности, ИсходныеСуммы, ОписаниеСумм, Ложь);
	СуммыПогрешности.Сумма = 0;
	КоллекцииСумм.Сложить(Доли, СуммыПогрешности, ОписаниеСумм);
		
КонецПроцедуры

Функция ЗнакЧисла(Значение)
	
	Если Значение > 0 Тогда
		Возврат 1;
	ИначеЕсли Значение < 0 Тогда
		Возврат -1;
	Иначе
		Возврат 0;
	КонецЕсли;
	
КонецФункции

#КонецОбласти
