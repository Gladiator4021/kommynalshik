﻿////////////////////////////////////////////////////////////////////////////////
// Подсистема "Контактная информация Бухгалтерии предприятия".
//
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Возвращает адрес в виде структуры полей. Если адрес нужного вида не задан, то будет возвращена структура с пустыми полями.
//
// Параметры:
//    Ссылка                  - СправочникСсылка - Ссылка на объект, который содержит контактную информацию.
//    ВидКонтактнойИнформации - СправочникСсылка.ВидыКонтактнойИнформации - Вид контактной информации, структуру которого нужно получить.
//
// Возвращаемое значение:
//  Структура - Структура со значениями полей адреса.
//
Функция АдресСтруктурой(Ссылка, ВидКонтактнойИнформации) Экспорт
	
	СтруктураАдреса = Новый Структура();
	СтруктураАдреса.Вставить("АдресРФ", Истина);
	СтруктураАдреса.Вставить("КодСтраны", "");
	СтруктураАдреса.Вставить("Страна", "");
	СтруктураАдреса.Вставить("Индекс", "");
	СтруктураАдреса.Вставить("Регион", "");
	СтруктураАдреса.Вставить("КодРегиона", "");
	СтруктураАдреса.Вставить("Район", "");
	СтруктураАдреса.Вставить("Город", "");
	СтруктураАдреса.Вставить("НаселенныйПункт", "");
	СтруктураАдреса.Вставить("Улица", "");
	СтруктураАдреса.Вставить("Дом", "");
	СтруктураАдреса.Вставить("Корпус", "");
	СтруктураАдреса.Вставить("Квартира", "");
	СтруктураАдреса.Вставить("Представление", "");
	СтруктураАдреса.Вставить("ЗначенияПолей", "");
	
	Адрес = УправлениеКонтактнойИнформацией.КонтактнаяИнформацияОбъектов(
							ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Ссылка),
							Перечисления.ТипыКонтактнойИнформации.Адрес, 
							ВидКонтактнойИнформации);
	Если Адрес.Количество() = 0 Тогда
		Возврат СтруктураАдреса;
	КонецЕсли;
	
	ДополнительныеПараметры = Новый Структура("НаименованиеВключаетСокращение", Истина);
	СведенияОбАдресе = УправлениеКонтактнойИнформацией.СведенияОбАдресе(Адрес[0].ЗначенияПолей, ДополнительныеПараметры);
	СтруктураАдреса.Представление = Адрес[0].Представление;
	СтруктураАдреса.ЗначенияПолей = Адрес[0].ЗначенияПолей;
	
	СтруктураАдреса.Страна    = СведенияОбАдресе.Страна;
	СтруктураАдреса.КодСтраны = СведенияОбАдресе.КодСтраны;
	Если СтруктураАдреса.Свойство("Страна") 
		И СтрСравнить(СтруктураАдреса.Страна, Справочники.СтраныМира.Россия.Наименование) = 0 Тогда
		СтруктураАдреса.АдресРФ = Истина;
	Иначе
		СтруктураАдреса.АдресРФ = Ложь;
	КонецЕсли;
	
	СтруктураАдреса.Индекс = СведенияОбАдресе.Индекс;
	СтруктураАдреса.Регион = СведенияОбАдресе.Регион;
	СтруктураАдреса.КодРегиона = ?(СведенияОбАдресе.Свойство("КодРегиона"), СведенияОбАдресе.КодРегиона, "");
	СтруктураАдреса.Район = СведенияОбАдресе.Район;
	СтруктураАдреса.Город = СведенияОбАдресе.Город;
	СтруктураАдреса.НаселенныйПункт = СведенияОбАдресе.НаселенныйПункт;
	СтруктураАдреса.Улица = СведенияОбАдресе.Улица;
	СтруктураАдреса.Дом = СведенияОбАдресе.Здание.Номер;
	
	Если СведенияОбАдресе.Корпуса.Количество() > 0 Тогда
		СтруктураАдреса.Корпус = СведенияОбАдресе.Корпуса[0].Номер;
	КонецЕсли;
	
	Если СведенияОбАдресе.Помещения.Количество() > 0 Тогда
		СтруктураАдреса.Квартира = СведенияОбАдресе.Помещения[0].Номер;
	КонецЕсли;
	
	Возврат СтруктураАдреса;
	
КонецФункции

// Заполняет дополнительные реквизиты табличной части "Контактная информация" для адреса.
//
// Параметры:
//    СтрокаТабличнойЧасти - СтрокаТабличнойЧасти - заполняемая строка табличной части "Контактная информация".
//    Источник             - ОбъектXDTO  - контактная информация.
//
Процедура ЗаполнитьРеквизитыТабличнойЧастиДляАдреса(СтрокаТабличнойЧасти, Источник) Экспорт
	
	// Умолчания
	СтрокаТабличнойЧасти.Страна = "";
	СтрокаТабличнойЧасти.Регион = "";
	СтрокаТабличнойЧасти.Город  = "";
	
	Адрес = Источник.Состав;
	
	ПространствоИмен = УправлениеКонтактнойИнформациейКлиентСервер.ПространствоИмен();
	ЭтоАдрес = ТипЗнч(Адрес) = Тип("ОбъектXDTO") И Адрес.Тип() = ФабрикаXDTO.Тип(ПространствоИмен, "Адрес");
	Если ЭтоАдрес И Адрес.Состав <> Неопределено Тогда 
		СтрокаТабличнойЧасти.Страна = Адрес.Страна;
		АдресРФ = УправлениеКонтактнойИнформациейСлужебный.РоссийскийАдрес(Адрес);
		Если АдресРФ <> Неопределено Тогда
			// Российский адрес
			СтрокаТабличнойЧасти.Регион = АдресРФ.СубъектРФ;
			СтрокаТабличнойЧасти.Город  = АдресРФ.Город;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

// Заполняет дополнительные реквизиты табличной части "Контактная информация" для адреса электронной почты.
//
// Параметры:
//    СтрокаТабличнойЧасти - СтрокаТабличнойЧасти - заполняемая строка табличной части "Контактная информация".
//    Источник             - ОбъектXDTO  - контактная информация.
//
Процедура ЗаполнитьРеквизитыТабличнойЧастиДляАдресаЭлектроннойПочты(СтрокаТабличнойЧасти, Источник) Экспорт
	
	Результат = ОбщегоНазначенияКлиентСервер.РазобратьСтрокуСПочтовымиАдресами(СтрокаТабличнойЧасти.Представление, Ложь);
	
	Если Результат.Количество() > 0 Тогда
		СтрокаТабличнойЧасти.АдресЭП = Результат[0].Адрес;
		
		Поз = СтрНайти(СтрокаТабличнойЧасти.АдресЭП, "@");
		Если Поз <> 0 Тогда
			СтрокаТабличнойЧасти.ДоменноеИмяСервера = Сред(СтрокаТабличнойЧасти.АдресЭП, Поз+1);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

// Заполняет дополнительные реквизиты табличной части "Контактная информация" для телефона и факса.
//
// Параметры:
//    СтрокаТабличнойЧасти - СтрокаТабличнойЧасти - заполняемая строка табличной части "Контактная информация".
//    Источник             - ОбъектXDTO  - контактная информация.
//
Процедура ЗаполнитьРеквизитыТабличнойЧастиДляТелефона(СтрокаТабличнойЧасти, Источник) Экспорт
	
	// Умолчания
	СтрокаТабличнойЧасти.НомерТелефонаБезКодов = "";
	СтрокаТабличнойЧасти.НомерТелефона         = "";
	
	Телефон = Источник.Состав;
	ПространствоИмен = УправлениеКонтактнойИнформациейКлиентСервер.ПространствоИмен();
	Если Телефон <> Неопределено И Телефон.Тип() = ФабрикаXDTO.Тип(ПространствоИмен, "НомерТелефона") Тогда
		КодСтраны     = Телефон.КодСтраны;
		КодГорода     = Телефон.КодГорода;
		НомерТелефона = Телефон.Номер;
		
		Если СтрНачинаетсяС(КодСтраны, "+") Тогда
			КодСтраны = Сред(КодСтраны, 2);
		КонецЕсли;
		
		Поз = СтрНайти(НомерТелефона, ",");
		Если Поз <> 0 Тогда
			НомерТелефона = Лев(НомерТелефона, Поз-1);
		КонецЕсли;
		
		Поз = СтрНайти(НомерТелефона, Символы.ПС);
		Если Поз <> 0 Тогда
			НомерТелефона = Лев(НомерТелефона, Поз-1);
		КонецЕсли;
		
		СтрокаТабличнойЧасти.НомерТелефонаБезКодов = УбратьРазделителиВНомерТелефона(НомерТелефона);
		СтрокаТабличнойЧасти.НомерТелефона         = УбратьРазделителиВНомерТелефона(Строка(КодСтраны) + КодГорода + НомерТелефона);
	КонецЕсли;
	
КонецПроцедуры

// Возвращает код региона по переданному адресу.
//
// Параметры:
//    Адрес - СписокЗначений - контактная информация.
//
// Возвращаемое значение:
//    Строка - код региона.
//
Функция КодРегионаПоАдресу(Адрес) Экспорт
	
	Если ТипЗнч(Адрес) = Тип("СписокЗначений") Тогда
		
		Регион = "";
		Для Каждого ЭлементАдреса ИЗ Адрес Цикл
			Если ЭлементАдреса.Представление = "КодРегиона" Тогда
				Возврат ЭлементАдреса.Значение;
			ИначеЕсли ЭлементАдреса.Представление = "Регион" Тогда
				Регион = ЭлементАдреса.Значение;
			КонецЕсли;
		КонецЦикла;
		Возврат АдресныйКлассификатор.КодРегионаПоНаименованию(Регион);
		
	Иначе
		Регион = УправлениеКонтактнойИнформацией.РегионАдресаКонтактнойИнформации(Адрес);
		Если ЗначениеЗаполнено(Регион) Тогда
			КодРегиона = АдресныйКлассификатор.КодРегионаПоНаименованию(Регион);
			Если ЗначениеЗаполнено(КодРегиона) Тогда
				Возврат КодРегиона;
			КонецЕсли;
		КонецЕсли;
		Возврат "";
	КонецЕсли;
	
КонецФункции

// Определяет, соответствует ли номер региона городу федерального значения.
// Города федерального значения перечислены в статье 65 Конституции РФ
// Также статусом города федерального значения наделен город Байконур 
// (Соглашение между Российской Федерацией и Республикой Казахстан о статусе города Байконур,
// порядке формирования и статусе его органов исполнительной власти (Москва, 23 декабря 1995 г.))
//
// Параметры:
//  КодРегиона	 - Строка - двухсимволный код региона в соответствии с ФИАС
// 
// Возвращаемое значение:
//  Булево - Истина, если код региона соответствует городу федерального значения.
//
Функция ГородФедеральногоЗначения(КодРегиона) Экспорт
	
	Возврат КодРегиона = "77"  // Москва
		Или КодРегиона = "78"  // Санкт-Петербург
		Или КодРегиона = "92"  // Севастополь
		Или КодРегиона = "99"; // Байконур
	
КонецФункции

// Определяет нижний уровень населенного пункта в адресе.
//
// Параметры:
//  ЗначенияПолей - Строка - XML контактной информации или пары ключ-значение.
//  Вид           - СправочникСсылка.ВидыКонтактнойИнформации, Структура - параметры контактной информации.
// 
// Возвращаемое значение:
//  Строка - наименование города или иного населенного пункта.
//
Функция НаселенныйПунктПоАдресу(ЗначенияПолей, Вид) Экспорт
	
	СтруктураПолейАдреса = УправлениеКонтактнойИнформацией.ПредыдущаяСтруктураКонтактнойИнформацииXML(ЗначенияПолей, Вид);
	
	Если СтруктураПолейАдреса.Свойство("Город") 
		И ЗначениеЗаполнено(СтруктураПолейАдреса.Город) Тогда
		
		Возврат СтруктураПолейАдреса.Город;
	
	ИначеЕсли СтруктураПолейАдреса.Свойство("НаселенныйПункт") 
		И ЗначениеЗаполнено(СтруктураПолейАдреса.НаселенныйПункт) Тогда
		
		Возврат СтруктураПолейАдреса.НаселенныйПункт;
	
	ИначеЕсли СтруктураПолейАдреса.Свойство("КодРегиона")
		И СтруктураПолейАдреса.Свойство("Регион")
		И ГородФедеральногоЗначения(СтруктураПолейАдреса.КодРегиона) Тогда
		
		// Города федерального значения и приравненные к ним
		Возврат СтруктураПолейАдреса.Регион;
		
	Иначе
		
		Возврат "";
		
	КонецЕсли;

КонецФункции

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает текст ссылки по которой осуществляется переход на карту,
// в виде форматированной строки (с картинкой)
//
Функция СтрокаСсылкиПоказатьНаКарте() Экспорт
	
	СоставСтроки = Новый Массив;
	СоставСтроки.Добавить(БиблиотекаКартинок.Пин);
	СоставСтроки.Добавить(НСтр("ru = 'показать на карте'"));
	Возврат Новый ФорматированнаяСтрока(СоставСтроки);
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Убирает разделители в номере телефона.
//
// Параметры:
//    НомерТелефона - Строка - номер телефона или факса.
//
// Возвращаемое значение:
//     Строка - номер телефона или факса без разделителей.
//
Функция УбратьРазделителиВНомерТелефона(Знач НомерТелефона)
	
	Поз = СтрНайти(НомерТелефона, ",");
	Если Поз <> 0 Тогда
		НомерТелефона = Лев(НомерТелефона, Поз-1);
	КонецЕсли;
	
	НомерТелефона = СтрЗаменить(НомерТелефона, "-", "");
	НомерТелефона = СтрЗаменить(НомерТелефона, " ", "");
	НомерТелефона = СтрЗаменить(НомерТелефона, "+", "");
	
	Возврат НомерТелефона;
	
КонецФункции

#КонецОбласти
