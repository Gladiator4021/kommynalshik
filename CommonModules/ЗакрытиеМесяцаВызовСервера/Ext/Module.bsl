﻿////////////////////////////////////////////////////////////////////////////////
// АКТУАЛИЗАЦИЯ ДАННЫХ

// Возвращает свойства выполняемого фонового задания по актуализации.
// Если ни одного задания не найдено, возвращается значение Неопределено.
//
// Параметры:
//  Организация                  - СправочникСсылка.Организации - Организация, 
//                                 по которой будет производиться поиск фонового
//                                 задания по актуализации.
//  УникальныйИдентификаторФормы - Строка - Уникальный идентификатор формы, из
//                                 которой вызывается поиск фонового задания.
//
// Возвращаемое значение:
//   Структура - всегда содержит ключ УникальныйИдентификатор.
//               Если задание выполняется, но его свойства получить не удалось,
//               в значении ключа УникальныйИдентификатор возвращается пустая строка.
//               Другие ключи структуры:
//               * Ключ         - Строка - Ключ фонового задания.
//               * Наименование - Строка - Наименование фонового задания.
//               * Организация  - СправочникСсылка.Организация - Организация, 
//               по которой выполняется задание.
//               * Пользователь - СправочникСсылка.Пользователи - Пользователь,
//               запустивший задание.
//
Функция НайтиФоновоеЗаданиеАктуализацииПоОрганизации(Знач Организация, Знач УникальныйИдентификаторФормы) Экспорт
	
	Результат = Неопределено;
	
	УстановитьПривилегированныйРежим(Истина);

	Если НЕ Обработки.ЗакрытиеМесяца.АктуализацияВозможна(Организация) Тогда
		// Для данной организации не поддерживается полная актуализация,
		// возможно только выполнение отложенных расчетов (если включена такая настройка).
		// Фоновое задание по актуализации отложенных расчетов не блокирует других пользователей,
		// поэтому не будем искать дальше.
		Возврат Результат;
	КонецЕсли;
	
	Отбор = Новый Структура("Состояние", СостояниеФоновогоЗадания.Активно);
	
	МассивФоновыхЗаданий = ФоновыеЗадания.ПолучитьФоновыеЗадания(Отбор);
	
	Для каждого ФоновоеЗадание Из МассивФоновыхЗаданий Цикл
		
		Ключ = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(ФоновоеЗадание.Ключ, ":");
		Если Ключ.Количество() > 1 Тогда
			
			ПрефиксКлючаЗадания = Ключ[0];
			ОрганизацияЗадания = Справочники.Организации.ПолучитьСсылку(Новый УникальныйИдентификатор(Ключ[1]));
			ПользовательЗадания = Справочники.Пользователи.ПолучитьСсылку(Новый УникальныйИдентификатор(Ключ[2]));
			
			Если ПрефиксКлючаЗадания = "АктуализацияДанных" И ОрганизацияЗадания = Организация Тогда
				СвойстваВыполняемогоЗадания = Новый Структура("УникальныйИдентификатор, Ключ, Наименование");
				ЗаполнитьЗначенияСвойств(СвойстваВыполняемогоЗадания, ФоновоеЗадание);
				СвойстваВыполняемогоЗадания.Вставить("Организация", ОрганизацияЗадания);
				СвойстваВыполняемогоЗадания.Вставить("Пользователь", ПользовательЗадания);
				Результат = СвойстваВыполняемогоЗадания;
				Прервать;
			КонецЕсли;
			
		КонецЕсли;
	КонецЦикла;
	
	ИБФайловая = ОбщегоНазначения.ИнформационнаяБазаФайловая();
	
	// Если не найдено ни одного задания в файловом режиме работы, это еще не значит, 
	// что никто не актуализирует данные.
	Если Результат = Неопределено И ИБФайловая Тогда 
		
		УспехБлокировки = Ложь;
		
		Попытка
			ЗаблокироватьДанныеДляРедактирования(Организация, , УникальныйИдентификаторФормы);
			УспехБлокировки = Истина;
		Исключение
			Результат = Новый Структура("УникальныйИдентификатор", "");
		КонецПопытки;
		
		Если УспехБлокировки Тогда
			РазблокироватьДанныеДляРедактирования(Организация, УникальныйИдентификаторФормы);
		КонецЕсли;
		
	КонецЕсли;

	Возврат Результат;
	
КонецФункции

Функция ПроверитьАктуальность(Знач ПараметрыЗадания) Экспорт
	
	ДлительныеОперации.ОтменитьВыполнениеЗадания(ПараметрыЗадания.ИдентификаторЗадания);

	ПодготовитьДанныеРасшифровкиДляФоновогоЗадания(ПараметрыЗадания);
	
	РезультатВыполнения = ДлительныеОперации.ЗапуститьВыполнениеВФоне(
		ПараметрыЗадания.УникальныйИдентификаторФормы,
		"Обработки.ЗакрытиеМесяца.ПроверитьАктуальностьВФоне",
		ПараметрыЗадания,
		НСтр("ru = 'Проверка актуальности данных'"));

	Возврат РезультатВыполнения;
	
КонецФункции

Функция АктуализироватьДанные(Знач ПараметрыЗадания) Экспорт
	
	ДлительныеОперации.ОтменитьВыполнениеЗадания(ПараметрыЗадания.ИдентификаторЗадания);
	
	НаименованиеЗадания = НСтр("ru='Актуализация данных: %1 (%2)'");
	НаименованиеЗадания = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НаименованиеЗадания,
		ПараметрыЗадания.Организация,
		ПользователиКлиентСервер.ТекущийПользователь());
		
	КлючЗадания = "АктуализацияДанных:%1:%2";
	КлючЗадания = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(КлючЗадания, 
		ПараметрыЗадания.Организация.УникальныйИдентификатор(),
		ПользователиКлиентСервер.ТекущийПользователь().УникальныйИдентификатор());
		
	ПодготовитьДанныеРасшифровкиДляФоновогоЗадания(ПараметрыЗадания);
		
	РезультатВыполнения = ЗакрытиеМесяца.ЗапуститьВыполнениеВФоне(
		ПараметрыЗадания.УникальныйИдентификаторФормы,
		"Обработки.ЗакрытиеМесяца.АктуализироватьВФоне",
		ПараметрыЗадания,
		НаименованиеЗадания,
		КлючЗадания);
	
	Возврат РезультатВыполнения;
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// РАБОТА С НАСТРОЙКАМИ

Функция ПроверятьАктуальность() Экспорт
	
	Возврат Константы.ПроверятьАктуальностьДанныхУчета.Получить()
		И ПравоДоступа("Чтение", Метаданные.Документы.РегламентнаяОперация);
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// РАБОТА С ФОНОВЫМИ ЗАДАНИЯМИ

Функция ЗаданиеВыполнено(Знач ИдентификаторЗадания) Экспорт
	
	Возврат ДлительныеОперации.ЗаданиеВыполнено(ИдентификаторЗадания);
	
КонецФункции

Процедура ОтменитьВыполнениеЗадания(Знач ИдентификаторЗадания) Экспорт
	
	ДлительныеОперации.ОтменитьВыполнениеЗадания(ИдентификаторЗадания);
	
КонецПроцедуры

// Читает сообщение от длительной операции о ее текущем состоянии.
//
// Возвращаемое значение:
//   Структура   - всегда содержит ключ ЗаданиеВыполнено. Ложь, если существует фоновое задание с ИдентификаторЗадания.
//                 Иные ключи структуры см. ДлительныеОперации.СообщитьПрогресс()
//
Функция ПрочитатьПрогресс(Знач ИдентификаторЗадания) Экспорт
	
	ПрогрессЗадания = ДлительныеОперации.ПрочитатьПрогресс(ИдентификаторЗадания);
	
	// Добавляем флаг "ЗаданиеВыполнено", чтобы различать случаи: когда отсутствуют сообщения и когда завершено задание.
	Если ПрогрессЗадания = Неопределено
	 Или ТипЗнч(ПрогрессЗадания) <> Тип("Структура") Тогда // или нет задания, или нет сообщений
		ПрогрессЗадания = Новый Структура;
	КонецЕсли;
	ПрогрессЗадания.Вставить("ЗаданиеВыполнено", ДлительныеОперации.ЗаданиеВыполнено(ИдентификаторЗадания));
	
	Возврат ПрогрессЗадания;
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// СЕРВИСНЫЕ ФУНКЦИИ

Функция ПолучитьПредупреждающийЦвет() Экспорт
	
	Возврат ЦветаСтиля.ЦветФонаНекорректногоКонтрагента;
	
КонецФункции

Функция ПоказатьПредупреждениеАктуализацияДанных(Организация) Экспорт
	
	Если НЕ ЗначениеЗаполнено(Организация) Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Возврат НЕ Константы.ПроверятьАктуальностьДанныхУчета.Получить()
		И Обработки.ЗакрытиеМесяца.ПравоИзмененияРегламентныхОпераций() 
		И Обработки.ЗакрытиеМесяца.АктуализацияВозможна(Организация);
	
КонецФункции

Процедура ПодготовитьДанныеРасшифровкиДляФоновогоЗадания(ПараметрыЗадания)

	Если ПараметрыЗадания.Свойство("АдресХранилищаДанныеРасшифровки")
		И ЭтоАдресВременногоХранилища(ПараметрыЗадания.АдресХранилищаДанныеРасшифровки) Тогда
		
		// В фоновое задание передаем не адрес временного хранилища, а само значение,
		// т.к. внутри фонового задания недоступны данные временного хранилища родительского сеанса.
		ДанныеРасшифровки = ПолучитьИзВременногоХранилища(ПараметрыЗадания.АдресХранилищаДанныеРасшифровки);
		
		ПараметрыЗадания.Удалить("АдресХранилищаДанныеРасшифровки");
		
		СжатыеДанныеРасшифровки = Новый ХранилищеЗначения(ДанныеРасшифровки, Новый СжатиеДанных(9));
		ПараметрыЗадания.Вставить("ДанныеРасшифровки", СжатыеДанныеРасшифровки);
	КонецЕсли;

КонецПроцедуры
