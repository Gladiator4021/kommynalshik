﻿
#Область СлужебныйПрограммныйИнтерфейс

Процедура СформироватьДвиженияПлановыхНачислений(РегистраторОбъект, Движения, СтруктураДанных, ОтдельнаяРегистрацияПлановогоФОТ, ЗаполнятьНаборЗаписей) Экспорт
	
	РасчетЗарплатыБазовый.СформироватьДвиженияПлановыхНачислений(РегистраторОбъект, Движения, СтруктураДанных, ОтдельнаяРегистрацияПлановогоФОТ, ЗаполнятьНаборЗаписей);
	
КонецПроцедуры

// Заполняет движения плановыми удержаниями.
//
// Параметры:
//		Движения		- Коллекция движений, в которой необходимо заполнить движения.
//		СтруктураДанных - Структура с ключами.
//			* ДанныеПлановыхУдержаний - таблица значений с колонками.
//				* ДатаСобытия.
//				* ДействуетДо (необязательно).
//				* ФизическоеЛицо.
//				* Организация
//				* Удержание
//				* ДокументОснование (необязательно).
//				* Используется
//				* ИспользуетсяПоОкончании (необязательно).
//
Процедура СформироватьДвиженияПлановыхУдержаний(Движения, СтруктураДанных) Экспорт

	РасчетЗарплатыБазовый.СформироватьДвиженияПлановыхУдержаний(Движения, СтруктураДанных);
	
КонецПроцедуры

// Заполняет движения плановыми выплатами (авансы).
//
// Параметры:
//	Движения - коллекция движений, в которой необходимо заполнить движения.
//	ДанныеОПлановыхВыплатах - таблица значений с полями:
//		ДатаСобытия
//		Сотрудник
//		ФизическоеЛицо
//		СпособРасчетаАванса
//		Аванс
// 		
Процедура СформироватьДвиженияПлановыхВыплат(Движения, ДанныеОПлановыхВыплатах) Экспорт

	РасчетЗарплатыБазовый.СформироватьДвиженияПлановыхВыплат(Движения, ДанныеОПлановыхВыплатах);
	
КонецПроцедуры

Процедура ПодобратьДокументНачисленияЗарплаты(ДокументСсылка, МесяцНачисления, Организация, Подразделение = Неопределено) Экспорт
	РасчетЗарплатыБазовый.ПодобратьДокументНачисленияЗарплаты(ДокументСсылка, МесяцНачисления, Организация, Подразделение);
КонецПроцедуры

Функция ОснованиеИсчисленияНалогаСОтсроченнойУплатой(Основания) Экспорт

	Возврат РасчетЗарплатыБазовый.ОснованиеИсчисленияНалогаСОтсроченнойУплатой(Основания)	

КонецФункции 

#Область КорректировкиВыплаты

Процедура ФормаПодробнееОРасчетеНДФЛПриЗаполнении(Форма) Экспорт
	РасчетЗарплатыФормы.ФормаПодробнееОРасчетеНДФЛПриЗаполнении(Форма);
КонецПроцедуры

// Дополняет структуру данных для проведения таблицей КорректировкиВыплаты.
//
Процедура ЗаполнитьДанныеКорректировкиВыплаты(ДанныеДляПроведения, ДокументСсылка, ФизическиеЛица = Неопределено) Экспорт
	РасчетЗарплатыБазовый.ЗаполнитьДанныеКорректировкиВыплаты(ДанныеДляПроведения, ДокументСсылка, ФизическиеЛица);
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция РезультатНачисленияРасчетаЗарплаты(Организация, ДатаНачала, ДатаОкончания, МесяцНачисления, Документ, Подразделение, Сотрудники) Экспорт
	Возврат РасчетЗарплатыБазовый.РезультатНачисленияРасчетаЗарплаты(Организация, НачалоМесяца(ДатаНачала), Документ, Подразделение, Сотрудники);
КонецФункции

// Конструирует объект для хранения данных для проведения.
// Структура может содержать
//		НачисленияПоСотрудникам - таблица значений
//			ФизическоеЛицо.
//			Сотрудник
//			Подразделение
//			Начисление
//			Сумма
//			ОтработаноДней
//			ОтработаноЧасов
//
//		УдержанияПоСотрудникам - таблица значений
//			ФизическоеЛицо.
//			Удержание
//			Сумма
//
//		ИсчисленныйНДФЛ - таблица значений.
//
//		ИсчисленныеВзносы - таблица значений.
//
//		МенеджерВременныхТаблиц - менеджер временных таблиц на котором могут 
//		удерживаться таблицы
//			ВТНачисления (данные о начисленных суммах).
//				Сотрудник
//				ПериодДействия
//				ДатаНачала
//				Начисление
//				СуммаДохода
//				СуммаВычетаНДФЛ
//				СуммаВычетаВзносы
//				КодВычетаНДФЛ
//				Подразделение
//			ВТФизическиеЛица (список людей по которым выполняется расчет)
//				ФизическоеЛицо.
//
Функция СоздатьДанныеДляПроведенияНачисленияЗарплаты() Экспорт
	
	Возврат РасчетЗарплатыБазовый.СоздатьДанныеДляПроведенияНачисленияЗарплаты();
	
КонецФункции

Процедура ЗаполнитьНачисления(ДанныеДляПроведенияНачисленияЗарплаты, Документ, ТаблицаНачислений, ПолеДатыДействия, ПолеВидаНачисления = Неопределено, ПолеВидаНачисленияВШапке = Неопределено, ФизическиеЛица = Неопределено) Экспорт
	РасчетЗарплатыБазовый.ЗаполнитьНачисления(ДанныеДляПроведенияНачисленияЗарплаты, Документ, ТаблицаНачислений, ПолеДатыДействия, ФизическиеЛица);
КонецПроцедуры

// Заполняет данные для проведения удержаниями.
//	
// Параметры:	
// 		ДанныеДляПроведенияНачисленияЗарплаты.
//		Документ
//		ТаблицаУдержаний - имя табличной части с удержаниями, не обязательно, по умолчанию - "Удержания".
//
Процедура ЗаполнитьУдержания(ДанныеДляПроведенияНачисленияЗарплаты, Документ, ТаблицаУдержаний = "Удержания", ФизическиеЛица = Неопределено) Экспорт
	РасчетЗарплатыБазовый.ЗаполнитьУдержания(ДанныеДляПроведенияНачисленияЗарплаты, Документ, ТаблицаУдержаний, ФизическиеЛица = Неопределено);
КонецПроцедуры

// Дополняет структуру данных для проведения таблицей НДФЛ.
//
Процедура ЗаполнитьДанныеНДФЛ(ДанныеДляПроведения, ДокументСсылка, ФизическиеЛица = Неопределено) Экспорт
	РасчетЗарплатыБазовый.ЗаполнитьДанныеНДФЛ(ДанныеДляПроведения, ДокументСсылка, ФизическиеЛица);
КонецПроцедуры

// Дополняет структуру данных для проведения таблицей страховых взносов.
//
Процедура ЗаполнитьДанныеСтраховыхВзносов(ДанныеДляПроведения, ДокументСсылка, ФизическиеЛица) Экспорт
	РасчетЗарплатыБазовый.ЗаполнитьДанныеСтраховыхВзносов(ДанныеДляПроведения, ДокументСсылка, ФизическиеЛица);
КонецПроцедуры

Функция ПолучитьБазуУдержанийПоУмолчанию(ФизическиеЛица, МесяцНачисления, Организация) Экспорт
	Возврат РасчетЗарплатыБазовый.ПолучитьБазуУдержанийПоУмолчанию(ФизическиеЛица, МесяцНачисления, Организация);
КонецФункции

Процедура СформироватьПланВидовРасчетаПоНастройкам(НачальноеЗаполнение = Ложь) Экспорт
	РасчетЗарплатыБазовый.СформироватьПланВидовРасчетаПоНастройкам();
КонецПроцедуры

Процедура СформироватьВидыРасчетаРКиСН() Экспорт
	РасчетЗарплатыБазовый.СформироватьВидыРасчетаРКиСН();
КонецПроцедуры

Процедура СоздатьВТПорядокПредопределенныхНачисленийУдержаний(МенеджерВременныхТаблиц, ТолькоРазрешенные) Экспорт

	РасчетЗарплатыБазовый.СоздатьВТПорядокПредопределенныхНачисленийУдержаний(МенеджерВременныхТаблиц, ТолькоРазрешенные);

КонецПроцедуры

Процедура ЗаполнитьДокументНачисленияЗарплаты(ДанныеНачисленияЗарплаты) Экспорт
	РасчетЗарплатыБазовый.ЗаполнитьДокументНачисленияЗарплаты(ДанныеНачисленияЗарплаты);
КонецПроцедуры	

Процедура УстановитьВходимостьНачисленийВБазуРКИСН() Экспорт
	
	РасчетЗарплатыБазовый.УстановитьВходимостьНачисленийВБазуРКИСН();
	
КонецПроцедуры

Процедура УстановитьСпособРасчетаАвансаСотрудников() Экспорт
	
	РасчетЗарплатыБазовый.УстановитьСпособРасчетаАвансаСотрудников();
	
КонецПроцедуры

Процедура СформироватьВидыРасчетаБольничныхОтпусковИсполнительныхЛистов() Экспорт
	
	РасчетЗарплатыБазовый.СформироватьВидыРасчетаБольничныхОтпусковИсполнительныхЛистов();
	
КонецПроцедуры

Процедура УстановитьЗачетОтработанногоВремениНачислений() Экспорт
	
	РасчетЗарплатыБазовый.УстановитьЗачетОтработанногоВремениНачислений();
	
КонецПроцедуры

Процедура ЗаполнитьОтработанноеВремяОтсутствий() Экспорт
	
	РасчетЗарплатыБазовый.ЗаполнитьОтработанноеВремяОтсутствий();
	
КонецПроцедуры

Процедура ЗаполнитьДатуНачалаРегистраНакопленияОтработанноеВремяПоСотрудникам() Экспорт
	
	РасчетЗарплатыБазовый.ЗаполнитьДатуНачалаРегистраНакопленияОтработанноеВремяПоСотрудникам();
	
КонецПроцедуры

Процедура ЗаполнитьПланируемыеДатыВыплатыОтпусковИБольничныхЛистов() Экспорт
	
	РасчетЗарплатыБазовый.ЗаполнитьПланируемыеДатыВыплатыОтпусковИБольничныхЛистов();
	
КонецПроцедуры

Процедура ЗаполнитьТаблицыКорректировкиВыплаты() Экспорт
	РасчетЗарплатыБазовый.ЗаполнитьТаблицыКорректировкиВыплаты();
КонецПроцедуры

Процедура ЗаполнитьДатыВыплатыВНачисленияхЗарплаты() Экспорт
	
	РасчетЗарплатыБазовый.ЗаполнитьДатыВыплатыВНачисленияхЗарплаты();
	
КонецПроцедуры

#КонецОбласти
