﻿
#Область ПрограммныйИнтерфейс

Функция СтавкаНДСПоУмолчанию(ПлательщикНДС) Экспорт
	
	Если ПлательщикНДС Тогда
		Возврат ПредопределенноеЗначение("Перечисление.СтавкиНДС.НДС18");
	Иначе
		Возврат ПредопределенноеЗначение("Перечисление.СтавкиНДС.БезНДС");
	КонецЕсли;

КонецФункции

// Рассчитывает сумму НДС исходя из суммы и флагов налогообложения
//
// Параметры: 
//  Сумма            - число, сумма от которой надо рассчитывать налоги, 
//  СуммаВключаетНДС - булево, признак включения НДС в сумму ("внутри" или "сверху"),
//  СтавкаНДС        - число , процентная ставка НДС,
//
// Возвращаемое значение:
//  Число, полученная сумма НДС
//
Функция РассчитатьСуммуНДС(Сумма, СуммаВключаетНДС, СтавкаНДС) Экспорт

	Если СуммаВключаетНДС Тогда
		СуммаБезНДС = 100 * Сумма / (100 + СтавкаНДС);
		СуммаНДС = Сумма - СуммаБезНДС;
	Иначе
		СуммаБезНДС = Сумма;
	КонецЕсли;

	Если НЕ СуммаВключаетНДС Тогда
		СуммаНДС = СуммаБезНДС * СтавкаНДС / 100;
	КонецЕсли;
	
	Возврат СуммаНДС;

КонецФункции // РассчитатьСуммуНДС()

// Производит пересчет цен при изменении флагов учета налогов.
// Пересчет зависит от способа заполнения цен, при заполнении По ценам номенклатуры (при продаже) 
// хочется избегать ситуаций, когда компания  «теряет деньги» при пересчете налогов. 
// Поэтому если в документе флаг "Учитывать налог" выключен, то цены должны браться напрямую из справочника, 
// потому что хочется продавать по той же цене, независимо от режима налогообложения. 
// Например, если отпускная цена задана с НП для избежания ошибок округления, то это не значит, 
// что при отпуске без НП мы должны продать дешевле. Если же флаг учета налога в документе включен, 
// то цены должны пересчитываться при подстановке в документ: 
// налог должен включаться или не включаться в зависимости от флага включения налога в типе цен.
// При заполнении по ценам контрагентов (при покупке) хочется хранить цены поставщиков. 
// Поэтому нужно пересчитывать всегда по установленным флагам в документе и в типе цен. 
// Это гарантирует, что при записи цен в регистр и последующем их чтении, 
// например, при заполнении следующего документа, мы с точностью до ошибок округления при пересчете 
// получим те же самые цены.
//
// Параметры: 
//  Цена                - число, пересчитываемое значение цены, 
//  ЦенаВключаетНДС     - булево, определяет содержит ли переданное значение цены НДС,
//  СуммаВключаетНДС    - булево, определяет должно ли новое значение цены включать НДС,
//  СтавкаНДС           - число, ставка НДС, 
//
// Возвращаемое значение:
//  Число, новое значение цены.
//
Функция ПересчитатьЦенуПриИзмененииФлаговНалогов(Цена, ЦенаВключаетНДС, СуммаВключаетНДС, СтавкаНДС) Экспорт

	// Инициализация переменных
	НадоВключитьНДС  = Ложь;
	НадоИсключитьНДС = Ложь;
	НоваяЦена		 = Цена;

	Если СуммаВключаетНДС
		И (НЕ ЦенаВключаетНДС) Тогда
		
		// Надо добавлять НДС       
		НадоВключитьНДС = Истина;
	ИначеЕсли (НЕ СуммаВключаетНДС)
		И ЦенаВключаетНДС  Тогда
		
		// Надо исключать НДС       
		НадоИсключитьНДС = Истина;
	КонецЕсли;
		
	Если НадоИсключитьНДС Тогда
		НоваяЦена = (НоваяЦена * 100) / (100 + СтавкаНДС);
	КонецЕсли;

	Если НадоВключитьНДС Тогда
		НоваяЦена = (НоваяЦена * (100 + СтавкаНДС)) / 100;
	КонецЕсли;

	Возврат НоваяЦена;

КонецФункции // ПересчитатьЦенуПриИзмененииФлаговНалогов()

// Возвращает номер версии подсистемы НДС.
//
Функция Версия(Дата) Экспорт
	
	Если Дата < '20120101' Тогда
		Возврат 1;
	Иначе
		Возврат 2;
	КонецЕсли;
	
КонецФункции

// Определяет версию перечня кодов видов операций для отчетности по НДС на переданную дату
//
// Параметры:
//   Период - Дата - дата, на которую требуется определить версию перечня видов операций
// Возвращаемое значение:
//  Число - номер версии кодов видов операций
//          1 - перечень, утвержденный приказом ФНС 14.02.2012 N ММВ-7-3/83@
//          2 - перечень, утвержденный письмом ФНС от 22.01.2015 N ГД-4-3/794@
//          3 - перечень, утвержденный приказом ФНС от 14.03.2016 N ММВ-7-3/136@
Функция ВерсияКодовВидовОпераций(Период) Экспорт
	
	Если Период >= '20160701' Тогда
		// С 1 июля 2016 года действует перечень, 
		// утвержденный приказом ФНС от 14.03.2016 N ММВ-7-3/136@
		Возврат 3;
	ИначеЕсли Период >= '20150101' Тогда
		// С 1 января 2015 года действует перечень,
		// утвержденный письмом ФНС от 22.01.2015 N ГД-4-3/794@
		Возврат 2;
	Иначе
		// До 1 января 2015 года действует перечень,
		// утвержденный приказом ФНС 14.02.2012 N ММВ-7-3/83@
		Возврат 1;
	КонецЕсли;
	
КонецФункции

// Заполнение реквизитов формы в документе - основании выданного счета-фактуры.

Процедура ЗаполнитьРеквизитыФормыПроСчетФактуруВыданный(Форма, РеквизитыСФ = Неопределено, ТребуетсяВсегда = Ложь, СтруктураОтбора = Неопределено, ИмяРеквизитаСчетФактура = "СчетФактура", ИмяРеквизитаСсылка = "Ссылка") Экспорт
	
	Если РеквизитыСФ <> Неопределено Тогда
		ИсходныеДанные = РеквизитыСФ;
	ИначеЕсли ЗначениеЗаполнено(Форма[ИмяРеквизитаСчетФактура]) Тогда
		ИсходныеДанные = Форма[ИмяРеквизитаСчетФактура];
	Иначе
		ИсходныеДанные = Форма.Объект[ИмяРеквизитаСсылка];
	КонецЕсли;
	
	Если ТребуетсяВсегда Тогда
		ТребуетсяСчетФактура = Истина;
	Иначе
		ТребуетсяСчетФактура = Форма.ТребуетсяСчетФактура;
	КонецЕсли;
	
	ДанныеНадписи = ДанныеОСчетеФактуре(
		ИсходныеДанные,
		"Выданный",
		ТребуетсяСчетФактура,
		СтруктураОтбора);
		
	Форма[ИмяРеквизитаСчетФактура]             = ДанныеНадписи.СчетФактура;
	Форма["Надпись" + ИмяРеквизитаСчетФактура] = ДанныеНадписи.НадписьСчетФактура;
	
КонецПроцедуры

Процедура ЗаполнитьРеквизитыФормыПроСчетФактуруПолученный(Форма, РеквизитыСФ = Неопределено, ТребуетсяСчетФактура = Истина, СтруктураОтбора = Неопределено, ИмяРеквизитаСчетФактура = "СчетФактура", ИмяРеквизитаСсылка = "Ссылка") Экспорт
	
	Если РеквизитыСФ <> Неопределено Тогда
		ИсходныеДанные = РеквизитыСФ;
	ИначеЕсли ЗначениеЗаполнено(Форма[ИмяРеквизитаСчетФактура]) Тогда
		ИсходныеДанные = Форма[ИмяРеквизитаСчетФактура];
	Иначе
		ИсходныеДанные = Форма.Объект[ИмяРеквизитаСсылка];
	КонецЕсли;
	
	ДанныеНадписи = ДанныеОСчетеФактуре(ИсходныеДанные, "Полученный", ТребуетсяСчетФактура, СтруктураОтбора);
		
	Форма[ИмяРеквизитаСчетФактура]             = ДанныеНадписи.СчетФактура;
	Форма["Надпись" + ИмяРеквизитаСчетФактура] = ДанныеНадписи.НадписьСчетФактура;
		
КонецПроцедуры

Процедура ДополнитьПараметрыСобытияЗаписьСчетаФактуры(ПараметрыЗаписи) Экспорт
	
	Если ТипЗнч(ПараметрыЗаписи) <> Тип("Структура") Тогда
		ПараметрыЗаписи = Новый Структура;
	КонецЕсли;
	
	Если Не ПараметрыЗаписи.Свойство("ДокументыОснования") Тогда
		ПараметрыЗаписи.Вставить("ДокументыОснования", Новый Массив);
	КонецЕсли;
	
	Если Не ПараметрыЗаписи.Свойство("РеквизитыСФ") Тогда
		ПараметрыЗаписи.Вставить("РеквизитыСФ", Неопределено);
	КонецЕсли;
	
КонецПроцедуры

Функция НовыеПараметрыСозданияСчетаФактуры() Экспорт
	
	ПараметрыСоздания = Новый Структура;
	ПараметрыСоздания.Вставить("Основание");
	ПараметрыСоздания.Вставить("Ссылка");
	ПараметрыСоздания.Вставить("РеквизитыСФ");
	ПараметрыСоздания.Вставить("ВыписыватьСчетаФактурыСпецРежимы");
	ПараметрыСоздания.Вставить("ОткрытьФормуОшибки", Ложь);
	ПараметрыСоздания.Вставить("АдресХранилищаСОшибками", "");
	
	Возврат ПараметрыСоздания;
	
КонецФункции

// Документы по учету НДС для передачи в электронном виде

Функция ПолучитьКодПоСКНП(Период, Реорганизация = Ложь) Экспорт 
	
	Мес = Цел((Месяц(Период) - 1)/3);
	
	Если Реорганизация Тогда
		Если Мес = 0 Тогда
			Возврат "51";
		ИначеЕсли Мес = 1 Тогда 
			Возврат "54";
		ИначеЕсли Мес = 2 Тогда 
			Возврат "55";
		ИначеЕсли Мес = 3 Тогда 
			Возврат "56";
		КонецЕсли;
	Иначе
		Если Мес = 0 Тогда
			Возврат "21";
		ИначеЕсли Мес = 1 Тогда 
			Возврат "22";
		ИначеЕсли Мес = 2 Тогда 
			Возврат "23";
		ИначеЕсли Мес = 3 Тогда 
			Возврат "24";
		КонецЕсли;	
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

Функция СтавкаНДСВДоговореКонтрагента(ВидДоговора, ПредъявляетНДС, УчетАгентскогоНДС) Экспорт
	
	Если УчетАгентскогоНДС Тогда
		Возврат ПредопределенноеЗначение("Перечисление.СтавкиНДС.ПустаяСсылка");
	ИначеЕсли НЕ ПредъявляетНДС 
		И (ВидДоговора = ПредопределенноеЗначение("Перечисление.ВидыДоговоровКонтрагентов.СПоставщиком")
		ИЛИ ВидДоговора = ПредопределенноеЗначение("Перечисление.ВидыДоговоровКонтрагентов.СКомитентом")
		ИЛИ ВидДоговора = ПредопределенноеЗначение("Перечисление.ВидыДоговоровКонтрагентов.СКомиссионеромНаЗакупку")
		ИЛИ ВидДоговора = ПредопределенноеЗначение("Перечисление.ВидыДоговоровКонтрагентов.СКомиссионером")) Тогда
		Возврат ПредопределенноеЗначение("Перечисление.СтавкиНДС.БезНДС");
	Иначе
		Возврат ПредопределенноеЗначение("Перечисление.СтавкиНДС.НДС18");
	КонецЕсли;
		
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// ИсходныеДанные - Структура, содержащая информацию о счете-фактуре см. УчетНДСВызовСервера.РеквизитыДляНадписиОСчетеФактуреВыданном()
//              или ДокументСсылка.СчетФактураВыданный 
//              или ДокументСсылка - основание счета-фактуры
//
// ВидСчетаФактуры - Строка - "Выданный" или "Полученный"
Функция ДанныеОСчетеФактуре(Знач ИсходныеДанные, ВидСчетаФактуры, ТребуетсяСФ, СтруктураОтбора = Неопределено)
	
	ДанныеНадписи = Новый Структура;
	ДанныеНадписи.Вставить("СчетФактура",        Неопределено);
	ДанныеНадписи.Вставить("НадписьСчетФактура", "");
	
	Если НЕ ТребуетсяСФ Тогда
		
		ДанныеНадписи.НадписьСчетФактура = НСтр("ru='Не требуется'");
		Возврат ДанныеНадписи;
		
	КонецЕсли;
	
	РеквизитыСФ = Неопределено;
	Если ТипЗнч(ИсходныеДанные) = Тип("Структура") Тогда
		
		РеквизитыСФ = ИсходныеДанные;
		
	ИначеЕсли ЗначениеЗаполнено(ИсходныеДанные) Тогда
		
		Если ВидСчетаФактуры = "Выданный" Тогда
			РеквизитыСФ = УчетНДСВызовСервера.РеквизитыДляНадписиОСчетеФактуреВыданном(ИсходныеДанные, СтруктураОтбора);
		Иначе
			РеквизитыСФ = УчетНДСВызовСервера.РеквизитыДляНадписиОСчетеФактуреПолученном(ИсходныеДанные, СтруктураОтбора);
		КонецЕсли;
			
	КонецЕсли;
		
	Если РеквизитыСФ = Неопределено Тогда
		ДанныеНадписи.Вставить("НадписьСчетФактура", НСтр("ru='Ввести счет-фактуру'"));
	Иначе
		ДанныеНадписи.Вставить("СчетФактура",        РеквизитыСФ.Ссылка);
		ДанныеНадписи.Вставить("НадписьСчетФактура", РеквизитыСФ.КраткоеПредставление);
	КонецЕсли;
	
	Возврат ДанныеНадписи;
		
КонецФункции

Функция ПредставлениеСсылкиПредмета(Предмет, СигнатураПредмета, ПервыйПредметСписка, КоличествоПредметов, ФорматнаяСтрока = Неопределено) Экспорт
	
	Результат = "";
	
	Если Не ЗначениеЗаполнено(Предмет) Или Не ЗначениеЗаполнено(СигнатураПредмета)
		Или Не ЗначениеЗаполнено(ПервыйПредметСписка) Или Не ЗначениеЗаполнено(КоличествоПредметов) Тогда
		
		Возврат Результат;
		
	КонецЕсли;
	
	Если ФорматнаяСтрока = Неопределено Тогда
		ФорматнаяСтрока  = "Л = ru_RU; ЧДЦ=0";
	КонецЕсли;
	
	ПрописьЧисла   = ЧислоПрописью(КоличествоПредметов, ФорматнаяСтрока, Предмет);
	ИндексПредмета = СтрНайти(ПрописьЧисла, СигнатураПредмета);
	ТекстДокументы = Строка(КоличествоПредметов) + " "
		+ Сред(ПрописьЧисла, ИндексПредмета, СтрДлина(ПрописьЧисла)- ИндексПредмета - 3);
		
	Результат = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = '%1 (%2 и еще %3)'"),
		ТекстДокументы, 
		Строка(ПервыйПредметСписка),
		КоличествоПредметов - 1);
		
	Возврат Результат;
	
КонецФункции

#Область УправлениеФормой

// Настраивает поля формы, отображающие сведения о счете-фактуре.
//  Если документ позволяет отражать операций комиссии по закупке на стороне комитента, необходимо вызвать
//  НастроитьПолеПродавецПоСчетуФактуре() для управления видимостью поля формы с данными о продавце.
//
// Параметры:
//  Кнопка               - ГруппаФормы - настраиваемый элемент формы, содержащий кнопку создания счета-фактуры. Обычно имеет имя СчетФактураКнопка.
//  Ссылка               - ГруппаФормы - настраиваемый элемент формы, группа, содержащая ссылку на счет-фактуру. Обычно имеет имя СчетФактураСсылка.
//  Надпись              - ПолеФормы   - настраиваемый элемент формы, отображающий ссылку на счет-фактуру или надпись о том,
//                                       что счет-фактура не требуется. Обычно имеет имя НадписьСчетФактура.
//  ДокументБезНДС       - Булево      - Истина, если НДС в документе не может быть выделен или выделение НДС не имеет смысла
//                                       (применимо для большинства операций организации - не плательщика НДС).
//  ТребуетсяСчетФактура - Булево      - Ложь, если характер операции плательщика НДС не предполагает регистрации счета-фактуры.
//  СчетФактура          - ДокументСсылка.СчетФактураВыданный, ДокументСсылка.СчетФактураПолученный- 
//                                       счет-фактура, относящийся к первичному документу.
//
Процедура НастроитьПоляСчетаФактуры(Кнопка, Ссылка, Надпись, ДокументБезНДС, ТребуетсяСчетФактура, СчетФактура) Экспорт
	
	Если ДокументБезНДС Тогда
		// Ничего не показываем
		Кнопка.Видимость  = Ложь;
		Ссылка.Видимость = Ложь;
	ИначеЕсли Не ЗначениеЗаполнено(СчетФактура) И ТребуетсяСчетФактура Тогда
		// Показываем кнопку ввода нового счета-фактуры
		Кнопка.Видимость  = Истина;
		Ссылка.Видимость = Ложь;
	Иначе
		// Показываем ссылку на существующий счет-фактуру или текст о том, что она не требуется
		Кнопка.Видимость  = Ложь;
		Ссылка.Видимость = Истина;
		
		Надпись.Гиперссылка = ТребуетсяСчетФактура;
		// Текст надписи выводится в УчетНДСКлиентСервер.ЗаполнитьРеквизитыФормыПроСчетФактуруВыданный()
		
	КонецЕсли;
	
КонецПроцедуры

// Настраивает поле формы, отображающие сведения о продавце.
//  Процедура вызывается после НастроитьПоляСчетаФактуры() если документ позволяет отражать операций комиссии по закупке на стороне комитента.
//
// Параметры:
//  Продавец    - ПолеФормы - настраиваемый элемент формы, поле ввода для выбора продавца. Обычно имеет имя Продавец.
//  ЭтоКомиссия - Булево    - Истина, если документом регистрируется операция по договору комиссии на закупку.
//
Процедура НастроитьПолеПродавецПоСчетуФактуре(Продавец, ЭтоКомиссия) Экспорт
	
	Продавец.Видимость = ЭтоКомиссия;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти




