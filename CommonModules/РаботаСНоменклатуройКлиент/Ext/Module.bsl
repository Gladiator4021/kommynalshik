﻿////////////////////////////////////////////////////////////////////////////////
// Работа с номенклатурой.
//
////////////////////////////////////////////////////////////////////////////////

#Область СлужебныеПроцедурыИФункции

#Область СозданиеНовыхОбъектовПриВводе

Процедура НоменклатураАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка) Экспорт
	
	Если НЕ ПустаяСтрока(Текст) 
		И (НЕ ЗначениеЗаполнено(Элемент.ОграничениеТипа)
		ИЛИ Элемент.ОграничениеТипа.СодержитТип(Тип("СправочникСсылка.Номенклатура"))) Тогда
		СтандартнаяОбработка = Ложь;
		ДанныеВыбора = РаботаСНоменклатуройВызовСервера.ДанныеВыбораНоменклатуры(ПараметрыПолученияДанных, Ложь);
	КонецЕсли;
	
КонецПроцедуры

Процедура НоменклатураОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, СтандартнаяОбработка) Экспорт
	
	Если НЕ ПустаяСтрока(Текст) 
		И (НЕ ЗначениеЗаполнено(Элемент.ОграничениеТипа)
		ИЛИ Элемент.ОграничениеТипа.СодержитТип(Тип("СправочникСсылка.Номенклатура"))) Тогда
		СтандартнаяОбработка = Ложь;
		ДанныеВыбора = РаботаСНоменклатуройВызовСервера.ДанныеВыбораНоменклатуры(ПараметрыПолученияДанных, Истина);
	КонецЕсли;
	
КонецПроцедуры

Процедура НоменклатураОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка) Экспорт
	
	Если ТипЗнч(ВыбранноеЗначение) = Тип("СправочникСсылка.ВидыНоменклатуры") Тогда
		
		СтандартнаяОбработка = Ложь;
		Наименование = Элемент.ТекстРедактирования;
		
		ПараметрыСоздания = Новый Структура;
		ПараметрыСоздания.Вставить("ЗначенияЗаполнения", ЗначенияЗаполненияПоПараметрамВыбора(Элемент.ПараметрыВыбора));
		
		ДополнительныеПараметрыСоздания = Новый Структура;
		ДополнительныеПараметрыСоздания.Вставить("Наименование", Наименование);
		ПараметрыСоздания.Вставить("ДополнительныеПараметрыСоздания", ДополнительныеПараметрыСоздания);
		
		ВыбранноеЗначение = РаботаСНоменклатуройВызовСервера.СоздатьНоменклатуруБыстро(
			ВыбранноеЗначение, ПараметрыСоздания);
		СтандартнаяОбработка = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

Функция ЗначенияЗаполненияПоПараметрамВыбора(ПараметрыВыбора)
	
	ВременнаяСтруктура = Новый Структура;
	ДобавитьПараметрыВыбораВСтруктуру(ВременнаяСтруктура, ПараметрыВыбора);
	
	ЗначенияЗаполнения = Новый Структура;
	Для каждого КлючИЗначение Из ВременнаяСтруктура.Отбор Цикл
		ЗначенияЗаполнения.Вставить(КлючИЗначение.Ключ, КлючИЗначение.Значение);
	КонецЦикла;
	
	Возврат ЗначенияЗаполнения;
	
КонецФункции

Процедура ДобавитьПараметрыВыбораВСтруктуру(Структура, ПараметрыВыбора)
	
	Если НЕ Структура.Свойство("Отбор") Тогда
		Структура.Вставить("Отбор", Новый Структура);
	КонецЕсли;
	
	Для каждого ПараметрВыбора Из ПараметрыВыбора Цикл
		Если Найти(ПараметрВыбора.Имя, "Отбор.") <> 0 Тогда
			Структура.Отбор.Вставить(СтрЗаменить(ПараметрВыбора.Имя, "Отбор.", ""), ПараметрВыбора.Значение);
		ИначеЕсли Найти(ПараметрВыбора.Имя, ".") = 0 Тогда
			Структура.Вставить(ПараметрВыбора.Имя, ПараметрВыбора.Значение);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти