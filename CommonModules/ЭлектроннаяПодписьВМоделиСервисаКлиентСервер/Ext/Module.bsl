﻿////////////////////////////////////////////////////////////////////////////////
// Подсистема "Электронная подпись в модели сервиса".
//  
////////////////////////////////////////////////////////////////////////////////


#Область ПрограммныйИнтерфейс

Функция ИспользованиеВозможно() Экспорт
	
	#Если Клиент Тогда
		Возврат СтандартныеПодсистемыКлиентПовтИсп.ПараметрыРаботыКлиента()["ИспользованиеЭлектроннойПодписиВМоделиСервисаВозможно"];
	#Иначе
		Возврат ЭлектроннаяПодписьВМоделиСервиса.ИспользованиеВозможно();		
	#КонецЕсли
	
КонецФункции

#КонецОбласти	