﻿
#Область ПрограммныйИнтерфейс

Процедура СоздатьИзменитьНастройкуПлатежаВБюджет(РеквизитыОбъекта, Настройки, РеквизитыВБюджетПоУмолчанию) Экспорт
	
	МенеджерЗаписи = РегистрыСведений.РеквизитыУплатыНалоговИПлатежейВБюджет.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Настройки.КлючНастройкиЗаполнения);
	МенеджерЗаписи.Прочитать();
	
	Если НЕ МенеджерЗаписи.Выбран() Тогда
		ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Настройки.КлючНастройкиЗаполнения);
	КонецЕсли;
	
	Если Настройки.ИзмененныеНастройки.СтатусСоставителя Тогда
		МенеджерЗаписи.СтатусСоставителя         = РеквизитыОбъекта.СтатусСоставителя;
	Иначе
		МенеджерЗаписи.СтатусСоставителя         = РеквизитыВБюджетПоУмолчанию.СтатусСоставителя;
	КонецЕсли;
	
	Если РеквизитыВБюджетПоУмолчанию.Свойство("ОчередностьПлатежа") Тогда
		МенеджерЗаписи.ОчередностьПлатежа        = РеквизитыВБюджетПоУмолчанию.ОчередностьПлатежа;
	Иначе
		МенеджерЗаписи.ОчередностьПлатежа        = УчетДенежныхСредствКлиентСервер.ОчередностьПлатежаНалогиВзносы();
	КонецЕсли;
	
	МенеджерЗаписи.ВидПеречисленияВБюджет        = Настройки.КлючНастройкиЗаполнения.ВидПеречисленияВБюджет;
	МенеджерЗаписи.СтатьяДвиженияДенежныхСредств = РеквизитыОбъекта.СтатьяДвиженияДенежныхСредств;
	
	Если Настройки.ИзмененныеНастройки.ПоказательПериода Тогда
		Если Настройки.КлючНастройкиЗаполнения.ВидПеречисленияВБюджет = Перечисления.ВидыПеречисленийВБюджет.ТаможенныйПлатеж Тогда
			МенеджерЗаписи.ПоказательПериода = РеквизитыОбъекта.ПоказательПериода;
		Иначе
			ПериодичностьПлатежаВБюджет = ПлатежиВБюджетКлиентСервер.РазобратьНалоговыйПериод(РеквизитыОбъекта.ПоказательПериода);
			МенеджерЗаписи.ПоказательПериода = ПериодичностьПлатежаВБюджет.Периодичность;
		КонецЕсли;
	Иначе
		ПериодичностьПлатежаВБюджет = ПлатежиВБюджетКлиентСервер.РазобратьНалоговыйПериод(РеквизитыВБюджетПоУмолчанию.ПоказательПериода);
		МенеджерЗаписи.ПоказательПериода = ПериодичностьПлатежаВБюджет.Периодичность;
	КонецЕсли;
	
	Если Настройки.ИзмененныеНастройки.РеквизитыПолучателя.ОчиститьНастройкиПолучателя Тогда
		МенеджерЗаписи.Получатель     = Справочники.Контрагенты.ПустаяСсылка();
		МенеджерЗаписи.СчетПолучателя = Справочники.БанковскиеСчета.ПустаяСсылка();
	ИначеЕсли Настройки.ИзмененныеНастройки.РеквизитыПолучателя.Получатель
		ИЛИ Настройки.ИзмененныеНастройки.РеквизитыПолучателя.СчетПолучателя Тогда
		МенеджерЗаписи.Получатель     = РеквизитыОбъекта.Контрагент;
		МенеджерЗаписи.СчетПолучателя = РеквизитыОбъекта.СчетКонтрагента;
	КонецЕсли;
	
	Попытка
		МенеджерЗаписи.Записать();
	Исключение
		// фиксация исключения не требуется
	КонецПопытки;
	
КонецПроцедуры

Функция РеквизитыПлатежногоДокумента() Экспорт
	
	РеквизитыОбъекта = Новый Структура;
	РеквизитыОбъекта.Вставить("Дата");
	РеквизитыОбъекта.Вставить("Организация");
	РеквизитыОбъекта.Вставить("Налог");
	РеквизитыОбъекта.Вставить("ВидОперации");
	РеквизитыОбъекта.Вставить("ВидПеречисленияВБюджет");
	РеквизитыОбъекта.Вставить("Контрагент");
	РеквизитыОбъекта.Вставить("СчетКонтрагента");
	РеквизитыОбъекта.Вставить("СтатьяДвиженияДенежныхСредств");
	РеквизитыОбъекта.Вставить("СтатусСоставителя");
	РеквизитыОбъекта.Вставить("ПоказательПериода");
	
	Возврат РеквизитыОбъекта;
	
КонецФункции

Функция ПроверитьНастройкуПлатежаВБюджет(РеквизитыОбъекта, РеквизитыВБюджетПоУмолчанию, РегистрацияВНалоговомОргане) Экспорт
	
	// Для того чтобы понять, нужно ли нам создать/изменить настройку, требуется получить:
	//  - значения из настройки, если она есть и значение в ней заполнено;
	//  - значения "по умолчанию", если настройки нет, или значение в ней не заполнено.
	
	ПоддерживаемыйНалог     = ЗначениеЗаполнено(РеквизитыОбъекта.Налог.ВидНалога);
	
	НастройкаЗаполнения     = Неопределено;
	КлючНастройкиЗаполнения = РегистрыСведений.РеквизитыУплатыНалоговИПлатежейВБюджет.КлючНастройкиУплатыНалога(
		РеквизитыОбъекта.Налог, РеквизитыОбъекта.Организация, РегистрацияВНалоговомОргане);
	Если КлючНастройкиЗаполнения <> Неопределено Тогда
		Если РеквизитыОбъекта.Свойство("ПоказателиПериода") Тогда
			НастройкаЗаполнения = РегистрыСведений.РеквизитыУплатыНалоговИПлатежейВБюджет.ДанныеЗаполнения(
				КлючНастройкиЗаполнения,
				РеквизитыОбъекта.ПоказателиПериода.Период,
				РеквизитыОбъекта.Организация,
				РеквизитыОбъекта.ПоказателиПериода);
		Иначе
			НастройкаЗаполнения = РегистрыСведений.РеквизитыУплатыНалоговИПлатежейВБюджет.ДанныеЗаполнения(
				КлючНастройкиЗаполнения, РеквизитыОбъекта.Дата, РеквизитыОбъекта.Организация);
		КонецЕсли;
	КонецЕсли;
	
	ИзмененныеНастройки     = ОтслеживаемыеНастройки();
	Для каждого КлючИЗначение Из ИзмененныеНастройки Цикл
		Если КлючИЗначение.Ключ = "СтатусСоставителя" Тогда
			ИзмененныеНастройки.Вставить(КлючИЗначение.Ключ, ПроверитьСтатусСоставителя(
				РеквизитыОбъекта, НастройкаЗаполнения, РеквизитыВБюджетПоУмолчанию, ПоддерживаемыйНалог));
		ИначеЕсли КлючИЗначение.Ключ = "ПоказательПериода" Тогда
			ИзмененныеНастройки.Вставить(КлючИЗначение.Ключ, ПроверитьПоказательПериода(
				РеквизитыОбъекта, НастройкаЗаполнения, РеквизитыВБюджетПоУмолчанию, ПоддерживаемыйНалог));
		ИначеЕсли КлючИЗначение.Ключ = "СтатьяДвиженияДенежныхСредств" Тогда
			ИзмененныеНастройки.Вставить(КлючИЗначение.Ключ, ПроверитьСтатьяДДС(РеквизитыОбъекта, НастройкаЗаполнения));
		ИначеЕсли КлючИЗначение.Ключ = "РеквизитыПолучателя" Тогда
			ИзмененныеНастройки.Вставить(КлючИЗначение.Ключ,
				ПроверитьРеквизитыПолучателя(РеквизитыОбъекта, НастройкаЗаполнения, РеквизитыВБюджетПоУмолчанию, КлючИЗначение.Значение));
		КонецЕсли;
	КонецЦикла;
	
	Настройки = Новый Структура;
	Настройки.Вставить("КлючНастройкиЗаполнения",
		Новый Структура("ВидПлатежа, Организация, РегистрацияВНалоговомОргане, ВидПеречисленияВБюджет",
			РеквизитыОбъекта.Налог, РеквизитыОбъекта.Организация, РегистрацияВНалоговомОргане, РеквизитыОбъекта.ВидПеречисленияВБюджет));
	
	Если НЕ НалоговыйУчетОбособленныхПодразделений.УчетВРазрезеНалоговыхОрганов()
		ИЛИ Справочники.РегистрацииВНалоговомОргане.ПолучитьКоличествоПодчиненныхЭлементовПоВладельцу(РеквизитыОбъекта.Организация) = 1 Тогда
		Настройки.КлючНастройкиЗаполнения.РегистрацияВНалоговомОргане = Справочники.РегистрацииВНалоговомОргане.ПустаяСсылка();
	КонецЕсли;
	
	Настройки.Вставить("ИзмененныеНастройки", ИзмененныеНастройки);
	
	Возврат Настройки;
	
КонецФункции

Функция НастройкиИзменились(ИзмененныеНастройки) Экспорт
	
	Для каждого КлючИЗначение Из ИзмененныеНастройки Цикл
		Если ТипЗнч(КлючИЗначение.Значение) = Тип("Структура") Тогда
			Если НастройкиИзменились(КлючИЗначение.Значение) Тогда
				Возврат Истина;
			КонецЕсли;
		Иначе
			Если КлючИЗначение.Значение Тогда
				Возврат Истина;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Ложь;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция РеквизитыПолучателя()
	
	Реквизиты = Новый Структура;
	Реквизиты.Вставить("Получатель",                  Ложь);
	Реквизиты.Вставить("СчетПолучателя",              Ложь);
	Реквизиты.Вставить("ОчиститьНастройкиПолучателя", Ложь);
	
	Возврат Реквизиты;
	
КонецФункции

Функция ОтслеживаемыеНастройки()
	
	Реквизиты = Новый Структура;
	Реквизиты.Вставить("СтатусСоставителя",             Ложь);
	Реквизиты.Вставить("ПоказательПериода",             Ложь);
	Реквизиты.Вставить("СтатьяДвиженияДенежныхСредств", Ложь);
	Реквизиты.Вставить("РеквизитыПолучателя",           РеквизитыПолучателя());
	
	Возврат Реквизиты;
	
КонецФункции

Функция ПроверитьСтатьяДДС(РеквизитыОбъекта, НастройкаЗаполнения)
	
	Если ЗначениеЗаполнено(НастройкаЗаполнения) Тогда
		Результат = РеквизитыОбъекта.СтатьяДвиженияДенежныхСредств <> НастройкаЗаполнения.СтатьяДвиженияДенежныхСредств;
	Иначе
		КонтекстОперации = РеквизитыОбъекта.ВидОперации;
		ВидНалога = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(РеквизитыОбъекта.Налог, "ВидНалога");
		Если ВидНалога = Перечисления.ВидыНалогов.НалогНаПрибыль_РегиональныйБюджет
			ИЛИ ВидНалога = Перечисления.ВидыНалогов.НалогНаПрибыль_ФедеральныйБюджет Тогда
			КонтекстОперации = "НалогНаПрибыль";
		КонецЕсли;
		СтатьяДДСПоУмолчанию = УчетДенежныхСредствБП.СтатьяДДСПоУмолчанию(КонтекстОперации);
		Результат = ЗначениеЗаполнено(РеквизитыОбъекта.СтатьяДвиженияДенежныхСредств)
			И РеквизитыОбъекта.СтатьяДвиженияДенежныхСредств <> СтатьяДДСПоУмолчанию;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

Функция ПроверитьСтатусСоставителя(РеквизитыОбъекта, НастройкаЗаполнения, РеквизитыВБюджетПоУмолчанию, ПоддерживаемыйНалог)
	
	Если НастройкаЗаполнения = Неопределено  Тогда
		Результат = РеквизитыОбъекта.СтатусСоставителя <> РеквизитыВБюджетПоУмолчанию.СтатусСоставителя;
	Иначе
		Результат = РеквизитыОбъекта.СтатусСоставителя <> НастройкаЗаполнения.СтатусСоставителя;
	КонецЕсли;
	
	Если НЕ ПоддерживаемыйНалог Тогда
		Возврат Результат;
	КонецЕсли;
	
	Если ПлатежиВБюджетКлиентСервер.ПлатежАдминистрируетсяПенсионнымФондом(РеквизитыОбъекта.Налог.КодБК)
		И ПлатежиВБюджетКлиентСерверПереопределяемый.ЭтоФиксированныеВзносы(РеквизитыОбъекта.Налог.ВидНалога) Тогда
		Результат = Результат И
			(РеквизитыОбъекта.СтатусСоставителя = ПлатежиВБюджетКлиентСервер.СтатусПлательщикаИныеПлатежи()
			ИЛИ РеквизитыОбъекта.СтатусСоставителя = "24"); // физическое лицо, уплачивающее страховые взносы и иные платежи
	Иначе
		Результат = Ложь;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

Функция ПроверитьПоказательПериода(РеквизитыОбъекта, НастройкаЗаполнения, РеквизитыВБюджетПоУмолчанию, ПоддерживаемыйНалог)
	
	Результат = Ложь;
	Если РеквизитыОбъекта.ВидПеречисленияВБюджет = Перечисления.ВидыПеречисленийВБюджет.ТаможенныйПлатеж Тогда
		// В этом случае в ПоказательПериода хранится Код таможенного органа
		Если ЗначениеЗаполнено(НастройкаЗаполнения) Тогда
			Результат = РеквизитыОбъекта.ПоказательПериода <> НастройкаЗаполнения.ПоказательПериода;
		Иначе
			Результат = ЗначениеЗаполнено(РеквизитыОбъекта.ПоказательПериода);
		КонецЕсли;
	Иначе
		ПериодичностьПлатежаВБюджет = ПлатежиВБюджетКлиентСервер.РазобратьНалоговыйПериод(РеквизитыОбъекта.ПоказательПериода);
		ПериодичностьВДокументе     = ПериодичностьПлатежаВБюджет.Периодичность;
		
		Если ЗначениеЗаполнено(НастройкаЗаполнения) Тогда
			ПериодичностьПлатежаВБюджет = ПлатежиВБюджетКлиентСервер.РазобратьНалоговыйПериод(НастройкаЗаполнения.ПоказательПериода);
			ПериодичностьНалогаТекущая  = ПериодичностьПлатежаВБюджет.Периодичность;
		Иначе
			Если РеквизитыВБюджетПоУмолчанию.ПоказательПериода = ПлатежиВБюджетКлиентСервер.НезаполненноеЗначение() Тогда
				ПериодичностьНалогаТекущая = ПлатежиВБюджетКлиентСервер.НезаполненноеЗначение();
			Иначе
				ПорядокУплатыНалога = РегистрыСведений.ЗадачиБухгалтера.ПорядокУплатыНалогаЗаПериод(
					РеквизитыОбъекта.Организация,
					РеквизитыОбъекта.Налог.ВидНалога,
					РеквизитыОбъекта.Дата);
				Если НЕ ПоддерживаемыйНалог ИЛИ ПорядокУплатыНалога = Неопределено Тогда
					ПериодичностьПлатежаВБюджет = ПлатежиВБюджетКлиентСервер.РазобратьНалоговыйПериод(РеквизитыВБюджетПоУмолчанию.ПоказательПериода);
					ПериодичностьНалогаТекущая  = ПериодичностьПлатежаВБюджет.Периодичность;
				Иначе
					ПериодичностьНалогаТекущая  = ПлатежиВБюджетПереопределяемый.ПериодичностьПоКлассификатору(ПорядокУплатыНалога.Периодичность);
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
		
		Если НЕ ПоддерживаемыйНалог
			ИЛИ ПлатежиВБюджетКлиентСервер.ПлатежАдминистрируетсяФСС(РеквизитыОбъекта.Налог.КодБК)
			И ПлатежиВБюджетКлиентСерверПереопределяемый.ЭтоОбязательныеСтраховыеВзносы(РеквизитыОбъекта.Налог.ВидНалога) Тогда
			Если ПериодичностьНалогаТекущая = ПлатежиВБюджетКлиентСервер.ПериодичностьМесяц()
				ИЛИ ПериодичностьНалогаТекущая = ПлатежиВБюджетКлиентСервер.НезаполненноеЗначение() Тогда
				Результат = ПериодичностьНалогаТекущая <> ПериодичностьВДокументе;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

Функция ПроверитьРеквизитыПолучателя(РеквизитыОбъекта, НастройкаЗаполнения, РеквизитыВБюджетПоУмолчанию, ИзмененныеНастройки)
	
	Реквизиты = РеквизитыПолучателя();
	ЗаполнитьЗначенияСвойств(Реквизиты, ИзмененныеНастройки);
	Если ЗначениеЗаполнено(НастройкаЗаполнения) И ЗначениеЗаполнено(НастройкаЗаполнения.Контрагент) Тогда
		Реквизиты.ОчиститьНастройкиПолучателя = ЗначениеЗаполнено(РеквизитыВБюджетПоУмолчанию.Получатель)
			И РеквизитыВБюджетПоУмолчанию.Получатель = РеквизитыОбъекта.Контрагент
			И ЗначениеЗаполнено(РеквизитыВБюджетПоУмолчанию.СчетПолучателя)
			И РеквизитыВБюджетПоУмолчанию.СчетПолучателя = РеквизитыОбъекта.СчетКонтрагента;
			
		Реквизиты.Получатель     = НЕ Реквизиты.ОчиститьНастройкиПолучателя
			И РеквизитыОбъекта.Контрагент <> НастройкаЗаполнения.Контрагент;
		Реквизиты.СчетПолучателя = НЕ Реквизиты.ОчиститьНастройкиПолучателя
			И (ИзмененныеНастройки.Получатель ИЛИ РеквизитыОбъекта.СчетКонтрагента <> НастройкаЗаполнения.СчетКонтрагента);
	Иначе
		Реквизиты.Получатель     = НЕ ЗначениеЗаполнено(РеквизитыВБюджетПоУмолчанию.Получатель)
			ИЛИ РеквизитыВБюджетПоУмолчанию.Получатель <> РеквизитыОбъекта.Контрагент;
		Реквизиты.СчетПолучателя = ИзмененныеНастройки.Получатель
			ИЛИ НЕ ЗначениеЗаполнено(РеквизитыВБюджетПоУмолчанию.СчетПолучателя)
			ИЛИ РеквизитыВБюджетПоУмолчанию.СчетПолучателя <> РеквизитыОбъекта.СчетКонтрагента;
	КонецЕсли;
	
	Возврат Реквизиты;
	
КонецФункции

#КонецОбласти
