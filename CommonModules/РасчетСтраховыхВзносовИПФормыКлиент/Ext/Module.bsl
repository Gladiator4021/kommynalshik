﻿#Область ПрограммныйИнтерфейс

// Обработка нажатия гиперссылки декорации формы
//
// Параметры:
//   ИмяЭлемента - Строка - имя декорации
//   КоллекцияПлатежей - ДанныеФормыКоллекция
//     * Ссылка - ДокументСсылка - ссылка на объект
//   ПрефиксИмени - Строка - префикс имени декорации (например, "ДекорацияПлатеж")
//
Процедура ПлатежНажатие(ИмяЭлемента, КоллекцияПлатежей, ПрефиксИмени) Экспорт
	
	Индекс = ИндексЭлемента(ИмяЭлемента, ПрефиксИмени);
	Если Индекс = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Индекс < КоллекцияПлатежей.Количество() Тогда
		ПоказатьЗначение(, КоллекцияПлатежей[Индекс].Ссылка);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Возвращает индекс имени элемента формы
//
// Параметры:
//   ИмяЭлемента - Строка - имя декорации
//   ПрефиксИмени - Строка - префикс имени декорации (например, "ДекорацияПлатеж")
//
// Возвращаемое значение:
//   Число, Неопределено
//
Функция ИндексЭлемента(ИмяЭлемента, ПрефиксИмени)
	
	ИндексСтрокой = Сред(ИмяЭлемента, СтрДлина(ПрефиксИмени) + 1, 1);
	
	Если ПустаяСтрока(ИндексСтрокой) Или Не СтроковыеФункцииКлиентСервер.ТолькоЦифрыВСтроке(ИндексСтрокой) Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Возврат Число(ИндексСтрокой);
	
КонецФункции

#КонецОбласти