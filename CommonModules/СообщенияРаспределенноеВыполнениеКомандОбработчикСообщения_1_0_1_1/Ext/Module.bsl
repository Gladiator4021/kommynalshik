﻿////////////////////////////////////////////////////////////////////////////////
// СообщенияРаспределенноеВыполнениеКомандОбработчикСообщения_1_0_1_1: 
// обработка сообщений подсистемы РаспределенноеВыполнениеКоманд.
////////////////////////////////////////////////////////////////////////////////

// Экспортные процедуры и функции для вызова из других подсистем
// 
#Область ПрограммныйИнтерфейс

// Возвращает номер текущей версии программного интерфейса.
//
// Параметры:
//  нет
//
// Возвращаемое значение:
//   Строка   - Номер версии интерфейса
//
Функция Версия() Экспорт

	Возврат "1.0.1.1";

КонецФункции // Версия() 

// Возвращает пространство имен текущей (используемой вызывающим кодом) версии интерфейса сообщений.
//
// Параметры:
//  нет
//
// Возвращаемое значение:
//   Строка   - Пространство имен интерфейса
//
Функция Пакет() Экспорт

	Возврат Метаданные.ПакетыXDTO.RemoteProcedureCall_v1.ПространствоИмен;

КонецФункции // Пакет() 

// Возвращает название программного интерфейса сообщений.
//
// Параметры:
//  нет
//
// Возвращаемое значение:
//   Строка   - Имя интерфейса
//
Функция ПрограммныйИнтерфейс() Экспорт

	Возврат "RemoteProcedureCall";

КонецФункции // ПрограммныйИнтерфейс() 

// Возвращает базовый тип для сообщений версии.
//
Функция БазовыйТип() Экспорт
	
	Возврат СообщенияВМоделиСервисаПовтИсп.ТипТело();
	
КонецФункции // БазовыйТип()

// Выполняет обработку входящих сообщений модели сервиса.
//
// Параметры:
//  Сообщение - ОбъектXDTO, входящее сообщение,
//  Отправитель - ПланОбменаСсылка.ОбменСообщениями, узел плана обмена, соответствующий отправителю сообщения.
//  СообщениеОбработано - булево, флаг успешной обработки сообщения. Значение данного параметра необходимо
//    установить равным Истина в том случае, если сообщение было успешно прочитано в данном обработчике.
//
Процедура ОбработатьСообщениеМоделиСервиса(Сообщение, Отправитель, СообщениеОбработано) Экспорт
	
	ТипСообщения = Сообщение.Body.Тип();
	ТипПрямойВызов = СообщенияРаспределенноеВыполнениеКомандИнтерфейс.ТипПрямойВызов();
	
	// Пока умеем только обрабатывать прямые асинхронные вызовы.
	
	Если НЕ ТипСообщения = ТипПрямойВызов Тогда
		Возврат;	
	КонецЕсли; 
	
	СообщениеОбработано = ОтработатьПрямойВызов(Сообщение);
	Если НЕ СообщениеОбработано Тогда
		ТекстОшибки = ПолучитьИзВременногоХранилища(РаспределенноеВыполнениеКоманд.АдресТекущейОшибки());
		ВызватьИсключение ТекстОшибки;
	КонецЕсли; 
		
КонецПроцедуры

#КонецОбласти  

// Служебные и вспомогательные процедуры и функции, для вызова только из самого модуля
// 
#Область СлужебныеФункции

// Разбирает параметры прямого вызова и запускает выполнение команды в фоновом задании.
// Важно! Фоновое задание выполняется через механизм очереди.
//
// Параметры:
//	Сообщение - ОбъектXDTO - Входящий пакет.
//
// Возвращаемое значение:
//   Булево   - результат операции (Истина = сообщение отработано)
//
Функция ОтработатьПрямойВызов(Сообщение)

	ТелоСообщения = Сообщение.Body;
	
	ПараметрыЗапуска = Новый Массив();
	ПараметрыЗапуска.Добавить(ТелоСообщения.Processor_ID);
	ПараметрыЗапуска.Добавить(ТелоСообщения.Function);
	ПараметрыЗапуска.Добавить(ТелоСообщения.Call_ID);
	
	ПараметрыЗадания = Новый Структура();
	ПараметрыЗадания.Вставить("ОбластьДанных", ОбщегоНазначения.ЗначениеРазделителяСеанса());
	ПараметрыЗадания.Вставить("Использование", Истина);
	ПараметрыЗадания.Вставить("ИмяМетода", "РаспределенноеВыполнениеКоманд.ВыполнитьКомандуДополнительнойОбработки");
	ПараметрыЗадания.Вставить("Параметры", ПараметрыЗапуска);
	ПараметрыЗадания.Вставить("Ключ", Строка(ТелоСообщения.Call_ID));
	ПараметрыЗадания.Вставить("ИнтервалПовтораПриАварийномЗавершении", 60);
	ПараметрыЗадания.Вставить("КоличествоПовторовПриАварийномЗавершении", 3);
	
	ОчередьЗаданий.ДобавитьЗадание(ПараметрыЗадания);
	
	Возврат Истина;
	
КонецФункции // ОтработатьПрямойВызов() 

#КонецОбласти  