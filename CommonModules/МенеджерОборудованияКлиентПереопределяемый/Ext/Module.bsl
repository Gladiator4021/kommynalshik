﻿
#Область ПрограммныйИнтерфейс

// Возвращает текущую дату, приведенную к часовому поясу сеанса.
// Предназначена для использования вместо функции ТекущаяДата().
//
Функция ДатаСеанса() Экспорт
	
	Возврат ТекущаяДата();
	
КонецФункции

// Функция возвращает объект обработчика драйвера по его наименованию.
//
Функция ПолучитьОбработчикДрайвера(ОбработчикДрайвера, ЗагружаемыйДрайвер) Экспорт
	
	// Используем универсальный обработчик драйвера по стандарту "1С:Совместимо".
#Если ВебКлиент Тогда
	Результат = ПодключаемоеОборудованиеУниверсальныйДрайверАсинхронноКлиент; 
#Иначе
	Результат = ПодключаемоеОборудованиеУниверсальныйДрайверКлиент;
#КонецЕсли

	// Обработчики драйверов не удовлетворяющие стандарту "1С:Совместимо".
	Если Не ЗагружаемыйДрайвер И ОбработчикДрайвера <> Неопределено Тогда
		
		// Сканеры штрихкода
		Если ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.Обработчик1ССканерыШтрихкода") Тогда
			Возврат ПодключаемоеОборудование1ССканерыШтрихкодаКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикСканкодСканерыШтрихкода") Тогда
			Возврат ПодключаемоеОборудованиеСканкодСканерыШтрихкодаКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикАтолСканерыШтрихкода") Тогда
			Возврат ПодключаемоеОборудованиеАтолСканерыШтрихкодаКлиент;
		КонецЕсли;
		// Конец Сканеры штрихкода
		
		// Фискальные регистраторы
		Если ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикАтолФискальныеРегистраторы") Тогда
			Возврат ПодключаемоеОборудованиеАтолФискальныеРегистраторыКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикВерсияТФискальныеРегистраторы") Тогда
			Возврат ПодключаемоеОборудованиеВерсияТФискальныеРегистраторыКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикККСФискальныеРегистраторы") Тогда
			Возврат ПодключаемоеОборудованиеККСФискальныеРегистраторыКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикШтрихМФискальныеРегистраторы") Тогда
			Возврат ПодключаемоеОборудованиеШтрихМФискальныеРегистраторыКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикОРИОНФискальныеРегистраторы") Тогда
			Возврат ПодключаемоеОборудованиеОРИОНФискальныеРегистраторыКлиент;
		КонецЕсли;
		// Конец Фискальные регистраторы.
		
		// Эквайринговые терминалы
		Если ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикСБРФЭквайринговыеТерминалы") Тогда
			Возврат ПодключаемоеОборудованиеСБРФЭквайринговыеТерминалыКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикИНПАСЭквайринговыеТерминалыPulsar") Тогда
			Возврат ПодключаемоеОборудованиеИНПАСЭквайринговыеТерминалыPulsarКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикИНПАСЭквайринговыеТерминалыSmart") Тогда
			Возврат ПодключаемоеОборудованиеИНПАСЭквайринговыеТерминалыSmartКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикСофткейсЭквайринговыеТерминалы") Тогда
			Возврат ПодключаемоеОборудованиеСофтКейсЭквайринговыеТерминалыКлиент;
		КонецЕсли;
		// Конец Эквайринговые терминалы.
		 
		// ККМ offline
		Если ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикАтолККМOffline") Тогда
			Возврат ПодключаемоеОборудованиеАтолККМOfflineКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.ОбработчикШтрихМККМOffline") Тогда
			Возврат ПодключаемоеОборудованиеШтрихМККМOfflineКлиент;
		ИначеЕсли ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.Обработчик1СККМOffline") Тогда
			Возврат ПодключаемоеОборудование1СККМOfflineКлиент;
		КонецЕсли;
		// Конец ККМ offline
		
		// Web-сервис оборудование
		Если ОбработчикДрайвера = ПредопределенноеЗначение("Перечисление.ОбработчикиДрайверовПодключаемогоОборудования.Обработчик1СWebСервисОборудование") Тогда
			Возврат Неопределено;
		КонецЕсли;
		// Конец Web-сервис оборудование

	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

// Переопределяет формируемый шаблон чека.
//
Функция СформироватьШаблонЧека(ВходныеПараметры, ДополнительныйТекст, СтандартнаяОбработка) Экспорт
	
КонецФункции

// Переопределяет формируемый текст нефискального чека по шаблону.
//
Функция СформироватьТексНефискальногоЧека(ШиринаСтроки, ВходныеПараметры, СтандартнаяОбработка) Экспорт
	
КонецФункции

// Переопределяет печать чека по шаблону.
//
Функция ПечатьЧекаПоШаблону(ОбщийМодульОборудования, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВходныеПараметры, ВыходныеПараметры, СтандартнаяОбработка) Экспорт
	
КонецФункции

// Переопределяет печать фискального чека.
//
Функция ПечатьЧека(ОбщийМодульОборудования, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВходныеПараметры, ВыходныеПараметры, СтандартнаяОбработка) Экспорт
	
КонецФункции

#КонецОбласти

#Область ПроцедурыПодключенияОтключенияОборудования

// Начать подключение необходимых типов оборудования при открытии формы.
//
// Параметры:
// Форма - УправляемаяФорма
// ПоддерживаемыеТипыПодключаемогоОборудования - Строка
//  Содержит перечень типов подключаемого оборудования, разделенных запятыми.
//
Процедура НачатьПодключениеОборудованиеПриОткрытииФормы(Форма, ПоддерживаемыеТипыПодключаемогоОборудования) Экспорт
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено, Форма, ПоддерживаемыеТипыПодключаемогоОборудования);
	
КонецПроцедуры

// Начать отключать оборудование по типу при закрытии формы.
//
Процедура НачатьОтключениеОборудованиеПриЗакрытииФормы(Форма) Экспорт
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, Форма);
	
КонецПроцедуры

#КонецОбласти
 
#Область РаботаСФормойЭкземпляраОборудования

// Дополнительные переопределяемые действия с управляемой формой в Экземпляре оборудования
// при событии "ПриОткрытии".
//
Процедура ЭкземплярОборудованияПриОткрытии(Объект, ЭтаФорма, Отказ) Экспорт
	
КонецПроцедуры

// Дополнительные переопределяемые действия с управляемой формой в Экземпляре оборудования
// при событии "ПередЗакрытием".
//
Процедура ЭкземплярОборудованияПередЗакрытием(Объект, ЭтаФорма, Отказ, СтандартнаяОбработка) Экспорт
	
КонецПроцедуры

// Дополнительные переопределяемые действия с управляемой формой в Экземпляре оборудования
// при событии "ПередЗаписью".
//
Процедура ЭкземплярОборудованияПередЗаписью(Объект, ЭтаФорма, Отказ, ПараметрыЗаписи) Экспорт
	
	Ошибки = Неопределено;
	
	Если (НЕ ЗначениеЗаполнено(ЭтаФорма.Организация))
		И Объект.ТипОборудования <> ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.СканерШтрихкода")
		И Объект.ТипОборудования <> ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.СчитывательRFID") Тогда
		ОбщегоНазначенияКлиентСервер.ДобавитьОшибкуПользователю(Ошибки, "Организация", НСтр("ru='Не указана организация'"), Неопределено);
	КонецЕсли;
	
	Если (НЕ ЗначениеЗаполнено(ЭтаФорма.Склад))
		И (Объект.ТипОборудования = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.ККМOffline")
		ИЛИ Объект.ТипОборудования = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.WebСервисОборудование")) Тогда
		ОбщегоНазначенияКлиентСервер.ДобавитьОшибкуПользователю(Ошибки, "Склад", НСтр("ru='Не указан склад (розничный магазин)'"), Неопределено);
	КонецЕсли;
	
	Если Ошибки <> Неопределено Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьОшибкиПользователю(Ошибки, Отказ);
	КонецЕсли;
	
	ПараметрыЗаписи.Вставить("Организация", ЭтаФорма.Организация);
	ПараметрыЗаписи.Вставить("Склад", ЭтаФорма.Склад);
	ПараметрыЗаписи.Вставить("УзелОбмена", ЭтаФорма.УзелОбмена);
КонецПроцедуры

// Дополнительные переопределяемые действия с управляемой формой в Экземпляре оборудования
// при событии "ПослеЗаписи".
//
Процедура ЭкземплярОборудованияПослеЗаписи(Объект, ЭтаФорма, ПараметрыЗаписи) Экспорт
	
КонецПроцедуры

// Дополнительные переопределяемые действия с управляемой формой в Экземпляре оборудования
// при событии "ТипОборудованияОбработкаВыбора".
//
Процедура ЭкземплярОборудованияТипОборудованияВыбор(Объект, ЭтаФорма, ЭтотОбъект, Элемент, ВыбранноеЗначение) Экспорт
	
	Если ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.ККМOffline")
		ИЛИ ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.WebСервисОборудование") Тогда
		ЭтаФорма.Элементы.Организация.Доступность = Истина;
		ЭтаФорма.Элементы.Организация.АвтоОтметкаНезаполненного = Истина;
		ЭтаФорма.Элементы.Склад.АвтоОтметкаНезаполненного = Истина;
		ЭтаФорма.Элементы.Склад.ПодсказкаВвода = НСтр("ru='Укажите розничный магазин'");
	ИначеЕсли ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.СканерШтрихкода")
		ИЛИ  ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.СчитывательRFID") Тогда
		ЭтаФорма.Организация = ПредопределенноеЗначение("Справочник.Организации.ПустаяСсылка");
		ЭтаФорма.Элементы.Организация.Доступность = Ложь;
		ЭтаФорма.Элементы.Организация.АвтоОтметкаНезаполненного = Ложь;
		ЭтаФорма.Элементы.Склад.ПодсказкаВвода = НСтр("ru='Если оборудование используется в розничном магазине'");
		ЭтаФорма.Элементы.Склад.АвтоОтметкаНезаполненного = Ложь;
		ЭтаФорма.Элементы.Склад.ОтметкаНезаполненного = Ложь;
	Иначе
		ЭтаФорма.Элементы.Организация.Доступность = Истина;
		ЭтаФорма.Элементы.Организация.АвтоОтметкаНезаполненного = Истина;
		ЭтаФорма.Элементы.Склад.ПодсказкаВвода = НСтр("ru='Если оборудование используется в розничном магазине'");
		ЭтаФорма.Элементы.Склад.АвтоОтметкаНезаполненного = Ложь;
		ЭтаФорма.Элементы.Склад.ОтметкаНезаполненного = Ложь;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти