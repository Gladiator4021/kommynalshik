﻿#Область СлужебныеПроцедурыИФункции

Процедура ЗарегистрироватьНачисленнуюЗарплату(Движения, Отказ, Организация, ПериодРегистрации, ХарактерВыплаты, Начисления = Неопределено, Удержания = Неопределено, Авансом = Ложь) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ЗарегистрироватьНачисленнуюЗарплату(Движения, Отказ, Организация, ПериодРегистрации, ХарактерВыплаты, Начисления, Удержания, Авансом);
КонецПроцедуры

Процедура ЗарегистрироватьВыплаченнуюЗарплату(Движения, Отказ, Организация, ПериодРегистрации, Зарплата) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ЗарегистрироватьВыплаченнуюЗарплату(Движения, Отказ, Организация, ПериодРегистрации, Зарплата)
КонецПроцедуры

Процедура ЗарегистрироватьНачальныеОстатки(Движения, Отказ, Организация, ПериодРегистрации, Остатки) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ЗарегистрироватьНачальныеОстатки(Движения, Отказ, Организация, ПериодРегистрации, Остатки)
КонецПроцедуры

Процедура СпособыВыплатыЗарплатыНачальноеЗаполнение() Экспорт
	ВзаиморасчетыССотрудникамиБазовый.СпособыВыплатыЗарплатыНачальноеЗаполнение();
КонецПроцедуры	

Процедура СпособыВыплатыЗарплатыЗаполнитьПризнакПоставляемый() Экспорт
	ВзаиморасчетыССотрудникамиБазовый.СпособыВыплатыЗарплатыЗаполнитьПризнакПоставляемый()
КонецПроцедуры	

// Возвращает структуру, используемую для заполнения ведомостей на выплату зарплаты.
//
Функция ДанныеЗаполненияВедомости() Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ДанныеЗаполненияВедомости()
КонецФункции	

Процедура ЗарегистрироватьНевыплатуПоВедомости(Движения, Отказ, Ведомость, ФизическиеЛица) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ЗарегистрироватьНеВыплатуПоВедомости(Движения, Отказ, Ведомость, ФизическиеЛица)
КонецПроцедуры

Функция СпособВыплатыПоРасчетномуДокументу(РасчетныйДокумент) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.СпособВыплатыПоРасчетномуДокументу(РасчетныйДокумент)
КонецФункции

Функция МенеджерДокументаВедомостьПоВидуМестаВыплаты(ВидМестаВыплаты) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.МенеджерДокументаВедомостьПоВидуМестаВыплаты(ВидМестаВыплаты)
КонецФункции

Функция ВидВзаиморасчетовССотрудникамиПоХарактеруВыплатыЗарплаты(ХарактерВыплаты) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВидВзаиморасчетовССотрудникамиПоХарактеруВыплатыЗарплаты(ХарактерВыплаты)
КонецФункции

Функция ПараметрыПолученияЗарплатыКВыплате() Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ПараметрыПолученияЗарплатыКВыплате()
КонецФункции

Функция ПараметрыПолученияЗарплатыКВыплатеВедомости(Ведомость) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ПараметрыПолученияЗарплатыКВыплатеВедомости(Ведомость)
КонецФункции

Процедура СоздатьВТЗарплатаКВыплате(МенеджерВременныхТаблиц, ТолькоРазрешенные, Параметры, ИмяВТСотрудники) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.СоздатьВТЗарплатаКВыплате(МенеджерВременныхТаблиц, ТолькоРазрешенные, Параметры, ИмяВТСотрудники)
КонецПроцедуры	

Процедура СоздатьВТПлановыйАванс(МенеджерВременныхТаблиц, ТолькоРазрешенные, Параметры, ИмяВТСотрудники, КадровыеДанные) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.СоздатьВТПлановыйАванс(МенеджерВременныхТаблиц, ТолькоРазрешенные, Параметры, ИмяВТСотрудники, КадровыеДанные);
КонецПроцедуры

Процедура СоздатьВТСотрудникиДляВедомостиПоШапке(МенеджерВременныхТаблиц, Ведомость) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.СоздатьВТСотрудникиДляВедомостиПоШапке(МенеджерВременныхТаблиц, Ведомость)
КонецПроцедуры	

Процедура СоздатьВТСотрудникиДляВедомостиПоФизическимЛицам(МенеджерВременныхТаблиц, Ведомость, ФизическиеЛица) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.СоздатьВТСотрудникиДляВедомостиПоФизическимЛицам(МенеджерВременныхТаблиц, Ведомость, ФизическиеЛица)
КонецПроцедуры	

/// Заполнение и расчет документа.

Функция ВедомостьВКассуМестоВыплаты(Ведомость) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВедомостьВКассуМестоВыплаты(Ведомость);
КонецФункции	

Процедура ВедомостьВКассуУстановитьМестоВыплаты(Ведомость, Значение) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьВКассуУстановитьМестоВыплаты(Ведомость, Значение);
КонецПроцедуры	

Функция ВедомостьМожноЗаполнитьЗарплату(Ведомость) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВедомостьМожноЗаполнитьЗарплату(Ведомость)
КонецФункции

Процедура ВедомостьРассчитатьСуммыТаблицыЗарплат(Ведомость, ТаблицаЗарплат) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьРассчитатьСуммыТаблицыЗарплат(Ведомость, ТаблицаЗарплат)
КонецПроцедуры

Функция ВедомостьСоставПоТаблицеЗарплат(Ведомость, ТаблицаЗарплат) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВедомостьСоставПоТаблицеЗарплат(Ведомость, ТаблицаЗарплат)
КонецФункции

Процедура ВедомостьОчиститьСостав(Ведомость) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьОчиститьСостав(Ведомость)
КонецПроцедуры	

Процедура ВедомостьДополнитьСостав(Ведомость, Состав) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьДополнитьСостав(Ведомость, Состав)
КонецПроцедуры

/// Обработчики событий модуля объекта документов Ведомости.

Процедура ВедомостьОбработкаЗаполнения(ДокументОбъект, ДанныеЗаполнения, СтандартнаяОбработка) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьОбработкаЗаполнения(ДокументОбъект, ДанныеЗаполнения, СтандартнаяОбработка);
КонецПроцедуры

Процедура ВедомостьПередЗаписью(ДокументОбъект, Отказ, РежимЗаписи) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьПередЗаписью(ДокументОбъект, Отказ, РежимЗаписи)
КонецПроцедуры
	
Процедура ВедомостьЗарегистрироватьВыплату(Ведомость, Отказ) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьЗарегистрироватьВыплату(Ведомость, Отказ);
КонецПроцедуры

/// Печать

Процедура ВедомостьВБанкДобавитьКомандыПечати(КомандыПечати) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьВБанкДобавитьКомандыПечати(КомандыПечати)
КонецПроцедуры

Процедура ВедомостьВБанкПечать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьВБанкПечать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода)
КонецПроцедуры

Функция ВедомостьВБанкВыборкаДляПечатиШапки(ИмяТипа, Ведомости) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВедомостьВБанкВыборкаДляПечатиШапки(ИмяТипа, Ведомости)
КонецФункции
	
Функция ВедомостьВБанкВыборкаДляПечатиТаблицы(ИмяТипа, Ведомости) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВедомостьВБанкВыборкаДляПечатиТаблицы(ИмяТипа, Ведомости)
КонецФункции

Процедура ВедомостьВКассуДобавитьКомандыПечати(КомандыПечати) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьВКассуДобавитьКомандыПечати(КомандыПечати)
КонецПроцедуры

Процедура ВедомостьВКассуПечать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.ВедомостьВКассуПечать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода)
КонецПроцедуры

Функция ВедомостьВКассуВыборкаДляПечатиШапки(ИмяТипа, Ведомости) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВедомостьВКассуВыборкаДляПечатиШапки(ИмяТипа, Ведомости)
КонецФункции
	
Функция ВедомостьВКассуВыборкаДляПечатиТаблицы(ИмяТипа, Ведомости) Экспорт
	Возврат ВзаиморасчетыССотрудникамиБазовый.ВедомостьВКассуВыборкаДляПечатиТаблицы(ИмяТипа, Ведомости)
КонецФункции

Процедура СпособыВыплатыЗарплатыОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	ВзаиморасчетыССотрудникамиБазовый.СпособыВыплатыЗарплатыОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
КонецПроцедуры

#КонецОбласти
