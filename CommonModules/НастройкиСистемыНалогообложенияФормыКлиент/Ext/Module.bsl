﻿
////////////////////////////////////////////////////////////////////////////////
// Универсальные методы для формы записи регистра и формы настройки налогов
//
// Клиентские методы формы записи регистра сведений НастройкиСистемыНалогообложения
//
////////////////////////////////////////////////////////////////////////////////

#Область СлужебныеПроцедурыИФункции

#Область ОбработчикиСобытийЭлементовШапкиФормы

Процедура ПоложенияПереходногоПериодаУСНПриИзменении(Форма, Элемент) Экспорт
	
	Запись = Форма.НастройкиСистемыНалогообложения;
	
	Если Не Форма.ПоложенияПереходногоПериодаУСН И ЗначениеЗаполнено(Запись.ДатаПереходаНаУСН)Тогда
		Запись.ДатаПереходаНаУСН = '00010101';
	КонецЕсли;
	
	НастройкиСистемыНалогообложенияФормыКлиентСервер.УправлениеФормой(Форма);
	
КонецПроцедуры

Процедура СистемаНалогообложенияПредставлениеПриИзменении(Форма, Элемент) Экспорт
	
	Запись = Форма.НастройкиСистемыНалогообложения;
	
	Запись.СистемаНалогообложения =
		НастройкиСистемыНалогообложенияФормыКлиентСервер.СистемаНалогообложенияПоИндексу(Форма.СистемаНалогообложенияПредставление);
	
	Если Форма.СистемаНалогообложенияПредставление = 3 Тогда
		Запись.ПлательщикЕНВД           = Истина;
		Запись.ПрименяетсяУСНПатент     = Ложь;
		Запись.УплачиваетсяТорговыйСбор = Ложь;
	КонецЕсли;
	Если Форма.СистемаНалогообложенияПредставление = 4 Тогда
		Запись.ПлательщикЕНВД           = Ложь;
		Запись.ПрименяетсяУСНПатент     = Истина;
		Запись.УплачиваетсяТорговыйСбор = Ложь;
	КонецЕсли;
	Если Форма.СистемаНалогообложенияПредставление = 1 Тогда
		Запись.ОбъектНалогообложенияУСН = ПредопределенноеЗначение("Перечисление.ОбъектыНалогообложенияПоУСН.Доходы");
	ИначеЕсли Форма.СистемаНалогообложенияПредставление = 2 Тогда
		Запись.ОбъектНалогообложенияУСН = ПредопределенноеЗначение("Перечисление.ОбъектыНалогообложенияПоУСН.ДоходыМинусРасходы");
	КонецЕсли;
	
	СистемаНалогообложенияПриИзмененииНаКлиенте(Форма);
	
	НастройкиСистемыНалогообложенияФормыКлиентСервер.УправлениеФормой(Форма);
	
КонецПроцедуры

#КонецОбласти

Процедура СистемаНалогообложенияПриИзмененииНаКлиенте(Форма)
	
	Запись = Форма.НастройкиСистемыНалогообложения;
	
	Запись.ПлательщикНалогаНаПрибыль =
		Запись.СистемаНалогообложения = ПредопределенноеЗначение("Перечисление.СистемыНалогообложения.Общая")
		И Форма.ЭтоЮрЛицо;
		
	Запись.ПрименяетсяУСН =
		Запись.СистемаНалогообложения = ПредопределенноеЗначение("Перечисление.СистемыНалогообложения.Упрощенная");
		
	Запись.ПрименяетсяУСНДоходы = 
		Запись.СистемаНалогообложения = ПредопределенноеЗначение("Перечисление.СистемыНалогообложения.Упрощенная")
		И Запись.ОбъектНалогообложенияУСН = ПредопределенноеЗначение("Перечисление.ОбъектыНалогообложенияПоУСН.Доходы");
		
	Запись.ПрименяетсяУСНДоходыМинусРасходы =
		Запись.СистемаНалогообложения = ПредопределенноеЗначение("Перечисление.СистемыНалогообложения.Упрощенная")
		И Запись.ОбъектНалогообложенияУСН = ПредопределенноеЗначение("Перечисление.ОбъектыНалогообложенияПоУСН.ДоходыМинусРасходы");
		
	Запись.ПлательщикНДФЛ =
		Запись.СистемаНалогообложения = ПредопределенноеЗначение("Перечисление.СистемыНалогообложения.Общая") И НЕ Форма.ЭтоЮрЛицо;

	Если Запись.ПрименяетсяУСН Тогда
		Запись.ПлательщикНДС = Ложь;
	Иначе
		Запись.ПлательщикНДС = Истина;
	КонецЕсли;

	Если НЕ Запись.ПрименяетсяУСН Тогда
		Запись.ДатаПереходаНаУСН = Дата('00010101');
		Форма.ПоложенияПереходногоПериодаУСН = Ложь;
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(Запись.ОбъектНалогообложенияУСН) Тогда
		Запись.ОбъектНалогообложенияУСН = ПредопределенноеЗначение("Перечисление.ОбъектыНалогообложенияПоУСН.Доходы");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти