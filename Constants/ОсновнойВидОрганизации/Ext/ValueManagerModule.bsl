﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ПриЗаписи(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если ЭтотОбъект.Значение = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо Тогда
		
		УстановитьПривилегированныйРежим(Истина);
		Если Константы.ОсновнаяСистемаНалогообложения.Получить() = Перечисления.СистемыНалогообложения.ОсобыйПорядок Тогда
			Константы.ОсновнаяСистемаНалогообложения.Установить(Перечисления.СистемыНалогообложения.Общая);
		КонецЕсли;
		
	КонецЕсли;

КонецПроцедуры

#КонецЕсли