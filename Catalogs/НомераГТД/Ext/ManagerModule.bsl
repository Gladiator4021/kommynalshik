﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияФормы(ВидФормы, Параметры, ВыбраннаяФорма, ДополнительнаяИнформация, СтандартнаяОбработка)
	
	Если ВидФормы = "ФормаВыбора" И Параметры.Свойство("ВыборПоОстаткам") И Параметры.ВыборПоОстаткам Тогда
		// Если требуется форма с выбором по остаткам, то проверим есть ли у пользователя доступ к регистру бухгалтерии.
		СтандартнаяОбработка = Ложь;
		Если ПравоДоступа("Просмотр", Метаданные.РегистрыБухгалтерии.Хозрасчетный) Тогда
			ВыбраннаяФорма = "ФормаВыбораПоОстаткам";
		Иначе
			ВыбраннаяФорма = "ФормаВыбора";
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
