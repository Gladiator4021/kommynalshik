﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

Функция ВремяДокументаПоУмолчанию() Экспорт
	
	Возврат Новый Структура("Часы, Минуты", 20, 0);
	
КонецФункции

// Производит заполнение табличной части "Авансы полученные"
//
// Параметры:
//   СтруктураПараметров - Струкутра - структура входных параметров с ключами:
//     * Организация           - СправочникСсылка.Организация - организация, для которой будут получены данные для заполнения документа.
//     * Дата                  - Дата - дата, на которую будут получены данные для заполнения документа.
//   АдресХранилища - Строка - адрес временного хранилища для помещения результата выполнения.
Процедура ПодготовитьДанныеДляЗаполненияАвансыПолученные(СтруктураПараметров, АдресХранилища) Экспорт
	
	ТаблицаРезультата = Новый ТаблицаЗначений;
	
	ТабличнаяЧастьМетаданные = Метаданные.Документы.СписаниеНДС.ТабличныеЧасти.АвансыПолученные.Реквизиты;
	
	ТипРеквизитаДокументАванса = ТабличнаяЧастьМетаданные.ДокументАванса.Тип;
	ТипРеквизитаПокупатель     = ТабличнаяЧастьМетаданные.Покупатель.Тип;
	ТипРеквизитаСуммаАванса    = ТабличнаяЧастьМетаданные.СуммаАванса.Тип;
	ТипРеквизитаНДС            = ТабличнаяЧастьМетаданные.НДС.Тип;
	
	ТаблицаРезультата.Колонки.Добавить("ДокументАванса", ТипРеквизитаДокументАванса);
	ТаблицаРезультата.Колонки.Добавить("Покупатель",     ТипРеквизитаПокупатель);
	ТаблицаРезультата.Колонки.Добавить("СуммаАванса",    ТипРеквизитаСуммаАванса);
	ТаблицаРезультата.Колонки.Добавить("НДС",            ТипРеквизитаНДС);
	
	ТаблицаАвансов = Документы.ФормированиеЗаписейКнигиПокупок.ОстаткиНДССАвансовПолученных(СтруктураПараметров);
	
	Если ТаблицаАвансов.Количество() = 0 Тогда
		ПоместитьВоВременноеХранилище(ТаблицаРезультата, АдресХранилища);
		Возврат;
	КонецЕсли;
		
	СписокСчетовФактур = Новый МенеджерВременныхТаблиц;
	
	СоздатьСписокСчетовФактур(
		ТаблицаАвансов,
		СписокСчетовФактур);
	
	СписаниеАвансов = СписанныеАвансы(
		СтруктураПараметров,
		СписокСчетовФактур);
	
	РаспределитьСписанныеАвансы(
		ТаблицаАвансов,
		СписаниеАвансов,
		ТаблицаРезультата);
		
	КолонкиСортировки = "Покупатель,ДокументАванса";
		
	ТаблицаРезультата.Сортировать(КолонкиСортировки, Новый СравнениеЗначений);
		
	ПоместитьВоВременноеХранилище(ТаблицаРезультата, АдресХранилища);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПодготовкаПараметровПроведения

Функция ПодготовитьПараметрыПроведения(Ссылка, Отказ) Экспорт
	
	ПараметрыПроведения = Новый Структура;
	НомераТаблиц = Новый Структура;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Запрос.Текст = ТекстЗапросаРеквизитыДокумента(НомераТаблиц)
		+ ТекстЗапросаСоставДокумента(НомераТаблиц)
		+ ТекстЗапросаАвансыПолученные(НомераТаблиц);
	
	Результат = Запрос.ВыполнитьПакет();
	
	Для каждого НомерТаблицы Из НомераТаблиц Цикл
		ПараметрыПроведения.Вставить(НомерТаблицы.Ключ, Результат[НомерТаблицы.Значение].Выгрузить());
	КонецЦикла;

	Реквизиты = ПараметрыПроведения.ТаблицаРеквизиты[0];
	
	Если НЕ УчетнаяПолитика.Существует(Реквизиты.Организация, Реквизиты.Период, Истина, Ссылка) Тогда
		Отказ = Истина;
		Возврат ПараметрыПроведения;
	КонецЕсли;
	
	Реквизиты.УпрощенныйУчетНДС = УчетнаяПолитика.УпрощенныйУчетНДС(Реквизиты.Организация, Реквизиты.Период);
	
	Возврат ПараметрыПроведения;
		
КонецФункции

Функция ТекстЗапросаРеквизитыДокумента(НомераТаблиц)

	НомераТаблиц.Вставить("ТаблицаРеквизиты", НомераТаблиц.Количество());
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	Реквизиты.Дата КАК Период,
	|	Реквизиты.Организация КАК Организация,
	|	Реквизиты.Ссылка КАК Регистратор,
	|	НЕОПРЕДЕЛЕНО КАК УпрощенныйУчетНДС,
	|	Реквизиты.СчетСписанияНДС,
	|	Реквизиты.СубконтоСписанияНДС1,
	|	Реквизиты.СубконтоСписанияНДС2,
	|	Реквизиты.СубконтоСписанияНДС3,
	|	ИСТИНА КАК СписатьНДС,
	|	Реквизиты.ПодразделениеОрганизации
	|ИЗ
	|	Документ.СписаниеНДС КАК Реквизиты
	|ГДЕ
	|	Реквизиты.Ссылка = &Ссылка";
	
	Возврат ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();
	
КонецФункции

Функция ТекстЗапросаСоставДокумента(НомераТаблиц)
	
	НомераТаблиц.Вставить("ТаблицаСоставДокумента", НомераТаблиц.Количество());
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	СписаниеНДССостав.Поставщик,
	|	СписаниеНДССостав.СчетФактура,
	|	СписаниеНДССостав.ВидЦенности,
	|	СписаниеНДССостав.СтавкаНДС,
	|	СписаниеНДССостав.СчетУчетаНДС,
	|	СписаниеНДССостав.ДокументОплаты,
	|	СписаниеНДССостав.ДатаОплаты,
	|	СписаниеНДССостав.СуммаБезНДС,
	|	СписаниеНДССостав.НДС,
	|	СписаниеНДССостав.Ссылка КАК Регистратор,
	|	СписаниеНДССостав.Ссылка.Дата КАК Период,
	|	СписаниеНДССостав.Ссылка.Организация,
	|	ЗНАЧЕНИЕ(Перечисление.СобытияПоНДСПокупки.НДСсписанНаРасходы) КАК Событие,
	|	СписаниеНДССостав.Ссылка.Дата КАК ДатаСобытия,
	|	СписаниеНДССостав.НомерСтроки КАК НомерСтроки,
	|	ВЫБОР
	|		КОГДА СписаниеНДССостав.ВидЦенности = ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.НалоговыйАгентАренда)
	|				ИЛИ СписаниеНДССостав.ВидЦенности = ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.НалоговыйАгентИностранцы)
	|				ИЛИ СписаниеНДССостав.ВидЦенности = ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.НалоговыйАгентКомитент)
	|				ИЛИ СписаниеНДССостав.ВидЦенности = ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.НалоговыйАгентРеализацияИмущества)
	|			ТОГДА СписаниеНДССостав.СчетФактура.ДоговорКонтрагента
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ДоговорыКонтрагентов.ПустаяСсылка)
	|	КОНЕЦ КАК ДоговорКонтрагента,
	|	ИСТИНА КАК СписатьНДСИзРегистра
	|ИЗ
	|	Документ.СписаниеНДС.Состав КАК СписаниеНДССостав
	|ГДЕ
	|	СписаниеНДССостав.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки";
	
	Возврат ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();
	
КонецФункции

Функция ТекстЗапросаАвансыПолученные(НомераТаблиц)
	
	НомераТаблиц.Вставить("ТаблицаАвансыПолученные", НомераТаблиц.Количество());
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	АвансыПолученные.Покупатель КАК Поставщик,
	|	АвансыПолученные.ДокументАванса КАК СчетФактура,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.ПустаяСсылка) КАК ВидЦенности,
	|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.ПустаяСсылка) КАК СтавкаНДС,
	|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.НДСпоАвансамИПредоплатам) КАК СчетУчетаНДС,
	|	НЕОПРЕДЕЛЕНО КАК ДокументОплаты,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДатаОплаты,
	|	0 КАК СуммаБезНДС,
	|	АвансыПолученные.НДС КАК НДС,
	|	АвансыПолученные.Ссылка КАК Регистратор,
	|	АвансыПолученные.Ссылка.Дата КАК Период,
	|	АвансыПолученные.Ссылка.Организация КАК Организация,
	|	ЗНАЧЕНИЕ(Перечисление.СобытияПоНДСПокупки.ПустаяСсылка) КАК Событие,
	|	АвансыПолученные.Ссылка.Дата КАК ДатаСобытия,
	|	АвансыПолученные.НомерСтроки КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(Справочник.ДоговорыКонтрагентов.ПустаяСсылка) КАК ДоговорКонтрагента,
	|	ЛОЖЬ КАК СписатьНДСИзРегистра
	|ИЗ
	|	Документ.СписаниеНДС.АвансыПолученные КАК АвансыПолученные
	|ГДЕ
	|	АвансыПолученные.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки";
	
	Возврат ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();
	
КонецФункции

#КонецОбласти

#Область ЗаполнениеТаблицыПолученныеАвансы

Процедура СоздатьСписокСчетовФактур(ТаблицаИсточник, СписокСчетовФактур)

	ТаблицаСчетовФактур = ТаблицаИсточник.Скопировать(, "СчетФактура");
	ТаблицаСчетовФактур.Свернуть("СчетФактура");

	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = СписокСчетовФактур;
	Запрос.УстановитьПараметр("ТаблицаСчетовФактур", ТаблицаСчетовФактур);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ТаблицаСчетовФактур.СчетФактура КАК СчетФактура
	|ПОМЕСТИТЬ СписокСчетовФактур
	|ИЗ
	|	&ТаблицаСчетовФактур КАК ТаблицаСчетовФактур
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	СчетФактура";
	
	УстановитьПривилегированныйРежим(Истина);
	Запрос.Выполнить();

КонецПроцедуры

Функция СтруктураОтбораСтрок()
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("Покупатель");
	СтруктураОтбора.Вставить("ДоговорКонтрагента");
	СтруктураОтбора.Вставить("СчетФактура");
	Возврат СтруктураОтбора;

КонецФункции

Функция СписанныеАвансы(СтруктураПараметров, СписокСчетовФактур)
	
	МассивСчетовАвансов = Новый Массив();
	МассивСчетовАвансов.Добавить(ПланыСчетов.Хозрасчетный.РасчетыПоАвансамПолученным);    // 62.02
	МассивСчетовАвансов.Добавить(ПланыСчетов.Хозрасчетный.РасчетыПоАвансамПолученнымВал); // 62.22
	МассивСчетовАвансов.Добавить(ПланыСчетов.Хозрасчетный.РасчетыПоАвансамПолученнымУЕ);  // 62.32
	МассивСчетовАвансов = БухгалтерскийУчет.СформироватьМассивСубсчетов(МассивСчетовАвансов);
	
	МассивСчетовСписания = Новый Массив;
	МассивСчетовСписания.Добавить(ПланыСчетов.Хозрасчетный.ПрочиеДоходы);      // 91.01
	СчетаСписанияАвансов = БухгалтерскийУчет.СформироватьМассивСубсчетов(МассивСчетовСписания);
	
	МассивСчетовСписанияНДС = Новый Массив;
	МассивСчетовСписанияНДС.Добавить(ПланыСчетов.Хозрасчетный.ПрочиеРасходы);  // 91.02
	СчетаСписанияНДСсАвансов = БухгалтерскийУчет.СформироватьМассивСубсчетов(МассивСчетовСписанияНДС);

	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = СписокСчетовФактур;
	Запрос.УстановитьПараметр("Организация",  СтруктураПараметров.Организация);
	Запрос.УстановитьПараметр("КонецПериода", КонецДня(СтруктураПараметров.Дата));
	
	Запрос.УстановитьПараметр("МассивСчетовАвансов",      МассивСчетовАвансов);
	Запрос.УстановитьПараметр("СчетаСписанияАвансов",     СчетаСписанияАвансов);
	Запрос.УстановитьПараметр("СчетаСписанияНДСсАвансов", СчетаСписанияНДСсАвансов);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Хозрасчетный.Организация КАК Организация,
	|	ХозрасчетныйСубконто1.Значение КАК Покупатель,
	|	ХозрасчетныйСубконто2.Значение КАК ДоговорКонтрагента,
	|	СписокСчетовФактур.СчетФактура КАК СчетФактура,
	|	СУММА(Хозрасчетный.Сумма) КАК СуммаСписания,
	|	НАЧАЛОПЕРИОДА(Хозрасчетный.Период, ДЕНЬ) КАК ДатаСобытия
	|ПОМЕСТИТЬ СписаниеАвансов
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Субконто КАК ХозрасчетныйСубконто3
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ СписокСчетовФактур КАК СписокСчетовФактур
	|		ПО (ХозрасчетныйСубконто3.Вид = ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыСубконтоХозрасчетные.ДокументыРасчетовСКонтрагентами))
	|			И ХозрасчетныйСубконто3.Значение = СписокСчетовФактур.СчетФактура
	|			И (ХозрасчетныйСубконто3.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияБухгалтерии.Дебет))
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный КАК Хозрасчетный
	|		ПО ХозрасчетныйСубконто3.Период = Хозрасчетный.Период
	|			И ХозрасчетныйСубконто3.Регистратор = Хозрасчетный.Регистратор
	|			И ХозрасчетныйСубконто3.НомерСтроки = Хозрасчетный.НомерСтроки
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Субконто КАК ХозрасчетныйСубконто1
	|		ПО ХозрасчетныйСубконто3.Период = ХозрасчетныйСубконто1.Период
	|			И ХозрасчетныйСубконто3.Регистратор = ХозрасчетныйСубконто1.Регистратор
	|			И ХозрасчетныйСубконто3.НомерСтроки = ХозрасчетныйСубконто1.НомерСтроки
	|			И (ХозрасчетныйСубконто1.Вид = ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты))
	|			И (ХозрасчетныйСубконто1.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияБухгалтерии.Дебет))
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Субконто КАК ХозрасчетныйСубконто2
	|		ПО ХозрасчетныйСубконто3.Период = ХозрасчетныйСубконто2.Период
	|			И ХозрасчетныйСубконто3.Регистратор = ХозрасчетныйСубконто2.Регистратор
	|			И ХозрасчетныйСубконто3.НомерСтроки = ХозрасчетныйСубконто2.НомерСтроки
	|			И (ХозрасчетныйСубконто2.Вид = ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыСубконтоХозрасчетные.Договоры))
	|			И (ХозрасчетныйСубконто2.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияБухгалтерии.Дебет))
	|ГДЕ
	|	Хозрасчетный.Активность
	|	И Хозрасчетный.Период <= &КонецПериода
	|	И Хозрасчетный.СчетДт В(&МассивСчетовАвансов)
	|	И Хозрасчетный.СчетКт В(&СчетаСписанияАвансов)
	|	И ХозрасчетныйСубконто1.Период ЕСТЬ НЕ NULL 
	|	И ХозрасчетныйСубконто2.Период ЕСТЬ НЕ NULL 
	|
	|СГРУППИРОВАТЬ ПО
	|	Хозрасчетный.Организация,
	|	ХозрасчетныйСубконто1.Значение,
	|	ХозрасчетныйСубконто2.Значение,
	|	СписокСчетовФактур.СчетФактура,
	|	НАЧАЛОПЕРИОДА(Хозрасчетный.Период, ДЕНЬ)
	|
	|ИМЕЮЩИЕ
	|	СУММА(Хозрасчетный.Сумма) > 0
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Организация,
	|	Покупатель,
	|	СчетФактура,
	|	ДоговорКонтрагента,
	|	ДатаСобытия
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Хозрасчетный.Организация КАК Организация,
	|	ХозрасчетныйСубконто1.Значение КАК Покупатель,
	|	СписокСчетовФактур.СчетФактура КАК СчетФактура,
	|	СУММА(ВЫРАЗИТЬ(Хозрасчетный.Сумма / 18 * 118 КАК ЧИСЛО(15, 2))) КАК РанееСписано,
	|	НАЧАЛОПЕРИОДА(Хозрасчетный.Период, ДЕНЬ) КАК ДатаСобытия
	|ПОМЕСТИТЬ РанееСписанныйНДС
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Субконто КАК ХозрасчетныйСубконто2
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ СписокСчетовФактур КАК СписокСчетовФактур
	|		ПО (ХозрасчетныйСубконто2.Вид = ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыСубконтоХозрасчетные.СФВыданные))
	|			И (ХозрасчетныйСубконто2.Значение = СписокСчетовФактур.СчетФактура)
	|			И (ХозрасчетныйСубконто2.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияБухгалтерии.Кредит))
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный КАК Хозрасчетный
	|		ПО (ХозрасчетныйСубконто2.Период = Хозрасчетный.Период)
	|			И (ХозрасчетныйСубконто2.Регистратор = Хозрасчетный.Регистратор)
	|			И (ХозрасчетныйСубконто2.НомерСтроки = Хозрасчетный.НомерСтроки)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Субконто КАК ХозрасчетныйСубконто1
	|		ПО (ХозрасчетныйСубконто2.Период = ХозрасчетныйСубконто1.Период)
	|			И (ХозрасчетныйСубконто2.Регистратор = ХозрасчетныйСубконто1.Регистратор)
	|			И (ХозрасчетныйСубконто2.НомерСтроки = ХозрасчетныйСубконто1.НомерСтроки)
	|			И (ХозрасчетныйСубконто1.Вид = ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты))
	|			И (ХозрасчетныйСубконто1.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияБухгалтерии.Кредит))
	|ГДЕ
	|	Хозрасчетный.Активность
	|	И Хозрасчетный.Период <= &КонецПериода
	|	И Хозрасчетный.СчетДт В(&СчетаСписанияНДСсАвансов)
	|	И Хозрасчетный.СчетКт = ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.НДСПоАвансамИПредоплатам)
	|	И ХозрасчетныйСубконто1.Период ЕСТЬ НЕ NULL 
	|	И ХозрасчетныйСубконто2.Период ЕСТЬ НЕ NULL 
	|
	|СГРУППИРОВАТЬ ПО
	|	Хозрасчетный.Организация,
	|	ХозрасчетныйСубконто1.Значение,
	|	СписокСчетовФактур.СчетФактура,
	|	НАЧАЛОПЕРИОДА(Хозрасчетный.Период, ДЕНЬ)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Организация,
	|	Покупатель,
	|	СчетФактура,
	|	ДатаСобытия
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СписаниеАвансов.Организация КАК Организация,
	|	СписаниеАвансов.Покупатель КАК Покупатель,
	|	СписаниеАвансов.ДоговорКонтрагента КАК ДоговорКонтрагента,
	|	СписаниеАвансов.СчетФактура КАК СчетФактура,
	|	СписаниеАвансов.ДатаСобытия КАК ДатаСобытия,
	|	СписаниеАвансов.СуммаСписания - ЕСТЬNULL(РанееСписанныйНДС.РанееСписано, 0) КАК СуммаАванса
	|ИЗ
	|	СписаниеАвансов КАК СписаниеАвансов
	|		ЛЕВОЕ СОЕДИНЕНИЕ РанееСписанныйНДС КАК РанееСписанныйНДС
	|		ПО СписаниеАвансов.Организация = РанееСписанныйНДС.Организация
	|			И СписаниеАвансов.Покупатель = РанееСписанныйНДС.Покупатель
	|			И СписаниеАвансов.СчетФактура = РанееСписанныйНДС.СчетФактура
	|			И СписаниеАвансов.ДатаСобытия <= РанееСписанныйНДС.ДатаСобытия
	|ГДЕ
	|	СписаниеАвансов.СуммаСписания - ЕСТЬNULL(РанееСписанныйНДС.РанееСписано, 0) > 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДоговорКонтрагента,
	|	Покупатель,
	|	СчетФактура,
	|	ДатаСобытия
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|УНИЧТОЖИТЬ РанееСписанныйНДС
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|УНИЧТОЖИТЬ СписаниеАвансов";

	УстановитьПривилегированныйРежим(Истина);

	СписаниеАвансов = Запрос.Выполнить().Выбрать();

	// Выборка специальным образом упорядочена для использования далее в алгоритме.

	Возврат СписаниеАвансов;
	
КонецФункции

Процедура РаспределитьСписанныеАвансы(ТаблицаАвансов, СписаниеАвансов, ТаблицаРезультатов)
	
	СравнениеПоИдентификатору = Новый СравнениеЗначений;
	// ТаблицаАвансов и СписаниеАвансов были отсортированы ранее в порядке "ДоговорКонтрагента, СчетФактура, Покупатель".

	СтруктураОтбора = СтруктураОтбораСтрок();
	МассивСуммаБезНДС = Новый Массив;
	МассивСуммаНДС = Новый Массив;
	ИндексСтрокиТаблицыАвансов = 0;
	Пока СписаниеАвансов.Следующий() Цикл
		
		СуммаБезНДС = 0;
		МассивСуммаБезНДС.Очистить();
		СуммаНДС = 0;
		МассивСуммаНДС.Очистить();
		
		Если СтруктураОтбора.СчетФактура <> СписаниеАвансов.СчетФактура
			Или СтруктураОтбора.Покупатель <> СписаниеАвансов.Покупатель Тогда
			// Повторный поиск (например, если разные ДатаСобытия) не выполняем. Используем ранее полученные строки.
		
			СтруктураОтбора.Покупатель         = СписаниеАвансов.Покупатель;
			СтруктураОтбора.СчетФактура        = СписаниеАвансов.СчетФактура;
			СтрокиАвансаПоОтбору = УчетНДСПереопределяемый.НайтиСтрокиПоОтбору(
				ТаблицаАвансов, ИндексСтрокиТаблицыАвансов, СтруктураОтбора, "Покупатель", СравнениеПоИдентификатору);
			СтруктураПредыдущегоОтбора = СтруктураОтбора;
			
		КонецЕсли;
		
		Если СтрокиАвансаПоОтбору.Количество() = 0 Тогда
			Продолжить;
		КонецЕсли;
		
		Для каждого СтрокаАвансаПоОтбору Из СтрокиАвансаПоОтбору Цикл
			Если СтрокаАвансаПоОтбору.ДатаСобытия > СписаниеАвансов.ДатаСобытия Тогда
				// Не учитываем списание аванса, произведенное до выписки счета-фактуры
				Продолжить;
			КонецЕсли;
			
			МассивСуммаБезНДС.Добавить(СтрокаАвансаПоОтбору.СуммаБезНДС);
			МассивСуммаНДС.Добавить(СтрокаАвансаПоОтбору.НДС);

			СуммаБезНДС = СуммаБезНДС + СтрокаАвансаПоОтбору.СуммаБезНДС;
			СуммаНДС = СуммаНДС + СтрокаАвансаПоОтбору.НДС;
		КонецЦикла;

		Если СуммаБезНДС + СуммаНДС = 0 Тогда
			Продолжить;
		КонецЕсли; 

		СуммаСНДС = Мин(СписаниеАвансов.СуммаАванса, СуммаБезНДС + СуммаНДС);
		Если СуммаСНДС = 0 Тогда
			Продолжить;
		КонецЕсли;

		СуммаБезНДС = Окр(СуммаБезНДС * СуммаСНДС / (СуммаБезНДС+СуммаНДС), 2);
		Если СуммаБезНДС > 0 Тогда
			МассивСуммаБезНДС = ОбщегоНазначенияБПКлиентСервер.РаспределитьПропорционально(СуммаБезНДС, МассивСуммаБезНДС);
		КонецЕсли;
		
		СуммаНДС = Окр(СуммаСНДС - СуммаБезНДС, 2);
		Если СуммаНДС > 0 Тогда
			МассивСуммаНДС = ОбщегоНазначенияБПКлиентСервер.РаспределитьПропорционально(СуммаНДС, МассивСуммаНДС);
		КонецЕсли; 
		
		Счетчик = 0;
		Для Каждого СтрокаАвансаПоОтбору Из СтрокиАвансаПоОтбору Цикл
			Если СтрокаАвансаПоОтбору.ДатаСобытия > СписаниеАвансов.ДатаСобытия Тогда
				// Не учитываем списание аванса, произведенное до выписки счета-фактуры
				Продолжить;
			КонецЕсли;
			
			Если ?(СуммаНДС > 0, МассивСуммаНДС[Счетчик], 0) = 0 Тогда
				Продолжить;
			КонецЕсли;
			
			СтрокаРезультата = ТаблицаРезультатов.Добавить();
			
			СтрокаРезультата.ДокументАванса = СтрокаАвансаПоОтбору.СчетФактура;
			СтрокаРезультата.Покупатель     = СтрокаАвансаПоОтбору.Покупатель;
			СтрокаРезультата.СуммаАванса    = ?(СуммаБезНДС > 0, МассивСуммаБезНДС[Счетчик], 0) 
				+ ?(СуммаНДС > 0, МассивСуммаНДС[Счетчик], 0);
			СтрокаРезультата.НДС            = ?(СуммаНДС > 0, МассивСуммаНДС[Счетчик], 0);
			
			СтрокаАвансаПоОтбору.СуммаБезНДС = СтрокаАвансаПоОтбору.СуммаБезНДС - ?(СуммаБезНДС > 0, МассивСуммаБезНДС[Счетчик], 0);
			СтрокаАвансаПоОтбору.НДС         = СтрокаАвансаПоОтбору.НДС - ?(СуммаНДС > 0, МассивСуммаНДС[Счетчик], 0);
			
			Счетчик = Счетчик + 1;
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
КонецПроцедуры


#КонецОбласти

#КонецЕсли