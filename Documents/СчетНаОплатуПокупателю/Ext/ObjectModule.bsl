﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ
//

Процедура СкопироватьТовары(Основание) Экспорт

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Основание);
	Запрос.Текст = "ВЫБРАТЬ
	|	РеализацияТоваровУслугТовары.Номенклатура,
	|	РеализацияТоваровУслугТовары.Цена,
	|	РеализацияТоваровУслугТовары.Сумма,
	|	РеализацияТоваровУслугТовары.СтавкаНДС,
	|	РеализацияТоваровУслугТовары.СуммаНДС,
	|	РеализацияТоваровУслугТовары.Количество
	|ИЗ
	|	Документ.РеализацияТоваровУслуг.Товары КАК РеализацияТоваровУслугТовары
	|ГДЕ
	|	РеализацияТоваровУслугТовары.Ссылка = &Ссылка";
	Товары.Загрузить(Запрос.Выполнить().Выгрузить());
	
	ОснованиеСуммаВключаетНДС = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Основание, "СуммаВключаетНДС");
	Если ОснованиеСуммаВключаетНДС <> СуммаВключаетНДС Тогда
		Для каждого СтрокаТЧ Из Товары Цикл
			СтрокаТЧ.Сумма = СтрокаТЧ.Сумма + ?(СуммаВключаетНДС, СтрокаТЧ.СуммаНДС, -СтрокаТЧ.СуммаНДС);
			Если СтрокаТЧ.Количество = 0 Тогда
				СтрокаТЧ.Цена = 0;
			Иначе
				СтрокаТЧ.Цена = СтрокаТЧ.Сумма / СтрокаТЧ.Количество;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;

КонецПроцедуры

Процедура СкопироватьВозвратнуюТару(Основание) Экспорт

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Основание);
	Запрос.Текст = "ВЫБРАТЬ
	|	РеализацияТоваровУслугВозвратнаяТара.НомерСтроки,
	|	РеализацияТоваровУслугВозвратнаяТара.Номенклатура,
	|	РеализацияТоваровУслугВозвратнаяТара.Количество,
	|	РеализацияТоваровУслугВозвратнаяТара.Сумма,
	|	РеализацияТоваровУслугВозвратнаяТара.Цена
	|ИЗ
	|	Документ.РеализацияТоваровУслуг.ВозвратнаяТара КАК РеализацияТоваровУслугВозвратнаяТара
	|ГДЕ
	|	РеализацияТоваровУслугВозвратнаяТара.Ссылка = &Ссылка";
	ВозвратнаяТара.Загрузить(Запрос.Выполнить().Выгрузить());

КонецПроцедуры

Процедура СкопироватьУслуги(Основание) Экспорт

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Основание);
	Запрос.Текст = "ВЫБРАТЬ
	|	РеализацияТоваровУслугУслуги.НомерСтроки КАК НомерСтроки,
	|	РеализацияТоваровУслугУслуги.Содержание,
	|	РеализацияТоваровУслугУслуги.Количество,
	|	РеализацияТоваровУслугУслуги.Цена,
	|	РеализацияТоваровУслугУслуги.Сумма,
	|	РеализацияТоваровУслугУслуги.СтавкаНДС,
	|	РеализацияТоваровУслугУслуги.СуммаНДС,
	|	РеализацияТоваровУслугУслуги.Номенклатура,
	|	1 КАК Порядок
	|ИЗ
	|	Документ.РеализацияТоваровУслуг.Услуги КАК РеализацияТоваровУслугУслуги
	|ГДЕ
	|	РеализацияТоваровУслугУслуги.Ссылка = &Ссылка
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	РеализацияТоваровУслугАгентскиеУслуги.НомерСтроки,
	|	РеализацияТоваровУслугАгентскиеУслуги.Содержание,
	|	РеализацияТоваровУслугАгентскиеУслуги.Количество,
	|	РеализацияТоваровУслугАгентскиеУслуги.Цена,
	|	РеализацияТоваровУслугАгентскиеУслуги.Сумма,
	|	РеализацияТоваровУслугАгентскиеУслуги.СтавкаНДС,
	|	РеализацияТоваровУслугАгентскиеУслуги.СуммаНДС,
	|	РеализацияТоваровУслугАгентскиеУслуги.Номенклатура,
	|	2
	|ИЗ
	|	Документ.РеализацияТоваровУслуг.АгентскиеУслуги КАК РеализацияТоваровУслугАгентскиеУслуги
	|ГДЕ
	|	РеализацияТоваровУслугАгентскиеУслуги.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	Порядок,
	|	НомерСтроки";
	ТаблицаУслуг = Запрос.Выполнить().Выгрузить();
	
	ОснованиеСуммаВключаетНДС = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Основание, "СуммаВключаетНДС");
	Для Каждого СтрокаТЧ Из ТаблицаУслуг Цикл
		Если ОснованиеСуммаВключаетНДС <> СуммаВключаетНДС Тогда
			СтрокаТЧ.Сумма = СтрокаТЧ.Сумма + ?(СуммаВключаетНДС, СтрокаТЧ.СуммаНДС, -СтрокаТЧ.СуммаНДС);
			Если СтрокаТЧ.Количество = 0 Тогда
				СтрокаТЧ.Цена = 0;
			Иначе
				СтрокаТЧ.Цена = СтрокаТЧ.Сумма / СтрокаТЧ.Количество;
			КонецЕсли;
		КонецЕсли;
		НоваяСтрока = Товары.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТЧ);
	КонецЦикла;

КонецПроцедуры

Процедура ЗаполнитьПоДокументуОснованию(Основание)

	Если ТипЗнч(Основание) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда

		// Заполним реквизиты шапки по документу основанию.
		АдресДоставки = Основание.АдресДоставки;
		ЗаполнениеДокументов.ЗаполнитьПоОснованию(ЭтотОбъект, Основание);

		// Заполним табличные части
		СкопироватьТовары(Основание);
		СкопироватьВозвратнуюТару(Основание);
		СкопироватьУслуги(Основание);

	ИначеЕсли ТипЗнч(Основание) = Тип("ДокументСсылка.ОтчетКомитентуОПродажах") Тогда

		// Заполним реквизиты шапки по документу основанию.
		ЗаполнениеДокументов.ЗаполнитьПоОснованию(ЭтотОбъект, Основание);

		Запрос = Новый Запрос();
		Запрос.УстановитьПараметр("Ссылка", Основание.Ссылка);
		Запрос.Текст =
		"ВЫБРАТЬ
		|	ОтчетКомитентуОПродажахТовары.Ссылка.УслугаПоВознаграждению КАК Номенклатура,
		|	ОтчетКомитентуОПродажахТовары.Ссылка.УслугаПоВознаграждению.НаименованиеПолное КАК Содержание,
		|	ОтчетКомитентуОПродажахТовары.Ссылка.СтавкаНДСВознаграждения КАК СтавкаНДС,
		|	СУММА(ОтчетКомитентуОПродажахТовары.СуммаВознаграждения) КАК Цена,
		|	СУММА(ОтчетКомитентуОПродажахТовары.СуммаВознаграждения) КАК Сумма,
		|	СУММА(ОтчетКомитентуОПродажахТовары.СуммаНДСВознаграждения) КАК СуммаНДС
		|ИЗ
		|	Документ.ОтчетКомитентуОПродажах.Товары КАК ОтчетКомитентуОПродажахТовары
		|ГДЕ
		|	ОтчетКомитентуОПродажахТовары.Ссылка = &Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	ОтчетКомитентуОПродажахТовары.Ссылка.УслугаПоВознаграждению,
		|	ОтчетКомитентуОПродажахТовары.Ссылка.СтавкаНДСВознаграждения";
		ВыборкаУслуг = Запрос.Выполнить().Выбрать();

		Пока ВыборкаУслуг.Следующий() Цикл
			НоваяСтрока = Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, ВыборкаУслуг);
		КонецЦикла;
		
	ИначеЕсли ТипЗнч(Основание) = Тип("ДокументСсылка.АктОбОказанииПроизводственныхУслуг") Тогда

		// Заполним реквизиты шапки по документу основанию.
		ЗаполнениеДокументов.ЗаполнитьПоОснованию(ЭтотОбъект, Основание);

		Запрос = Новый Запрос();
		Запрос.УстановитьПараметр("Ссылка", Основание.Ссылка);
		Запрос.Текст =
		"ВЫБРАТЬ
		|	АктОбОказанииПроизводственныхУслугУслуги.Номенклатура,
		|	АктОбОказанииПроизводственныхУслугУслуги.Номенклатура.НаименованиеПолное КАК Содержание,
		|	АктОбОказанииПроизводственныхУслугУслуги.СтавкаНДС,
		|	АктОбОказанииПроизводственныхУслугУслуги.Цена,
		|	СУММА(АктОбОказанииПроизводственныхУслугУслуги.Количество) КАК Количество,
		|	СУММА(АктОбОказанииПроизводственныхУслугУслуги.Сумма) КАК Сумма,
		|	СУММА(АктОбОказанииПроизводственныхУслугУслуги.СуммаНДС) КАК СуммаНДС
		|ИЗ
		|	Документ.АктОбОказанииПроизводственныхУслуг.Услуги КАК АктОбОказанииПроизводственныхУслугУслуги
		|ГДЕ
		|	АктОбОказанииПроизводственныхУслугУслуги.Ссылка = &Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	АктОбОказанииПроизводственныхУслугУслуги.Номенклатура,
		|	АктОбОказанииПроизводственныхУслугУслуги.СтавкаНДС,
		|	АктОбОказанииПроизводственныхУслугУслуги.Цена";
		ВыборкаУслуг = Запрос.Выполнить().Выбрать();

		Пока ВыборкаУслуг.Следующий() Цикл
			НоваяСтрока = Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, ВыборкаУслуг);
		КонецЦикла;

	КонецЕсли;

КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ
//

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	// Проверка табличной части "Возвратная тара"
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьВозвратнуюТару") Тогда
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.Номенклатура");
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.Количество");
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.Цена");
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.Сумма");
	КонецЕсли;
	
	Если НЕ Справочники.БанковскиеСчета.ИспользуетсяНесколькоБанковскихСчетовОрганизации(ОрганизацияПолучатель) Тогда
		МассивНепроверяемыхРеквизитов.Добавить("СтруктурнаяЕдиница");
		ПроверкаРеквизитовОрганизации.ОбработкаПроверкиЗаполнения(ОрганизацияПолучатель, СтруктурнаяЕдиница, Ложь, Отказ);
	КонецЕсли;
	
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Номенклатура");
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Содержание");
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Количество");
	
	МассивНоменклатуры    = Товары.ВыгрузитьКолонку("Номенклатура");
	РеквизитыНоменклатуры = ОбщегоНазначения.ЗначенияРеквизитовОбъектов(МассивНоменклатуры, "Услуга");
	
	Если УчетНДСПереопределяемый.ПолучитьСуммуДокументаСНДС(ЭтотОбъект) < 0 И СуммаСкидки > 0 Тогда
		
		ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения(,"КОРРЕКТНОСТЬ", НСтр("ru = 'Скидка'"), , , 
			НСтр("ru = 'Сумма скидки превышает сумму по документу'"));
		Поле = "СуммаСкидки";
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, "Объект", Отказ);
		
	КонецЕсли; 
	
	Для каждого СтрокаТаблицы Из Товары Цикл
		
		Префикс = "Товары[%1].";
		Префикс = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			Префикс, Формат(СтрокаТаблицы.НомерСтроки - 1, "ЧН=0; ЧГ="));
		
		ИмяСписка = НСтр("ru = 'Товары'");
		
		Если НЕ ЗначениеЗаполнено(СтрокаТаблицы.Номенклатура) 
		   И ПустаяСтрока(СтрокаТаблицы.Содержание) Тогда
		
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение",
				НСтр("ru = 'Номенклатура'") , СтрокаТаблицы.НомерСтроки, ИмяСписка, ТекстСообщения);
				Поле = Префикс + "Номенклатура";
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, "Объект", Отказ);
		
		КонецЕсли;
		
		Всего = СтрокаТаблицы.Сумма + ?(СуммаВключаетНДС, 0, СтрокаТаблицы.СуммаНДС);
		Если Всего > 0 И СтрокаТаблицы.СуммаСкидки > Всего Тогда
		
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка","КОРРЕКТНОСТЬ", НСтр("ru = 'Скидка'"),
					СтрокаТаблицы.НомерСтроки, ИмяСписка, НСтр("ru = 'Сумма скидки превышает сумму по строке'"));
			Поле = Префикс + "СуммаСкидки";
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, "Объект", Отказ);
		
		КонецЕсли; 
		
		ЭтоУслуга = Истина;
		СвойстваНоменклатуры = РеквизитыНоменклатуры[СтрокаТаблицы.Номенклатура];
		Если СвойстваНоменклатуры <> Неопределено Тогда
			ЭтоУслуга = СвойстваНоменклатуры.Услуга;
		КонецЕсли;
			
		Если НЕ ЭтоУслуга И НЕ ЗначениеЗаполнено(СтрокаТаблицы.Количество) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка",, НСтр("ru = 'Количество'"),
					СтрокаТаблицы.НомерСтроки, ИмяСписка);
			Поле = Префикс + "Количество";
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, "Объект", Отказ);
		КонецЕсли;
	КонецЦикла;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)

	ТипДанныхЗаполнения = ТипЗнч(ДанныеЗаполнения);
	Если ДанныеЗаполнения <> Неопределено И ТипДанныхЗаполнения <> Тип("Структура")
		И Метаданные().ВводитсяНаОсновании.Содержит(ДанныеЗаполнения.Метаданные()) Тогда
		ЗаполнитьПоДокументуОснованию(ДанныеЗаполнения);
	Иначе
		СуммаВключаетНДС = Истина;
	КонецЕсли;
	ЗаполнениеДокументов.Заполнить(ЭтотОбъект, ДанныеЗаполнения);

	Если ЗначениеЗаполнено(СтруктурнаяЕдиница) Тогда
		ОрганизацияПолучатель = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтруктурнаяЕдиница, "Владелец");
	Иначе
		ОрганизацияПолучатель = Организация;
	КонецЕсли;
	
	// Заполним дополнительные условия по умолчанию
	Если ЗначениеЗаполнено(Организация) Тогда
		ДополнительныеУсловия = Организация.ДополнительныеУсловияПоУмолчанию;
	КонецЕсли;
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьВозвратнуюТару")
		И ВозвратнаяТара.Количество() > 0 Тогда
		ВозвратнаяТара.Очистить();
	КонецЕсли;
	
	// Посчитать суммы документа и записать ее в соответствующий реквизит шапки для показа в журналах
	СуммаДокумента = УчетНДСПереопределяемый.ПолучитьСуммуДокументаСНДС(ЭтотОбъект);
	
	Если ЗначениеЗаполнено(ОрганизацияПолучатель) И НЕ ЗначениеЗаполнено(СтруктурнаяЕдиница)
		И НЕ Справочники.БанковскиеСчета.ИспользуетсяНесколькоБанковскихСчетовОрганизации(ОрганизацияПолучатель) Тогда
		УчетДенежныхСредствБП.УстановитьБанковскийСчет(СтруктурнаяЕдиница, ОрганизацияПолучатель, ВалютаДокумента, Истина);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)

	Дата = НачалоДня(ОбщегоНазначения.ТекущаяДатаПользователя());
	Ответственный = Пользователи.ТекущийПользователь();
	
	СтруктураКурсаВзаиморасчетов = РаботаСКурсамиВалют.ПолучитьКурсВалюты(
	ВалютаДокумента, Дата);
	
	ОтветственныеЛицаБП.УстановитьОтветственныхЛиц(ЭтотОбъект);
	
	КурсВзаиморасчетов      = СтруктураКурсаВзаиморасчетов.Курс;
	КратностьВзаиморасчетов = СтруктураКурсаВзаиморасчетов.Кратность;

КонецПроцедуры

#КонецЕсли