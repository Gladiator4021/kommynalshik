﻿#Если Сервер ИЛИ ТолстыйКлиентОбычноеПриложение ИЛИ ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

Функция ВремяДокументаПоУмолчанию() Экспорт
	
	Возврат Новый Структура("Часы, Минуты", 22, 0);
	
КонецФункции

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти

////////////////////////////////////////////////////////////////////////////////
// ПОДГОТОВКА ПАРАМЕТРОВ ПРОВЕДЕНИЯ ДОКУМЕНТА

Функция ПодготовитьПараметрыПроведения(Ссылка, Отказ) Экспорт
	
	ПараметрыПроведения = Новый Структура;
	НомераТаблиц = Новый Структура;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Запрос.Текст = ТекстЗапросаРеквизитыДокумента(НомераТаблиц)
		+ ТекстЗапросаСоставДокумента(НомераТаблиц)
		+ ТекстЗапросаПоФормированиюРегламентнойОперации(НомераТаблиц);

	Результат = Запрос.ВыполнитьПакет();
	
	Для каждого НомерТаблицы Из НомераТаблиц Цикл
		ПараметрыПроведения.Вставить(НомерТаблицы.Ключ, Результат[НомерТаблицы.Значение].Выгрузить());
	КонецЦикла;

	Реквизиты = ПараметрыПроведения.ТаблицаРеквизиты[0];
	
	Если НЕ УчетнаяПолитика.Существует(Реквизиты.Организация, Реквизиты.Период, Истина, Ссылка) Тогда
		Отказ = Истина;
		Возврат ПараметрыПроведения;
	КонецЕсли;
	
	Реквизиты.УпрощенныйУчетНДС = УчетнаяПолитика.УпрощенныйУчетНДС(Реквизиты.Организация, Реквизиты.Период);
	
	Возврат ПараметрыПроведения;
	
КонецФункции

Функция ТекстЗапросаРеквизитыДокумента(НомераТаблиц)

	НомераТаблиц.Вставить("ТаблицаРеквизиты", НомераТаблиц.Количество());
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ВосстановлениеНДС.Дата КАК Период,
	|	ВосстановлениеНДС.Организация КАК Организация,
	|	ВосстановлениеНДС.Ссылка КАК Регистратор,
	|	НЕОПРЕДЕЛЕНО КАК УпрощенныйУчетНДС,
	|	ВосстановлениеНДС.СчетСписанияНДС,
	|	ВосстановлениеНДС.СубконтоСписанияНДС1,
	|	ВосстановлениеНДС.СубконтоСписанияНДС2,
	|	ВосстановлениеНДС.СубконтоСписанияНДС3,
	|	ВосстановлениеНДС.ПодразделениеОрганизации,
	|	ВосстановлениеНДС.ОтразитьВКнигеПродаж,
	|	ВосстановлениеНДС.СписатьВосстановленныйНДСНаЗатраты КАК СписатьНДС
	|ИЗ
	|	Документ.ВосстановлениеНДС КАК ВосстановлениеНДС
	|ГДЕ
	|	ВосстановлениеНДС.Ссылка = &Ссылка";
	
	Возврат ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();
	
КонецФункции

Функция ТекстЗапросаСоставДокумента(НомераТаблиц)
	
	НомераТаблиц.Вставить("ТаблицаСостав", НомераТаблиц.Количество());
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	ВосстановлениеНДССостав.Ссылка.Организация КАК Организация,
	|	ВосстановлениеНДССостав.Поставщик КАК Покупатель,
	|	ВосстановлениеНДССостав.СчетФактура КАК СчетФактура,
	|	ВосстановлениеНДССостав.ВидЦенности КАК ВидЦенности,
	|	ВосстановлениеНДССостав.СтавкаНДС КАК СтавкаНДС,
	|	ВосстановлениеНДССостав.ДатаОплаты КАК ДатаОплаты,
	|	ВосстановлениеНДССостав.ДокументОплаты КАК ДокументОплаты,
	|	ВосстановлениеНДССостав.НетДанныхОСчетеФактуре,
	|	ВосстановлениеНДССостав.Ссылка.Дата КАК ДатаСобытия,
	|	ЗНАЧЕНИЕ(Перечисление.СобытияПоНДСПокупки.НДСсписанНаРасходы) КАК Событие,
	|	ВосстановлениеНДССостав.ЗаписьДополнительногоЛиста КАК ЗаписьДополнительногоЛиста,
	|	ВЫБОР
	|		КОГДА ВосстановлениеНДССостав.ЗаписьДополнительногоЛиста = ИСТИНА
	|			ТОГДА ВосстановлениеНДССостав.КорректируемыйПериод
	|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
	|	КОНЕЦ КАК КорректируемыйПериод,
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(ВЫРАЗИТЬ(ВосстановлениеНДССостав.СчетФактура.ДоговорКонтрагента КАК Справочник.ДоговорыКонтрагентов).УчетАгентскогоНДС, ЛОЖЬ)
	|			ТОГДА ВЫРАЗИТЬ(ВосстановлениеНДССостав.СчетФактура.ДоговорКонтрагента КАК Справочник.ДоговорыКонтрагентов)
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ДоговорыКонтрагентов.ПустаяСсылка)
	|	КОНЕЦ КАК ДоговорКонтрагента,
	|	ВосстановлениеНДССостав.НДС КАК НДС,
	|	ВосстановлениеНДССостав.СуммаБезНДС КАК СуммаБезНДС,
	|	ВосстановлениеНДССостав.Ссылка.Дата КАК Период,
	|	ВосстановлениеНДССостав.Ссылка КАК Регистратор,
	|	ВосстановлениеНДССостав.СчетУчетаНДС КАК СчетУчетаНДС,
	|	ВосстановлениеНДССостав.Поставщик КАК Поставщик,
	|	ВосстановлениеНДССостав.ИсправленныйСчетФактура,
	|	ВЫБОР
	|		КОГДА ВосстановлениеНДССостав.Ссылка.Дата < ДАТАВРЕМЯ(2015, 1, 1)
	|			ТОГДА """"
	|		ИНАЧЕ ""21""
	|	КОНЕЦ КАК КодВидаОперации,
	|	ВосстановлениеНДССостав.НомерДокументаОплаты,
	|	ВосстановлениеНДССостав.ДатаДокументаОплаты,
	|	ЛОЖЬ КАК СписатьНДСИзРегистра
	|ИЗ
	|	Документ.ВосстановлениеНДС.Состав КАК ВосстановлениеНДССостав
	|ГДЕ
	|	ВосстановлениеНДССостав.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	ВосстановлениеНДССостав.НомерСтроки";
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса,
		"ВЫРАЗИТЬ(ВосстановлениеНДССостав.СчетФактура.ДоговорКонтрагента КАК Справочник.ДоговорыКонтрагентов).УчетАгентскогоНДС",
		БухгалтерскийУчетПереопределяемый.ТекстЗапросаУчетАгентскогоНДС("ВосстановлениеНДССостав.СчетФактура.ДоговорКонтрагента"));
	
	Возврат ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();

КонецФункции

Функция ТекстЗапросаПоФормированиюРегламентнойОперации(НомераТаблиц)

	НомераТаблиц.Вставить("ДанныеРегламентнойОперации", НомераТаблиц.Количество());
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	Реквизиты.Ссылка КАК Регистратор,
	|	НАЧАЛОПЕРИОДА(Реквизиты.Дата, КВАРТАЛ) КАК Период,
	|	Реквизиты.Организация КАК Организация,
	|	ЗНАЧЕНИЕ(Перечисление.РегламентныеОперации.ВосстановлениеНДС) КАК РегламентнаяОперация
	|ИЗ
	|	Документ.ВосстановлениеНДС КАК Реквизиты
	|ГДЕ
	|	Реквизиты.Ссылка = &Ссылка";
	
	Возврат ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();
	
КонецФункции 

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ ПОДГОТОВКИ ДАННЫХ

// Процедура заполнения табличной части по данным подсистемы НДС
//
Процедура ПодготовитьДанныеДляЗаполнения(СтруктураПараметров, АдресХранилища) Экспорт
	
	ДанныеДляЗаполнения = Новый Структура;
	
	ТаблицаВосстановления = СтруктураПараметров.ТаблицаВосстановления;
	
	Дерево_НДСДляВосстановления = ЗаполнитьПоДаннымОперативныхРегистровНДС(СтруктураПараметров);
	Если Дерево_НДСДляВосстановления.Строки.Количество() = 0 Тогда
		// Не обнаружен НДС к восстановлению.
		ДанныеДляЗаполнения.Вставить("ТаблицаВосстановленияНДСПоРеализации", Неопределено);
	Иначе
		
		СписокСчетовФактур = ОбщегоНазначенияБПВызовСервера.УдалитьПовторяющиесяЭлементыМассива(
		Дерево_НДСДляВосстановления.Строки.ВыгрузитьКолонку("СчетФактура"), Истина);
		
		ЗаписиКнигиПокупок = ПолучитьЗаписиКнигиПокупокПоСпискуСФ(СписокСчетовФактур, СтруктураПараметров);
		
		ОпределитьДатуОплатыПоЗаписямКнигиПокупок(Дерево_НДСДляВосстановления, ТаблицаВосстановления,
		СписокСчетовФактур, ЗаписиКнигиПокупок, СтруктураПараметров);
		
		ДанныеДляЗаполнения.Вставить("ТаблицаВосстановленияНДСПоРеализации", ТаблицаВосстановления);
	КонецЕсли; 
	
	ПоместитьВоВременноеХранилище(ДанныеДляЗаполнения, АдресХранилища);
	
КонецПроцедуры

Процедура ПодготовитьДанныеДляЗаполненияНДСПоОстаткам(СтруктураПараметров, АдресХранилища) Экспорт

	// формирование таблицы данных по восстановлению НДС вынесено в отдельную экспортную функцию,
	// для использования в механизме "Длительные операции" при заполнении документа "извне"
	
	ДанныеДляЗаполнения = Новый Структура;

	ТаблицаВосстановленияНДС = СформироватьДанныеДляВосстановленияНДСПоОстаткам(СтруктураПараметров);
	ДанныеДляЗаполнения.Вставить("ТаблицаВосстановленияНДСПоОстаткам", ТаблицаВосстановленияНДС);
	
	ПоместитьВоВременноеХранилище(ДанныеДляЗаполнения, АдресХранилища);
	
КонецПроцедуры

Функция СформироватьДанныеДляВосстановленияНДСПоОстаткам(СтруктураПараметров) Экспорт
	
	РаздельныйУчетНДС = УчетнаяПолитика.РаздельныйУчетНДС(СтруктураПараметров.Организация, СтруктураПараметров.Дата);
	
	СчетаУчетаОС = Новый Массив;
	СчетаУчетаОС.Добавить(ПланыСчетов.Хозрасчетный.ОсновныеСредства);
	СчетаУчетаОС.Добавить(ПланыСчетов.Хозрасчетный.АмортизацияОсновныхСредств);
	
	СчетаУчетаНМА = Новый Массив;
	СчетаУчетаНМА.Добавить(ПланыСчетов.Хозрасчетный.НематериальныеАктивы);
	СчетаУчетаНМА.Добавить(ПланыСчетов.Хозрасчетный.АмортизацияНематериальныхАктивов);
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", 					СтруктураПараметров.Организация);
	Запрос.УстановитьПараметр("ДатаКон", 						Новый Граница(КонецМесяца(СтруктураПараметров.Дата), ВидГраницы.Включая));
	Запрос.УстановитьПараметр("СчетУчетаРБП", 					ПланыСчетов.Хозрасчетный.ПрочиеРасходыБудущихПериодов);
	Запрос.УстановитьПараметр("СчетаУчетаОС", 					СчетаУчетаОС);
	Запрос.УстановитьПараметр("СчетаУчетаНМА", 					СчетаУчетаНМА);
	Запрос.УстановитьПараметр("СубконтоОсновныеСредства", 		ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.ОсновныеСредства);
	Запрос.УстановитьПараметр("СубконтоНематериальныеАктивы", 	ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.НематериальныеАктивы);
	
	НомераТаблиц = Новый Структура;
	
	НомераТаблиц.Вставить("ВТ_СчетаУчетаОС",  НомераТаблиц.Количество());
	НомераТаблиц.Вставить("ВТ_СчетаУчетаНМА", НомераТаблиц.Количество());
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	Хозрасчетный.Ссылка КАК Счет
	|ПОМЕСТИТЬ ВТ_СчетаУчетаОС
	|ИЗ
	|	ПланСчетов.Хозрасчетный КАК Хозрасчетный
	|ГДЕ
	|	Хозрасчетный.Ссылка В ИЕРАРХИИ(&СчетаУчетаОС)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Счет
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Хозрасчетный.Ссылка КАК Счет
	|ПОМЕСТИТЬ ВТ_СчетаУчетаНМА
	|ИЗ
	|	ПланСчетов.Хозрасчетный КАК Хозрасчетный
	|ГДЕ
	|	Хозрасчетный.Ссылка В ИЕРАРХИИ(&СчетаУчетаНМА)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Счет
	|";
	ТекстЗапроса = ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();
	
	Если НЕ РаздельныйУчетНДС Тогда   
		
		НомераТаблиц.Вставить("ВТ_СчетаУчетаТоваров", НомераТаблиц.Количество());
		
		СчетаУчетаТоваров = Новый Массив;
		СчетаУчетаТоваров.Добавить(ПланыСчетов.Хозрасчетный.Товары);
		СчетаУчетаТоваров.Добавить(ПланыСчетов.Хозрасчетный.ТорговаяНаценка);
		СчетаУчетаТоваров.Добавить(ПланыСчетов.Хозрасчетный.ПокупныеТоварыОтгруженные);
		Запрос.УстановитьПараметр("СчетаУчетаТоваров", СчетаУчетаТоваров);
		
		ТекстЗапроса = ТекстЗапроса + 
		"ВЫБРАТЬ
		|	Хозрасчетный.Ссылка КАК Счет
		|ПОМЕСТИТЬ ВТ_СчетаУчетаТоваров
		|ИЗ
		|	ПланСчетов.Хозрасчетный КАК Хозрасчетный
		|ГДЕ
		|	Хозрасчетный.Ссылка В ИЕРАРХИИ(&СчетаУчетаТоваров)
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Счет
		|";
		ТекстЗапроса = ТекстЗапроса + ОбщегоНазначенияБПВызовСервера.ТекстРазделителяЗапросовПакета();
		
	КонецЕсли;
	
	НомераТаблиц.Вставить("ВТ_ОстаточнаяСтоимостьОС", НомераТаблиц.Количество());
	НомераТаблиц.Вставить("ВТ_ОстаточнаяСтоимостьНМА", НомераТаблиц.Количество());
	НомераТаблиц.Вставить("ВТ_ЗапасыПоВидамЦенностей", НомераТаблиц.Количество());
	
	ТекстЗапроса = ТекстЗапроса + 
	"ВЫБРАТЬ
	|	ХозрасчетныйОстатки.СуммаОстатокДт - ХозрасчетныйОстатки.СуммаОстатокКт КАК Сумма,
	|	ХозрасчетныйОстатки.Субконто1
	|ПОМЕСТИТЬ ОстаточнаяСтоимостьОС
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ДатаКон,
	|			Счет В
	|				(ВЫБРАТЬ
	|					ВТ_СчетаУчетаОС.Счет
	|				ИЗ
	|					ВТ_СчетаУчетаОС),
	|			&СубконтоОсновныеСредства,
	|			Организация = &Организация) КАК ХозрасчетныйОстатки
	|ГДЕ
	|	ХозрасчетныйОстатки.СуммаОстатокДт - ХозрасчетныйОстатки.СуммаОстатокКт > 0
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ХозрасчетныйОстатки.СуммаОстатокДт - ХозрасчетныйОстатки.СуммаОстатокКт КАК Сумма,
	|	ХозрасчетныйОстатки.Субконто1
	|ПОМЕСТИТЬ ОстаточнаяСтоимостьНМА
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ДатаКон,
	|			Счет В
	|				(ВЫБРАТЬ
	|					ВТ_СчетаУчетаНМА.Счет
	|				ИЗ
	|					ВТ_СчетаУчетаНМА),
	|			&СубконтоНематериальныеАктивы,
	|			Организация = &Организация) КАК ХозрасчетныйОстатки
	|ГДЕ
	|	ХозрасчетныйОстатки.СуммаОстатокДт - ХозрасчетныйОстатки.СуммаОстатокКт > 0
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ОстаточнаяСтоимостьОС.Сумма КАК Сумма,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.ОС) КАК ВидЦенности,
	|	НЕОПРЕДЕЛЕНО КАК СчетФактура,
	|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.НДС18) КАК СтавкаНДС,
	|	ОстаточнаяСтоимостьОС.Сумма * 18 / 100 КАК СуммаНДС,
	|	ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка) КАК Поставщик,
	|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.НДСприПриобретенииОсновныхСредств) КАК СчетУчетаНДС,
	|	ИСТИНА КАК НетДанныхОСчетеФактуре
	|ПОМЕСТИТЬ ЗапасыПоВидамЦенностей
	|ИЗ
	|	ОстаточнаяСтоимостьОС КАК ОстаточнаяСтоимостьОС
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ОстаточнаяСтоимостьНМА.Сумма,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.НМА),
	|	НЕОПРЕДЕЛЕНО,
	|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.НДС18),
	|	ОстаточнаяСтоимостьНМА.Сумма * 18 / 100,
	|	ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка),
	|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.НДСпоПриобретеннымНематериальнымАктивам),
	|	ИСТИНА
	|ИЗ
	|	ОстаточнаяСтоимостьНМА КАК ОстаточнаяСтоимостьНМА
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ХозрасчетныйОстатки.СуммаОстатокДт,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.ПрочиеРаботыИУслуги),
	|	НЕОПРЕДЕЛЕНО,
	|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.НДС18),
	|	ХозрасчетныйОстатки.СуммаОстатокДт * 18 / 100,
	|	ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка),
	|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.НДСпоПриобретеннымУслугам),
	|	ИСТИНА
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(&ДатаКон, Счет = &СчетУчетаРБП, , Организация = &Организация) КАК ХозрасчетныйОстатки";
	
	Если РаздельныйУчетНДС Тогда
		
		СписокВидовЦенностей = Новый Массив;
		СписокВидовЦенностей.Добавить(Перечисления.ВидыЦенностей.Материалы);
		СписокВидовЦенностей.Добавить(Перечисления.ВидыЦенностей.Товары);
		Запрос.УстановитьПараметр("ВидыЦенностейЗапасы", СписокВидовЦенностей);
		
		ТекстЗапроса = ТекстЗапроса + "
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|";
		
		ТекстЗапроса = ТекстЗапроса + 
		"ВЫБРАТЬ
		|	НДСПоПриобретеннымЦенностямОстатки.СтоимостьОстаток - НДСПоПриобретеннымЦенностямОстатки.НДСОстаток,
		|	НДСПоПриобретеннымЦенностямОстатки.ВидЦенности,
		|	НДСПоПриобретеннымЦенностямОстатки.СчетФактура,
		|	НДСПоПриобретеннымЦенностямОстатки.СтавкаНДС,
		|	НДСПоПриобретеннымЦенностямОстатки.НДСОстаток,
		|	ВЫБОР КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.ВозвратТоваровОтПокупателя
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.ВозвратТоваровОтПокупателя).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.ГТДИмпорт
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.ГТДИмпорт).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.КорректировкаПоступления
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.КорректировкаПоступления).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.ДокументРасчетовСКонтрагентом
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.ДокументРасчетовСКонтрагентом).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.ПоступлениеНМА
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.ПоступлениеНМА).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.ПоступлениеТоваровУслуг
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.ПоступлениеТоваровУслуг).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.СчетФактураПолученный
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.СчетФактураПолученный).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.ОтражениеНДСКВычету
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.ОтражениеНДСКВычету).Контрагент
		|	КОГДА НДСПоПриобретеннымЦенностямОстатки.СчетФактура ССЫЛКА Документ.ПоступлениеДопРасходов
		|		ТОГДА ВЫРАЗИТЬ(НДСПоПриобретеннымЦенностямОстатки.СчетФактура КАК Документ.ПоступлениеДопРасходов).Контрагент
		|	ИНАЧЕ ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка) КОНЕЦ,
		|	НДСПоПриобретеннымЦенностямОстатки.СчетУчетаНДС,
		|	Ложь
		|ИЗ
		|	РегистрНакопления.НДСПоПриобретеннымЦенностям.Остатки(&ДатаКон, Организация = &Организация) КАК НДСПоПриобретеннымЦенностямОстатки
		|ГДЕ
		|	НДСПоПриобретеннымЦенностямОстатки.ВидЦенности В(&ВидыЦенностейЗапасы)
		|	И НЕ НДСПоПриобретеннымЦенностямОстатки.НДСВключенВСтоимость";
		
	Иначе
		
		Запрос.УстановитьПараметр("СчетаУчетаМатериалов", 	
			БухгалтерскийУчетПовтИсп.СчетаВИерархии(ПланыСчетов.Хозрасчетный.Материалы));
		
		ТекстЗапроса = ТекстЗапроса + "
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|";
		
		ТекстЗапроса = ТекстЗапроса + 
		"ВЫБРАТЬ
		|	ХозрасчетныйОстатки.СуммаОстатокДт - ХозрасчетныйОстатки.СуммаОстатокКт,
		|	ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.Товары),
		|	НЕОПРЕДЕЛЕНО,
		|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.НДС18),
		|	(ХозрасчетныйОстатки.СуммаОстатокДт - ХозрасчетныйОстатки.СуммаОстатокКт) * 18 / 100,
		|	ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка),
		|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.НДСпоПриобретеннымМПЗ),
		|	ИСТИНА
		|ИЗ
		|	РегистрБухгалтерии.Хозрасчетный.Остатки(
		|			&ДатаКон,
		|			Счет В
		|				(ВЫБРАТЬ
		|					ВТ_СчетаУчетаТоваров.Счет
		|				ИЗ
		|					ВТ_СчетаУчетаТоваров),
		|			,
		|			Организация = &Организация) КАК ХозрасчетныйОстатки
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	ХозрасчетныйОстатки.СуммаОстатокДт,
		|	ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.Материалы),
		|	НЕОПРЕДЕЛЕНО,
		|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.НДС18),
		|	ХозрасчетныйОстатки.СуммаОстатокДт * 18 / 100,
		|	ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка),
		|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.НДСпоПриобретеннымМПЗ),
		|	ИСТИНА
		|ИЗ
		|	РегистрБухгалтерии.Хозрасчетный.Остатки(&ДатаКон, Счет В (&СчетаУчетаМатериалов), , Организация = &Организация) КАК ХозрасчетныйОстатки";
			
	КонецЕсли;
	
	НомераТаблиц.Вставить("ДанныеДляЗаполнения", НомераТаблиц.Количество());
	
	ТекстЗапроса = ТекстЗапроса + "
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЗапасыПоВидамЦенностей.ВидЦенности КАК ВидЦенности,
	|	ЗапасыПоВидамЦенностей.СчетФактура КАК СчетФактура,
	|	ЗапасыПоВидамЦенностей.СтавкаНДС КАК СтавкаНДС,
	|	ЗапасыПоВидамЦенностей.Поставщик,
	|	ЗапасыПоВидамЦенностей.СчетУчетаНДС,
	|	СУММА(ЗапасыПоВидамЦенностей.Сумма) КАК СуммаБезНДС,
	|	СУММА(ЗапасыПоВидамЦенностей.СуммаНДС) КАК НДС,
	|	ЗапасыПоВидамЦенностей.НетДанныхОСчетеФактуре
	|ИЗ
	|	ЗапасыПоВидамЦенностей КАК ЗапасыПоВидамЦенностей
	|
	|ГДЕ
	|	ЗапасыПоВидамЦенностей.Сумма > 0 И ЗапасыПоВидамЦенностей.СуммаНДС > 0
	|
	|СГРУППИРОВАТЬ ПО
	|	ЗапасыПоВидамЦенностей.ВидЦенности,
	|	ЗапасыПоВидамЦенностей.СчетФактура,
	|	ЗапасыПоВидамЦенностей.СтавкаНДС,
	|	ЗапасыПоВидамЦенностей.Поставщик,
	|	ЗапасыПоВидамЦенностей.СчетУчетаНДС,
	|	ЗапасыПоВидамЦенностей.НетДанныхОСчетеФактуре
	|
	|УПОРЯДОЧИТЬ ПО
	|	ВидЦенности,
	|	СчетФактура,
	|	СтавкаНДС";
	
	Запрос.Текст = ТекстЗапроса;
	Результат = Запрос.ВыполнитьПакет();	                                                          
	
	Возврат Результат[НомераТаблиц["ДанныеДляЗаполнения"]].Выгрузить();
	
КонецФункции // ПодготовитьДанныеДляЗаполненияНДСПоОстаткам()

Функция ЗаполнитьПоДаннымОперативныхРегистровНДС(Объект)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	НДСПредъявленныйОстатки.Организация КАК Организация,
	|	НДСПредъявленныйОстатки.СчетФактура КАК СчетФактура,
	|	НДСПредъявленныйОстатки.ВидЦенности КАК ВидЦенности,
	|	НДСПредъявленныйОстатки.СтавкаНДС КАК СтавкаНДС,
	|	НДСПредъявленныйОстатки.СчетУчетаНДС КАК СчетУчетаНДС,
	|	НДСПредъявленныйОстатки.СуммаБезНДСОстаток КАК СуммаБезНДСОстаток,
	|	НДСПредъявленныйОстатки.НДСОстаток КАК НДСОстаток
	|ПОМЕСТИТЬ ВТ_ОстаткиНДСКВычету
	|ИЗ
	|	РегистрНакопления.НДСПредъявленный.Остатки(&КонецПериода, Организация = &Организация) КАК НДСПредъявленныйОстатки
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	НДСПоПриобретеннымЦенностямОстатки.Организация,
	|	НДСПоПриобретеннымЦенностямОстатки.СчетФактура,
	|	НДСПоПриобретеннымЦенностямОстатки.ВидЦенности,
	|	НДСПоПриобретеннымЦенностямОстатки.СтавкаНДС,
	|	НДСПоПриобретеннымЦенностямОстатки.СчетУчетаНДС,
	|	-НДСПоПриобретеннымЦенностямОстатки.СтоимостьОстаток,
	|	-НДСПоПриобретеннымЦенностямОстатки.НДСОстаток
	|ИЗ
	|	РегистрНакопления.НДСПоПриобретеннымЦенностям.Остатки(
	|			&КонецПериода,
	|			Организация = &Организация
	|				И НДСВключенВСтоимость = ЛОЖЬ
	|				И ВидЦенности = ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.ОС)
	|				И СчетУчета <> ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ОборудованиеКУстановке)) КАК НДСПоПриобретеннымЦенностямОстатки
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	НДСПредъявленныйРеализация0Остатки.Организация,
	|	НДСПредъявленныйРеализация0Остатки.СчетФактура,
	|	НДСПредъявленныйРеализация0Остатки.ВидЦенности,
	|	НДСПредъявленныйРеализация0Остатки.СтавкаНДС,
	|	НДСПредъявленныйРеализация0Остатки.СчетУчетаНДС,
	|	-НДСПредъявленныйРеализация0Остатки.СуммаБезНДСОстаток,
	|	-НДСПредъявленныйРеализация0Остатки.НДСОстаток
	|ИЗ
	|	РегистрНакопления.НДСПредъявленныйРеализация0.Остатки(&КонецПериода, Организация = &Организация) КАК НДСПредъявленныйРеализация0Остатки
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	СчетФактура
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ОстаткиНДСКВычету.Организация,
	|	ОстаткиНДСКВычету.СчетФактура КАК СчетФактура,
	|	ОстаткиНДСКВычету.ВидЦенности,
	|	ОстаткиНДСКВычету.СтавкаНДС,
	|	ОстаткиНДСКВычету.СчетУчетаНДС,
	|	СУММА(-ОстаткиНДСКВычету.СуммаБезНДСОстаток) КАК СуммаБезНДС,
	|	СУММА(-ОстаткиНДСКВычету.НДСОстаток) КАК НДС,
	|	СУММА(-ОстаткиНДСКВычету.СуммаБезНДСОстаток - ОстаткиНДСКВычету.НДСОстаток) КАК СуммаСНДС_КВычету,
	|	ДанныеПервичныхДокументовСчетФактура.ДатаРегистратора КАК СчетФактураДата
	|ИЗ
	|	ВТ_ОстаткиНДСКВычету КАК ОстаткиНДСКВычету
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ДанныеПервичныхДокументов КАК ДанныеПервичныхДокументовСчетФактура
	|		ПО ОстаткиНДСКВычету.СчетФактура = ДанныеПервичныхДокументовСчетФактура.Документ
	|			И (ДанныеПервичныхДокументовСчетФактура.Организация = &Организация)
	|
	|СГРУППИРОВАТЬ ПО
	|	ОстаткиНДСКВычету.СтавкаНДС,
	|	ОстаткиНДСКВычету.СчетУчетаНДС,
	|	ОстаткиНДСКВычету.СчетФактура,
	|	ОстаткиНДСКВычету.ВидЦенности,
	|	ОстаткиНДСКВычету.Организация,
	|	ДанныеПервичныхДокументовСчетФактура.ДатаРегистратора
	|
	|ИМЕЮЩИЕ
	|	СУММА(ОстаткиНДСКВычету.СуммаБезНДСОстаток) + СУММА(ОстаткиНДСКВычету.НДСОстаток) < 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	СчетФактураДата
	|ИТОГИ
	|	СУММА(СуммаБезНДС),
	|	СУММА(НДС),
	|	СУММА(СуммаСНДС_КВычету)
	|ПО
	|	СчетФактура";
	
	Запрос.УстановитьПараметр("Организация", Объект.Организация);
	Запрос.УстановитьПараметр("КонецПериода", Новый	Граница(КонецДня(Объект.Дата), ВидГраницы.Включая));
	
	ДеревоРезультата = Запрос.Выполнить().Выгрузить(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	// Определение поставщика
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация"		, Объект.Организация);
	Запрос.УстановитьПараметр("СчетаФактуры"	, ДеревоРезультата.Строки.ВыгрузитьКолонку("СчетФактура"));
	Запрос.УстановитьПараметр("ПустойКонтрагент", Справочники.Контрагенты.ПустаяСсылка());
	
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	НДСПредъявленный.Поставщик,
	|	НДСПредъявленный.СчетФактура
	|ИЗ
	|	РегистрНакопления.НДСПредъявленный КАК НДСПредъявленный
	|ГДЕ
	|	НДСПредъявленный.Поставщик <> &ПустойКонтрагент
	|	И НДСПредъявленный.СчетФактура В(&СчетаФактуры)
	|	И НДСПредъявленный.Организация = &Организация";
	
	КонтрагентПоСчетуФактуре = Запрос.Выполнить().Выгрузить();
	КонтрагентПоСчетуФактуре.Индексы.Добавить("СчетФактура");
	
	ДеревоРезультата.Колонки.Добавить("Поставщик", Новый ОписаниеТипов("СправочникСсылка.Контрагенты"));
	
	Для Каждого СтрокаОбрабатываемая  Из ДеревоРезультата.Строки Цикл
		СтрокаКонтрагента = КонтрагентПоСчетуФактуре.Найти(СтрокаОбрабатываемая.СчетФактура,"СчетФактура");
		Если СтрокаКонтрагента <> Неопределено Тогда
			СтрокаОбрабатываемая.Поставщик = СтрокаКонтрагента.Поставщик;
			Для Каждого СтрокаРасшифровки Из СтрокаОбрабатываемая.Строки Цикл
				СтрокаРасшифровки.Поставщик = СтрокаОбрабатываемая.Поставщик;
			КонецЦикла; 
		ИначеЕсли ОбщегоНазначенияБП.ЕстьРеквизитДокумента("Контрагент", СтрокаОбрабатываемая.СчетФактура.Метаданные()) Тогда
			СтрокаОбрабатываемая.Поставщик = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(
				СтрокаОбрабатываемая.СчетФактура, "Контрагент");
			Для Каждого СтрокаРасшифровки Из СтрокаОбрабатываемая.Строки Цикл
				СтрокаРасшифровки.Поставщик = СтрокаОбрабатываемая.Поставщик;
			КонецЦикла; 
		КонецЕсли; 
	КонецЦикла;
	
	Возврат ДеревоРезультата;
	
КонецФункции

// Процедура вызывается из ЗаполнитьСтрокиДокумента.
// По списку счетов-фактур определяет суммы не использованных ранее распределенных оплат.
Функция ПолучитьЗаписиКнигиПокупокПоСпискуСФ(СписокСчетовФактур, Объект)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	НДСЗаписиКнигиПокупокОбороты.Поставщик КАК Поставщик,
	|	НДСЗаписиКнигиПокупокОбороты.СчетФактура КАК СчетФактура,
	|	НДСЗаписиКнигиПокупокОбороты.ВидЦенности,
	|	НДСЗаписиКнигиПокупокОбороты.СтавкаНДС,
	|	НДСЗаписиКнигиПокупокОбороты.СчетУчетаНДС,
	|	НДСЗаписиКнигиПокупокОбороты.ДатаОплаты КАК ДатаОплаты,
	|	НДСЗаписиКнигиПокупокОбороты.ДокументОплаты КАК ДокументОплаты,
	|	СУММА(НДСЗаписиКнигиПокупокОбороты.СуммаБезНДСОборот) КАК СуммаБезНДС,
	|	СУММА(НДСЗаписиКнигиПокупокОбороты.НДСОборот) КАК НДС
	|ПОМЕСТИТЬ ВТ_НДСЗаписиКнигиПокупокОбороты
	|ИЗ
	|	РегистрНакопления.НДСЗаписиКнигиПокупок.Обороты(
	|			,
	|			&КонецПериода,
	|			Период,
	|			Организация = &Организация
	|				И СчетФактура В (&СписокСчетовФактур)) КАК НДСЗаписиКнигиПокупокОбороты
	|
	|СГРУППИРОВАТЬ ПО
	|	НДСЗаписиКнигиПокупокОбороты.ВидЦенности,
	|	НДСЗаписиКнигиПокупокОбороты.ДокументОплаты,
	|	НДСЗаписиКнигиПокупокОбороты.СтавкаНДС,
	|	НДСЗаписиКнигиПокупокОбороты.СчетУчетаНДС,
	|	НДСЗаписиКнигиПокупокОбороты.ДатаОплаты,
	|	НДСЗаписиКнигиПокупокОбороты.Поставщик,
	|	НДСЗаписиКнигиПокупокОбороты.СчетФактура
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НДСЗаписиКнигиПокупокОбороты.Поставщик КАК Поставщик,
	|	НДСЗаписиКнигиПокупокОбороты.СчетФактура КАК СчетФактура,
	|	НДСЗаписиКнигиПокупокОбороты.ВидЦенности,
	|	НДСЗаписиКнигиПокупокОбороты.СтавкаНДС,
	|	НДСЗаписиКнигиПокупокОбороты.СчетУчетаНДС,
	|	НДСЗаписиКнигиПокупокОбороты.ДатаОплаты КАК ДатаОплаты,
	|	НДСЗаписиКнигиПокупокОбороты.ДокументОплаты КАК ДокументОплаты,
	|	НДСЗаписиКнигиПокупокОбороты.СуммаБезНДС КАК СуммаБезНДС,
	|	НДСЗаписиКнигиПокупокОбороты.НДС КАК НДС
	|ИЗ
	|	ВТ_НДСЗаписиКнигиПокупокОбороты КАК НДСЗаписиКнигиПокупокОбороты
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ДанныеПервичныхДокументов КАК ДанныеПервичныхДокументовСчетФактура
	|		ПО НДСЗаписиКнигиПокупокОбороты.СчетФактура = ДанныеПервичныхДокументовСчетФактура.Документ
	|			И (ДанныеПервичныхДокументовСчетФактура.Организация = &Организация)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ДанныеПервичныхДокументов КАК ДанныеПервичныхДокументовДокументОплаты
	|		ПО НДСЗаписиКнигиПокупокОбороты.ДокументОплаты = ДанныеПервичныхДокументовДокументОплаты.Документ
	|			И (ДанныеПервичныхДокументовДокументОплаты.Организация = &Организация)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДанныеПервичныхДокументовСчетФактура.ДатаРегистратора,
	|	ДатаОплаты,
	|	ДанныеПервичныхДокументовДокументОплаты.ДатаРегистратора
	|ИТОГИ
	|	СУММА(СуммаБезНДС),
	|	СУММА(НДС)
	|ПО
	|	СчетФактура";
	
	Запрос.УстановитьПараметр("КонецПериода", 		Новый Граница(КонецДня(Объект.Дата), ВидГраницы.Включая));
	Запрос.УстановитьПараметр("Организация", 		Объект.Организация);
	Запрос.УстановитьПараметр("СписокСчетовФактур", СписокСчетовФактур);
	
	ЗаписиКнигиПокупок = Запрос.Выполнить().Выгрузить(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	Возврат ЗаписиКнигиПокупок;	
	
КонецФункции

Процедура ОпределитьДатуОплатыПоЗаписямКнигиПокупок(Дерево_НДСДляВосстановления, ТаблицаРезультатов, 
		СписокСчетовФактур, ЗаписиКнигиПокупок, Объект)
	
	Для Каждого СтрокаСФ Из Дерево_НДСДляВосстановления.Строки Цикл
		НаборЗаписейКнигиПокупокПоСФ = ЗаписиКнигиПокупок.Строки.Найти(СтрокаСФ.СчетФактура,"СчетФактура");
		
		Если НаборЗаписейКнигиПокупокПоСФ = Неопределено Тогда
			// Сокращенный вариант - записи книги не обнаружены, отражаем по текущим данным,
			// без указания документа и даты оплаты.
			Для Каждого ЗаписьПОСФ Из СтрокаСФ.Строки Цикл
				СтрокаРезультата = ТаблицаРезультатов.Добавить();
				СтрокаРезультата.СчетФактура	= ЗаписьПОСФ.СчетФактура;
				СтрокаРезультата.Поставщик		= ЗаписьПОСФ.Поставщик;
				СтрокаРезультата.ВидЦенности	= ЗаписьПОСФ.ВидЦенности;
				СтрокаРезультата.СчетУчетаНДС	= ЗаписьПОСФ.СчетУчетаНДС;
				СтрокаРезультата.СтавкаНДС		= ЗаписьПОСФ.СтавкаНДС;
				
				СтрокаРезультата.СуммаБезНДС	= ЗаписьПОСФ.СуммаБезНДС;
				СтрокаРезультата.НДС			= ЗаписьПОСФ.НДС;
				Если НЕ Объект.ОтразитьВКнигеПродаж Тогда
					СтрокаРезультата.ЗаписьДополнительногоЛиста = Истина;
						СтрокаРезультата.КорректируемыйПериод = ЗаписьПОСФ.СчетФактураДата;
				КонецЕсли;
			КонецЦикла; 
			
			Продолжить;
		КонецЕсли;
		
		Для Каждого ЗаписьПОСФ Из СтрокаСФ.Строки Цикл
			Отбор = Новый Структура("Поставщик, ВидЦенности, СчетУчетаНДС, СтавкаНДС", ЗаписьПОСФ.Поставщик,
			ЗаписьПОСФ.ВидЦенности, ЗаписьПОСФ.СчетУчетаНДС, ЗаписьПОСФ.СтавкаНДС);
			
			ЗаписиКнигиПоОтбору = НаборЗаписейКнигиПокупокПоСФ.Строки.НайтиСтроки(Отбор);
			
			Для Каждого СторнируемаяЗаписьКнигиПокупок Из ЗаписиКнигиПоОтбору Цикл
				КВосстановлению_БезНДС = макс(0, мин(ЗаписьПОСФ.СуммаБезНДС, СторнируемаяЗаписьКнигиПокупок.СуммаБезНДС));
				КВосстановлению_НДС = макс(0, мин(ЗаписьПОСФ.НДС, СторнируемаяЗаписьКнигиПокупок.НДС));
				Если КВосстановлению_БезНДС = 0 И КВосстановлению_НДС = 0 Тогда
					Продолжить;
				КонецЕсли; 
				
				СтрокаРезультата = ТаблицаРезультатов.Добавить();
				СтрокаРезультата.СчетФактура	= ЗаписьПОСФ.СчетФактура;
				СтрокаРезультата.Поставщик		= ЗаписьПОСФ.Поставщик;
				СтрокаРезультата.ВидЦенности	= ЗаписьПОСФ.ВидЦенности;
				СтрокаРезультата.СчетУчетаНДС	= ЗаписьПОСФ.СчетУчетаНДС;
				СтрокаРезультата.СтавкаНДС		= ЗаписьПОСФ.СтавкаНДС;
				
				СтрокаРезультата.СуммаБезНДС	= КВосстановлению_БезНДС;
				СтрокаРезультата.НДС			= КВосстановлению_НДС;
				
				СтрокаРезультата.ДатаОплаты		= СторнируемаяЗаписьКнигиПокупок.ДатаОплаты;
				СтрокаРезультата.ДокументОплаты	= СторнируемаяЗаписьКнигиПокупок.ДокументОплаты;
				Если НЕ Объект.ОтразитьВКнигеПродаж Тогда
					СтрокаРезультата.ЗаписьДополнительногоЛиста = Истина;
					СтрокаРезультата.КорректируемыйПериод = ЗаписьПОСФ.СчетФактураДата;
					Если КонецКвартала(СтрокаРезультата.КорректируемыйПериод) = КонецКвартала(Объект.Дата) Тогда
						СтрокаРезультата.ЗаписьДополнительногоЛиста = Ложь;
						СтрокаРезультата.КорректируемыйПериод = '00010101';
					КонецЕсли;
				КонецЕсли;
				
				ЗаписьПОСФ.СуммаБезНДС						= ЗаписьПОСФ.СуммаБезНДС - КВосстановлению_БезНДС;
				ЗаписьПОСФ.НДС								= ЗаписьПОСФ.НДС - КВосстановлению_НДС;
				
				СторнируемаяЗаписьКнигиПокупок.СуммаБезНДС	= СторнируемаяЗаписьКнигиПокупок.СуммаБезНДС - КВосстановлению_БезНДС;
				СторнируемаяЗаписьКнигиПокупок.НДС 			= СторнируемаяЗаписьКнигиПокупок.НДС - КВосстановлению_НДС;
				
			КонецЦикла; 
			
			Если ЗаписьПОСФ.СуммаБезНДС > 0 ИЛИ ЗаписьПОСФ.НДС > 0 Тогда
				
				СтрокаРезультата = ТаблицаРезультатов.Добавить();
				СтрокаРезультата.СчетФактура	= ЗаписьПОСФ.СчетФактура;
				СтрокаРезультата.Поставщик		= ЗаписьПОСФ.Поставщик;
				СтрокаРезультата.ВидЦенности	= ЗаписьПОСФ.ВидЦенности;
				СтрокаРезультата.СчетУчетаНДС	= ЗаписьПОСФ.СчетУчетаНДС;
				СтрокаРезультата.СтавкаНДС		= ЗаписьПОСФ.СтавкаНДС;
				СтрокаРезультата.ЗаписьДополнительногоЛиста = Истина;
				СтрокаРезультата.КорректируемыйПериод = ЗаписьПОСФ.СчетФактураДата;
				
				СтрокаРезультата.СуммаБезНДС	= ЗаписьПОСФ.СуммаБезНДС;
				СтрокаРезультата.НДС			= ЗаписьПОСФ.НДС;
				
			КонецЕсли; 
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ПЕЧАТИ

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	// Восстановление НДС
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "ВосстановлениеНДС";
	КомандаПечати.Представление = НСтр("ru = 'Восстановление НДС'");
	КомандаПечати.Обработчик    = "УправлениеПечатьюБПКлиент.ВыполнитьКомандуПечати";
	
КонецПроцедуры

Функция ПечатьВосстановлениеНДС(МассивОбъектов, ОбъектыПечати)

	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос();
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ВосстановлениеНДС.СчетФактура КАК СчетФактура,
	|	ВосстановлениеНДС.ДокументОплаты КАК ДокументОплаты,
	|	ВосстановлениеНДС.Поставщик КАК КонтрагентПоСчетуФактуре,
	|	НЕОПРЕДЕЛЕНО КАК СтавкаНДС_Аванс,
	|	НЕОПРЕДЕЛЕНО КАК ДоговорАванса,
	|	ЛОЖЬ КАК НаАванс,
	|	ЛОЖЬ КАК ВозвратЧерезКомиссионера,
	|	ВосстановлениеНДС.Ссылка.Организация
	|ПОМЕСТИТЬ ЗаписиКнигиПокупок
	|ИЗ
	|	Документ.ВосстановлениеНДС.Состав КАК ВосстановлениеНДС
	|ГДЕ
	|	ВосстановлениеНДС.Ссылка В(&МассивОбъектов)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	СчетФактура,
	|	ДокументОплаты,
	|	КонтрагентПоСчетуФактуре,
	|	ДоговорАванса,
	|	НаАванс,
	|	СтавкаНДС_Аванс
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ЗаписиКнигиПокупок.Организация
	|ИЗ
	|	ЗаписиКнигиПокупок КАК ЗаписиКнигиПокупок";
	
	Выгрузка = Запрос.Выполнить().Выгрузить();
	Запрос.УстановитьПараметр("Организация",             Выгрузка.ВыгрузитьКолонку("Организация"));
	Запрос.УстановитьПараметр("КонецПериода",            '00010101');
	Запрос.УстановитьПараметр("ПравилаПостановления735", Истина);
	
	Отчеты.КнигаПокупок.ПолучитьСчетаФактурыДокументыКнигаПокупок(Запрос);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ВосстановлениеНДС.Ссылка КАК Ссылка,
	|	ВосстановлениеНДС.Ссылка КАК Регистратор,
	|	ВосстановлениеНДС.Ссылка.Номер,
	|	ВосстановлениеНДС.Ссылка.Дата,
	|	ВосстановлениеНДС.Ссылка.Организация,
	|	ВосстановлениеНДС.НомерСтроки,
	|	ВосстановлениеНДС.НетДанныхОСчетеФактуре,
	|	ВосстановлениеНДС.Поставщик,
	|	ВосстановлениеНДС.СчетФактура,
	|	ВосстановлениеНДС.СтавкаНДС,
	|	ВосстановлениеНДС.СуммаБезНДС,
	|	ВосстановлениеНДС.НДС,
	|	ВосстановлениеНДС.ВидЦенности,
	|	ТаблицаСчетаФактурыДокументы.ДатаСчетаФактуры,
	|	ТаблицаСчетаФактурыДокументы.НомерСчетаФактуры
	|ИЗ
	|	Документ.ВосстановлениеНДС.Состав КАК ВосстановлениеНДС
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаСчетаФактурыДокументы КАК ТаблицаСчетаФактурыДокументы
	|		ПО ВосстановлениеНДС.СчетФактура = ТаблицаСчетаФактурыДокументы.СчетФактура
	|ГДЕ
	|	ВосстановлениеНДС.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ВосстановлениеНДС.Ссылка.Дата,
	|	ВосстановлениеНДС.Ссылка,
	|	ВосстановлениеНДС.НомерСтроки
	|ИТОГИ ПО
	|	Ссылка";

	Шапка = Запрос.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам, "Ссылка");
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	ТабличныйДокумент.АвтоМасштаб           = Истина;
	ТабличныйДокумент.ОриентацияСтраницы    = ОриентацияСтраницы.Ландшафт;
	ТабличныйДокумент.ИмяПараметровПечати   = "ПАРАМЕТРЫ_ПЕЧАТИ_ВосстановлениеНДС";

	ПервыйДокумент = Истина;
	
	Пока Шапка.Следующий() Цикл

		Если НЕ ПервыйДокумент Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;

		ПервыйДокумент = Ложь;
		// Запомним номер строки, с которой начали выводить текущий документ.
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;

		Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ВосстановлениеНДС.ПФ_MXL_ВосстановлениеНДС");

		// Выводим шапку накладной

		ОбластьМакета = Макет.ПолучитьОбласть("Заголовок");

		СведенияОбОрганизации 	= БухгалтерскийУчетПереопределяемый.СведенияОЮрФизЛице(Шапка.Организация, Шапка.Дата);
		ОтветственныеЛица 		= ОтветственныеЛицаБП.ОтветственныеЛица(Шапка.Организация, Шапка.Дата);

		ОбластьМакета.Параметры.ТекстЗаголовка = ОбщегоНазначенияБПВызовСервера.СформироватьЗаголовокДокумента(Шапка, "Восстановление НДС");
		ОбластьМакета.Параметры.ПредставлениеОрганизации = ОбщегоНазначенияБПВызовСервера.ОписаниеОрганизации(СведенияОбОрганизации, "НаименованиеДляПечатныхФорм");
		
		ТабличныйДокумент.Вывести(ОбластьМакета);

		// Вывести табличную часть
		ОбластьМакета = Макет.ПолучитьОбласть("ШапкаТаблицы");
		ТабличныйДокумент.Вывести(ОбластьМакета);

		ВсегоСумма 	= 0;
		ВсегоНДС 	= 0;
		
		ВыборкаСтрок = Шапка.Выбрать(ОбходРезультатаЗапроса.Прямой);
		Пока ВыборкаСтрок.Следующий() Цикл
			
			ВсегоСумма 	= ВсегоСумма + ВыборкаСтрок.СуммаБезНДС;
			ВсегоНДС 	= ВсегоНДС + ВыборкаСтрок.НДС;
			
			ОбластьМакетаСтрока = Макет.ПолучитьОбласть("Строка");
			ОбластьМакетаСтрока.Параметры.Заполнить(ВыборкаСтрок);
			
			Если ВыборкаСтрок.НетДанныхОСчетеФактуре Тогда
				ТекстСчетФактура = ОбщегоНазначенияБПВызовСервера.СформироватьЗаголовокДокумента(Шапка, "Восстановление НДС");
			ИначеЕсли Не ЗначениеЗаполнено(ВыборкаСтрок.ДатаСчетаФактуры) Тогда
				ТекстСчетФактура = ОбщегоНазначенияБПВызовСервера.СформироватьЗаголовокДокумента(ВыборкаСтрок.СчетФактура);
			Иначе
				ТекстСчетФактура = "Счет-фактура № " + ВыборкаСтрок.НомерСчетаФактуры
												+ " от " + Формат(ВыборкаСтрок.ДатаСчетаФактуры, "ДФ='дд ММММ гггг'") + " г.";
			КонецЕсли;
			
			ОбластьМакетаСтрока.Параметры.СчетФактура = ТекстСчетФактура;
			ТабличныйДокумент.Вывести(ОбластьМакетаСтрока);
						
		КонецЦикла;
		
		// Вывести Итого
		ОбластьМакета = Макет.ПолучитьОбласть("Итого");
		ОбластьМакета.Параметры.ВсегоСумма	 = ОбщегоНазначенияБПВызовСервера.ФорматСумм(ВсегоСумма);
		ОбластьМакета.Параметры.ВсегоНДС	 = ОбщегоНазначенияБПВызовСервера.ФорматСумм(ВсегоНДС);
		ТабличныйДокумент.Вывести(ОбластьМакета);

		// Вывести подписи
		ОбластьМакета = Макет.ПолучитьОбласть("Подписи");
		ОбластьМакета.Параметры.ГлавныйБухгалтер      		= ОтветственныеЛица.ГлавныйБухгалтерПредставление;
		ОбластьМакета.Параметры.ДолжностьГлавногоБухгалтера = ОтветственныеЛица.ГлавныйБухгалтерДолжностьПредставление;
		ТабличныйДокумент.Вывести(ОбластьМакета);

		// В табличном документе зададим имя области, в которую был
		// выведен объект. Нужно для возможности печати покомплектно.
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент,
			НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);

	КонецЦикла;

	Возврат ТабличныйДокумент;

КонецФункции

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм,ОбъектыПечати, ПараметрыВывода) Экспорт

	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ВосстановлениеНДС") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "ВосстановлениеНДС", "Восстановление НДС",
			ПечатьВосстановлениеНДС(МассивОбъектов, ОбъектыПечати), , "Документ.ВосстановлениеНДС.ПФ_MXL_ВосстановлениеНДС");
	КонецЕсли;
	
	ОбщегоНазначенияБП.ЗаполнитьДополнительныеПараметрыПечати(МассивОбъектов, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода);	

КонецПроцедуры

#КонецЕсли