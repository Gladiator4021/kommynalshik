﻿
////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	// ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ДополнительныеОтчетыИОбработки
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	ОбщегоНазначенияБПВызовСервера.УстановитьОтборПоОсновнойОрганизации(ЭтаФорма);
	
	МожноРедактировать = ПравоДоступа("Редактирование", Метаданные.Документы.СчетФактураПолученный);
	Элементы.СписокКонтекстноеМенюИзменитьВыделенные.Видимость = МожноРедактировать;
	
	ИспользоватьКорректировочныеДокументы = ПолучитьФункциональнуюОпцию("ИспользоватьКорректировочныеДокументы");
	Элементы.ГруппаИсправление.Видимость  = ИспользоватьКорректировочныеДокументы;
	Элементы.Корректировка.Видимость      = ИспользоватьКорректировочныеДокументы;
	
	// СтандартныеПодсистемы.РаботаСКонтрагентами
	ПроверкаКонтрагентов.ПриСозданииНаСервереСписокДокументов(Список);
	// Конец СтандартныеПодсистемы.РаботаСКонтрагентами
	
	// ИнтернетПоддержкаПользователей.Новости.КонтекстныеНовости_ПриСозданииНаСервере
	ИдентификаторыСобытийПриОткрытии = "ПриОткрытии";
	ОбработкаНовостейПереопределяемый.КонтекстныеНовости_ПриСозданииНаСервере(
		ЭтаФорма,
		"БП.Документ.СчетФактураПолученный",
		"ФормаСписка",
		НСтр("ru='Новости: Счета-фактуры полученные'"),
		ИдентификаторыСобытийПриОткрытии
	);
	// Конец ИнтернетПоддержкаПользователей.Новости.КонтекстныеНовости_ПриСозданииНаСервере
	
	// Подсистема "ОбменСКонтрагентами".
	ПараметрыПриСозданииНаСервере = ОбменСКонтрагентами.ПараметрыПриСозданииНаСервере_ФормаСписка();
	ПараметрыПриСозданииНаСервере.Форма = ЭтотОбъект;
	ПараметрыПриСозданииНаСервере.МестоРазмещенияКоманд = Элементы.КомандыЭДО;
	ОбменСКонтрагентами.ПриСозданииНаСервере_ФормаСписка(Отказ, СтандартнаяОбработка, ПараметрыПриСозданииНаСервере);
	// Конец подсистема "ОбменСКонтрагентами".
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// ИнтернетПоддержкаПользователей.Новости.ПриОткрытии
	ОбработкаНовостейКлиент.КонтекстныеНовости_ПриОткрытии(ЭтаФорма);
	// Конец ИнтернетПоддержкаПользователей.Новости.ПриОткрытии
	
	// Подсистема "ОбменСКонтрагентами"
	ОбменСКонтрагентамиКлиент.ПриОткрытии(ЭтотОбъект);
	// Конец Подсистема "ОбменСКонтрагентами"
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ИзменениеОсновнойОрганизации" Тогда
		ОбщегоНазначенияБПКлиент.ИзменитьОтборПоОсновнойОрганизации(Список, ,Параметр);
	КонецЕсли;
	
	// ИнтернетПоддержкаПользователей.Новости.ОбработкаОповещения
	ОбработкаНовостейКлиент.КонтекстныеНовости_ОбработкаОповещения(ЭтотОбъект, ИмяСобытия, Параметр, Источник);
	// Конец ИнтернетПоддержкаПользователей.Новости.ОбработкаОповещения
	
	ПрисоединенныеФайлыБПКлиент.ОбновитьСписокПослеДобавленияФайла(ЭтотОбъект, ИмяСобытия, Параметр, Источник);
	
	// Подсистема "ОбменСКонтрагентами".
	ПараметрыОповещенияЭДО = ОбменСКонтрагентамиКлиент.ПараметрыОповещенияЭДО_ФормаСписка();
	ПараметрыОповещенияЭДО.Форма = ЭтотОбъект;
	ПараметрыОповещенияЭДО.ИмяДинамическогоСписка = "Список";
	ОбменСКонтрагентамиКлиент.ОбработкаОповещения_ФормаСписка(ИмяСобытия, Параметр, Источник, ПараметрыОповещенияЭДО);
	// Конец подсистема "ОбменСКонтрагентами".
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ТАБЛИЦЫ ФОРМЫ СПИСОК

&НаКлиенте
Процедура СписокПриИзменении(Элемент)
	
	ДанныеСтроки = Элемент.ТекущиеДанные;
	Если ДанныеСтроки = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Оповестить("Запись_СчетФактураПолученный", , ДанныеСтроки.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередНачаломИзменения(Элемент, Отказ)

	ДанныеСтроки = Элемент.ТекущиеДанные;
	Если ДанныеСтроки = Неопределено Тогда
		Возврат;
	КонецЕсли;

	Если ДанныеСтроки.ВидСчетаФактуры = ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.НаПоступление") Тогда
		Если ТипЗнч(ДанныеСтроки.ДокументОснование) = Тип("ДокументСсылка.АвансовыйОтчет") Тогда
			КлючеваяОперация = "ОткрытиеФормыСчетФактураПолученныйБланкСтрогойОтчетности";
		Иначе
			КлючеваяОперация = "ОткрытиеФормыСчетФактураПолученныйНаПоступление";
		КонецЕсли;
	ИначеЕсли ДанныеСтроки.ВидСчетаФактуры = ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.НаАванс")
		ИЛИ ДанныеСтроки.ВидСчетаФактуры = ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.НаАвансКомитента") Тогда
		КлючеваяОперация = "ОткрытиеФормыСчетФактураПолученныйНаАванс";
	ИначеЕсли ДанныеСтроки.ВидСчетаФактуры = ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.Корректировочный") Тогда
		КлючеваяОперация = "ОткрытиеФормыСчетФактураПолученныйКорректировочный";
	КонецЕсли;
	
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, КлючеваяОперация);

КонецПроцедуры

&НаСервере
Процедура СписокПередЗагрузкойПользовательскихНастроекНаСервере(Элемент, Настройки)

	ОбщегоНазначенияБП.ВосстановитьОтборСписка(Список, Настройки, "Организация");
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПеретаскивание(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка, Строка, Поле)
	
	Если Строка <> Неопределено Тогда
		
		Если ПрисоединенныеФайлыБПКлиент.ПараметрыПеретаскиванияСодержатФайлы(ПараметрыПеретаскивания) Тогда
			
			СтандартнаяОбработка = Ложь;
			
			ДополнительныеПараметры = Новый Структура;
			ДополнительныеПараметры.Вставить("Ссылка"                 , Строка);
			ДополнительныеПараметры.Вставить("ПараметрыПеретаскивания", ПараметрыПеретаскивания);
			ДополнительныеПараметры.Вставить("УникальныйИдентификатор", УникальныйИдентификатор);
			
			ОписаниеОповещения = Новый ОписаниеОповещения("ПеретаскиваниеФайловОтветПолучен",
				ПрисоединенныеФайлыБПКлиент,
				ДополнительныеПараметры);
			ШаблонВопроса = НСтр("ru='Присоединить файлы к документу %1?'");
			ТекстВопроса = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонВопроса, Строка);
			ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса,РежимДиалогаВопрос.ДаНет);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПроверкаПеретаскивания(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка, Строка, Поле)
	
	СтандартнаяОбработка = Ложь;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ПоказатьКонтекстныеНовости(Команда)

	ОбработкаНовостейКлиент.КонтекстныеНовости_ОбработкаКомандыНовости(
		ЭтаФорма,
		Команда
	);

КонецПроцедуры

&НаКлиенте
Процедура ИзменитьВыделенные(Команда)
	
	ГрупповоеИзменениеОбъектовКлиент.ИзменитьВыделенные(Элементы.Список);

КонецПроцедуры

&НаКлиенте
Процедура СоздатьСчетФактуруПоступление(Команда)
	
	КлючеваяОперация = "СозданиеФормыСчетФактураПолученныйНаПоступление";
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, КлючеваяОперация);
	
	СтруктураПараметров = ПолучитьСтруктуруПараметровФормы(ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.НаПоступление"));
	ОткрытьФорму("Документ.СчетФактураПолученный.Форма.ФормаДокументаНаПоступление", СтруктураПараметров, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьСчетФактураАванс(Команда)

	КлючеваяОперация = "СозданиеФормыСчетФактураПолученныйНаАванс";
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, КлючеваяОперация);
	
	СтруктураПараметров = ПолучитьСтруктуруПараметровФормы(ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.НаАванс"));
	ОткрытьФорму("Документ.СчетФактураПолученный.Форма.ФормаДокументаНаАванс", СтруктураПараметров, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьСчетФактуруАвансКомитента(Команда)
	
	КлючеваяОперация = "СозданиеФормыСчетФактураПолученныйНаАванс";
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, КлючеваяОперация);
	
	СтруктураПараметров = ПолучитьСтруктуруПараметровФормы(ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.НаАвансКомитента"));
	ОткрытьФорму("Документ.СчетФактураПолученный.Форма.ФормаДокументаНаАванс", СтруктураПараметров, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьСчетФактураКорректировочный(Команда)
	
	// Для корректировочного счета-фактуры показывается сначала форма подбора исходного документа,
	// поэтому счетчик для него здесь не включаем.

	СтруктураПараметров = ПолучитьСтруктуруПараметровФормы(ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.Корректировочный"));
	ОткрытьФорму("Документ.СчетФактураПолученный.Форма.ФормаПодбора", СтруктураПараметров, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьСчетФактураИсправление(Команда)

	// Для исправленного счета-фактуры показывается сначала форма подбора исходного документа,
	// поэтому счетчик для него здесь не включаем.
	
	СтруктураПараметров = ПолучитьСтруктуруПараметровФормы(ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.ПустаяСсылка"));
	ОткрытьФорму("Документ.СчетФактураПолученный.Форма.ФормаПодбора", СтруктураПараметров, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьИсправлениеСобственнойОшибки(Команда)
	
	СтруктураПараметров = ПолучитьСтруктуруПараметровФормы(ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.ПустаяСсылка"));
	ЗначенияЗаполнения = СтруктураПараметров.ЗначенияЗаполнения;
	ЗначенияЗаполнения.ИсправлениеСобственнойОшибки = Истина;
	ОткрытьФорму("Документ.СчетФактураПолученный.Форма.ФормаПодбора", СтруктураПараметров, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьЖурналСчетовФактур(Команда)
	
	СтруктураПараметров = Новый Структура;
	ОтборПоОрганизации  = ВернутьПолеПользовательскогоОтбораКомпоновки(ЭтаФорма,, "Организация");
	
	Если ОтборПоОрганизации <> Неопределено И ОтборПоОрганизации.Использование Тогда
		СтруктураПараметров.Вставить("Организация", ОтборПоОрганизации.ПравоеЗначение);
	КонецЕсли;
	
	ОткрытьФорму("Отчет.ЖурналУчетаСчетовФактур.Форма", СтруктураПараметров);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьКнигуПокупок(Команда)
	
	СтруктураПараметров = Новый Структура;
	ОтборПоОрганизации  = ВернутьПолеПользовательскогоОтбораКомпоновки(ЭтаФорма,, "Организация");
	
	Если ОтборПоОрганизации <> Неопределено И ОтборПоОрганизации.Использование Тогда
		СтруктураПараметров.Вставить("Организация", ОтборПоОрганизации.ПравоеЗначение);
	КонецЕсли;
	
	ОткрытьФорму("Отчет.КнигаПокупок.Форма", СтруктураПараметров);
	
КонецПроцедуры
 
////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

// Процедура показывает новости, требующие прочтения (важные и очень важные)
//
// Параметры:
//  Нет
//
&НаКлиенте
Процедура Подключаемый_ПоказатьНовостиТребующиеПрочтенияПриОткрытии()

	// ИнтернетПоддержкаПользователей.Новости.Подключаемый_ПоказатьНовостиТребующиеПрочтенияПриОткрытии
	ИдентификаторыСобытийПриОткрытии = "ПриОткрытии";
	// Конец ИнтернетПоддержкаПользователей.Новости.Подключаемый_ПоказатьНовостиТребующиеПрочтенияПриОткрытии

	ОбработкаНовостейКлиент.КонтекстныеНовости_ПоказатьНовостиТребующиеПрочтенияПриОткрытии(ЭтаФорма, ИдентификаторыСобытийПриОткрытии);

КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуЭДО(Команда)
	
	ЭлектронноеВзаимодействиеСлужебныйКлиент.ВыполнитьПодключаемуюКомандуЭДО(Команда, ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбработчикОжиданияЭДО()

	ОбменСКонтрагентамиКлиент.ОбработчикОжиданияЭДО(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Функция ПолучитьСтруктуруПараметровФормы(ВидСчетаФактуры)
	
	СтруктураПараметров = Новый Структура;
	
	ОтборПоОрганизации = ВернутьПолеПользовательскогоОтбораКомпоновки(ЭтаФорма,, "Организация");
	ОтборПоКонтрагенту = ВернутьПолеПользовательскогоОтбораКомпоновки(ЭтаФорма,, "Контрагент");
	
	ЗначенияЗаполнения = Новый Структура();
	
	Если ОтборПоОрганизации <> Неопределено И ОтборПоОрганизации.Использование И ЗначениеЗаполнено(ОтборПоОрганизации.ПравоеЗначение) Тогда
		ЗначенияЗаполнения.Вставить("Организация", ОтборПоОрганизации.ПравоеЗначение);
	КонецЕсли;
	
	Если ОтборПоКонтрагенту <> Неопределено И ОтборПоКонтрагенту.Использование И ЗначениеЗаполнено(ОтборПоКонтрагенту.ПравоеЗначение) Тогда
		ЗначенияЗаполнения.Вставить("Контрагент", ОтборПоКонтрагенту.ПравоеЗначение);
	КонецЕсли;
	
	ЗначенияЗаполнения.Вставить("ВидСчетаФактуры", ВидСчетаФактуры);
	ЗначенияЗаполнения.Вставить("ИсправлениеСобственнойОшибки", Ложь);
	
	СтруктураПараметров.Вставить("ЗначенияЗаполнения", ЗначенияЗаполнения);
	
	Если ВидСчетаФактуры = ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.Корректировочный")
		ИЛИ ВидСчетаФактуры = ПредопределенноеЗначение("Перечисление.ВидСчетаФактурыПолученного.ПустаяСсылка") Тогда
		
		ЗначениеОтбора = Новый Структура();
		ЗначениеОтбора.Вставить("Исправление", Ложь);
		
		Если ОтборПоОрганизации <> Неопределено Тогда
			ЗначениеОтбора.Вставить("Организация", ОтборПоОрганизации.ПравоеЗначение);
			ЗначениеОтбора.Вставить("ОрганизацияИспользование", ОтборПоОрганизации.Использование);
		КонецЕсли;

		Если ОтборПоКонтрагенту <> Неопределено Тогда
			ЗначениеОтбора.Вставить("Контрагент", ОтборПоКонтрагенту.ПравоеЗначение);
			ЗначениеОтбора.Вставить("КонтрагентИспользование", ОтборПоКонтрагенту.Использование);
		КонецЕсли;
		
		СтруктураПараметров.Вставить("Отбор", ЗначениеОтбора); 
		
	КонецЕсли;
	
	Возврат СтруктураПараметров;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ВернутьПолеПользовательскогоОтбораКомпоновки(Форма, Настройки = Неопределено, Знач ИмяПоля)
	
	Если Настройки = Неопределено Тогда
		Настройки = Форма.Список.КомпоновщикНастроек.ПользовательскиеНастройки;
	КонецЕсли;
	
	ПолеОтбораКомпоновки = Неопределено;
	ЛевоеЗначение = Новый ПолеКомпоновкиДанных(ИмяПоля);
	Для каждого ЭлементОтбора Из Форма.Список.КомпоновщикНастроек.Настройки.Отбор.Элементы Цикл
		Если ЭлементОтбора.ЛевоеЗначение = ЛевоеЗначение Тогда
			ПолеОтбораКомпоновки = Настройки.Элементы.Найти(ЭлементОтбора.ИдентификаторПользовательскойНастройки);
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
	Возврат ПолеОтбораКомпоновки;
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ БСП

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.Список);
	
КонецПроцедуры


// Конец СтандартныеПодсистемы.Печать