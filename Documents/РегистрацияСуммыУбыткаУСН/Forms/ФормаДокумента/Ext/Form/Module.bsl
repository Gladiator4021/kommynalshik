﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	// ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ДополнительныеОтчетыИОбработки
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	ПодготовитьФормуНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ПараметрыЗаписи.Вставить("Организация", Объект.Организация);
	ПараметрыЗаписи.Вставить("Год",         Год(Объект.Дата));
	
	Оповестить("Запись_РегистрацияСуммыУбыткаУСН", ПараметрыЗаписи, Объект.Ссылка);
	
КонецПроцедуры

&НаСервере
Процедура ПодготовитьФормуНаСервере()

	ОбщегоНазначенияБПКлиентСервер.УстановитьПараметрыФункциональныхОпцийФормыДокумента(ЭтаФорма);

	НачалоПериода = ВыборПериодаКлиентСервер.НачалоПериодаОтчета(Перечисления.ДоступныеПериодыОтчета.Год, Объект.Дата);
	КонецПериода  = ВыборПериодаКлиентСервер.КонецПериодаОтчета(Перечисления.ДоступныеПериодыОтчета.Год, Объект.Дата);
	ЭтаФорма.Год  = ВыборПериодаКлиентСервер.ПолучитьПредставлениеПериодаОтчета(Перечисления.ДоступныеПериодыОтчета.Год, НачалоПериода, КонецПериода);
 
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОбщегоНазначенияБПКлиентСервер.УстановитьПараметрыФункциональныхОпцийФормыДокумента(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ГодНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ВидПериода = ПредопределенноеЗначение("Перечисление.ДоступныеПериодыОтчета.Год");
	НачалоПериода = Объект.Дата;
	ОписаниеОповещения = Новый ОписаниеОповещения("ГодНачалоВыбораЗавершение", ЭтотОбъект);
	ВыборПериодаКлиент.ПериодНачалоВыбора(ЭтаФорма, Элемент, СтандартнаяОбработка, ВидПериода, НачалоПериода, ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ГодНачалоВыбораЗавершение(СтруктураПериода, ДополнительныеПараметры) Экспорт
	
	// Установим полученный период
	Если СтруктураПериода <> Неопределено Тогда
		Год = СтруктураПериода.Период;
		Объект.Дата = СтруктураПериода.КонецПериода;
	КонецЕсли;
	ГодНачалоВыбораНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ГодНачалоВыбораНаСервере()
	
	Документы.РегистрацияСуммыУбыткаУСН.ОбработатьИзменениеГодаНаСервере(Объект);
	
КонецПроцедуры

&НаКлиенте
Процедура ГодОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ВидПериода = ПредопределенноеЗначение("Перечисление.ДоступныеПериодыОтчета.Год");
	НачалоПериода = Объект.Дата;
	ВыборПериодаКлиент.ПериодОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка, ВидПериода, Год, НачалоПериода, Объект.Дата);		
		
КонецПроцедуры

&НаКлиенте
Процедура ГодПриИзменении(Элемент)
	
	НачалоПериода = Объект.Дата;	
	
	ВыборПериодаКлиент.ПериодПриИзменении(Элемент, Год, НачалоПериода, Объект.Дата);
	ОбщегоНазначенияБПКлиентСервер.УстановитьПараметрыФункциональныхОпцийФормыДокумента(ЭтаФорма);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ БСП

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.Печать
