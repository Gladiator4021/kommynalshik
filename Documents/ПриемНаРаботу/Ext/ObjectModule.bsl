﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИФизическимЛицам(ЭтотОбъект, Таблица, "Организация", "ФизическоеЛицо");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	ДанныеДляПроведения = ПолучитьДанныеДляПроведения();
	
	КадровыйУчет.СформироватьКадровыеДвижения(ЭтотОбъект, Движения, ДанныеДляПроведения.КадровыеДвижения);
	
	СтруктураПлановыхНачислений = Новый Структура;
	СтруктураПлановыхНачислений.Вставить("ДанныеОПлановыхНачислениях", ДанныеДляПроведения.ПлановыеНачисления);
	
	РасчетЗарплаты.СформироватьДвиженияПлановыхНачислений(ЭтотОбъект, Движения, СтруктураПлановыхНачислений);
	РасчетЗарплаты.СформироватьДвиженияПлановыхВыплат(Движения, ДанныеДляПроведения.КадровыеДвижения);
	
	УчетСтажаПФР.ЗарегистрироватьПериодыВУчетеСтажаПФР(Движения, ДанныеДляРегистрацииВУчетаСтажаПФР());

КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ДатаПриема = ТекущаяДатаСеанса();
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("СправочникСсылка.Сотрудники") Тогда
		ЗарплатаКадры.ЗаполнитьПоОснованиюСотрудником(ЭтотОбъект, ДанныеЗаполнения, Истина, Ложь)
	КонецЕсли;
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		
		Если ДанныеЗаполнения.Свойство("Сотрудник") Тогда
			Сотрудник = ДанныеЗаполнения.Сотрудник;
		КонецЕсли;
		
		Если ДанныеЗаполнения.Свойство("ДатаПриема") Тогда
			ДатаПриема = ДанныеЗаполнения.ДатаПриема;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	КадровыйУчет.ПроверитьВозможностьПроведенияПоКадровомуУчету(
		ЭтотОбъект.ДатаПриема,
		ЭтотОбъект.Сотрудник,
		ЭтотОбъект.Ссылка,
		Отказ,
		Перечисления.ВидыКадровыхСобытий.Прием);
		
	КадровыйУчет.ПроверитьСоответствиеСотрудниковОрганизации(ЭтотОбъект.Организация, ЭтотОбъект.Сотрудник, Отказ);
	
	СообщениеПроверкиВидЗанятости = СотрудникиФормы.СообщениеОКонфликтеВидаЗанятостиНовогоСотрудникаССуществующими(
		Сотрудник,
		ФизическоеЛицо,
		Организация,
		ВидЗанятости,
		ДатаПриема);
		
	Если Не ПустаяСтрока(СообщениеПроверкиВидЗанятости) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(СообщениеПроверкиВидЗанятости, , "ВидЗанятости", "Объект", Отказ);
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Получает данные для формирования движений.
// Возвращает Структуру с полями.
//		КадровыеДвижения - данные, необходимые для формирования 
//				- кадровой истории (см. КадровыйУчетРасширенный.СформироватьКадровыеДвижения)
//				- авансов (см. РасчетЗарплатыРасширенный.СформироватьДвиженияПлановыхВыплат)
//				- истории применяемых графиков работы (см. КадровыйУчетРасширенный.СформироватьИсториюИзмененияГрафиков).
//		ПлановыеНачисления - данные, необходимые для формирования истории плановых начислений.
//		(см. РасчетЗарплатыРасширенный.СформироватьДвиженияПлановыхНачислений)
//		ЗначенияПоказателей (см. там же).
//
Функция ПолучитьДанныеДляПроведения()
	
	Запрос = Новый Запрос;
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПриемНаРаботу.ДатаПриема КАК ДатаСобытия,
	|	ПриемНаРаботу.Сотрудник КАК Сотрудник,
	|	ПриемНаРаботу.Сотрудник.ГоловнаяОрганизация КАК ГоловнаяОрганизация,
	|	ПриемНаРаботу.Организация КАК Организация,
	|	ПриемНаРаботу.Подразделение КАК Подразделение,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Прием) КАК ВидСобытия,
	|	ПриемНаРаботу.Должность КАК Должность,
	|	ПриемНаРаботу.ВидЗанятости КАК ВидЗанятости,
	|	ПриемНаРаботу.ФизическоеЛицо,
	|	ПриемНаРаботу.Аванс,
	|	ПриемНаРаботу.СпособРасчетаАванса
	|ИЗ
	|	Документ.ПриемНаРаботу КАК ПриемНаРаботу
	|ГДЕ
	|	ПриемНаРаботу.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПриемНаРаботуНачисления.Ссылка.ДатаПриема КАК ДатаСобытия,
	|	ПриемНаРаботуНачисления.Ссылка.Сотрудник КАК Сотрудник,
	|	ПриемНаРаботуНачисления.Начисление,
	|	ПриемНаРаботуНачисления.Размер,
	|	ПриемНаРаботуНачисления.Ссылка.ФизическоеЛицо,
	|	ПриемНаРаботуНачисления.Ссылка.Организация.ГоловнаяОрганизация КАК ГоловнаяОрганизация
	|ИЗ
	|	Документ.ПриемНаРаботу.Начисления КАК ПриемНаРаботуНачисления
	|ГДЕ
	|	ПриемНаРаботуНачисления.Ссылка = &Ссылка";
	
	РезультатыЗапроса = Запрос.ВыполнитьПакет();
	
	ДанныеДляПроведения = Новый Структура; 
	
	// Первый набор данных для проведения - таблица для формирования кадровых движений, истории графиков, авансов.
	ДанныеДляПроведения.Вставить("КадровыеДвижения", РезультатыЗапроса[0].Выгрузить());
	
	// Второй набор данных для проведения - таблица для формирования плановых начислений.
	ДанныеДляПроведения.Вставить("ПлановыеНачисления", РезультатыЗапроса[1].Выгрузить());
	
	Возврат ДанныеДляПроведения;
	
КонецФункции

Функция ДанныеДляРегистрацииВУчетаСтажаПФР()
	МассивСсылок = Новый Массив;
	МассивСсылок.Добавить(Ссылка);
	
	ДанныеДляРегистрацииВУчете = Документы.ПриемНаРаботу.ДанныеДляРегистрацииВУчетаСтажаПФР(МассивСсылок);
		
	Возврат ДанныеДляРегистрацииВУчете[Ссылка];
														
КонецФункции

#КонецОбласти

#КонецЕсли