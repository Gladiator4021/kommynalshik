﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Если Параметры.Ключ.Пустая() Тогда
		
		ЗначенияДляЗаполнения = Новый Структура("Организация, Ответственный, ДатаСобытия", 
			"Объект.Организация", "Объект.Ответственный", "Объект.ДатаОперации");
			ЗарплатаКадры.ЗаполнитьПервоначальныеЗначенияВФорме(ЭтаФорма, ЗначенияДляЗаполнения);
		
		ПриПолученииДанныхНаСервере();
		
	КонецЕсли;
	
	УчетНДФЛФормы.СведенияОДоходахПриСозданииНаСервере(ЭтаФорма, "Объект.СведенияОДоходах.КодДохода", "СведенияОДоходахКодВычета", "СведенияОДоходахСуммаВычета");
	УстановитьОформлениеКолонкиИсчисленногоНалога(ЭтаФорма);
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.Печать
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
	ПриПолученииДанныхНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДатеВТабличнойЧасти(Объект.ПредоставленныеВычеты, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДатеВТабличнойЧасти(Объект.НДФЛПеречисленный, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");
	Оповестить("ЗаписьДокумента", Объект.Ссылка);
	
КонецПроцедуры

#Область ПроцедурыОбработчикиСобытийТабличногоПоляСведенияОДоходах

&НаКлиенте
Процедура СведенияОДоходахДатаПолученияДоходаПриИзменении(Элемент)

	ДанныеТекущейСтроки = Элементы.СведенияОДоходах.ТекущиеДанные;
	
	Если Не ЗначениеЗаполнено(ДанныеТекущейСтроки.КодДохода) Тогда
		Возврат;
	КонецЕсли;
	
	УчетНДФЛКлиент.КодДоходаПриИзменении(
		ЭтаФорма, 
		Год(ДанныеТекущейСтроки.ДатаПолученияДохода),
		"СведенияОДоходах", 
		"КодДохода", 
		"СведенияОДоходахКодВычета", 
		"КодВычета", 
		"СуммаВычета");
		
КонецПроцедуры

&НаКлиенте
Процедура СведенияОДоходахКодДоходаПриИзменении(Элемент)
	
	ДанныеТекущейСтроки = Элементы.СведенияОДоходах.ТекущиеДанные;
	
	Если ДанныеТекущейСтроки.КодДохода <> ПредопределенноеЗначение("Справочник.ВидыДоходовНДФЛ.Код2762") Тогда
		ДанныеТекущейСтроки.Количество = 0;
	КонецЕсли;
	
	УчетНДФЛКлиент.КодДоходаПриИзменении(
		ЭтаФорма, 
		Год(ДанныеТекущейСтроки.ДатаПолученияДохода),
		"СведенияОДоходах", 
		"КодДохода", 
		"СведенияОДоходахКодВычета", 
		"КодВычета", 
		"СуммаВычета");
		
КонецПроцедуры

&НаКлиенте
Процедура СведенияОДоходахПриАктивизацииСтроки(Элемент)
	ДанныеТекущейСтроки = Элементы.СведенияОДоходах.ТекущиеДанные;
	
	Если ДанныеТекущейСтроки <> Неопределено Тогда
		УчетНДФЛКлиент.КодДоходаАктивацииСтроки(
			ЭтаФорма, 
			Год(ДанныеТекущейСтроки.ДатаПолученияДохода), 
			"СведенияОДоходах", 
			"КодДохода", 
			"СведенияОДоходахКодВычета"); 
	КонецЕсли;		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТаблицыФормыНДФЛИсчисленныйПоСтавке13

&НаКлиенте
Процедура НДФЛИсчисленныйПоСтавке13ПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	Если НоваяСтрока Тогда
		Элемент.ТекущиеДанные.МесяцНалоговогоПериода = КонецМесяца(ОбщегоНазначенияКлиент.ДатаСеанса());
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТаблицыФормыПредоставленныеВычеты

&НаКлиенте
Процедура ПредоставленныеВычетыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	Если НоваяСтрока Тогда
		Элемент.ТекущиеДанные.МесяцНалоговогоПериода = НачалоМесяца(ОбщегоНазначенияКлиент.ДатаСеанса());
		ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДате(Элемент.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");	
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПредоставленныеВычетыМесяцНалоговогоПериодаСтрокойПриИзменении(Элемент)
	
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(Элементы.ПредоставленныеВычеты.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой", Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура ПредоставленныеВычетыМесяцНалоговогоПериодаСтрокойНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтаФорма, Элементы.ПредоставленныеВычеты.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");
	
КонецПроцедуры

&НаКлиенте
Процедура ПредоставленныеВычетыМесяцНалоговогоПериодаСтрокойРегулирование(Элемент, Направление, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(Элементы.ПредоставленныеВычеты.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой", Направление, Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура ПредоставленныеВычетыМесяцНалоговогоПериодаСтрокойАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПредоставленныеВычетыМесяцНалоговогоПериодаСтрокойОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТаблицыФормыНДФЛУдержанный

&НаКлиенте
Процедура НДФЛУдержанныйПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	Если НоваяСтрока Тогда
		Элемент.ТекущиеДанные.МесяцНалоговогоПериода = КонецМесяца(ОбщегоНазначенияКлиент.ДатаСеанса());
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТаблицыФормыНДФЛПеречисленный

&НаКлиенте
Процедура НДФЛПеречисленныйПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	Если НоваяСтрока Тогда
		Элемент.ТекущиеДанные.МесяцНалоговогоПериода = НачалоМесяца(ОбщегоНазначенияКлиент.ДатаСеанса());
		ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДате(Элемент.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");	
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура НДФЛПеречисленныйМесяцНалоговогоПериодаСтрокойПриИзменении(Элемент)
	
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(Элементы.НДФЛПеречисленный.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой", Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПеречисленныйМесяцНалоговогоПериодаСтрокойНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтаФорма, Элементы.НДФЛПеречисленный.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПеречисленныйМесяцНалоговогоПериодаСтрокойРегулирование(Элемент, Направление, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(Элементы.НДФЛПеречисленный.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой", Направление, Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПеречисленныйМесяцНалоговогоПериодаСтрокойАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПеречисленныйМесяцНалоговогоПериодаСтрокойОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ПриПолученииДанныхНаСервере()
	
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДатеВТабличнойЧасти(Объект.ПредоставленныеВычеты, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДатеВТабличнойЧасти(Объект.НДФЛПеречисленный, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");
	
КонецПроцедуры

&НаСервере
Процедура УстановитьОформлениеКолонкиИсчисленногоНалога(Форма)
	
	СписокДоходов = Новый СписокЗначений;
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ВидыДоходовНДФЛ.Ссылка,
	|	ВидыДоходовНДФЛ.Код
	|ИЗ
	|	Справочник.ВидыДоходовНДФЛ КАК ВидыДоходовНДФЛ
	|ГДЕ
	|	ВидыДоходовНДФЛ.СтавкаНалогообложенияРезидента = ЗНАЧЕНИЕ(Перечисление.НДФЛСтавкиНалогообложенияРезидента.Ставка13)";
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		СписокДоходов.Добавить(Выборка.Ссылка, Выборка.Код);
	КонецЦикла;
	
	ЭлементУсловногоОформления = Форма.УсловноеОформление.Элементы.Добавить();
	ЭлементУсловногоОформления.Использование = Истина;
	
	ЭлементОтбора = ЭлементУсловногоОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ЭлементОтбора.Использование = Истина;
	ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.ВСписке;
	ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.СведенияОДоходах.КодДохода");
	ЭлементОтбора.ПравоеЗначение = СписокДоходов;
	
	ОформляемоеПоле =  ЭлементУсловногоОформления.Поля.Элементы.Добавить();
	ОформляемоеПоле.Использование = Истина;
	ОформляемоеПоле.Поле = Новый ПолеКомпоновкиДанных("СведенияОДоходахСуммаНалогаИсчисленная");
	
	ЭлементОформления = ЭлементУсловногоОформления.Оформление.Элементы.Найти("ТолькоПросмотр");
	ЭлементОформления.Использование = Истина;
	ЭлементОформления.Значение = Истина;
	
КонецПроцедуры	

#КонецОбласти
