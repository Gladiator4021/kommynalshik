﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ВзаиморасчетыССотрудникамиФормы.ВедомостьПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.Печать
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения

	ВзаиморасчетыССотрудникамиФормы.ВедомостьПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект); 
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОбновитьКомандыОбмена(ЭтотОбъект, Объект.ЗарплатныйПроект);
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если ПараметрыЗаписи.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		
		ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "ПроведениеДокументаВедомостьНаВыплатуЗарплатыВБанк");
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	ОповеститьОбИзменении(Тип("РегистрСведенийКлючЗаписи.СостоянияДокументовЗачисленияЗарплаты"));
	Оповестить("ЗаписьДокумента", Объект.Ссылка);
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	ВзаиморасчетыССотрудникамиКлиент.ВедомостьОбработкаОповещения(ЭтаФорма, ИмяСобытия, Параметр, Источник);
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	ВзаиморасчетыССотрудникамиФормы.ОбработкаПроверкиЗаполненияНаСервере(ЭтаФорма, Отказ, ПроверяемыеРеквизиты)
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	ВзаиморасчетыССотрудникамиФормы.ВедомостьПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи); 
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОрганизацияПриИзмененииНаСервере();
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОбновитьКомандыОбмена(ЭтотОбъект, Объект.ЗарплатныйПроект);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗарплатныйПроектПриИзменении(Элемент)
	
	УстановитьВидимостьЛицевыхСчетов();
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОбновитьКомандыОбмена(ЭтотОбъект, Объект.ЗарплатныйПроект);
	
КонецПроцедуры

&НаКлиенте
Процедура РуководительПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура ГлавныйБухгалтерПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура БухгалтерПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

#Область РедактированиеМесяцаСтрокой

&НаКлиенте
Процедура ПериодРегистрацииПриИзменении(Элемент)
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(ЭтаФорма, "Объект.ПериодРегистрации", "ПериодРегистрацииСтрокой", Модифицированность);
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтаФорма, ЭтаФорма, "Объект.ПериодРегистрации", "ПериодРегистрацииСтрокой");
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииРегулирование(Элемент, Направление, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(ЭтаФорма, "Объект.ПериодРегистрации", "ПериодРегистрацииСтрокой", Направление, Модифицированность);
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиСобытийТаблицыФормы

&НаКлиенте
Процедура СоставВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ВзаиморасчетыССотрудникамиКлиент.ВедомостьСоставВыбор(ЭтаФорма, Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)	
КонецПроцедуры

&НаКлиенте
Процедура СоставОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	СоставОбработкаВыбораНаСервере(ВыбранноеЗначение, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура СоставПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	Если Не Копирование Тогда
		ВзаиморасчетыССотрудникамиКлиент.ВедомостьПодобрать(ЭтаФорма);
	КонецЕсли;	
	Отказ = Истина;
КонецПроцедуры

&НаКлиенте
Процедура СоставПередУдалением(Элемент, Отказ)
	ВзаиморасчетыССотрудникамиКлиент.ВедомостьСоставПередУдалением(ЭтаФорма, Элемент, Отказ) 
КонецПроцедуры

&НаКлиенте
Процедура СоставПослеУдаления(Элемент)
	СоставПослеУдаленияНаСервере()
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормы

&НаКлиенте
Процедура СоставНомерЛицевогоСчетаПриИзменении(Элемент)
	СоставПриИзмененииНаСервере();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаКлиенте
Процедура Заполнить(Команда)
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "ЗаполнениеДокументаВедомостьНаВыплатуЗарплатыВБанк");
	ВзаиморасчетыССотрудникамиКлиент.ВедомостьЗаполнить(ЭтаФорма)
КонецПроцедуры

&НаКлиенте
Процедура Подобрать(Команда)
	
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "ПодборСотрудникаВФормеДокументаВедомостьНаВыплатуЗарплатыВБанк");
	
	ВзаиморасчетыССотрудникамиКлиент.ВедомостьПодобрать(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура РедактироватьЗарплату(Команда)
	РедактироватьЗарплатуСтроки(Элементы.Состав.ТекущиеДанные);	
КонецПроцедуры

&НаКлиенте
Процедура ОплатыПредставлениеОбработкаНавигационнойСсылки(Элемент, НавигационнаяСсылкаФорматированнойСтроки, СтандартнаяОбработка)
	ВзаиморасчетыССотрудникамиКлиент.ВедомостьОплатаПоказать(ЭтаФорма, Элемент, НавигационнаяСсылкаФорматированнойСтроки, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОткрытьДокументПодтверждение(Команда)
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОткрытьДокументПодтверждение(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ЗагрузитьДокументПодтверждение(Команда)
	
	ОткрытьФорму("Документ.ПодтверждениеЗачисленияЗарплаты.ФормаОбъекта");
	
КонецПроцедуры

&НаКлиенте
Процедура ОтправитьВБанк(Команда)
	
	ОбменСБанкамиКлиент.СформироватьПодписатьОтправитьЭД(Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьОтправленныйДокумент(Команда)
	
	ОбменСБанкамиКлиент.ОткрытьАктуальныйЭД(Объект.Ссылка, ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура УстановитьВидимостьЛицевыхСчетов()
	
	ПроектВыбран = ЗначениеЗаполнено(Объект.ЗарплатныйПроект);
	
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СоставНомерЛицевогоСчета", "АвтоОтметкаНезаполненного", ПроектВыбран);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СоставНомерЛицевогоСчета", "Видимость", ПроектВыбран);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "НомерРеестра", "Видимость", ПроектВыбран);
	
КонецПроцедуры

#Область ВызовыСервера

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере() Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьОрганизацияПриИзмененииНаСервере(ЭтаФорма);
	УстановитьВидимостьЛицевыхСчетов();
КонецПроцедуры

&НаСервере
Процедура СоставОбработкаВыбораНаСервере(ВыбранноеЗначение, СтандартнаяОбработка)
	ВзаиморасчетыССотрудникамиФормы.ВедомостьСоставОбработкаВыбораНаСервере(ЭтаФорма, ВыбранноеЗначение, СтандартнаяОбработка)
КонецПроцедуры

&НаСервере
Процедура СоставПриИзмененииНаСервере()
	ВзаиморасчетыССотрудникамиФормы.ВедомостьСоставПриИзмененииНаСервере(ЭтаФорма)
КонецПроцедуры

&НаСервере
Процедура СоставПослеУдаленияНаСервере()
	ВзаиморасчетыССотрудникамиФормы.ВедомостьСоставПослеУдаленияНаСервере(ЭтаФорма)
КонецПроцедуры

&НаСервере
Процедура ПодписантПриИзмененииНаСервере()
	ВзаиморасчетыССотрудникамиФормы.ВедомостьПодписантПриИзмененииНаСервере(ЭтаФорма)
КонецПроцедуры

#КонецОбласти

#Область ОбратныеВызовы

&НаСервере
Процедура ЗаполнитьПервоначальныеЗначения() Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьЗаполнитьПервоначальныеЗначения(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура ПриПолученииДанныхНаСервере(ТекущийОбъект) Экспорт
	
	ВзаиморасчетыССотрудникамиФормы.ВедомостьПриПолученииДанныхНаСервере(ЭтаФорма, ТекущийОбъект);
	
	УстановитьВидимостьЛицевыхСчетов();
	ОбменСБанкамиПоЗарплатнымПроектам.ГруппаПодтверждениеИзБанкаДополнитьФорму(ЭтотОбъект);
	ОбменСБанкамиПоЗарплатнымПроектам.КомандыОбменаДополнитьФорму(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПриПолученииДанныхСтрокиСостава(СтрокаСостава) Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьПриПолученииДанныхСтрокиСостава(ЭтаФорма, СтрокаСостава)
КонецПроцедуры

&НаСервере
Процедура ОбработатьСообщенияПользователю() Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьОбработатьСообщенияПользователю(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьЭлементов() Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьУстановитьДоступностьЭлементов(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура НастроитьОтображениеГруппыПодписей() Экспорт
	ЗарплатаКадры.НастроитьОтображениеГруппыПодписей(Элементы.ПодписиГруппа, "Объект.Руководитель", "Объект.ГлавныйБухгалтер", "Объект.Бухгалтер");
КонецПроцедуры

&НаСервере
Процедура УстановитьПредставлениеОплаты() Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьУстановитьПредставлениеОплаты(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьНаСервере() Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьЗаполнитьНаСервере(ЭтаФорма);
КонецПроцедуры

&НаСервере
Функция АдресСпискаПодобранныхСотрудников() Экспорт
	Возврат ВзаиморасчетыССотрудникамиФормы.ВедомостьАдресСпискаПодобранныхСотрудников(ЭтаФорма)
КонецФункции

&НаКлиенте
Процедура РедактироватьЗарплатуСтроки(ДанныеСтроки) Экспорт
	ВзаиморасчетыССотрудникамиКлиент.ВедомостьРедактироватьЗарплатуСтроки(ЭтаФорма, ДанныеСтроки);	
КонецПроцедуры

&НаСервере
Процедура РедактироватьЗарплатуСтрокиЗавершениеНаСервере(РезультатыРедактирования) Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьРедактироватьЗарплатуСтрокиЗавершениеНаСервере(ЭтаФорма, РезультатыРедактирования) 
КонецПроцедуры

&НаСервере
Функция АдресВХранилищеЗарплатыПоСтроке(ИдентификаторСтроки) Экспорт
	Возврат ВзаиморасчетыССотрудникамиФормы.ВедомостьАдресВХранилищеЗарплатыПоСтроке(ЭтаФорма, ИдентификаторСтроки)
КонецФункции	

#КонецОбласти

#КонецОбласти
