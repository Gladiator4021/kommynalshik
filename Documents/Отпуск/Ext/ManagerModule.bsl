﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
#Область ПрограммныйИнтерфейс

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	КадровыйУчет.ДобавитьКомандуПечатиПриказаОПредоставленииОтпуска(КомандыПечати);
	
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.МенеджерПечати = "Обработка.ПечатьРасчетаСреднегоЗаработка";
	КомандаПечати.Идентификатор = "РасчетСреднегоЗаработка";
	КомандаПечати.Представление = НСтр("ru = 'Расчет среднего заработка'");
	КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
			
КонецПроцедуры

Функция ДанныеДляРегистрацииВУчетаСтажаПФР(МассивСсылок) Экспорт
	ДанныеДляРегистрацииВУчете = Новый Соответствие;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("МассивСсылок", МассивСсылок);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Отпуск.Сотрудник,
	|	Отпуск.ДатаНачалаОсновногоОтпуска КАК ДатаНачала,
	|	Отпуск.ДатаОкончанияОсновногоОтпуска КАК ДатаОкончания,
	|	Отпуск.Ссылка
	|ИЗ
	|	Документ.Отпуск КАК Отпуск
	|ГДЕ
	|	Отпуск.Ссылка В(&МассивСсылок)";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		ДанныеДляРегистрацииВУчетеПоДокументу = УчетСтажаПФР.ДанныеДляРегистрацииВУчетеСтажаПФР();
		ДанныеДляРегистрацииВУчете.Вставить(Выборка.Ссылка, ДанныеДляРегистрацииВУчетеПоДокументу);
		
		ВидСтажа = Перечисления.ВидыСтажаПФР2014.ДЛОТПУСК;		
						
		ОписаниеПериода = УчетСтажаПФР.ОписаниеРегистрируемогоПериода();
		ОписаниеПериода.Сотрудник = Выборка.Сотрудник;	
		ОписаниеПериода.ДатаНачалаПериода = Выборка.ДатаНачала;
		ОписаниеПериода.ДатаОкончанияПериода = Выборка.ДатаОкончания;
		ОписаниеПериода.Состояние = Перечисления.СостоянияСотрудника.ОтпускОсновной;
				
		РегистрируемыйПериод = УчетСтажаПФР.ДобавитьЗаписьВДанныеДляРегистрацииВУчета(ДанныеДляРегистрацииВУчетеПоДокументу, ОписаниеПериода);
												 					
		УчетСтажаПФР.УстановитьЗначениеРегистрируемогоРесурса(РегистрируемыйПериод, "ВидСтажаПФР", ВидСтажа);		
	КонецЦикла;	

	Возврат ДанныеДляРегистрацииВУчете;
														
КонецФункции	

#Область ПечатьРасчетаСреднегоЗаработка

// Заполняет структуру - описание документа для формирования печатной формы расчета среднего заработка.
//
// Параметры:
//   ОписаниеДокумента - структура, определяется в
//                       Обработки.ПечатьРасчетаСреднегоЗаработка.ОписаниеДокументаРассчитывающегоСреднийЗаработок.
//
Процедура ЗаполнитьОписаниеДокументаРассчитывающегоСреднийЗаработок(ОписаниеДокумента, ИмяМакета) Экспорт
	МетаданныеДокумента = ПустаяСсылка().Метаданные();
	ОписаниеДокумента.Вставить("ИмяДокумента", МетаданныеДокумента.Имя);
	ОписаниеДокумента.Вставить("СинонимДокумента", МетаданныеДокумента.Синоним);
КонецПроцедуры 

// Заполняет таблицу значений - параметры формирования печатной формы расчета среднего заработка.
//
// Параметры:
//	 МассивСсылок 		- массив, печатаемые документы.
//   ДанныеДокумента 	- таблица значений, определяется в
//                      Обработки.ПечатьРасчетаСреднегоЗаработка.ДанныеДокументовРасчетаСреднегоЗаработка.
//   ИмяМакета          - строка, вариант печати расчета среднего заработка.
//
Процедура ЗаполнитьДанныеДокументовДляПечатиРасчетаСреднегоЗаработка(МассивСсылок, ДанныеДокументов, ИмяМакета) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("МассивСсылок", МассивСсылок);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Документ.Сотрудник,
	|	Документ.ДатаНачалаОсновногоОтпуска КАК ДатаНачалаСобытия,
	|	Документ.Ссылка КАК Ссылка,
	|	Документ.Организация,
	|	Документ.Дата КАК ДатаДокумента,
	|	Документ.Номер КАК НомерДокумента,
	|	Документ.ПериодРегистрации,
	|	Документ.ДатаНачалаОсновногоОтпуска КАК ДатаНачалаОтсутствия,
	|	Документ.ДатаОкончанияОсновногоОтпуска КАК ДатаОкончанияОтсутствия,
	|	ДОБАВИТЬКДАТЕ(НАЧАЛОПЕРИОДА(Документ.ДатаНачалаОсновногоОтпуска, МЕСЯЦ), МЕСЯЦ, -12) КАК НачалоРасчетногоПериода,
	|	ДОБАВИТЬКДАТЕ(НАЧАЛОПЕРИОДА(Документ.ДатаНачалаОсновногоОтпуска, МЕСЯЦ), СЕКУНДА, -1) КАК ОкончаниеРасчетногоПериода,
	|	Документ.НачалоПериодаЗаКоторыйПредоставляетсяОтпуск,
	|	Документ.КонецПериодаЗаКоторыйПредоставляетсяОтпуск,
	|	Документ.КоличествоДнейОсновногоОтпуска КАК ДнейОсновногоОтпуска
	|ИЗ
	|	Документ.Отпуск КАК Документ
	|ГДЕ
	|	Документ.Ссылка В(&МассивСсылок)
	|
	|СГРУППИРОВАТЬ ПО
	|	Документ.Ссылка,
	|	Документ.Сотрудник,
	|	Документ.ДатаНачалаОсновногоОтпуска,
	|	Документ.Организация,
	|	Документ.Дата,
	|	Документ.Номер,
	|	Документ.ПериодРегистрации,
	|	Документ.ДатаОкончанияОсновногоОтпуска,
	|	Документ.НачалоПериодаЗаКоторыйПредоставляетсяОтпуск,
	|	Документ.КонецПериодаЗаКоторыйПредоставляетсяОтпуск,
	|	Документ.КоличествоДнейОсновногоОтпуска,
	|	Документ.ДатаНачалаОсновногоОтпуска
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка";

	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.СледующийПоЗначениюПоля("Ссылка") Цикл
		
		ДанныеДокумента = ДанныеДокументов.Добавить(); 
		
		ЗаполнитьЗначенияСвойств(ДанныеДокумента, Выборка);
		ДанныеДокумента.ДнейОтпускаВсего 	= Выборка.ДнейОсновногоОтпуска;

		ОписанияСотрудников = Новый Соответствие;
		МассивСотрудников = Новый Массив;
		Пока Выборка.Следующий() Цикл
			МассивСотрудников.Добавить(Выборка.Сотрудник);
			ОписанияСотрудников.Вставить(Выборка.Сотрудник, Новый Структура("ИспользоватьСреднечасовойЗаработок", Ложь));
		КонецЦикла;
		ДанныеДокумента.МассивСотрудников 	= МассивСотрудников;
		ДанныеДокумента.ОписанияСотрудников = ОписанияСотрудников;
	КонецЦикла;

КонецПроцедуры

#КонецОбласти

Функция ДатаНаступленияСтраховогоСлучая(Ссылка) Экспорт 
	
	ДатаНаступленияСтраховогоСлучая = Неопределено;
	
	Если ЗначениеЗаполнено(Ссылка) И ТипЗнч(Ссылка) = Тип("ДокументСсылка.Отпуск") Тогда
		ДатаНаступленияСтраховогоСлучая = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Ссылка, "ДатаНачалаСобытия");
	КонецЕсли;
	
	Возврат ДатаНаступленияСтраховогоСлучая;	
	
КонецФункции

Функция ПланируемаяДатыВыплатыОтпуска(ДатаНачалаСобытия) Экспорт
	
	ПланируемаяДатыВыплатыОтпуска = Неопределено;
	
	Если ЗначениеЗаполнено(ДатаНачалаСобытия) Тогда
		
		ДатаТриДняДоОтпуска = ДатаНачалаСобытия - (86400 * 3);
		Даты = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ДатаТриДняДоОтпуска);
		КалендарьРФ = КалендарныеГрафики.ПроизводственныйКалендарьРоссийскойФедерации();
		
		ДатыРабочихДней = КалендарныеГрафики.ДатыБлижайшихРабочихДней(КалендарьРФ, Даты, Истина, Ложь);
		
		Если ДатыРабочихДней <> Неопределено Тогда
			ПланируемаяДатыВыплатыОтпуска = ДатыРабочихДней.Получить(ДатаТриДняДоОтпуска);
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ПланируемаяДатыВыплатыОтпуска;
	
КонецФункции

#КонецОбласти

#КонецЕсли
