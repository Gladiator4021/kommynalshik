﻿&НаКлиенте
Перем СуммаПолученоНаличными;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЗаполнитьЗначенияСвойств(ЭтаФорма, Параметры, "СуммаДокумента, Организация, ЭтоВозврат");
	
	Элементы.ФормаВыполнитьОперацию.Заголовок = Параметры.ЗаголовокКнопкиВыполнить;
	Заголовок = ?(ЭтоВозврат, НСтр("ru = 'Возврат'"), НСтр("ru = 'Оплата'"));
	
	УстановитьВидОплаты();
	
	ПолученоНаличными = ?(Параметры.ЭтоВозврат, СуммаДокумента, 0);
	
	УправлениеФормой(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	
	Если Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаНаличные Тогда
		Если ПолученоНаличными < СуммаДокумента Тогда
			ТекстСообщения = НСтр("ru = 'Сумма введенной оплаты меньше суммы чека'");

			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Поле", "КОРРЕКТНОСТЬ", НСтр("ru = 'Наличными'"),,,ТекстСообщения);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "ПолученоНаличными", , Отказ);
		КонецЕсли;
	ИначеЕсли Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаДругое Тогда
		ИмяСписка = "Оплата";
		
		Если Оплата.Итог("Сумма") > СуммаДокумента  Тогда
			
			ТекстОписаниеОшибки = НСтр("ru = 'Сумма безналичных оплат превышает сумму выручки от реализации!'");
			ТекстСообщения      = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Список", "Корректность",,, НСтр("ru = 'Безналичные оплаты'"), ТекстОписаниеОшибки);
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , ИмяСписка, "Объект", Отказ);
			
		КонецЕсли;
		
		НомерСтроки = 0;
		Для каждого СтрокаОплата Из Оплата Цикл
			НомерСтроки = НомерСтроки + 1;
		
			Если НЕ ЗначениеЗаполнено(СтрокаОплата.ВидОплаты) Тогда
				
				Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти(ИмяСписка, НомерСтроки, "ВидОплаты");
				
				ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение", 
					НСтр("ru = 'Вид оплаты'"), НомерСтроки, ИмяСписка);
					
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , Поле, "Объект", Отказ);

			КонецЕсли; 
			
			Если СтрокаОплата.Сумма = 0 Тогда
				
				Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти(ИмяСписка, НомерСтроки, "Сумма");
				
				ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение", 
					НСтр("ru = 'Сумма'"), НомерСтроки, ИмяСписка);
					
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , Поле, "Объект", Отказ);
				
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ВидОплатыПриИзменении(Элемент)
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы["Страница"+ВидОплаты];
	
	УправлениеФормой(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ПолученоНаличнымиИзменениеТекстаРедактирования(Элемент, Текст, СтандартнаяОбработка)
	СуммаПолученоНаличными = Число(Текст);
	РассчитатьСдачу();

КонецПроцедуры


&НаКлиенте
Процедура ОплатаПриИзменении(Элемент)
	УправлениеФормой(ЭтотОбъект);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВыполнитьОперацию(Команда)
	Если НЕ ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли; 
	
	СтруктураРезультата = СтруктураРезультата();
	
	Закрыть(СтруктураРезультата);
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьВидОплаты()

	Оплата.Загрузить(ПолучитьИзВременногоХранилища(Параметры.АдресТаблицаОплата));
	
	Если Оплата.Количество() = 0 Тогда
		
		ВидОплаты = "Наличные";
		
	ИначеЕсли Оплата.Количество() = 1 
			И Оплата[0].ВидОплаты.ТипОплаты = Перечисления.ТипыОплат.ПлатежнаяКарта 
			И Оплата[0].Сумма = СуммаДокумента Тогда 
			
		ВидОплаты = "Карта";
		
	Иначе
		
		ВидОплаты = "Другое";
		
	КонецЕсли;
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы["Страница"+ВидОплаты];

КонецПроцедуры

&НаСервере
Функция СтруктураРезультата()

	СтруктураРезультата = Новый Структура("ОплатаНаличные, ОплатаКарта, АдресТаблицыОплаты, ПечататьТоварныйЧек", 0, 0, Неопределено, ПечататьТоварныйЧек);
	
	
	Если Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаНаличные Тогда
		СтруктураРезультата.Вставить("ОплатаНаличные",       ПолученоНаличными);
	ИначеЕсли Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаКарта Тогда
		СтруктураРезультата.Вставить("ОплатаКарта",         СуммаДокумента);
	Иначе
		СтруктураРезультата.Вставить("ОплатаНаличные",       Макс(СуммаДокумента - Оплата.Итог("Сумма"), 0));
		СтруктураРезультата.Вставить("АдресТаблицыОплаты",  ПоместитьВоВременноеХранилище(Оплата.Выгрузить(), Новый УникальныйИдентификатор));
	КонецЕсли;
	
	Возврат СтруктураРезультата;

КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура УправлениеФормой(Форма)
	Элементы = Форма.Элементы;
	Элементы.ДекорацияРазделитель.Видимость = НЕ (Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаДругое);
	Элементы.СуммаСдача.Видимость = НЕ Форма.ЭтоВозврат;
	Элементы.ПолученоНаличными.Видимость = НЕ Форма.ЭтоВозврат;
КонецПроцедуры

&НаКлиенте
Процедура РассчитатьСдачу()
	СуммаСдача = Макс(СуммаПолученоНаличными - СуммаДокумента, 0);
КонецПроцедуры

#КонецОбласти
