﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИФизическимЛицам(ЭтотОбъект, Таблица, "Организация", "ФизическиеЛица.ФизическоеЛицо");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	Если РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
		ЗарплатаОтраженаВБухучете = Ложь;
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ТребуетсяУтверждениеДокумента = Документы.ОтражениеЗарплатыВБухучете.ТребуетсяУтверждениеДокументаБухгалтером(Организация);
	
	Если Не ТребуетсяУтверждениеДокумента Или ЗарплатаОтраженаВБухучете Тогда
		
		Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ОценочныеОбязательстваЗарплатаКадры") Тогда
			 
			МодульРезервОтпусков = ОбщегоНазначения.ОбщийМодуль("РезервОтпусковПереопределяемый");
			РезервыРассчитываются = Истина;
			МодульРезервОтпусков.ПолучитьЗначениеРезервыРассчитываются(РезервыРассчитываются);
			
			Если РезервыРассчитываются Тогда
				МодульРезервОтпусков = ОбщегоНазначения.ОбщийМодуль("РезервОтпусков");
				НастройкиРезервовОтпусков = МодульРезервОтпусков.НастройкиРезервовОтпусков(Организация, ПериодРегистрации);
				Если НастройкиРезервовОтпусков.ФормироватьРезервОтпусковБУ Тогда
					ТаблицаВыплатаОтпусковЗаСчетРезерва = ВыплатаОтпусковЗаСчетРезерва.Выгрузить();
					МодульРезервОтпусков.СформироватьДвиженияВыплатаОтпусковЗаСчетРезерва(Движения, Отказ, Организация, ПериодРегистрации, ТаблицаВыплатаОтпусковЗаСчетРезерва);
					ДвиженияВыплатаОтпусковЗаСчетРезерва = Движения.ВыплатаОтпусковЗаСчетРезерва.Выгрузить();
					МодульРезервОтпусков.СформироватьДвиженияСписаниеРезерваОтпусков (Движения, Отказ, Организация, ПериодРегистрации, ДвиженияВыплатаОтпусковЗаСчетРезерва);
				КонецЕсли;
			КонецЕсли;
			
		КонецЕсли;
		
		ТаблицаНачисленныйНДФЛ = НачисленныйНДФЛ.Выгрузить();
		ОтражениеЗарплатыВБухучете.ЗаполнитьРегистрациюВНалоговомОрганеВКоллекцииСтрок(Организация, ПериодРегистрации, ТаблицаНачисленныйНДФЛ);
		
		ДанныеДляОтражения = Новый Структура;
		ДанныеДляОтражения.Вставить("НачисленнаяЗарплатаИВзносы", НачисленнаяЗарплатаИВзносы.Выгрузить());
		ДанныеДляОтражения.Вставить("НачисленныйНДФЛ", ТаблицаНачисленныйНДФЛ);
		ДанныеДляОтражения.Вставить("УдержаннаяЗарплата", УдержаннаяЗарплата.Выгрузить());
		
		ОтражениеЗарплатыВБухучетеПереопределяемый.СформироватьДвижения(ЭтотОбъект.Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляОтражения);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	ЗарплатаОтраженаВБухучете = Ложь;
	Бухгалтер = Справочники.Пользователи.ПустаяСсылка();
	
КонецПроцедуры

#КонецОбласти


#КонецЕсли
