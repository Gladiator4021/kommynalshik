﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ

Процедура ЗаполнитьПоДокументуОснованию(ДанныеЗаполнения)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ОтчетКомитентуОПродажах") Тогда
		Если ДанныеЗаполнения.ВидОперации = Перечисления.ВидыОперацийОтчетКомитентуОПродажах.ОтчетОЗакупках Тогда
			
			ЗаполнениеДокументов.ЗаполнитьПоОснованию(ЭтотОбъект, ДанныеЗаполнения);
			
			ВидОперации	= Перечисления.ВидыОперацийПередачаТоваров.ПередачаТоваровКомитенту;
			
			Если ДанныеЗаполнения.Проведен Тогда
				
				ЗаполнитьТоварыПоОснованию(ДанныеЗаполнения);
				
			КонецЕсли;
			
		Иначе
			
			ТекстСообщения =
				НСтр("ru = 'Передача товаров комитенту вводятся на основании документа ""Отчет комитенту""
					|с видом отчета ""Отчет о закупках""'");
			ВызватьИсключение ТекстСообщения;
			
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьТоварыПоОснованию(ДокументОснование) Экспорт
	
	Если ТипЗнч(ДокументОснование) = Тип("ДокументСсылка.ОтчетКомитентуОПродажах") Тогда
		
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
		
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	МИНИМУМ(ОтчетКомитентуОПродажахТовары.НомерСтроки) КАК НомерСтроки,
		|	ОтчетКомитентуОПродажахТовары.Номенклатура,
		|	СУММА(ОтчетКомитентуОПродажахТовары.Количество) КАК Количество
		|ИЗ
		|	Документ.ОтчетКомитентуОПродажах.Товары КАК ОтчетКомитентуОПродажахТовары
		|ГДЕ
		|	ОтчетКомитентуОПродажахТовары.Ссылка = &ДокументОснование
		|	И ОтчетКомитентуОПродажахТовары.Ссылка.ВидОперации = ЗНАЧЕНИЕ(Перечисление.ВидыОперацийОтчетКомитентуОПродажах.ОтчетОЗакупках)
		|	И НЕ ОтчетКомитентуОПродажахТовары.Номенклатура.Услуга
		|
		|СГРУППИРОВАТЬ ПО
		|	ОтчетКомитентуОПродажахТовары.Номенклатура
		|
		|УПОРЯДОЧИТЬ ПО
		|	НомерСтроки";
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			НоваяСтрока = Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, Выборка);
		КонецЦикла;
		
		Документы.ПередачаТоваров.ЗаполнитьСчетаУчетаВТабличнойЧасти(ЭтотОбъект, "Товары");
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьТоварыПоОстаткам(СчетУчета) Экспорт

	Товары.Очистить();
	
	Если НЕ ЗначениеЗаполнено(СчетУчета) Тогда
		Возврат;
	КонецЕсли;
	
	СвойстваСчетаУчета = БухгалтерскийУчетВызовСервераПовтИсп.ПолучитьСвойстваСчета(СчетУчета);
	
	НомерСубконтоНоменклатура	= 0;
	НомерСубконтоКонтрагенты	= 0;
	
	Для Позиция = 1 По СвойстваСчетаУчета.КоличествоСубконто Цикл
		
		НомерСубконто	= СвойстваСчетаУчета.КоличествоСубконто - Позиция + 1;
		
		Если СвойстваСчетаУчета["ВидСубконто" + НомерСубконто] =
				ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Номенклатура Тогда
				
				НомерСубконтоНоменклатура	= НомерСубконто;
				
		ИначеЕсли СвойстваСчетаУчета["ВидСубконто" + НомерСубконто] =
			ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты Тогда
			
			НомерСубконтоКонтрагенты	= НомерСубконто;
			
		КонецЕсли;
		
	КонецЦикла; 
	
	МассивВидыСубконто = Новый Массив;
	МассивВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Номенклатура);
	Если НомерСубконтоНоменклатура = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Если НомерСубконтоКонтрагенты > 0 Тогда
		МассивВидыСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты);
		Если НЕ ЗначениеЗаполнено(Контрагент) Тогда
			Возврат;
		КонецЕсли;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ДатаДок",			Новый Граница(Дата, ВидГраницы.Исключая));
	Запрос.УстановитьПараметр("Организация",		Организация);
	Запрос.УстановитьПараметр("Контрагент",			Контрагент);
	Запрос.УстановитьПараметр("Счет",				СчетУчета);
	Запрос.УстановитьПараметр("МассивВидыСубконто",	МассивВидыСубконто);
	
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	Хозрасчетный.Ссылка
		|ПОМЕСТИТЬ ВТ_Счета
		|ИЗ
		|	ПланСчетов.Хозрасчетный КАК Хозрасчетный
		|ГДЕ
		|	Хозрасчетный.Ссылка В ИЕРАРХИИ(&Счет)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	&СубконтоНоменклатура КАК Номенклатура,
		|	ХозрасчетныйОстатки.Счет КАК СчетУчета,
		|	ХозрасчетныйОстатки.КоличествоОстаток КАК Количество
		|ИЗ
		|	РегистрБухгалтерии.Хозрасчетный.Остатки(
		|			&ДатаДок,
		|			Счет В
		|				(ВЫБРАТЬ
		|					ВТ_Счета.Ссылка
		|				ИЗ
		|					ВТ_Счета),
		|			&МассивВидыСубконто,
		|			Организация = &Организация
		|				И &УсловиеПоКонтрагентам) КАК ХозрасчетныйОстатки
		|ГДЕ
		|	ХозрасчетныйОстатки.КоличествоОстаток > 0";
		
	Запрос.Текст	= СтрЗаменить(Запрос.Текст, "&СубконтоНоменклатура", "ХозрасчетныйОстатки.Субконто" + НомерСубконтоНоменклатура);
	
	Если НомерСубконтоКонтрагенты > 0 Тогда
		Запрос.Текст	= СтрЗаменить(Запрос.Текст, "&УсловиеПоКонтрагентам",
			"Субконто" + НомерСубконтоКонтрагенты + " = &Контрагент");
	Иначе
		Запрос.Текст	= СтрЗаменить(Запрос.Текст, "&УсловиеПоКонтрагентам", "ИСТИНА");
	КонецЕсли;
	
	Результат	= Запрос.Выполнить();
	Если НЕ Результат.Пустой() Тогда
		ТаблицаОстатков	= Результат.Выгрузить();
		Товары.Загрузить(ТаблицаОстатков);
	КонецЕсли;
	
КонецПроцедуры

// Функция готовит пакетный запрос для ОбработкиПроверкиЗаполнения.
//	Табличные части объекта (еще не сохраненного в информационной базе) выгружаются во временные таблицы,
//	соединяются с другими нужными талицами.
//
//Параметры:
//	СтруктураРезультатов - <Структура> - описание пакета запросов. Ключ - имя результата запроса, значение - индекс этого результата
//
//Возвращает массив результатов запроса
Функция ПолучитьДанныеОбъектаДляПроверкиЗаполнения(СтруктураРезультатов)

	Запрос	= Новый Запрос;
	Запрос.Текст	= "";
	
	Если Товары.Количество() > 0 Тогда
		
		Запрос.УстановитьПараметр("ТаблицаТовары", Товары.Выгрузить());
		
		СтруктураРезультатов.Вставить("ТаблицаТовары",	СтруктураРезультатов.Количество());
		СтруктураРезультатов.Вставить("Товары",			СтруктураРезультатов.Количество());

		Запрос.Текст	= Запрос.Текст +
		"ВЫБРАТЬ
		|	ВремТаблица.НомерСтроки,
		|	ВремТаблица.Номенклатура,
		|	ВремТаблица.Количество,
		|	ВремТаблица.СчетУчета,
		|	ВремТаблица.СчетПередачи
		|ПОМЕСТИТЬ ТаблицаТовары
		|ИЗ
		|	&ТаблицаТовары КАК ВремТаблица
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	ВремТаблица.Номенклатура
		|;
		|
		|ВЫБРАТЬ
		|	ТаблицаДокумента.НомерСтроки,
		|	ТаблицаДокумента.Номенклатура,
		|	ТаблицаДокумента.Количество,
		|	ТаблицаДокумента.СчетУчета,
		|	ЕСТЬNULL(ТаблицаДокумента.СчетУчета.Забалансовый, ЛОЖЬ) КАК СчетУчетаЗабалансовый,
		|	ТаблицаДокумента.СчетПередачи
		|ИЗ
		|	ТаблицаТовары КАК ТаблицаДокумента
		|
		|УПОРЯДОЧИТЬ ПО
		|	ТаблицаДокумента.НомерСтроки";
		
	КонецЕсли;
	
	Если НЕ ПустаяСтрока(Запрос.Текст) Тогда
		Возврат Запрос.ВыполнитьПакет();
	Иначе
		Возврат Неопределено;
	КонецЕсли;

КонецФункции

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ТипДанныхЗаполнения = ТипЗнч(ДанныеЗаполнения);
	
	Если ДанныеЗаполнения <> Неопределено Тогда
		Если ТипДанныхЗаполнения <> Тип("Структура")
			И Метаданные().ВводитсяНаОсновании.Содержит(ДанныеЗаполнения.Метаданные()) Тогда
			ДокументОснование = ДанныеЗаполнения;
		ИначеЕсли ТипДанныхЗаполнения = Тип("Структура")
			И ДанныеЗаполнения.Свойство("Основание")
			И Метаданные().ВводитсяНаОсновании.Содержит(ДанныеЗаполнения.Основание.Метаданные()) Тогда
			ДокументОснование = ДанныеЗаполнения.Основание;
		КонецЕсли;

		Если ДокументОснование <> Неопределено Тогда
			ЗаполнитьПоДокументуОснованию(ДокументОснование);
		КонецЕсли;
	КонецЕсли;
	
	ЗаполнениеДокументов.Заполнить(ЭтотОбъект, ДанныеЗаполнения);
	
	// Заполнение реквизитов, специфичных для документа:
	Если ЗначениеЗаполнено(Организация)
		И ЗначениеЗаполнено(Контрагент)
		И (ЗначениеЗаполнено(ДоговорКонтрагента) ИЛИ НЕ ПолучитьФункциональнуюОпцию("ВестиУчетПоДоговорам")) Тогда
		Документы.ПередачаТоваров.ЗаполнитьСчетаУчетаРасчетов(ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)

	Дата          = НачалоДня(ОбщегоНазначения.ТекущаяДатаПользователя());
	Ответственный = Пользователи.ТекущийПользователь();

КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)

	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если ПроверяемыеРеквизиты.Найти("Склад") = Неопределено Тогда
		ПроверяемыеРеквизиты.Добавить("Склад");
	КонецЕсли;

	Если НЕ ПолучитьФункциональнуюОпцию("ВестиУчетПоДоговорам") Тогда
		МассивНепроверяемыхРеквизитов.Добавить("ДоговорКонтрагента");
	КонецЕсли;
	
	// Проверка табличной части "Товары"
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Номенклатура");
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Количество");
	МассивНепроверяемыхРеквизитов.Добавить("Товары.СчетУчета");
	МассивНепроверяемыхРеквизитов.Добавить("Товары.СчетПередачи");
	
	// Получаем содержимое табличных частей объекта с вспомогательными реквизитами:
	СтруктураРезультатов = Новый Структура;
	ТаблицыДокумента =  ПолучитьДанныеОбъектаДляПроверкиЗаполнения(СтруктураРезультатов);
	
	Если Товары.Количество() > 0 Тогда
		
		ВыборкаТоваров	= ТаблицыДокумента[СтруктураРезультатов.Товары].Выбрать();
		
		ИмяСписка	= НСтр("ru = 'Товары'");
		
		Пока ВыборкаТоваров.Следующий() Цикл
		
			Префикс	= "Товары[" + Формат(ВыборкаТоваров.НомерСтроки - 1, "ЧН=0; ЧГ=") + "].";
			
			Если НЕ ЗначениеЗаполнено(ВыборкаТоваров.Номенклатура) Тогда
				ТекстСообщения	= ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение", НСтр("ru = 'Номенклатура'"),
					ВыборкаТоваров.НомерСтроки, ИмяСписка);
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Префикс + "Номенклатура", "Объект", Отказ);
			КонецЕсли;
			
			Если НЕ ЗначениеЗаполнено(ВыборкаТоваров.Количество) Тогда
				ТекстСообщения	= ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение", НСтр("ru = 'Количество'"),
					ВыборкаТоваров.НомерСтроки, ИмяСписка);
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Префикс + "Количество", "Объект", Отказ);
			КонецЕсли;
			
			Если ВидОперации = Перечисления.ВидыОперацийПередачаТоваров.ВПереработку  Тогда
				
				Если НЕ ЗначениеЗаполнено(ВыборкаТоваров.СчетУчета) Тогда
					ТекстСообщения	= ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение", НСтр("ru = 'Счет учета'"),
						ВыборкаТоваров.НомерСтроки, ИмяСписка);
					ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Префикс + "СчетУчета", "Объект", Отказ);
				КонецЕсли;
				
				Если НЕ ЗначениеЗаполнено(ВыборкаТоваров.СчетПередачи) Тогда
					ТекстСообщения	= ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение", НСтр("ru = 'Счет передачи'"),
						ВыборкаТоваров.НомерСтроки, ИмяСписка);
					ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Префикс + "СчетПередачи", "Объект", Отказ);
				КонецЕсли;
				
			ИначеЕсли ВидОперации = Перечисления.ВидыОперацийПередачаТоваров.ПередачаТоваровКомитенту  Тогда
				
				Если НЕ ЗначениеЗаполнено(ВыборкаТоваров.СчетУчета) Тогда
					
					ТекстСообщения	= ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Заполнение", НСтр("ru = 'Счет учета'"),
						ВыборкаТоваров.НомерСтроки, ИмяСписка);
					ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Префикс + "СчетУчета", "Объект", Отказ);
					
				ИначеЕсли НЕ ВыборкаТоваров.СчетУчетаЗабалансовый Тогда
					
					ТекстСообщения	= ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", "Корректность", НСтр("ru = 'Счет учета'"),
						ВыборкаТоваров.НомерСтроки, ИмяСписка);
					ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Префикс + "СчетУчета", "Объект", Отказ);
					
				КонецЕсли;
			
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЕсли;
	
	// Проверка табличной части "Возвратная тара"
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьВозвратнуюТару") Тогда
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.Номенклатура");
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.Количество");
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.Сумма");
		МассивНепроверяемыхРеквизитов.Добавить("ВозвратнаяТара.СчетУчета");
    ИначеЕсли ВозвратнаяТара.Количество() = 0 Тогда
		МассивНепроверяемыхРеквизитов.Добавить("СчетУчетаРасчетовПоТаре");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);

КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	РаботаСДоговорамиКонтрагентовБП.ЗаполнитьДоговорПередЗаписью(ЭтотОбъект);
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьВозвратнуюТару")
		И ВозвратнаяТара.Количество() > 0 Тогда
		ВозвратнаяТара.Очистить();
	КонецЕсли;

КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)

	// ПОДГОТОВКА ПРОВЕДЕНИЯ ПО ДАННЫМ ДОКУМЕНТА

	ПроведениеСервер.ПодготовитьНаборыЗаписейКПроведению(ЭтотОбъект);
	Если РучнаяКорректировка Тогда
		Возврат;
	КонецЕсли;

	ПараметрыПроведения = Документы.ПередачаТоваров.ПодготовитьПараметрыПроведения(Ссылка, Отказ);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;

	// ПОДГОТОВКА ПРОВЕДЕНИЯ ПО ДАННЫМ ИНФОРМАЦИОННОЙ БАЗЫ

	// Таблица списанных товаров
	ТаблицаСписанныеТовары = УчетТоваров.ПодготовитьТаблицуСписанныеТовары(ПараметрыПроведения.СписаниеТоваровТаблицаТовары,
		ПараметрыПроведения.СписаниеТоваровРеквизиты, Отказ);

	// Таблица списанной тары
	ТаблицаСписаннаяТара = УчетТоваров.ПодготовитьТаблицуСписанныеТовары(ПараметрыПроведения.СписаниеТарыТаблицаТара,
		ПараметрыПроведения.СписаниеТарыРеквизиты, Отказ);

	// Структура таблиц для отражения в налоговом учете УСН
	СтруктураТаблицУСН = Новый Структура("ТаблицаТМЦ", ТаблицаСписанныеТовары);
		
	// ФОРМИРОВАНИЕ ДВИЖЕНИЙ

	// Списание товаров и тары
	УчетТоваров.СформироватьДвиженияПеремещениеТоваров(
		ТаблицаСписанныеТовары,
		ПараметрыПроведения.СписаниеТоваровРеквизиты,
		Движения,
		Отказ);
		
	УчетТоваров.СформироватьДвиженияВозвратТоваровПоставщику(
		ПараметрыПроведения.СписаниеТарыТаблицаТара,
		ТаблицаСписаннаяТара,
		ПараметрыПроведения.СписаниеТарыРеквизиты, 
		Движения,
		Отказ);
		
	// Корректировка стоимости тары
	УчетТоваров.СформироватьДвиженияКорректировкаСтоимостиТары(
		ПараметрыПроведения.КорректировкаСтоимостиТарыТаблицаТара, ТаблицаСписаннаяТара,
		ПараметрыПроведения.КорректировкаСтоимостиТарыРеквизиты, Движения, Отказ);
		
	// Учет НДС
	// Отражается как внутреннее перемещение только передача материалов переработчику
	УчетНДСБП.СформироватьДвиженияПеремещениеТоваров(ПараметрыПроведения.ТоварыНДС,
		ТаблицаСписанныеТовары, ПараметрыПроведения.РеквизитыНДС, Неопределено, Движения, Отказ);

	НалоговыйУчетУСН.СформироватьДвиженияУСН(ЭтотОбъект, СтруктураТаблицУСН);
	
	// Отложенные расчеты с контрагентами.
	Если ТаблицаСписаннаяТара.Количество() > 0 Тогда
		УчетВзаиморасчетовОтложенноеПроведение.ЗарегистрироватьОтложенныеРасчетыСКонтрагентами(
			ЭтотОбъект, Отказ, ПараметрыПроведения.РасчетыСКонтрагентамиОтложенноеПроведение);
	КонецЕсли;
	
	// Регистрация в последовательности
	РаботаСПоследовательностями.ЗарегистрироватьОтложенныеРасчетыВПоследовательности(
		ЭтотОбъект, Отказ, ,
		РаботаСПоследовательностями.ПодготовитьТаблицуСчетовТоваровДляАнализа(ТаблицаСписанныеТовары, ТаблицаСписаннаяТара));
		
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)

	ПроведениеСервер.ПодготовитьНаборыЗаписейКОтменеПроведения(ЭтотОбъект);
	Движения.Записать();
	
	РаботаСПоследовательностями.ОтменитьРегистрациюВПоследовательности(ЭтотОбъект, Отказ);

КонецПроцедуры


#КонецЕсли