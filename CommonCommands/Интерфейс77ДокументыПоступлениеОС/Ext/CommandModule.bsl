﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Если ПараметрыВыполненияКоманды.Окно <> Неопределено Тогда
		ВариантОткрытияОкнаФормы = ВариантОткрытияОкна.ОтдельноеОкно;
		Уникальность = Истина;
		Источник = Неопределено;
	Иначе
		ВариантОткрытияОкнаФормы = ПараметрыВыполненияКоманды.Окно;
		Уникальность = ПараметрыВыполненияКоманды.Уникальность;
		Источник = ПараметрыВыполненияКоманды.Источник;
	КонецЕсли;
	
	УсловияЗаполнения = Новый Структура("ВидОперации", ПредопределенноеЗначение("Перечисление.ВидыОперацийПоступлениеТоваровУслуг.ОбъектыСтроительства"));
	ПараметрыФормы = Новый Структура("ЗначенияЗаполнения", УсловияЗаполнения);
	ОткрытьФорму("Документ.ПоступлениеТоваровУслуг.ФормаОбъекта", ПараметрыФормы, Источник, Уникальность, ВариантОткрытияОкнаФормы);
	
КонецПроцедуры 