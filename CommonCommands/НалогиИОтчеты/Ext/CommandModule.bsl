﻿#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ЭтоВебКлиент = Ложь;
	#Если ВебКлиент Тогда
		ЭтоВебКлиент = Истина;
	#КонецЕсли
	Если ЭтоМедленныйРежимРаботы() ИЛИ ЭтоВебКлиент Тогда
		ИдентификаторФормы = "ДлительнаяОперацияНалогиИОтчеты";
		ОткрытьФорму("ОбщаяФорма.ДлительнаяОперацияНалогиИОтчеты", Новый Структура("РежимНастройки", Истина),,ИдентификаторФормы);
	Иначе
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("Организация", ОсновнаяОрганизация());
		ОткрытьФорму("ОбщаяФорма.НалогиИОтчеты", ПараметрыФормы);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Функция ЭтоМедленныйРежимРаботы()
	
	Возврат ПроверкаКонтрагентов.ЭтоМедленныйРежимРаботы();
	
КонецФункции

&НаСервере
Функция ОсновнаяОрганизация()
	
	Возврат БухгалтерскийУчетПереопределяемый.ПолучитьЗначениеПоУмолчанию("ОсновнаяОрганизация");
	
КонецФункции

#КонецОбласти