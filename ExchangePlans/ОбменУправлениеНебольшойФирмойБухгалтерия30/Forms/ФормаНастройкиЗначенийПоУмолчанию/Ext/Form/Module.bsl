﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбменДаннымиСервер.ФормаНастройкиЗначенийПоУмолчаниюПриСозданииНаСервере(ЭтаФорма, "ОбменУправлениеНебольшойФирмойБухгалтерия30");
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	СтатьиЗатрат.Ссылка
	|ИЗ
	|	Справочник.СтатьиЗатрат КАК СтатьиЗатрат
	|ГДЕ
	|	СтатьиЗатрат.ИмяПредопределенныхДанных = ""ПрочиеЗатраты""
	|	И СтатьиЗатрат.Предопределенный
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПрочиеДоходыИРасходы.Ссылка
	|ИЗ
	|	Справочник.ПрочиеДоходыИРасходы КАК ПрочиеДоходыИРасходы
	|ГДЕ
	|	ПрочиеДоходыИРасходы.ИмяПредопределенныхДанных = ""ПрочиеВнереализационныеДоходыРасходы""
	|	И ПрочиеДоходыИРасходы.Предопределенный";
	
	Результат = Запрос.ВыполнитьПакет();
	
	Если Не ЗначениеЗаполнено(СтатьяЗатрат)
		И Не Результат[0].Пустой() Тогда
		
		СтатьяЗатрат = Результат[0].Выгрузить()[0][0];
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(СтатьяПрочихДоходовРасходов)
		И Не Результат[1].Пустой() Тогда
		
		СтатьяПрочихДоходовРасходов = Результат[1].Выгрузить()[0][0];
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	ОбменДаннымиКлиент.ФормаНастройкиПередЗакрытием(Отказ, ЭтотОбъект, ЗавершениеРаботы);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаОК(Команда)
	
	ОбменДаннымиКлиент.ФормаНастройкиЗначенийПоУмолчаниюКомандаЗакрытьФорму(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти


