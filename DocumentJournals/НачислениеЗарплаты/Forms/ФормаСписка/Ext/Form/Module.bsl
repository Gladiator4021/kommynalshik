﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Если Параметры.РежимВыбора Тогда
		Элементы.Список.РежимВыбора = Истина;
	Иначе
		// СтандартныеПодсистемы.ВерсионированиеОбъектов
		ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
		// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
		
		// СтандартныеПодсистемы.Печать
		УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаПечать);
		// Конец СтандартныеПодсистемы.Печать
		
		ОбщегоНазначенияБПВызовСервера.УстановитьОтборПоОсновнойОрганизации(ЭтаФорма);
		
	КонецЕсли;
	
	Если Константы.РасчетЗарплатыДляНебольшихОрганизаций.Получить() Тогда
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"ФормаСоздатьНачисление",
			"Видимость",
			Ложь);
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"СписокСоздатьНачисление",
			"Видимость",
			Ложь);
		
	Иначе
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"ФормаГруппаСоздать",
			"Видимость",
			Ложь);
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"СписокГруппаСоздать",
			"Видимость",
			Ложь);
		
	КонецЕсли;
	
	// ИнтернетПоддержкаПользователей.Новости.КонтекстныеНовости_ПриСозданииНаСервере
	ИдентификаторыСобытийПриОткрытии = "ПриОткрытии";
	ОбработкаНовостейПереопределяемый.КонтекстныеНовости_ПриСозданииНаСервере(
		ЭтаФорма,
		"БП.ЖурналДокументов.НачислениеЗарплаты",
		"ФормаСписка",
		НСтр("ru='Новости: Все начисления'"),
		ИдентификаторыСобытийПриОткрытии
	);
	// Конец ИнтернетПоддержкаПользователей.Новости.КонтекстныеНовости_ПриСозданииНаСервере
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ИзменениеОсновнойОрганизации" Тогда
		ОбщегоНазначенияБПКлиент.ИзменитьОтборПоОсновнойОрганизации(Список,, Параметр);
	КонецЕсли;
	
	// ИнтернетПоддержкаПользователей.Новости.ОбработкаОповещения
	ОбработкаНовостейКлиент.КонтекстныеНовости_ОбработкаОповещения(ЭтаФорма, ИмяСобытия, Параметр, Источник);
	// Конец ИнтернетПоддержкаПользователей.Новости.ОбработкаОповещения
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	УправлениеПечатьюКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура СписокПередЗагрузкойПользовательскихНастроекНаСервере(Элемент, Настройки)
		
	ОбщегоНазначенияБП.ВосстановитьОтборСписка(Список, Настройки, "Организация");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// ТехнологияСервиса.ИнформационныйЦентр
&НаКлиенте
Процедура Подключаемый_НажатиеНаИнформационнуюСсылку(Элемент)
	
	Если ОбщегоНазначенияКлиент.ПодсистемаСуществует("ТехнологияСервиса.ИнформационныйЦентр") Тогда
		Модуль = ОбщегоНазначенияКлиент.ОбщийМодуль("ИнформационныйЦентрКлиент");
		Модуль.НажатиеНаИнформационнуюСсылку(ЭтаФорма, Элемент);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_НажатиеНаСсылкуВсеИнформационныеСсылки(Элемент)
	
	Если ОбщегоНазначенияКлиент.ПодсистемаСуществует("ТехнологияСервиса.ИнформационныйЦентр") Тогда
		Модуль = ОбщегоНазначенияКлиент.ОбщийМодуль("ИнформационныйЦентрКлиент");
		Модуль.НажатиеНаСсылкуВсеИнформационныеСсылки(ЭтаФорма.ИмяФормы);
	КонецЕсли;
	
КонецПроцедуры

// Конец ТехнологияСервиса.ИнформационныйЦентр

&НаКлиенте
Процедура СоздатьНачислениеЗарплаты(Команда)
	
	ОткрытьФорму("Документ.НачислениеЗарплаты.ФормаОбъекта", ПараметрыСозданияДокумента(), ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьБольничныйЛист(Команда)
	
	ОткрытьФорму("Документ.БольничныйЛист.ФормаОбъекта", ПараметрыСозданияДокумента(), ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьОтпуск(Команда)
	
	ОткрытьФорму("Документ.Отпуск.ФормаОбъекта", ПараметрыСозданияДокумента(), ЭтаФорма);
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Элементы.Список);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	УправлениеПечатьюКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Элементы.Список);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Функция ПараметрыСозданияДокумента()
	
	ЗначенияЗаполнения = Новый Структура;
	
	Для каждого НастройкаКомпоновщика Из Список.КомпоновщикНастроек.ПользовательскиеНастройки.Элементы Цикл
		Если ТипЗнч(НастройкаКомпоновщика) = Тип("ОтборКомпоновкиДанных") Тогда
			Для каждого ЭлементОтбора Из НастройкаКомпоновщика.Элементы Цикл
				Если ЭлементОтбора.Использование И ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно Тогда
					ЗначенияЗаполнения.Вставить(ЭлементОтбора.ЛевоеЗначение, ЭлементОтбора.ПравоеЗначение);
				КонецЕсли;
			КонецЦикла;
		КонецЕсли; 
	КонецЦикла;
	
	ПараметрыОткрытия = Новый Структура;
	ПараметрыОткрытия.Вставить("ЗначенияЗаполнения", ЗначенияЗаполнения);
	
	Возврат ПараметрыОткрытия;
	
КонецФункции

#КонецОбласти
