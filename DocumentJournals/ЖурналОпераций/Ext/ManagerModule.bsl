﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Возвращает список документов, которые регистрируются в журнале.
// Из списка исключаются документы отключенные функциональными опциями.
//
// Возвращаемое значение:
// Массив объектов метаданных
//
Функция СоставДокументов() Экспорт
	
	СписокДокументов = Новый Массив;
	
	МетаданныеЖурнала = Метаданные.ЖурналыДокументов.ЖурналОпераций;
	
	Для Каждого РегистрируемыйДокумент Из МетаданныеЖурнала.РегистрируемыеДокументы Цикл
		
		Если ОбщегоНазначения.ОбъектМетаданныхДоступенПоФункциональнымОпциям(РегистрируемыйДокумент) Тогда
			СписокДокументов.Добавить(РегистрируемыйДокумент);
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат СписокДокументов;
	
КонецФункции

Функция ПолучитьДополнительныеРеквизитыДляРеестра() Экспорт
	
	Результат = Новый Соответствие;
	
	ПолеЗапросаНомер =
	"	ВЫБОР
	|		КОГДА ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.СчетФактураВыданный)
	|			ТОГДА ВЫРАЗИТЬ(Таб.Ссылка КАК Документ.СчетФактураВыданный).ПредставлениеНомера
	|		КОГДА ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.СчетФактураПолученный)
	|			ТОГДА ВЫРАЗИТЬ(Таб.Ссылка КАК Документ.СчетФактураПолученный).ПредставлениеНомера
	|		ИНАЧЕ Таб.Номер
	|	КОНЕЦ";
	
	ПолеЗапросаДатаВх =
	"	ВЫБОР
	|		КОГДА ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.ОтчетКомиссионераОПродажах)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.ПоступлениеДенежныхДокументов)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.ПоступлениеДопРасходов)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.ПоступлениеНаРасчетныйСчет)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.ПоступлениеНМА)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.ПоступлениеТоваровУслуг)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.СписаниеСРасчетногоСчета)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.СчетНаОплатуПоставщика)
	|				ИЛИ ТИПЗНАЧЕНИЯ(Таб.Ссылка) = ТИП(Документ.СчетФактураПолученный)
	|			ТОГДА Таб.ДатаВходящегоДокумента
	|		ИНАЧЕ """"
	|	КОНЕЦ";
	
	
	Результат.Вставить("Номер,",                 ПолеЗапросаНомер);
	Результат.Вставить("ДатаВходящегоДокумента", ПолеЗапросаДатаВх);
	Результат.Вставить("НомерВходящегоДокумента",
		СтрЗаменить(ПолеЗапросаДатаВх, "ДатаВходящегоДокумента", "НомерВходящегоДокумента"));
	
	Возврат Результат;
	
КонецФункции

// Возвращает дату первого проведенного документа
//
// Параметры:
// Организация - СправочникСсылка.Организация
//
// Возвращаемое значение:
// Дата или Неопределено, если нет ни одного проведенного документа
//
Функция ДатаПервогоПроведенногоДокумента(Организация) Экспорт
	
	ДоступныеОрганизации = ОбщегоНазначенияБПВызовСервераПовтИсп.ВсеОрганизацииДанныеКоторыхДоступныПоRLS(ложь);
	
	Запрос = Новый Запрос;
	
	ТекстЗапроса =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ЖурналОпераций.Дата
	|ИЗ
	|	ЖурналДокументов.ЖурналОпераций КАК ЖурналОпераций
	|ГДЕ
	|	&УсловиеПоОрганизации
	|	И ЖурналОпераций.Проведен
	|	И НЕ ЖурналОпераций.Ссылка ССЫЛКА Документ.ВводНачальныхОстатков
	|
	|УПОРЯДОЧИТЬ ПО
	|	ЖурналОпераций.Дата";
	
	Если ЗначениеЗаполнено(Организация) Тогда
		
		Если ДоступныеОрганизации.Найти(Организация) = Неопределено Тогда
			Возврат Неопределено;
		КонецЕсли;
		
		Запрос.УстановитьПараметр("Организация", Организация);
		УсловиеПоОрганизации = "ЖурналОпераций.Организация = &Организация";
		
	Иначе
		
		Запрос.УстановитьПараметр("ДоступныеОрганизации", ДоступныеОрганизации);
		УсловиеПоОрганизации = "ЖурналОпераций.Организация В(&ДоступныеОрганизации)";
		
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить(ТекстЗапроса, "&УсловиеПоОрганизации", УсловиеПоОрганизации);
	
	УстановитьПривилегированныйРежим(Истина);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Возврат Выборка.Дата;
	КонецЕсли;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт

	// Реестр документов
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "Реестр";
	КомандаПечати.Представление = НСтр("ru = 'Реестр документов'");
	КомандаПечати.ЗаголовокФормы= НСтр("ru = 'Реестр документов'");
	КомандаПечати.Обработчик    = "УправлениеПечатьюБПКлиент.ВыполнитьКомандуПечатиРеестраДокументов";
	КомандаПечати.СписокФорм    = "ФормаСписка";
	КомандаПечати.Порядок       = 100;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли