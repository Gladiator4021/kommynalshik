﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Проверки = БухгалтерскиеОтчетыВызовСервера.СтандартныеПроверкиЗаполнения();
	Проверки.Вставить("СписокВидовСубконто", Истина);
	
	БухгалтерскиеОтчетыВызовСервера.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, Проверки);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли