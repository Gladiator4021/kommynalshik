﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Функция УсловияПоВозрастаниюСложности() Экспорт
	
	ЗначенияПеречисления = Перечисления.УсловияПримененияТребованийЗаконодательства;
	
	ПорядокУсловий = Новый Массив; 
	ПорядокУсловий.Добавить(ЗначенияПеречисления.ФизическиеЛица);
	ПорядокУсловий.Добавить(ЗначенияПеречисления.ЮридическиеЛица);
	ПорядокУсловий.Добавить(ЗначенияПеречисления.ОбщаяСистемаНалогообложения);
	ПорядокУсловий.Добавить(ЗначенияПеречисления.УпрощеннаяСистемаНалогообложения);
	ПорядокУсловий.Добавить(ЗначенияПеречисления.ПлательщикиНДС);
	ПорядокУсловий.Добавить(ЗначенияПеречисления.ПлательщикиЕНВД);
	
	// Все остальные считаем сложными
	
	Возврат ПорядокУсловий;
	
КонецФункции

Функция ПроверитьУсловия(Знач УсловияДляПроверки, Период, Периодичность, Организация, ВыполненныеУсловия = Неопределено) Экспорт
	
	ВыполненныеУсловия = Новый Массив;
	
	Если УсловияДляПроверки.Количество() = 0 Тогда
		Возврат Неопределено; // Все условия выполнены
	КонецЕсли;
	
	Для Каждого Условие Из УсловияДляПроверки Цикл
		
		Если УсловиеВыполняется(Условие, Период, Периодичность, Организация) Тогда
			ВыполненныеУсловия.Добавить(Условие);
		Иначе
			Возврат Условие;
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат Неопределено; // Все условия выполнены
	
КонецФункции

Функция УсловиеВыполняется(Условие, Период, Периодичность, Организация) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	ЗначенияПеречисления = Перечисления.УсловияПримененияТребованийЗаконодательства;
	
	// ЮридическиеЛица, ФизическиеЛица
	Если Условие = ЗначенияПеречисления.ФизическиеЛица Тогда
		Возврат ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Организация, "ЮридическоеФизическоеЛицо") = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
	ИначеЕсли Условие = ЗначенияПеречисления.ЮридическиеЛица Тогда
		Возврат ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Организация, "ЮридическоеФизическоеЛицо") <> Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
	КонецЕсли;
	
	// ЕстьСотрудники
	Если Условие = ЗначенияПеречисления.ЕстьСотрудники Тогда
		Если ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Организация, "ЮридическоеФизическоеЛицо") <> Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо Тогда
			Возврат Истина;
		Иначе
			Возврат УчетЗарплаты.ИПИспользуетТрудНаемныхРаботников(Организация);
		КонецЕсли;
	КонецЕсли;
	
	// СистемаНалогообложения
	Если Условие = ЗначенияПеречисления.ОбщаяСистемаНалогообложения Тогда
		Возврат УчетнаяПолитика.СистемаНалогообложения(Организация, Период) = Перечисления.СистемыНалогообложения.Общая
		Или УчетнаяПолитика.СистемаНалогообложения(Организация, ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период)) = Перечисления.СистемыНалогообложения.Общая;
	ИначеЕсли Условие = ЗначенияПеречисления.УпрощеннаяСистемаНалогообложения Тогда
		Возврат УчетнаяПолитика.СистемаНалогообложения(Организация, Период) = Перечисления.СистемыНалогообложения.Упрощенная
		Или УчетнаяПолитика.СистемаНалогообложения(Организация, ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период)) = Перечисления.СистемыНалогообложения.Упрощенная;
	КонецЕсли;
	
	// Отдельные виды налогов
	Если Условие = ЗначенияПеречисления.ПлательщикиЕНВД Тогда
		Возврат УчетнаяПолитика.ПлательщикЕНВД(Организация, Период) 
		Или УчетнаяПолитика.ПлательщикЕНВД(Организация, ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период));
	ИначеЕсли Условие = ЗначенияПеречисления.ПлательщикиНДС Тогда
		Возврат УчетнаяПолитика.ПлательщикНДС(Организация, Период)
		Или УчетнаяПолитика.ПлательщикНДС(Организация, ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период));
	КонецЕсли;
	
	// Порядок уплаты авансов по налогу на прибыль
	Если Условие = НалогНаПрибыльАвансыЕжеквартально Тогда
		Возврат УчетнаяПолитика.ПорядокУплатыАвансов(Организация, Период) = Перечисления.ПорядокУплатыАвансовПоНалогуНаПрибыль.Ежеквартально;
	ИначеЕсли Условие = НалогНаПрибыльАвансыЕжемесячно Тогда
		Возврат УчетнаяПолитика.ПорядокУплатыАвансов(Организация, Период) = Перечисления.ПорядокУплатыАвансовПоНалогуНаПрибыль.Ежемесячно;
	ИначеЕсли Условие = НалогНаПрибыльАвансыПоФактическойПрибыли Тогда
		Возврат УчетнаяПолитика.ПорядокУплатыАвансов(Организация, Период) = Перечисления.ПорядокУплатыАвансовПоНалогуНаПрибыль.ПоФактическойПрибыли;
	КонецЕсли;
	
	// Вариант бухгалтерской отчетности
	Если Условие = БухгалтерскаяОтчетностьМалыеПредприятия Тогда
		Возврат УчетнаяПолитика.ВариантБухгалтерскойОтчетности(Организация, Период) = Перечисления.ВариантыБухгалтерскойОтчетности.ДляМалыхПредприятий;
	ИначеЕсли Условие = БухгалтерскаяОтчетностьНекоммерческиеОрганизации Тогда
		Возврат УчетнаяПолитика.ВариантБухгалтерскойОтчетности(Организация, Период) = Перечисления.ВариантыБухгалтерскойОтчетности.ДляНекоммерческихОрганизаций;
	ИначеЕсли Условие = БухгалтерскаяОтчетностьОбщаяФорма Тогда
		Возврат УчетнаяПолитика.ВариантБухгалтерскойОтчетности(Организация, Период) = Перечисления.ВариантыБухгалтерскойОтчетности.ОбщиеПравила;
	КонецЕсли;
	
	// Виды имущества
	Если Условие = ЗначенияПеречисления.ЕстьИмущество Тогда
		Возврат ЕстьИмущество(Период, Периодичность, Организация);
	ИначеЕсли Условие = ЗначенияПеречисления.ЕстьТранспортныеСредства Тогда
		Возврат ЕстьТранспортныеСредства(Период, Периодичность, Организация);
	ИначеЕсли Условие = ЗначенияПеречисления.ЕстьЗемельныеУчастки Тогда
		Возврат ЕстьЗемельныеУчастки(Период, Периодичность, Организация);
	КонецЕсли;
	
	// Акцизы
	Если Условие = ЗначенияПеречисления.АкцизыОбщаяФорма Тогда
		ВидыТоваровСпециальныеФормы = Новый Массив;
		// см. АкцизыФормаАлкоголь
		ВидыТоваровСпециальныеФормы.Добавить(Перечисления.ВидыПодакцизныхТоваров.Спирт);
		ВидыТоваровСпециальныеФормы.Добавить(Перечисления.ВидыПодакцизныхТоваров.СпиртосодержащаяПродукция);
		ВидыТоваровСпециальныеФормы.Добавить(Перечисления.ВидыПодакцизныхТоваров.АлкогольнаяПродукция);
		// см. АкцизыФормаТабак
		ВидыТоваровСпециальныеФормы.Добавить(Перечисления.ВидыПодакцизныхТоваров.Табак);
		Возврат ЕстьАкцизыИныеТовары(Организация, ВидыТоваровСпециальныеФормы);
	ИначеЕсли Условие = АкцизыФормаАлкоголь Тогда
		ВидыТоваровАлкоголь = Новый Массив;
		ВидыТоваровАлкоголь.Добавить(Перечисления.ВидыПодакцизныхТоваров.Спирт);
		ВидыТоваровАлкоголь.Добавить(Перечисления.ВидыПодакцизныхТоваров.СпиртосодержащаяПродукция);
		ВидыТоваровАлкоголь.Добавить(Перечисления.ВидыПодакцизныхТоваров.АлкогольнаяПродукция);
		Возврат ЕстьАкцизыПоВидуТоваров(Организация, ВидыТоваровАлкоголь);
	ИначеЕсли Условие = АкцизыФормаТабак Тогда
		Возврат ЕстьАкцизыПоВидуТоваров(Организация, Перечисления.ВидыПодакцизныхТоваров.Табак);
	ИначеЕсли Условие = АкцизыОбщийСрок Тогда
		Возврат ЕстьАкцизыОбщийСрок(Организация);
	ИначеЕсли Условие = АкцизыЛьготныйСрок Тогда
		Возврат ЕстьАкцизыЛьготныйСрок(Организация);
	ИначеЕсли Условие = АвансыПоАкцизам Тогда
		Возврат ЕстьАвансыПоАкцизам(Организация);
	КонецЕсли;
	
	// Фиксированные страховые взносы
	Если Условие = ЗначенияПеречисления.ФиксированныеСтраховыеВзносы_Квартал Тогда
		ПериодичностьУплатыФиксированныхСтраховыхВзносов = 
			УчетСтраховыхВзносовИП.ПериодичностьУплатыФиксированныхСтраховыхВзносов(Организация, Период);
		Возврат (ПериодичностьУплатыФиксированныхСтраховыхВзносов = Перечисления.Периодичность.Квартал);
	ИначеЕсли Условие = ЗначенияПеречисления.ФиксированныеСтраховыеВзносы_Год Тогда
		ПериодичностьУплатыФиксированныхСтраховыхВзносов = 
			УчетСтраховыхВзносовИП.ПериодичностьУплатыФиксированныхСтраховыхВзносов(Организация, Период);
		Возврат (ПериодичностьУплатыФиксированныхСтраховыхВзносов = Перечисления.Периодичность.Год);
	КонецЕсли;
	
	// Представление отчетов в электронном виде
	Если Условие = ОтчетностьПФРвЭлектронномВиде Тогда
		Возврат ИнтерфейсыВзаимодействияБРО.ПодключенДокументооборотСКонтролирующимОрганом(
			Организация, 
			Перечисления.ТипыКонтролирующихОрганов.ПФР);
	ИначеЕсли Условие = ОтчетностьПФРнаБумажномНосителе Тогда
		Возврат Не ИнтерфейсыВзаимодействияБРО.ПодключенДокументооборотСКонтролирующимОрганом(
			Организация, 
			Перечисления.ТипыКонтролирующихОрганов.ПФР);
	ИначеЕсли Условие = ОтчетностьФССвЭлектронномВиде Тогда
		Возврат ИнтерфейсыВзаимодействияБРО.ПодключенДокументооборотСКонтролирующимОрганом(
			Организация, 
			Перечисления.ТипыКонтролирующихОрганов.ФСС);
	ИначеЕсли Условие = ОтчетностьФССнаБумажномНосителе Тогда
		Возврат Не ИнтерфейсыВзаимодействияБРО.ПодключенДокументооборотСКонтролирующимОрганом(
			Организация, 
			Перечисления.ТипыКонтролирующихОрганов.ФСС);
	КонецЕсли;
	
	// Косвенные налоги при импорте товаров из таможенного союза
	Если Условие = ЗначенияПеречисления.ИмпортТоваровТаможенныйСоюз Тогда
		Возврат ЕстьИмпортТоваровИзТаможенногоСоюза(Период, Периодичность, Организация);
	КонецЕсли; 
	
	Если Условие = ЗначенияПеречисления.ПлательщикиТорговогоСбора Тогда
		Возврат УчетнаяПолитика.ПлательщикТорговогоСбора(Организация, Период)
			ИЛИ ЕстьОбъектыТорговогоСбора(Период, Периодичность, Организация);
	КонецЕсли;
	
	Если Условие = ЗначенияПеречисления.СверкаДанныхНДС Тогда
		
		НачалоПериода = ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период);
		КонецПериода  = ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период);
		
		Возврат Обработки.СверкаДанныхУчетаНДС.ВозможнаСверкаНДСЗаПериод(НачалоПериода, КонецПериода, Организация);
		
	КонецЕсли;
	
	Если Условие = ЗначенияПеречисления.ЖурналСчетовФактур Тогда
		
		Если УчетнаяПолитика.СистемаНалогообложения(Организация, Период) = Перечисления.СистемыНалогообложения.Упрощенная Тогда
			НачалоПериода = ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период);
			КонецПериода  = ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период);
			
			Возврат Обработки.СверкаДанныхУчетаНДС.ВозможнаСверкаНДСЗаПериод(НачалоПериода, КонецПериода, Организация);
		Иначе
			Возврат Ложь;
		КонецЕсли;
		
	КонецЕсли;
	
	// Остальные условия не умеем определять
	Возврат Ложь;
	
КонецФункции

Функция НепериодическиеУсловия() Экспорт
	
	Условия = Новый Соответствие;
	
	ЗначенияПеречисления = Перечисления.УсловияПримененияТребованийЗаконодательства;
	
	Условия.Вставить(ЗначенияПеречисления.АкцизыОбщаяФорма,   Истина);
	Условия.Вставить(ЗначенияПеречисления.АкцизыФормаТабак,   Истина);
	Условия.Вставить(ЗначенияПеречисления.АкцизыОбщийСрок,    Истина);
	Условия.Вставить(ЗначенияПеречисления.АкцизыЛьготныйСрок, Истина);
	Условия.Вставить(ЗначенияПеречисления.АвансыПоАкцизам,    Истина);
	Условия.Вставить(ЗначенияПеречисления.ЮридическиеЛица,    Истина);
	Условия.Вставить(ЗначенияПеречисления.ФизическиеЛица,     Истина);
	
	Возврат Условия;
	
КонецФункции

Функция ЕстьИмущество(Период, Периодичность, Организация)
	
	// Налог на имущество заведомо не платят те, у кого не было ОС
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация",  Организация);
	Запрос.УстановитьПараметр("КонецПериода", ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период));
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	СобытияОСОрганизаций.ОсновноеСредство
	|ИЗ
	|	РегистрСведений.СобытияОСОрганизаций КАК СобытияОСОрганизаций
	|ГДЕ
	|	СобытияОСОрганизаций.Организация = &Организация
	|	И СобытияОСОрганизаций.Период <= &КонецПериода";
	Если Запрос.Выполнить().Пустой() Тогда
		Возврат Ложь;
	КонецЕсли;
	
	// По старым правилам налог платили практически все, у кого есть хоть какие-то ОС
	Если Период < '2013-01-01' Тогда
		Возврат Истина;
	КонецЕсли;
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("НедвижимоеИмущество", Перечисления.ГруппыОС.НедвижимоеИмущество());
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ОсновныеСредства.Ссылка КАК ОсновноеСредство
	|ПОМЕСТИТЬ Недвижимость
	|ИЗ
	|	Справочник.ОсновныеСредства КАК ОсновныеСредства
	|ГДЕ
	|	ОсновныеСредства.ГруппаОС В(&НедвижимоеИмущество)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОсновноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	Недвижимость.ОсновноеСредство
	|ИЗ
	|	Недвижимость КАК Недвижимость";
	ЕстьНедвижимость = Не Запрос.Выполнить().Пустой();
	
	Если ЕстьНедвижимость Тогда
		// Дешевле всего проверить, были ли движения по ОС за период.
		// Если были, то надо платить налог.
		Запрос = Новый Запрос;
		Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
		Запрос.УстановитьПараметр("НачалоПериода", ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период));
		Запрос.УстановитьПараметр("КонецПериода",  ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период));
		Запрос.УстановитьПараметр("Организация",   Организация);
		Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	СобытияОСОрганизаций.ОсновноеСредство
		|ИЗ
		|	РегистрСведений.СобытияОСОрганизаций КАК СобытияОСОрганизаций
		|ГДЕ
		|	СобытияОСОрганизаций.Период МЕЖДУ &НачалоПериода И &КонецПериода
		|	И СобытияОСОрганизаций.Организация = &Организация
		|	И СобытияОСОрганизаций.ОсновноеСредство В
		|			(ВЫБРАТЬ
		|				Недвижимость.ОсновноеСредство
		|			ИЗ
		|				Недвижимость)";
		Если Не Запрос.Выполнить().Пустой() Тогда
			Возврат Истина;
		КонецЕсли;
		
		// Также недорого проверить, есть ли объекты недвижимости на начало периода.
		// Если есть, то надо платить налог.
		Запрос = Новый Запрос;
		Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
		Запрос.УстановитьПараметр("Период",      ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период));
		Запрос.УстановитьПараметр("Организация", Организация);
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	Хозрасчетный.Ссылка КАК Счет
		|ПОМЕСТИТЬ СчетаОсновныхСредств
		|ИЗ
		|	ПланСчетов.Хозрасчетный КАК Хозрасчетный
		|ГДЕ
		|	Хозрасчетный.Ссылка В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ОсновныеСредства))
		|	И НЕ Хозрасчетный.Ссылка В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ВыбытиеОС))
		|
		|ОБЪЕДИНИТЬ
		|
		|ВЫБРАТЬ
		|	Хозрасчетный.Ссылка
		|ИЗ
		|	ПланСчетов.Хозрасчетный КАК Хозрасчетный
		|ГДЕ
		|	Хозрасчетный.Ссылка В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ДоходныеВложенияВ_МЦ))
		|	И НЕ Хозрасчетный.Ссылка В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ВыбытиеМЦ))
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Счет
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ ПЕРВЫЕ 1
		|	ХозрасчетныйОстатки.Счет
		|ИЗ
		|	РегистрБухгалтерии.Хозрасчетный.Остатки(
		|			&Период,
		|			Счет В
		|				(ВЫБРАТЬ
		|					СчетаОсновныхСредств.Счет
		|				ИЗ
		|					СчетаОсновныхСредств),
		|			ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыСубконтоХозрасчетные.ОсновныеСредства),
		|			Организация = &Организация
		|				И Субконто1 В
		|					(ВЫБРАТЬ
		|						Недвижимость.ОсновноеСредство
		|					ИЗ
		|						Недвижимость)) КАК ХозрасчетныйОстатки";
		
		Если Не Запрос.Выполнить().Пустой() Тогда
			Возврат Истина;
		КонецЕсли;
		
	КонецЕсли;
	
	// Если недвижимости нет (совсем нет или нет в анализируемом периоде), 
	// то придется проверить, а нет ли движимого имущества, принятого на учет до 2013 года.
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Период",      ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период));
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ОсновныеСредства.ОсновноеСредство КАК ОсновноеСредство
	|ИЗ
	|	РегистрСведений.СостоянияОСОрганизаций.СрезПоследних(&Период, Организация = &Организация) КАК ОсновныеСредства
	|ГДЕ
	|	ОсновныеСредства.Состояние = ЗНАЧЕНИЕ(Перечисление.СостоянияОС.ПринятоКУчету)
	|	И ОсновныеСредства.Период < ДАТАВРЕМЯ(2013, 1, 1)";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция ЕстьИмуществоЕСГС() Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.ОсновноеСредство КАК ОсновноеСредство
	|ИЗ
	|	РегистрСведений.СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам КАК СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам
	|ГДЕ
	|	СтавкиНалогаНаИмуществоПоОтдельнымОсновнымСредствам.ВидИмущества = ЗНАЧЕНИЕ(Перечисление.ВидыИмущества.ВходитВСоставЕСГС)";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция ЕстьТранспортныеСредства(Период = Неопределено, Периодичность = Неопределено, Организация = Неопределено) Экспорт
	
	Если Организация = Неопределено Тогда
		
		УстановитьПривилегированныйРежим(Истина);
		
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	РегистрацияТранспортныхСредств.ОсновноеСредство
		|ИЗ
		|	РегистрСведений.РегистрацияТранспортныхСредств КАК РегистрацияТранспортныхСредств
		|ГДЕ
		|	РегистрацияТранспортныхСредств.ВключатьВНалоговуюБазу";
		
		Возврат Не Запрос.Выполнить().Пустой();
		
	Иначе
		
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("НачалоПериода", ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период));
		Запрос.УстановитьПараметр("КонецПериода",  ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период));
		Запрос.УстановитьПараметр("Организация",   Организация);
		Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	РегистрацияТранспортныхСредств.Организация
		|ИЗ
		|	РегистрСведений.РегистрацияТранспортныхСредств.СрезПоследних(&НачалоПериода, Организация = &Организация) КАК РегистрацияТранспортныхСредств
		|ГДЕ
		|	РегистрацияТранспортныхСредств.ВключатьВНалоговуюБазу
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ ПЕРВЫЕ 1
		|	РегистрацияТранспортныхСредств.Организация
		|ИЗ
		|	РегистрСведений.РегистрацияТранспортныхСредств КАК РегистрацияТранспортныхСредств
		|ГДЕ
		|	РегистрацияТранспортныхСредств.Организация = &Организация
		|	И РегистрацияТранспортныхСредств.Период МЕЖДУ &НачалоПериода И &КонецПериода
		|	И РегистрацияТранспортныхСредств.ВключатьВНалоговуюБазу";
		
		Возврат Не Запрос.Выполнить().Пустой();
		
	КонецЕсли;
	
КонецФункции

Функция ЕстьЗемельныеУчастки(Период, Периодичность, Организация)
	
	НачалоПериода = ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период);
	КонецПериода = ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период);
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("НачалоПериода", Дата(Год(НачалоПериода), Месяц(НачалоПериода), 15));
	Запрос.УстановитьПараметр("КонецПериода", Дата(Год(КонецПериода), Месяц(КонецПериода), 15));
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	РегистрацияЗемельныхУчастков.Организация
	|ИЗ
	|	РегистрСведений.РегистрацияЗемельныхУчастков.СрезПоследних(&НачалоПериода, Организация = &Организация) КАК РегистрацияЗемельныхУчастков
	|ГДЕ
	|	РегистрацияЗемельныхУчастков.ВключатьВНалоговуюБазу
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	РегистрацияЗемельныхУчастков.Организация
	|ИЗ
	|	РегистрСведений.РегистрацияЗемельныхУчастков КАК РегистрацияЗемельныхУчастков
	|ГДЕ
	|	РегистрацияЗемельныхУчастков.Организация = &Организация
	|	И РегистрацияЗемельныхУчастков.Период МЕЖДУ &НачалоПериода И &КонецПериода
	|	И РегистрацияЗемельныхУчастков.ВключатьВНалоговуюБазу";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция КатегорииПодчиненияЗемельныхУчастковПоВидамНалогов() Экспорт
	
	КатегорииПодчинения = Новый Массив;
	КатегорииПодчинения.Добавить("103"); // ЗемельныйНалог_ГородФедеральногоЗначения
	КатегорииПодчинения.Добавить("204"); // ЗемельныйНалог_ГородскойОкруг
	КатегорииПодчинения.Добавить("211"); // ЗемельныйНалог_ГородскойОкругСВнутригородскимДелением
	КатегорииПодчинения.Добавить("212"); // ЗемельныйНалог_ВнутригородскойОкруг
	КатегорииПодчинения.Добавить("305"); // ЗемельныйНалог_МежселеннаяТерритория
	КатегорииПодчинения.Добавить("310"); // ЗемельныйНалог_СельскоеПоселение
	КатегорииПодчинения.Добавить("313"); // ЗемельныйНалог_ГородскоеПоселение
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("КатегорииПодчинения", КатегорииПодчинения);
	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ПОДСТРОКА(РегистрацияЗемельныхУчастков.КБК, 11, 3) КАК КатегорияПодчинения
	|ИЗ
	|	РегистрСведений.РегистрацияЗемельныхУчастков КАК РегистрацияЗемельныхУчастков
	|ГДЕ
	|	ПОДСТРОКА(РегистрацияЗемельныхУчастков.КБК, 11, 3) В (&КатегорииПодчинения)
	|	И ПОДСТРОКА(РегистрацияЗемельныхУчастков.КБК, 18, 3) = ""110""
	|	И РегистрацияЗемельныхУчастков.ВключатьВНалоговуюБазу";
	
	УстановитьПривилегированныйРежим(Истина);
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("КатегорияПодчинения");
	
КонецФункции

Функция КатегорииПодчиненияПатентовПоВидамНалогов() Экспорт
	
	КатегорииПодчинения = Новый Массив;
	КатегорииПодчинения.Добавить("010"); // ПСН_ГородскойОкруг
	КатегорииПодчинения.Добавить("020"); // ПСН_МуниципальныйРайон
	КатегорииПодчинения.Добавить("030"); // ПСН_ГородФедеральногоЗначения
	КатегорииПодчинения.Добавить("040"); // ПСН_ГородскойОкругСВнутригородскимДелением
	КатегорииПодчинения.Добавить("050"); // ПСН_ВнутригородскойРайон
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("КатегорииПодчинения", КатегорииПодчинения);
	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ПОДСТРОКА(СправочникПатенты.КБК, 9, 3) КАК КатегорияПодчинения
	|ИЗ
	|	Справочник.Патенты КАК СправочникПатенты
	|ГДЕ
	|	ПОДСТРОКА(СправочникПатенты.КБК, 9, 3) В (&КатегорииПодчинения)
	|	И ПОДСТРОКА(СправочникПатенты.КБК, 18, 3) = ""110""
	|	И НЕ СправочникПатенты.ПометкаУдаления";
	
	УстановитьПривилегированныйРежим(Истина);
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("КатегорияПодчинения");
	
КонецФункции

Функция ЕстьАкцизыИныеТовары(Организация, ИсключаемыеТовары)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация",       Организация);
	Запрос.УстановитьПараметр("ИсключаемыеТовары", ИсключаемыеТовары);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ИСТИНА КАК Результат
	|ИЗ
	|	РегистрСведений.ПорядокУплатыАкцизов КАК ПорядокУплатыАкцизов
	|ГДЕ
	|	ПорядокУплатыАкцизов.Организация = &Организация
	|	И НЕ ПорядокУплатыАкцизов.ВидТовара В (&ИсключаемыеТовары)";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция ЕстьАкцизыПоВидуТоваров(Организация, ВидыТоваров)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("ВидыТоваров",  ВидыТоваров);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ИСТИНА КАК Результат
	|ИЗ
	|	РегистрСведений.ПорядокУплатыАкцизов КАК ПорядокУплатыАкцизов
	|ГДЕ
	|	ПорядокУплатыАкцизов.Организация = &Организация
	|	И ПорядокУплатыАкцизов.ВидТовара В (&ВидыТоваров)";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция ЕстьАкцизыОбщийСрок(Организация)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ИСТИНА КАК Результат
	|ИЗ
	|	РегистрСведений.ПорядокУплатыАкцизов КАК ПорядокУплатыАкцизов
	|ГДЕ
	|	ПорядокУплатыАкцизов.Организация = &Организация
	|	И НЕ ПорядокУплатыАкцизов.ЛьготныйСрокУплаты";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция ЕстьАкцизыЛьготныйСрок(Организация)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ИСТИНА КАК Результат
	|ИЗ
	|	РегистрСведений.ПорядокУплатыАкцизов КАК ПорядокУплатыАкцизов
	|ГДЕ
	|	ПорядокУплатыАкцизов.Организация = &Организация
	|	И ПорядокУплатыАкцизов.ЛьготныйСрокУплаты";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция ЕстьАвансыПоАкцизам(Организация)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ИСТИНА КАК Результат
	|ИЗ
	|	РегистрСведений.ПорядокУплатыАкцизов КАК ПорядокУплатыАкцизов
	|ГДЕ
	|	ПорядокУплатыАкцизов.Организация = &Организация
	|	И ПорядокУплатыАкцизов.АвансыПриЗакупкеСырья";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

Функция ЕстьИмпортТоваровИзТаможенногоСоюза(Период = Неопределено, Периодичность = Неопределено, Организация = Неопределено) Экспорт
	
	Если НЕ ПолучитьФункциональнуюОпцию("ВедетсяУчетИмпортныхТоваров") Тогда
		Возврат Ложь;
	КонецЕсли; 

	Запрос = Новый Запрос;
	
	ТекстЗапроса = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ИмпортТоваровТаможенныйСоюз.Организация
	|ИЗ
	|	РегистрСведений.ИмпортТоваровТаможенныйСоюз КАК ИмпортТоваровТаможенныйСоюз";
	
	МассивУсловий = Новый Массив;
	
	Если ЗначениеЗаполнено(Организация) Тогда
		МассивУсловий.Добавить("ИмпортТоваровТаможенныйСоюз.Организация = &Организация");
		Запрос.УстановитьПараметр("Организация", Организация);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Период) И ЗначениеЗаполнено(Периодичность) Тогда
		МассивУсловий.Добавить("ИмпортТоваровТаможенныйСоюз.ДатаПериода = &Период");
		Запрос.УстановитьПараметр("Период", ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период));
	КонецЕсли;
	
	Если МассивУсловий.Количество() > 0 Тогда
		ТекстЗапроса = ТекстЗапроса + "
			|ГДЕ
			| "+ СтрСоединить(МассивУсловий, " И ");
	КонецЕсли;
		
	Запрос.Текст = ТекстЗапроса;
	
	Возврат Не Запрос.Выполнить().Пустой();

КонецФункции 

Функция ЕстьОбъектыТорговогоСбора(Период = Неопределено, Периодичность = Неопределено, Организация = Неопределено) Экспорт

	Если Период = Неопределено ИЛИ Организация = Неопределено Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если НЕ УчетнаяПолитика.ПлательщикТорговогоСбора(Организация, Период) Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ВидОперацииСнятиеСУчета", Перечисления.ВидыОперацийТорговыеТочки.СнятиеСУчета);
	
	ТекстЗапроса = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПараметрыТорговыхТочекСрезПоследних.ТорговаяТочка
	|ИЗ
	|	РегистрСведений.ПараметрыТорговыхТочек.СрезПоследних(&КонецПериода, &Организация) КАК ПараметрыТорговыхТочекСрезПоследних
	|ГДЕ
	|	(&Условия)";
	
	МассивУсловий = Новый Массив;
	МассивУсловий.Добавить("ПараметрыТорговыхТочекСрезПоследних.ВидОперации <> &ВидОперацииСнятиеСУчета");
	
	Если ЗначениеЗаполнено(Организация) Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&Организация", "Организация = &Организация");
		Запрос.УстановитьПараметр("Организация", Организация);
	Иначе
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&Организация", "");
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Период) И ЗначениеЗаполнено(Периодичность) Тогда
		МассивУсловий.Добавить("ПараметрыТорговыхТочекСрезПоследних.Период >= &НачалоПериода");
		Запрос.УстановитьПараметр("НачалоПериода",      ИнтерфейсыВзаимодействияБРОКлиентСервер.НачалоПериода(Периодичность, Период));
		Запрос.УстановитьПараметр("КонецПериода",      ИнтерфейсыВзаимодействияБРОКлиентСервер.КонецПериода(Периодичность, Период));
	Иначе
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&КонецПериода", "");
	КонецЕсли;
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&Условия", СтрСоединить(МассивУсловий, " ИЛИ "));
	
	Запрос.Текст = ТекстЗапроса;
	
	Возврат Не Запрос.Выполнить().Пустой();

КонецФункции

Функция УсловияИсключенияПоТипуОрганизации(Организация, Период = Неопределено) Экспорт
	
	УсловияИсключения = Новый Массив;
	
	ЭтоФизическоеЛицо = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Организация, "ЮридическоеФизическоеЛицо") = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
	Если ЭтоФизическоеЛицо Тогда
		
		УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ЮридическиеЛица);
		
		Если НЕ УчетЗарплаты.ИПИспользуетТрудНаемныхРаботников(Организация) Тогда
			УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ЕстьСотрудники);
			УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.СтраховыеВзносыСотрудников);
		КонецЕсли;
		
		УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.РозничнаяПродажаАлкоголя);
		УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ОптоваяТорговляАлкоголем);
		УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ПроизводствоАлгоколя);
		УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ПеревозкаАлкоголя);
		
		УсловияИсключения.Добавить(Перечисления.ВидыПодакцизныхТоваров.АлкогольнаяПродукция);
		УсловияИсключения.Добавить(Перечисления.ВидыПодакцизныхТоваров.Спирт);
		УсловияИсключения.Добавить(Перечисления.ВидыПодакцизныхТоваров.СпиртосодержащаяПродукция);
		
	Иначе
		УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ФизическиеЛица);
	КонецЕсли;
	
	Если Период <> Неопределено Тогда
		Если УчетнаяПолитика.ПрименяетсяУСН(Организация, Период) Тогда
			УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ОбщаяСистемаНалогообложения);
		КонецЕсли;
		Если УчетнаяПолитика.ПлательщикНалогаНаПрибыль(Организация, Период)
			ИЛИ УчетнаяПолитика.ПлательщикНДФЛ(Организация, Период) Тогда
			УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.УпрощеннаяСистемаНалогообложения);
		КонецЕсли;
		Если НЕ УчетнаяПолитика.ПлательщикЕНВД(Организация, Период) Тогда
			УсловияИсключения.Добавить(Перечисления.УсловияПримененияТребованийЗаконодательства.ПлательщикиЕНВД);
		КонецЕсли;
	КонецЕсли;
	
	Возврат УсловияИсключения;
	
КонецФункции

#КонецЕсли
