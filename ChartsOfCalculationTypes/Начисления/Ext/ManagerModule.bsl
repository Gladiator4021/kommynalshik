﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

#Область БлокФункцийПервоначальногоЗаполненияИОбновленияИБ

// Процедура заполняет план видов расчета т.н. псевдопредопределенными элементами, 
// идентифицируемыми из кода
//
Процедура СоздатьНачисленияПоНастройкам() Экспорт
	
	КодДоходаНДФЛ_2012 = ВидыДоходовНДФЛПоКоду("2012");
	КодДоходаНДФЛ_2300 = ВидыДоходовНДФЛПоКоду("2300");
	
	// Безусловно создаем оклад
	Описание = ОписаниеНачисления();
	Описание.Код 						= НСтр("ru = 'ОКЛ'");
	Описание.Наименование 				= НСтр("ru = 'Оплата по окладу'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ПовременнаяОплатаТруда;
	Описание.ВидНачисленияДляНУ 		= Перечисления.ВидыНачисленийОплатыТрудаДляНУ.пп1ст255;
	Описание.КодДоходаНДФЛ 				= Справочники.ВидыДоходовНДФЛ.КодДоходаПоУмолчанию;
	Описание.КодДоходаСтраховыеВзносы 	= Справочники.ВидыДоходовПоСтраховымВзносам.ОблагаетсяЦеликом;
	Описание.ВходитВБазуРКИСН 			= Истина;
	ЗаписатьНачисление(Описание);
	
	// Отпуск по беременности и родам.
	Описание = ОписаниеНачисления();
	Описание.Код						= НСтр("ru = 'ОТБРР'");
	Описание.Наименование				= НСтр("ru = 'Отпуск по беременности и родам'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОтпускПоБеременностиИРодам;
	Описание.ВидНачисленияДляНУ			= Перечисления.ВидыНачисленийОплатыТрудаДляНУ.ПустаяСсылка();
	Описание.КодДоходаНДФЛ 				= Справочники.ВидыДоходовНДФЛ.ПустаяСсылка();
	Описание.КодДоходаСтраховыеВзносы 	= Справочники.ВидыДоходовПоСтраховымВзносам.ПособияЗаСчетФСС;
	Описание.ВходитВБазуРКИСН 			= Ложь;
	Описание.ВидОперацииПоЗарплате 		= Перечисления.ВидыОперацийПоЗарплате.РасходыПоСтрахованиюФСС;
	ЗаписатьНачисление(Описание);
	
	// Больничный при травме на производстве.
	Описание = ОписаниеНачисления();
	Описание.Код						= НСтр("ru = 'БЛТП'");
	Описание.Наименование				= НСтр("ru = 'Больничный при травме на производстве'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоНесчастныйСлучайНаПроизводстве;
	Описание.ВидНачисленияДляНУ			= Перечисления.ВидыНачисленийОплатыТрудаДляНУ.ПустаяСсылка();
	Описание.КодДоходаНДФЛ 				= КодДоходаНДФЛ_2300;
	Описание.КодДоходаСтраховыеВзносы 	= Справочники.ВидыДоходовПоСтраховымВзносам.ПособияЗаСчетФСС_НС;
	Описание.ВходитВБазуРКИСН 			= Ложь;
	Описание.ВидОперацииПоЗарплате 		= Перечисления.ВидыОперацийПоЗарплате.РасходыПоСтрахованиюФССНС;
	ЗаписатьНачисление(Описание);
	
	// Больничный при профзаболевании.
	Описание = ОписаниеНачисления();
	Описание.Код						= НСтр("ru = 'БЛПЗ'");
	Описание.Наименование				= НСтр("ru = 'Больничный при профзаболевании'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоПрофзаболевание;
	Описание.ВидНачисленияДляНУ 		= Перечисления.ВидыНачисленийОплатыТрудаДляНУ.ПустаяСсылка();
	Описание.КодДоходаНДФЛ 				= КодДоходаНДФЛ_2300;
	Описание.КодДоходаСтраховыеВзносы 	= Справочники.ВидыДоходовПоСтраховымВзносам.ПособияЗаСчетФСС_НС;
	Описание.ВходитВБазуРКИСН 			= Ложь;
	Описание.ВидОперацииПоЗарплате 		= Перечисления.ВидыОперацийПоЗарплате.РасходыПоСтрахованиюФССНС;
	ЗаписатьНачисление(Описание);
	
	// Больничный
	Описание = ОписаниеНачисления();
	Описание.Код						= НСтр("ru = 'БЛН'");
	Описание.Наименование				= НСтр("ru = 'Больничный'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоЛиста;
	Описание.ВидНачисленияДляНУ 		= Перечисления.ВидыНачисленийОплатыТрудаДляНУ.ПустаяСсылка();
	Описание.КодДоходаНДФЛ 				= КодДоходаНДФЛ_2300;
	Описание.КодДоходаСтраховыеВзносы 	= Справочники.ВидыДоходовПоСтраховымВзносам.ПособияЗаСчетФСС;
	Описание.ВходитВБазуРКИСН 			= Ложь;
	Описание.ВидОперацииПоЗарплате 		= Перечисления.ВидыОперацийПоЗарплате.РасходыПоСтрахованиюФСС;
	ЗаписатьНачисление(Описание);
	
	// Оплата больничных листов за счет работодателя.
	Описание = ОписаниеНачисления();
	Описание.Код						= НСтр("ru = 'БЛРДТ'");
	Описание.Наименование				= НСтр("ru = 'Больничный за счет работодателя'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоЛистаЗаСчетРаботодателя;
	Описание.ВидНачисленияДляНУ 		= Перечисления.ВидыНачисленийОплатыТрудаДляНУ.пп48ст266;
	Описание.КодДоходаНДФЛ 				= КодДоходаНДФЛ_2300;
	Описание.КодДоходаСтраховыеВзносы 	= Справочники.ВидыДоходовПоСтраховымВзносам.НеОблагаетсяЦеликом;
	Описание.ВходитВБазуРКИСН 			= Ложь;
	Описание.ВидОперацииПоЗарплате 		= Перечисления.ВидыОперацийПоЗарплате.РасходыПоСтрахованиюРаботодатель;
	ЗаписатьНачисление(Описание);
	
	// Оплата отпуска
	Описание = ОписаниеНачисления();
	Описание.Код						= НСтр("ru = 'ОТ'");
	Описание.Наименование				= НСтр("ru = 'Отпуск основной'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаОтпуска;
	Описание.ВидНачисленияДляНУ 		= Перечисления.ВидыНачисленийОплатыТрудаДляНУ.пп7ст255;
	Описание.КодДоходаНДФЛ 				= КодДоходаНДФЛ_2012;
	Описание.КодДоходаСтраховыеВзносы 	= Справочники.ВидыДоходовПоСтраховымВзносам.ОблагаетсяЦеликом;
	Описание.ВходитВБазуРКИСН 			= Ложь;
	Описание.ВидОперацииПоЗарплате 		= Перечисления.ВидыОперацийПоЗарплате.ЕжегодныйОтпуск;
	ЗаписатьНачисление(Описание);
	
	// Если используется РК и/или СН - создаем соответствующие виды начислений.
	СоздатьНачисленияРКиСН();
	
КонецПроцедуры

Процедура СоздатьНачисленияРКиСН() Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	ВостребованностьРКиСН = РасчетЗарплаты.ВостребованностьРКиСН();
	
	// Районный коэффициент
	Описание = ОписаниеНачисления();
	Описание.Код = НСтр("ru = 'РК'");
	Описание.Наименование = НСтр("ru = 'Районный коэффициент'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.РайонныйКоэффициент;
	Описание.ВидНачисленияДляНУ = Перечисления.ВидыНачисленийОплатыТрудаДляНУ.пп11ст255;
	Описание.КодДоходаНДФЛ = Справочники.ВидыДоходовНДФЛ.КодДоходаПоУмолчанию;
	Описание.КодДоходаСтраховыеВзносы = Справочники.ВидыДоходовПоСтраховымВзносам.ОблагаетсяЦеликом;
	
	Если ВостребованностьРКиСН.РайонныйКоэффициент Тогда
		ЗаписатьНачисление(Описание);
	Иначе
		ОтключитьИспользованиеНачисленийПоОписанию(Описание);
	КонецЕсли;
	
	// Северная надбавка
	Описание = ОписаниеНачисления();
	Описание.Код = НСтр("ru = 'СН'");
	Описание.Наименование = НСтр("ru = 'Северная надбавка'");
	Описание.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.СевернаяНадбавка;
	Описание.ВидНачисленияДляНУ = Перечисления.ВидыНачисленийОплатыТрудаДляНУ.пп12ст255;
	Описание.КодДоходаНДФЛ = Справочники.ВидыДоходовНДФЛ.КодДоходаПоУмолчанию;
	Описание.КодДоходаСтраховыеВзносы = Справочники.ВидыДоходовПоСтраховымВзносам.ОблагаетсяЦеликом;
	
	Если ВостребованностьРКиСН.СевернаяНадбавка Тогда
		ЗаписатьНачисление(Описание);
	Иначе
		ОтключитьИспользованиеНачисленийПоОписанию(Описание);
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

Функция ОписаниеНачисления()
	
	Описание = Новый Структура(
	"Код,
	|Наименование,
	|КатегорияНачисленияИлиНеоплаченногоВремени,
	|ВидНачисленияДляНУ,
	|КодДоходаНДФЛ,
	|КодДоходаСтраховыеВзносы,
	|ОтношениеКЕНВД,
	|КлючевыеСвойства,
	|СвойстваПоКатегории,
	|ВходитВБазуРКИСН,
	|ВидОперацииПоЗарплате");
	
	Описание.КлючевыеСвойства = "";
	Описание.ВидОперацииПоЗарплате = Перечисления.ВидыОперацийПоЗарплате.НачисленоДоход;
	
	Возврат Описание;
	
КонецФункции

Функция ЗаписатьНачисление(Описание, ПроверятьНаличиеНачислений = Истина)
	
	Если ПроверятьНаличиеНачислений Тогда
		НачисленияПоОписанию = НачисленияПоОписанию(Описание);
		Если НачисленияПоОписанию.Количество() > 0 Тогда
			// Если начисления по такому описанию уже существуют, 
			// надо проверить все ли они используются, если нет - нужно их «включить».
			УстановитьИспользованиеНачислений(НачисленияПоОписанию, Истина);
			Возврат НачисленияПоОписанию;
		КонецЕсли;
	КонецЕсли;
	
	СвойстваПоКатегории = Описание.СвойстваПоКатегории;
	
	НачислениеОбъект = ПланыВидовРасчета.Начисления.СоздатьВидРасчета();
		
	ЗаполнитьЗначенияСвойств(НачислениеОбъект, Описание);
	
	НачислениеОбъект.Записать();
	
	Возврат ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(НачислениеОбъект.Ссылка);
	
КонецФункции

Процедура ОтключитьИспользованиеНачисленийПоОписанию(Описание)
	
	УстановитьИспользованиеНачислений(НачисленияПоОписанию(Описание), Ложь);

КонецПроцедуры

Функция НачисленияПоОписанию(Описание)
	
	Отбор = Неопределено;
	
	// Если указаны, ключевые свойства используются для уточнения критериев отбора искомого начисления.
	Если ЗначениеЗаполнено(Описание.КлючевыеСвойства) Тогда
		Отбор = Новый Структура(Описание.КлючевыеСвойства);
		ЗаполнитьЗначенияСвойств(Отбор, Описание);
	КонецЕсли;
	
	Возврат РасчетЗарплаты.НачисленияПоКатегории(Описание.КатегорияНачисленияИлиНеоплаченногоВремени, Отбор);
	
КонецФункции

Процедура УстановитьИспользованиеНачислений(Начисления, Использование)
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	Начисления.Ссылка
	|ИЗ
	|	ПланВидовРасчета.Начисления КАК Начисления
	|ГДЕ
	|	Начисления.Ссылка В(&Начисления)
	|	И Начисления.ПометкаУдаления <> &ПометкаУдаления";
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("Начисления", Начисления);
	Запрос.УстановитьПараметр("ПометкаУдаления", Не Использование);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		НачислениеОбъект = Выборка.Ссылка.ПолучитьОбъект();
		НачислениеОбъект.ПометкаУдаления = Не Использование;
		НачислениеОбъект.Записать();
	КонецЦикла;
	
КонецПроцедуры

Функция ВидыДоходовНДФЛПоКоду(КодДохода)
	
	КодДоходаНДФЛ = Справочники.ВидыДоходовНДФЛ.НайтиПоКоду(КодДохода);
	Если НЕ ЗначениеЗаполнено(КодДоходаНДФЛ) Тогда
		 
		Справочники.ВидыДоходовНДФЛ.СоздатьКодыДоходовНДФЛ();
		КодДоходаНДФЛ = Справочники.ВидыДоходовНДФЛ.НайтиПоКоду(КодДохода);
		
	КонецЕсли; 
	
	Возврат КодДоходаНДФЛ;
	
КонецФункции

#КонецОбласти

#КонецЕсли