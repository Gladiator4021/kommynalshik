﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьНачальноеОтображениеЭлементовУправления();
	Если Параметры.Ключ.Пустая() Тогда
		// Поле НДФЛ
		ОблагаетсяНДФЛ = Число(ЗначениеЗаполнено(Объект.КодДоходаНДФЛ));
		УстановитьДоступностьКодаДоходаНДФЛ(ЭтаФорма);
		// Поле ВидНачисленияПоНУ
		ВключаетсяВРасходыПоСт255НКРФ = Число(ЗначениеЗаполнено(Объект.ВидНачисленияДляНУ));
		УстановитьДоступностьВидаНачисленияДляНУ(ЭтаФорма);
	КонецЕсли;
	
	// СтандартныеПодсистемы.Свойства
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
	// Поле НДФЛ
	ОблагаетсяНДФЛ = Число(ЗначениеЗаполнено(ТекущийОбъект.КодДоходаНДФЛ));
	УстановитьДоступностьКодаДоходаНДФЛ(ЭтаФорма);
	
	// Поле ВидНачисленияПоНУ
	ВключаетсяВРасходыПоСт255НКРФ = Число(ЗначениеЗаполнено(ТекущийОбъект.ВидНачисленияДляНУ));
	УстановитьДоступностьВидаНачисленияДляНУ(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "ЗаписьЭлементаПланаВидовРасчетаНачисления");
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// СтандартныеПодсистемы.Свойства 
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства
	
	Если ОблагаетсяНДФЛ = 1 И Не ЗначениеЗаполнено(Объект.КодДоходаНДФЛ) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Код дохода НДФЛ не заполнен.'"), , , "Объект.КодДоходаНДФЛ" , Отказ);
	КонецЕсли;
	
	Если ВключаетсяВРасходыПоСт255НКРФ = 1 И Не ЗначениеЗаполнено(Объект.ВидНачисленияДляНУ) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Вид расхода по ст. 255 НК РФ не заполнен.'"), , , "Объект.ВидНачисленияДляНУ" , Отказ);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ВидНачисленияДляНУПриИзменении(Элемент)
	ОбновитьИнформационныеНадписи(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ОблагаетсяНДФЛПриИзменении(Элемент)
	УстановитьДоступностьКодаДоходаНДФЛ(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ВключаетсяВРасходыПоСт255НКРФПриИзменении(Элемент)
	УстановитьДоступностьВидаНачисленияДляНУ(ЭтаФорма);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.Свойства
&НаКлиенте
Процедура Подключаемый_РедактироватьСоставСвойств()
	УправлениеСвойствамиКлиент.РедактироватьСоставСвойств(ЭтотОбъект, Объект.Ссылка);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.Свойства 

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры
&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтаФорма, РеквизитФормыВЗначение("Объект"));
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьИнформационныеНадписи(Форма)
	Форма.ПредставлениеВидаНачисленияПоНКРФ = ЗарплатаКадрыКлиентСервер.ПредставлениеВидаНачисленияПоНКРФ(Форма.Объект.ВидНачисленияДляНУ);
КонецПроцедуры

&НаСервере
Процедура УстановитьНачальноеОтображениеЭлементовУправления()
	
	Если Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.РайонныйКоэффициент
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.СевернаяНадбавка Тогда
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"ВходитВБазуРКИСН",
			"Видимость",
			Ложь);
			
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"ЯвляетсяДоходомВНатуральнойФорме",
			"Видимость",
			Ложь);	
			
	ИначеЕсли Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ПовременнаяОплатаТруда
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаОтпуска
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОтпускПоБеременностиИРодам
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоЛистаЗаСчетРаботодателя
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоЛиста
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоПрофзаболевание
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоНесчастныйСлучайНаПроизводстве Тогда
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"ВходитВБазуРКИСН",
			"ТолькоПросмотр",
			Истина);
			
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"ЯвляетсяДоходомВНатуральнойФорме",
			"Видимость",
			Ложь);	
			
	КонецЕсли; 
		
	Если Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОтпускПоБеременностиИРодам
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоЛиста
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоПрофзаболевание
		ИЛИ Объект.КатегорияНачисленияИлиНеоплаченногоВремени = Перечисления.КатегорииНачисленийИНеоплаченногоВремени.ОплатаБольничногоНесчастныйСлучайНаПроизводстве Тогда
		
		Элементы.ОтражениеВБухучетеСтраницы.ТекущаяСтраница = Элементы.ОтражениеВБухучетеБольничный;
	Иначе
		Элементы.ОтражениеВБухучетеСтраницы.ТекущаяСтраница = Элементы.ОтражениеВБухучетеНастройки;
	КонецЕсли;	
	
	ОбновитьИнформационныеНадписи(ЭтаФорма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступностьКодаДоходаНДФЛ(Форма)
	
	Форма.Элементы.КодДоходаНДФЛ.Доступность = Форма.ОблагаетсяНДФЛ = 1;
	Форма.Элементы.КодДоходаНДФЛ.АвтоОтметкаНезаполненного = Форма.ОблагаетсяНДФЛ = 1;
	Форма.Элементы.КодДоходаНДФЛ.ОтметкаНезаполненного = Не ЗначениеЗаполнено(Форма.Объект.КодДоходаНДФЛ);
	
	Если Не Форма.Элементы.КодДоходаНДФЛ.Доступность Тогда
		Форма.Объект.КодДоходаНДФЛ = Неопределено;
	КонецЕсли;
	
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступностьВидаНачисленияДляНУ(Форма)
	
	Форма.Элементы.ВидНачисленияДляНУ.Доступность = Форма.ВключаетсяВРасходыПоСт255НКРФ = 1;
	Форма.Элементы.ВидНачисленияДляНУ.АвтоОтметкаНезаполненного = Форма.ВключаетсяВРасходыПоСт255НКРФ = 1;
	Форма.Элементы.ВидНачисленияДляНУ.ОтметкаНезаполненного = Не ЗначениеЗаполнено(Форма.Объект.ВидНачисленияДляНУ);
	
	Если Не Форма.Элементы.ВидНачисленияДляНУ.Доступность Тогда
		Форма.Объект.ВидНачисленияДляНУ = Неопределено;
		ОбновитьИнформационныеНадписи(Форма);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
